function s = msgid(s)
%Construct Error/Warning message ID for backtrace purpose.
%
% SYNOPSIS:
%   s = msgid(s)
%
% PARAMETERS:
%   s - A string of the form '[<component>]:<mnemonic>' suitable as an
%       ERROR or WARNING-type message identifier.
%
% RETURNS:
%   s - The same string, though with the name of the function calling
%       'msgid' prepended to 's' (or the string 'BASE' if function 'msgid'
%       is called from the base workspace).
%
% SEE ALSO:
%   error, warning.

   st = dbstack(1);
   try
      caller = st(1).name;
   catch %#ok
      caller = 'BASE';
   end
   s = [caller, ':', s];
end
