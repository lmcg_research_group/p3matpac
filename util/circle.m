function [xunit,yunit] = circle(x,y,r)
%x and y are the coordinates of the center of the circle
%r is the radius of the circle
th = 0:pi/50:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;
