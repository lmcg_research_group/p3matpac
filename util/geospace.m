function v = geospace(v0,vn,nv,r)
%Generate geometric progressived spaced vector
%
% SYNOPSIS:
%   v = geospace(v0,vn,n,ratio);
%
% PARAMETERS:
%   v0, vn - Point interval.
%   n      - Number of points, optional, default (50)
%   ratio  - geometric progression ratio, optional, default (2).
%            if ratio<0 the spaced direction is inverted, i.e.
%            ratio=+2
%           *-*--*----*----------------*
%           v0                         vn
%            ratio=-2
%           *----------------*----*--*-*
%           v0                         vn
%
% RETURNS:
%   v   - geometric progressived spaced vector
%
% USAGE:
%   v0 = 0;
%   vn = 42;
%   n  = 5;
%   v  = geospace(v0,vn,n)
% 
%   v = 
%          0    2.8000    8.4000   19.6000   42.0000
%
% SEE ALSO:
%

n = 50;
ratio = 2;
if nargin == 3
    n = floor(nv);
end
if nargin > 3
    ratio = r;
end


a = abs(ratio).^[0:n-1];
if ratio<0
    v = vn.*(a-a(end))/(a(1)-a(end)) + v0.*(a-a(1))/(a(end)-a(1));
else
    v = v0.*(a-a(end))/(a(1)-a(end)) + vn.*(a-a(1))/(a(end)-a(1));
end


end

