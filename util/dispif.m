function varargout = dispif(bool, varargin)
%Produce textual output contingent upon predicate.
%
% SYNOPSIS:
%        dispif(bool, format, arg, ...)
%   nc = dispif(bool, format, arg, ...)
%
% PARAMETERS:
%   bool   - Boolean variable.
%
%   format - SPRINTF format specification.
%
%   arg    - OPTIONAL arguments to complete 'format'.
%
% RETURNS:
%   nc     - Number of characters printed to output device.  If 'bool' is
%            FALSE, then nc=0.  Only returned if specifcially requested.
%
% COMMENTS:
%   Function used for making code cleaner where 'verbose' option is used.
%
% SEE ALSO:
%   SPRINTF, tocif.


%error(nargoutchk(0, 1, nargout, 'struct'));

nc = 0;
if bool, nc = fprintf(varargin{:}); end

if nargout > 0, varargout{1} = nc; end
end
