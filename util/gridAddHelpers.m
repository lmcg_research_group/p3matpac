function G2 = gridAddHelpers(G)
% Add helpers/shortcut to grid structure for cleaner code structure (inspired in MRST2015b).
%
% SYNOPSIS:
%   G = gridAddHelpers(G);
%
% PARAMETERS:
%   G    - Grid structure
%
% RETURNS:
%   G    - Grid structure with added fields:
%             - plot contains handles to plotting functions
%             - helpers contains various helper functions commonly used
%             when plotting/debugging grid.
%

    assert(isstruct(G), ['Grids passed to gridAddHelpers', ...
    'must be a structure variable...'])
    G2 = G;

    % Plotting
    G2.plot.grid      = @(varargin) iotools.plotGrid(G, varargin{:});
    G2.plot.cellData  = @(data, varargin) iotools.plotCellData(G, data, varargin{:});
    G2.plot.data      = @(data, varargin) iotools.plotData(G, data, varargin{:});


    % Helpers for grid operations
    G2.helpers.getBCFlux         = @(varargin) getBCFlux_(G, varargin{:});
    G2.helpers.getBCMech         = @(varargin) getBCMech_(G, varargin{:});
    G2.helpers.getX              = @(varargin) getX_(G, varargin{:});
    G2.helpers.getY              = @(varargin) getY_(G, varargin{:});
    G2.helpers.getZ              = @(varargin) getZ_(G, varargin{:});
    G2.helpers.dimension         = G.nodes.dim;
    G2.helpers.numNodes          = G.nodes.num;
    
    G2.helpers.getCentroids      = @(varargin) getCentroids_(G, varargin{:});
    G2.helpers.getCellVol        = @(varargin) getVolume_(G, varargin{:});
    G2.helpers.getNNEl           = @(varargin) getNNEL_(G, varargin{:});
    G2.helpers.getCellNodes      = @(varargin) getLNODS_(G, varargin{:});
    G2.helpers.getCellType       = @(varargin) getType_(G, varargin{:});
    G2.helpers.getGroup          = @(varargin) getMat_(G, varargin{:});
    G2.helpers.getNumGroups      = G.cells.nummat;
    G2.helpers.numCells          = G.cells.num;
    
    G2.type = [G2.type, {'gridAddHelpers'}];
end


function out = getBCFlux_(G, varargin)
    out = G.nodes.bcflux;
    if ~isempty(varargin)
        out = G.nodes.bcflux(varargin{:});
    end
end

function out = getBCMech_(G, varargin)
    out = G.nodes.bcmech;
    if ~isempty(varargin)
        out = G.nodes.bcmech(varargin{:});
    end
end

function out = getX_(G, varargin)
    out = G.nodes.coords(:,1);
    if ~isempty(varargin)
        out = G.nodes.coords(varargin{:},1);
    end
end

function out = getY_(G, varargin)
    out = G.nodes.coords(:,2);
    if ~isempty(varargin)
        out = G.nodes.coords(varargin{:},2);
    end
end

function out = getZ_(G, varargin)
    z=3; if (G.nodes.dim==2), z=2; end
    out = G.nodes.coords(:,z);
    if ~isempty(varargin)
        out = G.nodes.coords(varargin{:},z);
    end
end

function out = getCentroids_(G, varargin)
    out = G.cells.centroids;
    if ~isempty(varargin)
        out = G.cells.centroids(varargin{:},:);
    end
end

function out = getVolume_(G, varargin)
    out = G.cells.volume;
    if ~isempty(varargin)
        out = G.cells.volume(varargin{:});
    end
end

function out = getNNEL_(G, varargin)
    out = G.cells.nnel;
    if ~isempty(varargin)
        out = G.cells.nnel(varargin{:});
    end
end

function out = getLNODS_(G, varargin)
    mxnnel = max(G.cells.nnel);
    out = G.cells.lnods(:,mxnnel);
    if ~isempty(varargin)
        mxnnel = max(G.cells.nnel(varargin{:}));
        out = G.cells.lnods(varargin{:},mxnnel);
    end
end

function out = getType_(G, varargin)
    out = G.cells.type;
    if ~isempty(varargin)
        out = G.cells.type(varargin{:});
    end
end

function out = getMat_(G, varargin)
    out = G.cells.mat;
    if ~isempty(varargin)
        out = G.cells.mat(varargin{:});
    end
end
