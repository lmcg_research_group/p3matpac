% UTIL
%
% Files
%   GradFunction  - Compute a linear gradient by: val(x) = val0 + grad*(d-d0)
%   merge_options - Override default control options.
%   msbtrace      - Construct Error/Warning message ID for backtrace purpose.
