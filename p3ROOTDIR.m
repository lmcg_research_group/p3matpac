function root = p3ROOTDIR
%Returne full path of Toolbox installation directory.
%
% SYNOPSIS:
%   root = p3ROOTDIR
%
% PARAMETERS:
%   none.
%
% RETURNS:
%   root - Full path to p3matpac installation directory.

nm = mfilename('fullpath');
ix = strfind(nm, filesep);
if ~isempty(ix),
   root = nm(1 : ix(end));
else
   root = nm;
end
