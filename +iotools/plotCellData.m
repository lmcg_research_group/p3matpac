function varargout = plotCellData(G, celldata, varargin)
%Plot exterior grid cells, coloured by given data, to current axes.
%
% SYNOPSIS
%       iotools.plotCellData_(G, data)
%       iotools.plotCellData_(G, data, 'pn1', pv1, ...)
%       iotools.plotCellData_(G, data, cells)
%       iotools.plotCellData_(G, data, cells, 'pn1', pv1, ...)
%       h = iotools.plotCellData_(...)
%       % Make sure that iotools package was imported
%       plotCellData_(G, data)
%       plotCellData_(G, data, 'pn1', pv1, ...)
%       plotCellData_(G, data, cells)
%       plotCellData_(G, data, cells, 'pn1', pv1, ...)
%       h = plotCellData_(...)
%
%
% PARAMETERS
%   G        - Grid data structure.
%
%   celldata - Scalar cell data with which to colour the grid.  One scalar,
%              indexed colour value for each cell in the grid or one
%              TrueColor value for each cell.  If a cell subset is specified
%              in terms of the 'cells' parameter, 'data' must either contain
%              one scalar value for each cell in the model or one scalar
%              value for each cell in this subset.
%
%   cells    - Vector of cell indices defining sub grid.  The graphical
%              output of function 'plotCellData' will be restricted to the
%              subset of cells from 'G' represented by 'cells'.
%
%              If unspecified, function 'plotCellData' will behave as if the
%              user defined
%
%                 cells = 1 : G.cells.num
%
%              meaning graphical output will be produced for all cells in
%              the grid model 'G'.  If 'cells' is empty (i.e., if
%              ISEMPTY(cells)), then no graphical output will be produced.
%
%   'pn'/pv  - List of property names/property values.  OPTIONAL.
%              This list will be passed directly on to function PATCH
%              meaning all properties supported by PATCH are valid.
%
% RETURNS:
%   h  - Handle to resulting patch object.  The patch object is added
%        directly to the current AXES object (GCA).
%        OPTIONAL.  Only returned if specifically requested.  If
%        ISEMPTY(cells), then h==-1.
%
% EXAMPLE
%	1)>> iotools.plotCellData_(G, data);
%
%   2)>> import iotools.*
%	  >> plotCellData_(G,data);  colorbar;
%
% SEE ALSO
%   plotGrid_, patch, plotData_, shading, gridprocessing.grid_structure
%

assert (sum(size(celldata, 2) == [1, 3]) == 1, ...
       'Second input, DATA, must have one or three columns.');

% Default to providing graphical output from all cells in the grid model.
%
cells = (1 : G.cells.num) .';
cdata = nan(G.cells.num,1);
if mod(numel(varargin), 2) == 1,
   % Caller requested graphical output from a particular subset of the grid
   % cells.  Honour that request, but only if it makes sense in context.
   %
   if isnumeric(varargin{1}),
      cells = varargin{1};
   elseif islogical(varargin{1}) && ...
         numel(varargin{1}) == G.cells.num,
         cells = find(varargin{1});
   else
      error(['Third parameter ''cells'' must either be a list of ', ...
             'explicit cell indices or a logical mask into the '  , ...
             'grid''s cells.']);
   end

   % Strip 'cells' argument off of remaining input argument list.
   %
   varargin = varargin(2 : end);
end

if isempty(cells),
   warning('CellsList:empty', ...
       'Empty cell selection in ''plotCellData''.  No graphics for you.')
   if nargout > 0, varargout{1} = -1; end
   return
end

% Assert that remaining arguments at least appear to be 'name'/value pairs
% intended for PATCH.
%
if ~isempty(varargin),
   assert (all(cellfun(@ischar, varargin(1 : 2 : end))));
end
assert (size(celldata, 1) == G.cells.num || ...
        size(celldata, 1) == numel(cells),  ...
        'The DATA should have one value for each grid cell in output.');

if numel(celldata) < G.cells.num,
    cdata(cells) = celldata;
else
    cdata = celldata;
end

node=G.nodes.coords;
minCellNodes = min(G.cells.nnel);
maxCellNodes = max(G.cells.nnel);

hold on
if G.nodes.dim == 2
    if (minCellNodes == maxCellNodes)
        switch (minCellNodes)
            case {3,6}                      % triangles
                for i=1:G.cells.num
                    kxx = G.cells.lnods(i,1:3);
                    p = patch('Faces',               kxx,      'Vertices',   node,...
                        'FaceVertexCData', cdata(i) , 'EdgeColor', 'none', ...
                        'FaceColor', 'flat',varargin{:}); %#ok<*NASGU>
                end
            case 4
                for i=1:G.cells.num
                    kxx = G.cells.lnods(i,1:4);
                    p = patch('Faces',               kxx,      'Vertices',   node,...
                        'FaceVertexCData', cdata(i) , 'EdgeColor', 'none', ...
                        'FaceColor', 'flat', varargin{:});
                end
        end
    else
        for i=1:G.cells.num
            nnel = G.cells.nnel(i);
            if (nnel == 6)
                nnel = 3;
            end

            kxx = G.cells.lnods(i,1:nnel);
            p = patch('Faces',               kxx, 'Vertices',   node, 'EdgeColor', 'none', ...
                      'FaceVertexCData', cdata(i) , 'FaceColor', 'flat', varargin{:});
        end
    end
else
    if G.cells.nnel(1) == 8
        face_matrix = zeros(6*G.cells.num,5);

        kxx = [1,2,3,4,1; 5,6,7,8,5; 1,2,6,5,1; ...
               2,3,7,6,2; 3,4,8,7,3; 1,4,8,5,1];

        for iel = 1:G.cells.num
           face_matrix((iel-1)*6+1,:) = G.cells.lnods(iel,kxx(1,:));        % faces per cell
           face_matrix((iel-1)*6+2,:) = G.cells.lnods(iel,kxx(2,:));        % faces per cell
           face_matrix((iel-1)*6+3,:) = G.cells.lnods(iel,kxx(3,:));        % faces per cell
           face_matrix((iel-1)*6+4,:) = G.cells.lnods(iel,kxx(4,:));        % faces per cell
           face_matrix((iel-1)*6+5,:) = G.cells.lnods(iel,kxx(5,:));        % faces per cell
           face_matrix((iel-1)*6+6,:) = G.cells.lnods(iel,kxx(6,:));        % faces per cell
        end
        cdata = reshape(repmat(cdata,1,6)',6*G.cells.num,1);

        p = patch('Vertices',G.nodes.coords,'Faces',face_matrix,...
            'FaceVertexCData',cdata,'FaceColor','flat',...
            'facea', 0.3, 'edgea',0.1,varargin{:});
        view(3);
    end

    if G.cells.nnel(1) == 4
        tetramesh(G.cells.lnods(:,1:G.cells.nnel(:)),G.nodes.coords,...
                  cdata,'FaceColor','flat','facea', 0.7, ...
                  'edgea',0.1,varargin{:});
        view(3);
    end
end
axis equal tight;
hold off
if nargout > 0, varargout{1} = h; end
colormap('jet'); colorbar;
end
