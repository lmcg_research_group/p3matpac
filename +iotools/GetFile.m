function [filename, pathname]=GetFile(ext)
%This function get information about file name and path locations of file selected
% 
% SYNOPSIS
%	GetFile('*.dat')
%
% PARAMETERS
%	ext       - char type variable expressing extension or files to be got.
% 
% RETURNS
%	filename  - File name of file selected.
%	pathname  - Path of file selected.
% 
% EXAMPLE
%	[filename, pathname] = GetFile_('root.dat');
% 
% SEE ALSO
%   inputdlg, uigetfile
% 


% In this way we force (brutally!!) the uigetfile works
filename = 0;
while (isnumeric(filename))
    [filename, pathname] = uigetfile(ext,sprintf('Select file %s',ext),...
        'Multiselect','off'); % choose file to open
end

end
