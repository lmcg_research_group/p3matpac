function varargout = plotData(G, data, varargin)
%Plot selection of coloured grid faces to current axes (reversed Z axis).
%
% SYNOPSIS:
%       iotools.plotData_(G, data)
%       iotools.plotData_(G, data, 'pn1', pv1, ...)
%       iotools.plotData_(G, data, nodes)
%       iotools.plotData_(G, data, nodes, 'pn1', pv1, ...)
%       h = iotools.plotData_(...)
%       % Make sure that iotools package was imported
%       plotData_(G, data)
%       plotData_(G, data, 'pn1', pv1, ...)
%       plotData_(G, data, nodes)
%       plotData_(G, data, nodes, 'pn1', pv1, ...)
%       h = plotData_(...)
%
% PARAMETERS:
%   G       - Grid data structure.
%
%
%   nodedata - Scalar node data with which to colour the grid.  One scalar,
%              indexed colour value for each node in the grid or one
%              TrueColor value for each node.  If a node subset is specified
%              in terms of the 'nodes' parameter, 'data' must either contain
%              one scalar value for each node in the model or one scalar
%              value for each node in this subset.
%
%   nodes    - Vector of node indices defining sub grid.  The graphical
%              output of function 'plotData' will be restricted to the
%              subset of node from 'G' represented by 'nodes'.
%
%              If unspecified, function 'plotData' will behave as if the
%              user defined
%
%                 nodes = 1 : G.nodes.num
%
%              meaning graphical output will be produced for all cells in
%              the grid model 'G'.  If 'nodes' is empty (i.e., if
%              ISEMPTY(cells)), then no graphical output will be produced.
%
%   'pn'/pv  - List of property names/property values.  OPTIONAL.
%              This list will be passed directly on to function PATCH
%              meaning all properties supported by PATCH are valid.
%
% RETURNS:
%   h  - Handle to resulting patch object.  The patch object is added
%        directly to the current AXES object (GCA).
%        OPTIONAL.  Only returned if specifically requested.  If
%        ISEMPTY(cells), then h==-1.
%
% NOTES:
%   Function 'plotData' is implemented directly in terms of the low-level
%   function PATCH.  If a separate axes is needed for the graphical output,
%   callers should employ function newplot prior to calling 'plotFaces'.
%
% EXAMPLE:
%	1)>> iotools.plotData(G, G.nodes.bcflux);
%
%   2)>> import iotools.*
%     >> h = plotData(G, G.nodes.bcmech);
%
% SEE ALSO:
%   plotCellData_, plotGrid_, patch, shading, gridprocessing.grid_structure
%

assert (sum(size(data, 2) == [1, 3]) == 1, ...
       'Second input, DATA, must have one or three columns.');

% Default to providing graphical output from all cells in the grid model.
%
nodes = (1 : G.nodes.num) .';
pdata = nan(G.nodes.num,1);
if mod(numel(varargin), 2) == 1,
   % Caller requested graphical output from a particular subset of the grid
   % cells.  Honour that request, but only if it makes sense in context.
   %
   if isnumeric(varargin{1}),
      nodes = varargin{1};
   elseif islogical(varargin{1}) && ...
         numel(varargin{1}) == G.nodes.num,
         nodes = find(varargin{1});
   else
      error(['Third parameter ''nodes'' must either be a list of ', ...
             'explicit node indices or a logical mask into the '  , ...
             'grid''s node.']);
   end

   % Strip 'nodes' argument off of remaining input argument list.
   %
   varargin = varargin(2 : end);
end

if isempty(nodes),
   warning('NodeList:empty', ...
       'Empty node selection in ''plotData''.  No graphics for you.')
   if nargout > 0, varargout{1} = -1; end
   return
end

% Assert that remaining arguments at least appear to be 'name'/value pairs
% intended for PATCH.
%
if ~isempty(varargin),
   assert (all(cellfun(@ischar, varargin(1 : 2 : end))));
end
assert (size(data, 1) == G.nodes.num || ...
        size(data, 1) == numel(nodes),  ...
        'The DATA should have one value for each grid nodes in output.');

if numel(data) < G.nodes.num,
    pdata(nodes) = data;
else
    pdata = data;
end

vert=[G.nodes.coords];
minCellNodes = min(G.cells.nnel);
maxCellNodes = max(G.cells.nnel);

hold on
if (minCellNodes == maxCellNodes)
    switch (minCellNodes)
        case {3,6}                      % triangles
            kxx = G.cells.lnods(:,1:3);
            p = patch('Faces', kxx, 'Vertices', vert, 'FaceVertexCData', pdata, ...
                'EdgeColor', 'none', 'FaceColor', 'interp',varargin{:}); %#ok<*NASGU>
        case {4}
            kxx = G.cells.lnods(:,1:4);
            p = patch('Faces', kxx, 'Vertices', vert, 'FaceVertexCData', pdata,...
                'EdgeColor', 'none', 'FaceColor', 'interp', varargin{:});
    end
else
    for i=1:G.cells.num
        nnel = G.cells.nnel(i);
        if (nnel == 6)
            nnel = 3;
        end

        kxx = G.cells.lnods(i,1:nnel);
        p = patch('Faces',1:nnel, 'Vertices',   vert(kxx,:), ...
                  'FaceVertexCData', pdata(kxx) , 'EdgeColor', 'none', ...
                  'FaceColor', 'interp', varargin{:});
    end
end
axis equal tight;
hold off
if nargout > 0, varargout{1} = h; end
colormap('jet'); colorbar;
end

