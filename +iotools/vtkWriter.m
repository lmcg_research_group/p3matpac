function vtkWriter(basename,G,varargin)
%Write vtkfile format of geometry
%
% SYNOPSIS
%       iotools.vtkWriter(filename,G)
%       iotools.vtkWriter(filename,G, 'pn1', pv1, ...)
%
% PARAMETERS
%   G        - Grid structure mostly as detailed in grid_structure (see >> help
%              gridprocessing.grid_structure).
%
%   filename - Name of vtk file
%
%   'pn'/pv  - List of property names/property values.  OPTIONAL.
%              This list will be passed to block node data or cell data of
%              vtk file (i.e. if 'pn' = 'X5' and pv = cheinit [from
%              initcond.chemInitCond_] into vtk file will have the node
%              variable called X5 with values cheinit, where
%              numel(cheinit) = G.nodes.num )
%
% RETURNS
%   vtk file format with geometry data
%
% EXAMPLE
%   1)>> iotools.vtkWriter('fivespot',G);
%   2)>> iotools.vtkWriter('fault',G,'X5',cheinit);
%
% SEE ALSO:
%   fopen, fclose, fprintf, gridprocessing.grid_structure
%
% NOTES
%   to see mesh you need install paraview program or similar program that
%   can be read vtk files data.
%

%%  check input

assert(ischar(basename), ...
    'First argument must be a filename.')

assert(isstruct(G),...
    'First argument must be a struct variable see help gridprocessing.grid_structure.');

assert(mod(numel(varargin), 2) == 0,...
    'Last arguments must be a list of property names/property values')

opt = struct('timestep',0,'cycle',0, 'verbose', true);
[opt, varin] = merge_options(opt, varargin{:});

vararg = zeros(numel(varin)/2,2);

for i=1:2:numel(varin)
    assert(ischar(varin{i}),...
        'Last arguments must be a list of property names/property values');
    assert(isnumeric(varin{i+1}),...
        'Last arguments must be a list of property names/property values');
    [vararg(i,1), vararg(i,2)] = size(varin{i+1});
end



%% mesh dimension
nn   = G.nodes.num;
ndim = G.nodes.dim;
nel  = G.cells.num;

%==================================< vtk file >==
disp('** creating *.vtk file');


%% ---------------------------------------------------------- vtk file headline

arqvtk=strcat(basename,sprintf('%05d',opt.cycle),'.vtk');
fvtk = fopen(arqvtk,'w');
fprintf(fvtk,'# vtk DataFile Version 2.0\n');
fprintf(fvtk,'problem created by MMGT - MATLAB Mesh Generator Toolkit\n');
fprintf(fvtk,'ASCII\n');
fprintf(fvtk,'DATASET UNSTRUCTURED_GRID\n');
fprintf(fvtk,'FIELD FieldData 2\n');
fprintf(fvtk,'TIME 1 1 float\n');
fprintf(fvtk,'%f\n',opt.timestep);
fprintf(fvtk,'CYCLE 1 1 int\n');
fprintf(fvtk,'%d\n',opt.cycle);

%% -------------------------------------------------- node coordinates
progress=textprogressbar(nn, 'showbar', opt.verbose, 'showpercentage',opt.verbose, ...
    'startmsg','writing coordinates    ');
fprintf(fvtk,'POINTS %i float\n',nn);
if (ndim < 3)

    for i=1:nn,                         % coordenadas dos nos
       fprintf(fvtk,'%22.15e %22.15e 0.0\n',G.nodes.coords(i,1),G.nodes.coords(i,2));
       progress(i);
    end
    
else

    for i=1:nn,                         % coordenadas dos nos
       fprintf(fvtk,'%22.15e %22.15e %22.15e \n',G.nodes.coords(i,1),G.nodes.coords(i,2),...
           G.nodes.coords(i,3));
       progress(i);
    end

end

%% ---------------------------------------------------- Connectivity
progress=textprogressbar(nel, 'showbar', opt.verbose, 'showpercentage',opt.verbose, ...
    'startmsg','writing connectivities ');

cell_type = zeros(nel,1);
fprintf(fvtk,'CELLS  %i   %i \n',nel, nel + sum(G.cells.nnel));

for iel=1:nel

    nnel=G.cells.nnel(iel);
    switch (nnel)
        case 3                           % 3-nodes triangle

            fprintf(fvtk,' %5i  %5i  %5i  %5i \n',nnel, G.cells.lnods(iel,1) - 1,...
                G.cells.lnods(iel,2) - 1, G.cells.lnods(iel,3) - 1);
            cell_type(iel) = 5;

        case 4                       % 4-nodes element (tetra. or qrad.)

            fprintf(fvtk,' %5i  %5i  %5i  %5i  %5i \n',nnel, G.cells.lnods(iel,1) - 1,...
                G.cells.lnods(iel,2) - 1, G.cells.lnods(iel,3) - 1, G.cells.lnods(iel,4) - 1);
            if (ndim == 2)
                cell_type(iel) = 9;
            else
                cell_type(iel) = 10;
            end

        case 6                       % 6-nodes triangle

            fprintf(fvtk,' %5i  %5i  %5i  %5i  %5i   %5i  %5i \n',nnel, ...
                G.cells.lnods(iel,1) - 1, G.cells.lnods(iel,2) - 1, G.cells.lnods(iel,3) - 1,...
                G.cells.lnods(iel,4) - 1, G.cells.lnods(iel,5) - 1, G.cells.lnods(iel,6) - 1);
            cell_type(iel) = 22;

        case 8                       % 8-nodes hexahedron

            fprintf(fvtk,' %5i  %5i  %5i  %5i  %5i  %5i  %5i  %5i  %5i \n', nnel, ...
                G.cells.lnods(iel,1) - 1, G.cells.lnods(iel,2) - 1, G.cells.lnods(iel,3) - 1,...
                G.cells.lnods(iel,4) - 1, G.cells.lnods(iel,5) - 1, G.cells.lnods(iel,6) - 1,...
                G.cells.lnods(iel,7) - 1, G.cells.lnods(iel,8) - 1);
            cell_type(iel) = 12;

        case 2

            fprintf(fvtk,' %5i  %5i  %5i\n',nnel, G.cells.lnods(iel,1) - 1,...
                G.cells.lnods(iel,2) - 1);
            cell_type(iel) = 3;

    end
    progress(iel);
end


%% ---------------------------------------------------- Cells type ID

progress=textprogressbar(nel, 'showbar', opt.verbose, 'showpercentage',opt.verbose, ...
    'startmsg','writing vtk_cell_type  ');
fprintf(fvtk,'CELL_TYPES  %i \n',nel);
for iel = 1:nel
    fprintf(fvtk,'%5i  \n',cell_type(iel));
    progress(iel);
end

%% ----------------------------------------------------------- Point Data flux bcond
tot = 0;
for ivar = 1:2:numel(varin)    
    if (vararg(ivar,1) ~= nn), continue; end;
    tot = tot + 1;
end
tot = (tot+2)*nn;
progress=textprogressbar(tot, 'showbar', opt.verbose, 'showpercentage',opt.verbose, ...
    'startmsg','writing point data     ');

fprintf(fvtk,'POINT_DATA  %i \n',nn);
fprintf(fvtk,'SCALARS fluxbcond float\n');
fprintf(fvtk,'LOOKUP_TABLE default\n');

for i=1:nn
    fprintf(fvtk,'%i\n',G.nodes.bcflux(i));
    progress(i);
end

%% ----------------------------------------------------------- Point Data mech bcond
fprintf(fvtk,'SCALARS mechbcond float\n');
fprintf(fvtk,'LOOKUP_TABLE default\n');

for i=1:nn
    fprintf(fvtk,'%i\n',G.nodes.bcmech(i));
    progress(i+nn);
end

%% ----------------------------------------------------------- Point Data varargin
count = 0;
for ivar = 1:2:numel(varin)
    
    if (vararg(ivar,1) ~= nn), continue; end;
    
    if vararg(ivar,2)>1
        fprintf(fvtk,'VECTORS %s float\n',varin{ivar});
        if ndim > 2
            for i=1:nn
                fprintf(fvtk,'%f ',varin{ivar+1}(i,:));            
                fprintf(fvtk,'\n');
                progress((2+count)*nn+i);
            end
        else
            for i=1:nn
                fprintf(fvtk,'%f ',[varin{ivar+1}(i,:), 0]);
                fprintf(fvtk,'\n');
                progress((2+count)*nn+i);
            end
        end
    else
        fprintf(fvtk,'SCALARS %s float\n',varin{ivar});
        fprintf(fvtk,'LOOKUP_TABLE default\n');

        for i=1:nn
            fprintf(fvtk,'%f\n',varin{ivar+1}(i));
            progress((2+count)*nn+i);
        end
    end
    count = count + 1;
end

%% ----------------------------------------------------------- Cells Data Materials
tot = 0;
for ivar = 1:2:numel(varin)    
    if (vararg(ivar,1) ~= nel), continue; end;
    tot = tot + 1;
end
tot = (tot+1)*nel;
progress=textprogressbar(tot, 'showbar', opt.verbose, 'showpercentage',opt.verbose, ...
    'startmsg','writing cell data      ');



fprintf(fvtk,'CELL_DATA  %i \n',nel);
fprintf(fvtk,'SCALARS materials float\n');
fprintf(fvtk,'LOOKUP_TABLE default\n');

for i=1:nel
    fprintf(fvtk,'%i\n',G.cells.mat(i));
    progress(i);
end

%% ----------------------------------------------------------- Cells Data
count = 0;
for ivar = 1:2:numel(varin)
    
%     var = 

    if (vararg(ivar,1) ~= nel), continue; end;
    
    if vararg(ivar,2)>1
        fprintf(fvtk,'VECTORS %s float\n',varin{ivar});
        if ndim > 2
            for i=1:nel
                fprintf(fvtk,'%f ',varin{ivar+1}(i,:));            
                fprintf(fvtk,'\n');
                progress((1+count)*nel+i);
            end
        else
            for i=1:nel
                fprintf(fvtk,'%f ',[varin{ivar+1}(i,:), 0]);
                fprintf(fvtk,'\n');
                progress((1+count)*nel+i);
            end
        end
    else
        fprintf(fvtk,'SCALARS %s float\n',varin{ivar});
        fprintf(fvtk,'LOOKUP_TABLE default\n');

        for i=1:nel
            fprintf(fvtk,'%f\n',varin{ivar+1}(i));
            progress((1+count)*nel+i);
        end
    end
    count = count + 1;
end

fclose(fvtk);

end
