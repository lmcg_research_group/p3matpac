function saveGrid(fout, G)
%Write mesh data, fout.coords and fout.lnods files.
%
% SYNOPSIS
%  iotools.saveGrid(fout,G);
%
% PARAMETERS
%	fout - Name of programa, i.e the name of prefix of *_gri.dat *_gen.dat, the
%          string will be printted into root.dat.
%	G    - Grid structure mostly as detailed in grid_structure.
%
% RETURNS
%	ascii files format with geometry data.
%
% USAGE
%	1)>> iotools.saveGrid( 'falha', G);
%
% SEE ALSO
%   gridprocessing.grid_structure
%

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%}

% check input arguments
if sscanf(version('-release'),'%u%*s')<=2013
    if and(nargin<2,nargin>3)
        error('please usage: iotools.save(<fid>,<G>)');
    end
else
    narginchk(2, 3);
end

assert(ischar(fout),...
    'First argument must be string type.');

assert(isstruct(G),...
    'Second argument must be a struct variable see help gridprocessing.grid_structure.');

fprintf('-> Into %s\n',mfilename)

nn = G.nodes.num;
ne = G.cells.num;


fname = strcat(fout,'.coords');
M = [reshape(1:nn,nn,1), G.nodes.coords, G.nodes.bcmech, G.nodes.bcflux];
save(fname, 'M','-ascii');

fname = strcat(fout,'.lnods');
M = [reshape(1:ne,ne,1), G.cells.mat, G.cells.type, G.cells.lnods];
save(fname, 'M','-ascii');

end
