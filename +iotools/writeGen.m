function writeGen(programname, prb, G, BCs)
%Write *gen.dat file
%
% SYNOPSIS
%  iotools.writeGen( programname, G, prb);
%
% PARAMETERS
%   programname - Name of programa, i.e the name of prefix of *_gri.dat *_gen.dat, the
%                 string will be printted into root.dat.
%   prb         - string variable formed by initial letter of each problems:
%                 T-themo; H-hydro (liquid); H2-hydro2ph (liquid & gas) M-Mechanical;
%                 G-Geochemical(multi-physics)
%   G           - Grid structure mostly as detailed in grid_structure.
%   BCs         - Boundary Condition structure mostly as detailed in bconds.bc_structure.
%
% RETURNS
%   *gri.dat file format with geometry data without initial values of unknowns.
%
% USAGE
%   1)>> iotools.writeGen( 'falha', 'H2M', G); % hydro-mechanical (2phase)
%   2)>> iotools.writeGen( 'falha', 'HG', G, BCs);  % hydro-Geochemical (2 prob)
%   3)>> iotools.writeGen( 'falha', 'M', G);   % mechamical
%
% SEE ALSO
%   gridprocessing.grid_structure
%

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%
%  Programmer
%  ==========
%  Jonathan C. Teixeira
%
%  Creation
%  ========
%  Jul. 30, 2015
%
%  Modification
%  ============
%   * Débora Assis, Fri Set 26 2017
%      Add -- flag para escrever cond. de cont mec 3d
%
%   * JCT, Tue Jun 26 2018
%      change order of variables, more intuitive
%  ....
%  please when some programmer modify this script put you name, date of addiction
%  or modification statement and describe your modification/addiction like this:
%   * Tommy Hilfiger, Fri Oct 26 2012
%      Add --cflag, --cxxflag.  These *append* a specific flag.
%      Getting '--cflags' to quote reliably is a nightmare.
%
%}

fprintf('-> Into %s\n',mfilename)
% check input arguments
if (nargin<4),
    BCs = [];
end

assert(ischar(programname),...
    'First argument must be string type.');

assert(isstruct(G),...
    'Third argument must be a struct variable see help gridprocessing.grid_structure.');

fid=fopen('root.dat','w');
fprintf(fid,'%5s\n',programname);
fclose(fid);

fid=fopen(strcat(programname,'_gen.dat'),'w');
fprintf(fid,'_%s: %s problem\n',programname, prb);
disp('* writing general header');

%% ---------------------------------------------------------------------------------------
% writing general header ...
fmt = zeros(2,1); ifmt='%5d'; dfmt='%10.6f';
if or(G.nodes.num>99999,G.cells.num>99999)
   ifmt = '%10d'; fmt(1)=1;
end
if any(max(abs(G.nodes.coords))>=999.999999)
   dfmt = '%20.13e'; fmt(2)=2;
end

if any(fmt>0)
    fprintf( fid,'%5d%5d%5d\n',-1,fmt(1),fmt(2));
end
fprintf( fid,[repmat(ifmt,1,2),'%5d    0%5d   44    0    0    0'], ...
   G.nodes.num, G.cells.num, G.nodes.dim, G.cells.nummat);
fprintf(fid,'      ; np,nel,dim,axisym,mat,nhv,igausspoint,ext-fields,fracdb\n');


% if G.nodes.num<99999
   % nz=gridprocessing.computeNonZeros(G);
% else
   nz=1;
% end
if nz<=99999,
    fprintf(fid,'%5d %4d    0',nz,1);
elseif and(nz>99999,nz<=999999)
    fprintf(fid,'%5d %4d    0',floor(nz/10),10);
elseif and(nz>999999,nz<=9999999)
    fprintf(fid,'%5d %4d    0',floor(nz/100),100);
elseif and(nz>9999999,nz<=99999999)
    fprintf(fid,'%5d %4d    0',floor(nz/1000),1000);
end

mec=0; liq=0; gas=0; temp=0; xws=0; che=0;
if regexpi(prb,'M'), mec=1; end
if regexpi(prb,'T'), temp=1; end
if regexpi(prb,'H'), liq=1; end
if regexpi(prb,'2'), gas=1; end
if regexpi(prb,'G'), che=1; end
dim = G.nodes.dim;

fprintf(fid,'%5d%5d    6    1    0',dim*mec+liq+gas+temp+xws,2^G.nodes.dim);
fprintf(fid,'           ; mxdifn,mbandt(2*(1+difnmx)-1),mfronth,ndf,mnval,sol.,conv,th\n');
fprintf(fid,'%5d%5d                                         ; mecbc, fluxbc\n', ...
    max(G.nodes.bcmech), max(G.nodes.bcflux));

fprintf(fid,'%5d%5d%5d%5d%5d%5d                     ; mech,liq,gas,therm,xws,mphys\n', ...
    mec, liq, gas, temp, xws, che);
if (che)
    fprintf(fid,'    0    0    0    0    1    1    0    0    0    1 ');
    fprintf(fid,'; thmche,deriv,iter,bexm,updpor,rhol,vapour,osmotic,press\n');
end



tid = fopen(fullfile(p3ROOTDIR,'templates','template_gen_cte.dat'),'r');
A = fread(tid,'*char');
fclose(tid);

% Append gen header template, constants block, into *gen.dat
fwrite(fid,A);

if (mec),
    fprintf(fid,'  1.00E-04  5.00E+02  1.00E-03  5.000E+2  0.00e-00 ');
    fprintf(fid,'; delmxu,facu,delfmx,dumx,dftimeu\n');
end
if (liq),
    fprintf(fid,'  1.00E-04  5.00E+02  1.00E-03  5.000E+2  0.00e-00 ');
    fprintf(fid,'; delmxpl,facpl,delqwmx,dplmx,dpltimeu\n');
end
if (gas),
    fprintf(fid,'  1.00E-04  5.00E+02  1.00E-03  5.000E+2  0.00e-00 ');
    fprintf(fid,'; delmxpg,facpg,delqgmx,dplmx,dqgtimeu\n');
end
if (temp)
    fprintf(fid,'  1.00E-04  5.00E+02  1.00E-03  5.000E+2  0.00e-00 ');
    fprintf(fid,'; delmxt,fact,delqmx,dtmx,dfqimeu\n');
end
if (che),
    fprintf(fid,'  1.00E-04  5.00E+02  1.00E-03  5.000E+2  0.00e-00 ');
    fprintf(fid,'; delmxche,facche,delchemx,dchemx,dchetimeu\n');
end

fprintf(fid,'    1.e-09                                       0         0 ');
fprintf(fid,'; dxs,drs,drsrel,iteropts,thmsolsparse,chesolsparse\n');


tid = fopen(fullfile(p3ROOTDIR,'templates','template_gen_gtime.dat'),'r');
A = fread(tid,'*char');
fclose(tid);

% Append gen header template, gravity and time step info, into *gen.dat
fwrite(fid,A);


%% ---------------------------------------------------------------------------------------
% writing materials
%
disp('* writing material block');
try
    fid2 = fopen('Materials_ID','r');
    MAT = fread(fid2,'*char');
    fclose(fid2);
    fwrite(fid,MAT);
catch
    for i=1:G.cells.nummat
        fprintf(fid,'*%4d\n',i);
        fprintf(fid,'mat_%d.prop\n',i);
    end
end
fprintf(fid,'   -1                                           ;NO MORE MATERIALS\n');


%% ---------------------------------------------------------------------------------------
% writing bcs
%
disp('* writing BC block');
if (mec)
    if or(isempty(BCs),~isfield(BCs,'mech'))
        if G.nodes.dim == 2
           tid = fopen(fullfile(p3ROOTDIR,'templates','template_mech.bct'),'r');
           A = fread(tid,'*char');
           fclose(tid);
           for i=1:max(G.nodes.bcmech)
               fprintf(fid,'%5d                                           ;Force/Disp. B.C #%d\n',i,i);
               fwrite(fid,A);
           end
        elseif G.nodes.dim == 3
           tid = fopen(fullfile(p3ROOTDIR,'templates','template_mech3d.bct'),'r');
           A = fread(tid,'*char');
           fclose(tid);
               for i=1:max(G.nodes.bcmech)
                   fprintf(fid,'%5d                                           ;Force/Disp. B.C #%d\n',i,i);
                   fwrite(fid,A);
               end    
        end
    else
        for i=1:max(G.nodes.bcmech)
            fprintf(fid,'%5d                                           ;Force/Disp. B.C #%d\n',i,i);
            npar = numel(BCs.mech(i).paramName);
            for j=1:npar/2
                k=npar/2+j;
                fprintf(fid,'%-20s%10.3E %-19s%10.3E\n',...
                        BCs.mech(i).paramName{j},BCs.mech(i).value(j),...
                        BCs.mech(i).paramName{k},BCs.mech(i).value(k));
            end
        end
    end
    fprintf(fid,'   -1                                       ;NO MORE Force/Disp BC\n');
end
if or(liq,or(gas,or(temp,che)))
    if or(isempty(BCs),~isfield(BCs,'flux'))
        tid = fopen(fullfile(p3ROOTDIR,'templates','template_flux.bct'),'r');
        A = fread(tid,'*char');
        fclose(tid);
        tid = fopen(fullfile(p3ROOTDIR,'templates','template_mflux.bct'),'r');
        B = fread(tid,'*char');
        fclose(tid);
        for i=1:max(G.nodes.bcflux)
            fprintf(fid,'%5d                                           ;Flux B.C #%d\n',i,i);
            fwrite(fid,A);
            if (che), fwrite(fid,B); end
        end
    else
        tid = fopen(fullfile(p3ROOTDIR,'templates','template_mflux.bct'),'r');
        B = fread(tid,'*char');
        fclose(tid);
        for i=1:max(G.nodes.bcflux)
            fprintf(fid,'%5d                                           ;Flux B.C #%d\n',i,i);
            npar = numel(BCs.flux(i).paramName);
            for j=1:npar
                fprintf(fid,'%-20s%10.3E\n',...
                        BCs.flux(i).paramName{j},BCs.flux(i).value(j));
            end
            if (che), fwrite(fid,B); end
        end
    end
    fprintf(fid,'   -1                                       ;NO MORE Flux BC\n');
end

fprintf(fid,'   -1                                       ;END PROBLEM\n');

fclose(fid);
end
