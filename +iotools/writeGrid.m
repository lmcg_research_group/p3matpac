function writeGrid(programname, G, I)
%Write *gri.dat file
%
% SYNOPSIS
%  iotools.writeGrid(programname,G);
%  writeGrid(programname,G);
%  iotools.writeGrid( programname, G, I );
%  writeGrid( programname, G, I );
%
% PARAMETERS
%	programname - Name of programa, i.e the name of prefix of *_gri.dat *_gen.dat, the
%                 string will be printted into root.dat.
%	G           - Grid structure mostly as detailed in grid_structure.
%   I           - struct variable formed by initial values of unknowns
%                 OPTIONAL. see help initcond.initval_structure
%
% RETURNS
%	*gri.dat file format with geometry data without initial values of unknowns.
%
% USAGE
%	1)>> iotools.writeGrid( 'falha', G);
%   2)>> iotools.writeGrid( 'falha', G, I);
%
% SEE ALSO
%   gridprocessing.grid_structure
%

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%
%  Programmer
%  ==========
%  Jonathan C. Teixeira
%
%  Creation
%  ========
%  Aug. 13, 2012
%
%  Modification
%  ============
%  ....
%  please when some programmer modify this script put you name, date of addiction
%  or modification statement and describe your modification/addiction like this:
%   * Tommy Hilfiger, Fri Oct 26 2012
%      Add --cflag, --cxxflag.  These *append* a specific flag.
%      Getting '--cflags' to quote reliably is a nightmare.
%
%}

% check input arguments
if sscanf(version('-release'),'%u%*s')<=2013
    if and(nargin<2,nargin>3)
        error('please usage: iotools.writeGrid(<programname>, <G>, <I>) or iotools.writeGrid( <programname>,<G>)');
    end
else
    narginchk(2, 3);
end

assert(ischar(programname),...
    'First argument must be string type.');

assert(isstruct(G),...
    'Second argument must be a struct variable see help gridprocessing.grid_structure.');

if nargin < 3
    che  = []; blko = []; poro = []; Kfactor = []; stresses = []; thm = [];
else
    assert( isstruct(I),...
        'Third argument must be a struct variable see help initcof.initval_structure.');
    che  = []; blko = []; poro = []; Kfactor = []; stresses = []; thm = [];
    if isfield(I,'che')
        che = I.che;
    end
    if isfield(I,'blko')
        blko = I.blko;
    end
    if isfield(I,'poro')
        poro = I.poro;
    end
    if isfield(I,'perm')
        Kfactor = I.perm;
    end
    if isfield(I,'thm')
        thm = I.thm;
    end
    if isfield(I,'stresses')
        stresses = I.stresses;
    end
end

fprintf('-> Into %s\n',mfilename)

xyfrm = '%10.6f';
if any(max(abs(G.nodes.coords))>=999.9999)
   xyfrm = '%20.13e';
end
fmt = '%5d';
if or(G.nodes.num>99999,G.cells.num>99999)
   fmt = '%10d';
end

fid=fopen(strcat(programname,'_gri.dat'),'w');
fprintf(fid,'%5d%5d%5d\n',0,0,0);
progress = textprogressbar(G.nodes.num,'showremtime', true, ...
						 'startmsg','writing coords block  ');

for i=1:G.nodes.num
    fprintf(fid,fmt,i);
    for idim = 1: G.nodes.dim
        fprintf(fid,xyfrm,G.nodes.coords(i,idim));
    end
    fprintf(fid,fmt,G.nodes.bcmech(i),G.nodes.bcflux(i));
    fprintf(fid,'\n');
	progress(i);
end

progress = textprogressbar(G.cells.num,'showremtime', true, ...
						 'startmsg','writing lnods block   ');
for i = 1:G.cells.num
   fprintf(fid,fmt,i);
   fprintf(fid,'%5d',G.cells.mat(i),G.cells.type(i));
   fprintf(fid,fmt,G.cells.lnods(i,:));
   fprintf(fid,'\n');
	progress(i);
end

% initial condition block
iotools.printTHM(fid, G, thm, stresses)


% Porosity and permeability
iotools.printPoroK(fid, G, poro, Kfactor);

% Initial values of unknowns (others physics - che, blko, bsresim, bio, corr,...)
if (~isempty(che))
    % chemical initial condition block
    iotools.printCHE(fid, G, che);
end

if (~isempty(blko))
    % blko initial condition block
    iotools.printBLKO(fid, G, blko);
end

fprintf(fid,'    0\n    0\n    0    0\n    2\n    0\n    0\n');
fprintf(fid,'    0\n    0\n    0\n    0\n;end gri file');
fclose(fid);
end
