function printTHM( fout, G, thm, stress )
%write block of initial condition of thermo-hydro-mechanical problem
%
% SYNOPSIS:
%   iotools.printTHM(fout, G, thm, temp);
%
% PARAMETERS:
%   fout        -   Integer file identifier;
%   G           -   Grid structure mostly as detailed in grid_structure.
%   thm         -   A G.nodes.num-by-1 array of initial thermo-hydro-
%                   mechanical unknowns;
%   stress      -   A G.cells.num-by-6 or G.cells.nummat-by-6 array of
%                   initial stress tensor;
%
% RETURNS:
%
% EXAMPLE:
%   iotools.printTHM(fout, G, thm, stress);
%
% SEE ALSO:
%   fprintf, fopen, fclose
%

% check input arguments
if sscanf(version('-release'),'%u%*s')<=2013
    if and(nargin<2,nargin>4)
        error('please usage: iotools.printTHM(<fid>, <G>, <thm>, <temp>);');
    end
else
    error(nargchk(2, 4, nargin));;
end


assert(fout > 0,...
    'Cannot open or no found file for writing.');

assert(isstruct(G),...
    'Second argument must be a struct variable see help gridprocessing.grid_structure.');

nn = G.nodes.num; ne = G.cells.num; nm = G.cells.nummat;
thm_ = thm; stress_ = stress;
if ~isempty(thm_)
    assert( nn == length(thm_), ...
        'Third argument error, length(thm) ~= %d',nn);
    [nn, nthm] = size(thm_);
end
if ~isempty(stress_)
    assert( ne == length(stress_) ||  nm == length(stress_), ...
        'Third argument error, length(stress) ~= %d or %d',ne,nm);
    [ns, nstress] = size(stress_);
end

% -----< WRITING INITIAL VALUES FOR UNKNOWNS >-----

progress = textprogressbar(nn,'showremtime', true, ...
						 'startmsg','writting thm block     ');
fmt = '%5i'; 
if or(G.nodes.num>99999,G.cells.num>99999)
    fmt = '%10i';
end
if isempty(thm_)
    fprintf(fout,[fmt,' !Insert initial values of unknowns [ux,uy,uz,pl,pg,T] using format F15.9  \n'],1);
	progress(nn/2);
    fprintf(fout,[fmt,' !Insert initial values of unknowns [ux,uy,uz,pl,pg,T] using format F15.9  \n'],G.nodes.num);
	progress(nn);
else
    k=1;
    pt(k) = 1; %#ok<*NASGU>
    fprintf(fout,fmt,1);
    for j=1:nthm
        fprintf(fout,'%15.8e',thm_(1,j));
    end
    fprintf(fout,'\n');
    for i=2:nn-1
        if all(thm_(i,:) == thm_(i-1,:))
            continue;
        end
        k = k + 1;
        pt(k) = i;
        if (pt(k)-pt(k-1))==1   % node on boundary between two materials!!!!!
            fprintf(fout,fmt,i);
            for j=1:nthm
                fprintf(fout,'%15.8e',thm_(i,j));
            end
            fprintf(fout,'\n');
        else
            fprintf(fout,fmt,i-1);
            for j=1:nthm
                fprintf(fout,'%15.8e',thm_(i-1,j));
            end
            fprintf(fout,'\n');
            fprintf(fout,fmt,i);
            for j=1:nthm
                fprintf(fout,'%15.8e',thm_(i,j));
            end
            fprintf(fout,'\n');
        end
        progress(i);
    end
    fprintf(fout,fmt,nn);
    for j=1:nthm
        fprintf(fout,'%15.8e',thm_(nn,j));
    end
    fprintf(fout,'\n');
	progress(nn);
end


% -----< WRITING INITIAL VALUES FOR STRESSES >-----
if ~isempty(stress_)
	progress = textprogressbar(ns,'showremtime', true, ...
							 'startmsg','writing stress block  ');

    k=1;
    pt(k) = 1; %#ok<*NASGU>
    fprintf(fout,fmt,1);
    for j=1:nstress
        fprintf(fout,'%15.8e',stress_(1,j));
    end
    fprintf(fout,'\n');
    for i=2:ns-1
        if all(stress_(i,:) == stress_(i-1,:))
            continue;
        end
        k = k + 1;
        pt(k) = i;
        if (pt(k)-pt(k-1))==1   % node on boundary between two materials!!!!!
            fprintf(fout,fmt,i);
            for j=1:nstress
                fprintf(fout,'%15.8e',stress_(i,j));
            end
            fprintf(fout,'\n');
        else
            fprintf(fout,fmt,i-1);
            for j=1:nstress
                fprintf(fout,'%15.8e',stress_(i-1,j));
            end
            fprintf(fout,'\n');
            
            fprintf(fout,fmt,i);
            for j=1:nstress
                fprintf(fout,'%15.8e',stress_(i,j));
            end
            fprintf(fout,'\n');
        end
        progress(i);
    end
    fprintf(fout,fmt,ns);
    for j=1:nstress
        fprintf(fout,'%15.8e',stress_(i,j));
    end
    fprintf(fout,'\n');
	progress(ns);
end
end

