function  varargout = plotGrid( G, varargin)
%This function plot grid.
%
% SYNOPSIS
%       iotools.plotGrid_(G)
%       iotools.plotGrid_(G, 'pn1', pv1, ...)
%       h = iotools.plotGrid_(...)
%       % Make sure that gridding package was imported
%       import iotools.*
%       plotGrid_(...)
%
% PARAMETERS
%   G       - Grid structure mostly as detailed in grid_structure (see >> help
%             mesh.grid_structure).
%
%   'pn'/pv - List of other property name/value pairs.  OPTIONAL.
%             This list will be passed directly on to function PATCH
%             meaning all properties supported by PATCH are valid
%
% RETURNS
%	h - Handle to resulting PATCH object.  The patch object is added to the
%       current AXES object.
%
% EXAMPLE
%	1)>> G = mesh.getGridInfo()
%	  >> iotools.plotGrid_(G)
%
%   2)>> G = mesh.getGridInfo()
%	  >> import iotools.* ; plotGrid_(G)
%
% SEE ALSO:
%   plotCellData_, plotData_, patch, shading, gridprocessing.grid_structure
%

% Default to showing the entire grid model.
%
if G.nodes.dim == 2
    idx = find(G.cells.type ~= 1 & G.cells.type ~= 2 & G.cells.type ~= 3 ...
        & G.cells.type ~= 5 & G.cells.type ~= 6 & G.cells.type ~= 8 & ...
        G.cells.type ~= 12 & G.cells.type ~= 101 & G.cells.type ~= 102 ); %#ok<*EFIND>
    assert(isempty(idx), 'element type on lnods block not supported')
else
    idx = find(G.cells.type ~= 1 & G.cells.type ~= 3 & G.cells.type ~= 101 ...
         & G.cells.type ~= 102);
    assert(isempty(idx), 'element type on lnods block not supported')
end

% Assert that remaining arguments at least appear to be 'name'/value pairs
% intended for PATCH.
%
if ~isempty(varargin)
  assert (all(cellfun(@ischar, varargin(1 : 2 : end))), ...
     'Additional arguments to plotGrid should be ''prop''/value pairs.');
end

node=[G.nodes.coords zeros(G.nodes.num,1)];
minCellsType = min(G.cells.type);
maxCellsType = max(G.cells.type);

col = ['y';'w';'r';'b';'g';'c';'m'];
modulus = mod(G.cells.mat,length(col));
idx = modulus == 0;
modulus(idx) = 7;

%% plot grid
if G.nodes.dim == 2
    colk = col(modulus);
    colour = get_rgb(colk);

    faces_matrix = zeros(G.cells.num,5);
    T3 = find(G.cells.nnel == 3);
    if ~isempty(T3)
        kxx = [ 1, 2, 3, 1, 1];
        faces_matrix(T3,:) = G.cells.lnods(T3,kxx);
    end
    T6 = find(G.cells.nnel == 6);
    if ~isempty(T6)
        kxx = [ 1, 2, 3, 1, 1];
        faces_matrix(T6,:) = G.cells.lnods(T6,kxx);
    end
    Q4 = find(G.cells.nnel == 4);
    if ~isempty(Q4)
        kxx = [ 1, 2, 3, 4, 1];
        faces_matrix(Q4,:) = G.cells.lnods(Q4,kxx);
    end

    p = patch('Vertices',G.nodes.coords,'Faces',faces_matrix,...
            'FaceVertexCData',colour,'FaceColor','flat','facea', 0.3, ...
            'edgea',0.1,varargin{:});                 %#ok<*NASGU>
else

    if G.cells.nnel(1) == 8
        face_matrix = zeros(6*G.cells.num,5);

        kxx = [1,2,3,4,1; 5,6,7,8,5; 1,2,6,5,1; ...
               2,3,7,6,2; 3,4,8,7,3; 1,4,8,5,1];

        for iel = 1:G.cells.num
           face_matrix((iel-1)*6+1,:) = G.cells.lnods(iel,kxx(1,:));        % faces per cell
           face_matrix((iel-1)*6+2,:) = G.cells.lnods(iel,kxx(2,:));        % faces per cell
           face_matrix((iel-1)*6+3,:) = G.cells.lnods(iel,kxx(3,:));        % faces per cell
           face_matrix((iel-1)*6+4,:) = G.cells.lnods(iel,kxx(4,:));        % faces per cell
           face_matrix((iel-1)*6+5,:) = G.cells.lnods(iel,kxx(5,:));        % faces per cell
           face_matrix((iel-1)*6+6,:) = G.cells.lnods(iel,kxx(6,:));        % faces per cell
        end
        cdata = reshape(repmat(G.cells.mat(:),1,6)',6*G.cells.num,1);

        p = patch('Vertices',G.nodes.coords,'Faces',face_matrix,...
            'EdgeColor', 'k','FaceVertexCData',cdata,'FaceColor','flat',...
            'facea', 0.3, 'edgea',0.1,varargin{:}); colorbar;
        view(3);
    end

    if G.cells.nnel(1) == 4
        tetramesh(G.cells.lnods(:,1:G.cells.nnel(:)),G.nodes.coords,...
                  G.cells.mat,'FaceColor','flat','facea', 0.7, ...
                  'edgea',0.1,varargin{:}); colorbar;
        view(3);
    end
end

axis([min(G.nodes.coords(:,1)) max(G.nodes.coords(:,1)) ...
      min(G.nodes.coords(:,2)) max(G.nodes.coords(:,2))])

% for compatibility elements
if (any(G.cells.type>100))
    elm = G.cells.type>100;
    hold on
    plot(G.nodes.coords(G.cells.lnods(elm,1:max(G.cells.nnel)+1),1), ...
         G.nodes.coords(G.cells.lnods(elm,1:max(G.cells.nnel)+1),2),'sb');
    hold off
end
axis equal tight;
end




%--------------------------------------------------------------------------

function rgb = get_rgb(colour)
	rgb = zeros(length(colour),3);
    for i=1:length(colour)
       switch lower(colour(i)),
          case {'y', 'yellow' }, rgb(i,:) = [1, 1, 0];
          case {'m', 'magenta'}, rgb(i,:) = [1, 0, 1];
          case {'c', 'cyan'   }, rgb(i,:) = [0, 1, 1];
          case {'r', 'red'    }, rgb(i,:) = [1, 0, 0];
          case {'g', 'green'  }, rgb(i,:) = [0, 1, 0];
          case {'b', 'blue'   }, rgb(i,:) = [0, 0, 1];
          case {'w', 'white'  }, rgb(i,:) = [1, 1, 1];
          case {'k', 'black'  }, rgb(i,:) = [0, 0, 0];
          otherwise            , rgb(i,:) = [0, 0, 1]; % Unknown colour -> 'blue'.
       end
    end
end
