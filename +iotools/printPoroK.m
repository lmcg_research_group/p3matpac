function printPoroK( fid, G, phi, K )
%write  Porosity block of problem
%
% SYNOPSIS:
%   iotools.printPoroK(fout, G, phi, K);
%
% PARAMETERS:
%   fout        -   Integer file identifier;
%   G           -   Grid structure mostly as detailed in grid_structure.
%   phi         -   A e-by-1 or m-by-1 array of initial porosity values;
%   K           -   A e-by-1 or m-by-1 array of initial permeability tensor
%                   multipler factor;
%
% RETURNS:
%
% EXAMPLE:
%   iotools.printPoroK(fout,  G, phi, K);
%
% SEE ALSO:
%   fprintf, fopen, fclose
%

% check input arguments
if sscanf(version('-release'),'%u%*s')<=2013
    if (nargin~=4)
        error('please usage: iotools.printPoroK(<fid>, <G>, <phi>, <K>);');
    end
else
    error(nargchk(4, 4, nargin));
end


assert(fid > 0,...
    'Cannot open or no found file for writing.');

assert(isstruct(G),...
    'Second argument must be a struct variable see help gridprocessing.grid_structure.');

nel = G.cells.num; nmat = G.cells.nummat;

progress = textprogressbar(nel,'showremtime', true, ...
						 'startmsg','writting porosity block');
fmt = '%5i'; 
if or(G.nodes.num>99999,G.cells.num>99999)
    fmt = '%10i';
end
if all([isempty(phi), isempty(K)])
    fprintf(fid,[fmt,' !Porosity(%d)\n'],1, G.cells.mat(1));
    ipim=1;
    pimp(ipim)=1;
    for i=2:nel-1
        if G.cells.mat(i)~=G.cells.mat(pimp(ipim))
            ipim=ipim+1;
            pimp(ipim)=i;
            if (pimp(ipim)-pimp(ipim-1)>1)
                fprintf(fid,[fmt,' !Porosity(%d)\n'],i-1, G.cells.mat(i-1));
                fprintf(fid,[fmt,' !Porosity(%d)\n'],i, G.cells.mat(i));
            else
                fprintf(fid,[fmt,' !Porosity(%d)\n'],i, G.cells.mat(i));
            end
        end

        progress(i);
    end
    fprintf(fid,[fmt,' !Porosity(%d)\n'],nel, G.cells.mat(nel));
	progress(nel);
elseif and(isempty(K),~isempty(phi))
    nphi = length(phi);
    if ( nel == nphi )
        for i=1:nel
            fprintf(fid,[fmt,'%15.7e\n'],i, phi(i));
            progress(i);
        end
    end

    if (nmat == nphi)
        mat = G.cells.mat(1);
        fprintf(fid,[fmt,'%15.7e\n'],1, phi(mat));
        ipim=1;
        pimp(ipim)=1;
        for i=2:nel-1
            mat1 = G.cells.mat(i-1);
            mat2 = G.cells.mat(i);
            if G.cells.mat(i)~=G.cells.mat(pimp(ipim))
                ipim=ipim+1;
                pimp(ipim)=i;
                if (pimp(ipim)-pimp(ipim-1)>1)
                    fprintf(fid,[fmt,'%15.7e\n'],i-1, phi(mat1));
                    fprintf(fid,[fmt,'%15.7e\n'],i, phi(mat2));
                else
                    fprintf(fid,[fmt,'%15.7e\n'],i, phi(mat2));
                end

                progress(i);
            end
        end
        mat = G.cells.mat(nel);
        fprintf(fid,[fmt,'%15.7e\n'],nel, phi(mat));
		progress(nel);
    end
else
    nphi = numel(phi);
    [nK, dK] = size(K);
    assert(nphi == nK,...
        'phi and K arguments error, they must have same lenght');
    if ( nel == nK )
        for i=1:nel
            fprintf(fid,[fmt,'%15.7e'],i, phi(i));
            for d=1:dK
                fprintf(fid,'%15.7e', K(i,d));
            end
            fprintf(fid,'\n');
            progress(i);
        end
    end

    if (nmat == nK)
        mat = G.cells.mat(1);
        fprintf(fid,[fmt,'%15.7e'],1, phi(mat));
        for d=1:dK
            fprintf(fid,'%15.7e', K(mat,d));
        end
        fprintf(fid,'\n');
        ipim=1;
        pimp(ipim)=1;
        for i=2:nel-1
            mat1 = G.cells.mat(i-1);
            mat2 = G.cells.mat(i);
            if mat1~=mat2
                ipim=ipim+1;
                pimp(ipim)=i;
                if (pimp(ipim)-pimp(ipim-1)>1)
                    fprintf(fid,[fmt,'%15.7e'],i-1, phi(mat1));
                    for d=1:dK
                        fprintf(fid,'%15.7e', K(mat1,d));
                    end
                    fprintf(fid,'\n');

                    fprintf(fid,[fmt,'%15.7e'],i, phi(mat2));
                    for d=1:dK
                        fprintf(fid,'%15.7e', K(mat2,d));
                    end
                    fprintf(fid,'\n');
                else
                    fprintf(fid,[fmt,'%15.7e'],i, phi(mat2));
                    for d=1:dK
                        fprintf(fid,'%15.7e', K(mat2,d));
                    end
                    fprintf(fid,'\n');
                end

                progress(i);
            end
        end
        mat = G.cells.mat(nel);
        fprintf(fid,[fmt,'%15.7e'],nel, phi(mat));
        for d=1:dK
            fprintf(fid,'%15.7e', K(mat,d));
        end
        fprintf(fid,'\n');
		progress(nel);
    end
end


end

