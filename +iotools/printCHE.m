function printCHE( fidout, G, cheinit)
%write block of initial condition of reative transport problem
%
% SYNOPSIS:
%   iotools.printCHE(fout, cheinit);
%
% PARAMETERS:
%   fout        -   Integer file identifier
%   cheinit     -   Matrix, G.nodes.num-by-m, of initial species chemical
%       concetration.
%
% RETURNS:
%
% EXAMPLE:
%   iotools.printCHE( fin, G, cheinit );
%
% SEE ALSO:
%   fprintf, fopen, fclose
%

[nn, species] = size( cheinit );

progress = textprogressbar(nn,'showremtime', true, ...
						 'startmsg','writting chemical block');

fmt = '%5i'; 
if or(G.nodes.num>99999,G.cells.num>99999)
    fmt = '%10i';
end

% first node impress
k=1;
pt(k) = 1;
fprintf(fidout,fmt,1);
for ii=1:species
    fprintf(fidout,'%15.8e',cheinit(1,ii));
end
fprintf(fidout,'\n');

for i=2:nn-1
    if all(cheinit(i,:) == cheinit(i-1,:))
        continue;
    end
    k = k + 1;
    pt(k) = i;
    if (pt(k)-pt(k-1))==1   % node on boundary between two materials!!!!!
        fprintf(fidout,fmt,i);
        for ii=1:species
            fprintf(fidout,'%15.8e',cheinit(i,ii));
        end
        fprintf(fidout,'\n');
    else
        fprintf(fidout,fmt,i-1);
        for ii=1:species
            fprintf(fidout,'%15.8e',cheinit(i-1,ii));
        end
        fprintf(fidout,'\n');

        fprintf(fidout,fmt,i);
        for ii=1:species
            fprintf(fidout,'%15.8e',cheinit(i,ii));
        end
        fprintf(fidout,'\n');
    end
    progress(i);
end

% last node impress
fprintf(fidout,fmt,nn);
for ii=1:species
    fprintf(fidout,'%15.8e',cheinit(nn,ii));
end
fprintf(fidout,'\n');
progress(nn);

end

