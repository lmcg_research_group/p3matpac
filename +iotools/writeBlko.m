function writeBlko(programname, PVT, reK)
%Write *blko.dat file
%
% SYNOPSIS
%  iotools.writeBlko( programname, PVT, reK );
%  writeBlko( programname, PVT, reK );
%
% PARAMETERS
%	programname - Name of programa, i.e the name of prefix of *_gri.dat *_gen.dat, the
%                 string will be printted into root.dat.
%	PVT         - PVT data structure mostly as detailed in iotools.pvt_structure.
%   reK           - struct variable formed by initial values of unknowns
%                 OPTIONAL. see help initcond.initval_structure
%
% RETURNS
%	*gri.dat file format with geometry data without initial values of unknowns.
%
% USAGE
%	1)>> import iotools.*
%	  >> writeBlko( 'falha', PVT, reK);;
%   2)>> iotools.writeBlko( 'falha', PVT, reK);
%
% SEE ALSO
%   
%

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%
%  Programmer
%  ==========
%  Jonathan C. Teixeira
%
%  Creation
%  ========
%  Aug. 13, 2012
%
%  Modification
%  ============
%  ....
%  please when some programmer modify this script put you name, date of addiction
%  or modification statement and describe your modification/addiction like this:
%   * Tommy Hilfiger, Fri Oct 26 2012
%      Add --cflag, --cxxflag.  These *append* a specific flag.
%      Getting '--cflags' to quote reliably is a nightmare.
%
%}

    %----------------------------------------------------------------------
    % Check input data ----------------------------------------------------

    % check input arguments
    if sscanf(version('-release'),'%u%*s')<=2013
        if and(nargin<3)
            error('please usage: iotools.writeBlko(<programname>, <PVT>, <reK>);');
        end
    else
        narginchk(3, 3);
    end

    assert(ischar(programname),...
        'First argument must be string type.');

    assert(isstruct(PVT),...
        'Second argument must be a struct variable see help iotools.pvt_structure.');

    assert(isstruct(reK),...
        'Second argument must be a struct variable see help iotools.relK_structure.');


    if isfield(PVT,'FileFormat')
        fformat = PVT.FileFormat;
    elseif isfield(reK,'FileFormat')
        fformat = reK.FileFormat;
    else
        fformat = 1;        % file format to impet version
    end

    fid=fopen(strcat(programname,'_blko.dat'),'w');

    if (fformat)
        impetFormat(fid,PVT,reK);
    else
        fullyFormat(fid,PVT,reK);
    end
    fclose(fid);
end

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------

function impetFormat(fid,PVT_,relK)


    %----------------------------------------------------------------------
    % Check input data ----------------------------------------------------
    if isfield(relK,'sw')
        [np, nph]=size(relK.sw);
        assert(relK.numphase==nph,...
            'number of phase (colunm) into reK.sw no mismatch with reK.nphase');
    end
    if isfield(relK,'kr')
        [np, nph]=size(relK.kr);
        assert(relK.numphase==nph,...
            'number of phase (colunm) into reK.kr no mismatch with reK.nphase');
    end
    if isfield(relK,'phaseName')
        [np, nph]=size(relK.phaseName);
        assert(relK.numphase==nph,...
            'number of phase into reK.phaseName no mismatch with reK.nphase');
    end
    if isfield(relK,'numpoint')
        [np, nph]=size(relK.numpoint);
        assert(relK.numphase==nph,...
            'number of phase (colunm) into reK.numpoint no mismatch with reK.nphase');
    else
        relK.numpoint = length(relK.sw)*ones(nph,1);
    end

    if isfield(PVT_,'compress')
        assert(PVT_.numphase==length(PVT_.compress),...
            'number of phase into PVT.compress no mismatch with PVT.nphase');
    else
        PVT_.compress = zeros(PVT_.numphase,1);
    end
    if isfield(PVT_,'pstd')
        assert(PVT_.numphase==length(PVT_.pstd),...
            'number of phase into PVT.pstd no mismatch with PVT.nphase');
    else
        PVT_.pstd = zeros(PVT_.numphase,1);
    end
    if isfield(PVT_,'density')
        assert(PVT_.numphase==length(PVT_.density),...
            'number of phase into PVT.density no mismatch with PVT.nphase');
    end
    if isfield(PVT_,'viscosity')
        assert(PVT_.numphase==length(PVT_.viscosity),...
            'number of phase into PVT.viscosity no mismatch with PVT.nphase');
    end
    if isfield(PVT_,'phaseName')
        [np, nph]=size(PVT_.phaseName);
        assert(PVT_.numphase==nph,...
            'number of phase into PVT.phaseName no mismatch with PVT.nphase');
    end



    %----------------------------------------------------------------------
    % printing input data -------------------------------------------------
    fprintf(fid,'file created by %s\n',mfilename);

    fprintf(fid,'   _Module   _upwind  _CFL-flux _fluidSys_OutLevel\n');
    fprintf(fid,'         0         0          0         0        0\n');
    fprintf(fid,'_CFLNumber  _FluxTol  minDVTOL  maxDVTOL\n');
    fprintf(fid,'    0.6500   1.0e-04      0.55      1.10\n');
    fprintf(fid,'_Relative Permeability and PVT Data\n');

    for i=1:relK.numphase
        fprintf(fid,'    S%1s       Kr%1s\n',relK.phaseName(i),relK.phaseName(i));
        fprintf(fid,'%5d\n',relK.numpoint(i));
        for j=1:relK.numpoint(i)
            fprintf(fid,'%10.6f%10.6f\n',relK.sw(j,i),relK.kr(j,i));
        end
    end

    for i=1:relK.numphase
        fprintf(fid,'    P%1s    rho%1s     visc%1s      c%1s\n',...
            relK.phaseName(i),relK.phaseName(i),relK.phaseName(i),relK.phaseName(i));
        fprintf(fid,'%10.4f%10.4f%10.4f%10.4f\n',...
            PVT_.pstd(i),PVT_.density(i), PVT_.viscosity(i), PVT_.compress(i));
    end
end


% -------------------------------------------------------------------------
% -------------------------------------------------------------------------

function fullyFormat(fid,PVT_,relK)


    %----------------------------------------------------------------------
    % Check input data ----------------------------------------------------
    if isfield(relK,'sw')
        [np, nph]=size(relK.sw);
        assert(relK.numphase==nph,...
            'number of phase (colunm) into reK.sw no mismatch with reK.nphase');
    end
    if isfield(relK,'kr')
        [np, nph]=size(relK.kr);
        assert(relK.numphase==nph,...
            'number of phase (colunm) into reK.kr no mismatch with reK.nphase');
    end
    if isfield(relK,'numpoint')
        [np, nph]=size(relK.numpoint);
        assert(relK.numphase==nph,...
            'number of phase (colunm) into reK.numpoint no mismatch with reK.nphase');
    else
        relK.numpoint = length(relK.sw)*ones(nph,1);
    end

    if isfield(PVT_,'compress')
        assert(PVT_.numphase==length(PVT_.compress),...
            'number of phase into PVT.compress no mismatch with PVT.nphase');
    else
        PVT_.compress = zeros(PVT_.numphase,1);
    end
    if isfield(PVT_,'pstd')
        assert(PVT_.numphase==length(PVT_.pstd),...
            'number of phase into PVT.pstd no mismatch with PVT.nphase');
    else
        PVT_.pstd = zeros(PVT_.numphase,1);
    end
    if isfield(PVT_,'density')
        assert(PVT_.numphase==length(PVT_.density),...
            'number of phase into PVT.density no mismatch with PVT.nphase');
    end
    if isfield(PVT_,'viscosity')
        assert(PVT_.numphase==length(PVT_.viscosity),...
            'number of phase into PVT.viscosity no mismatch with PVT.nphase');
    end



    %----------------------------------------------------------------------
    % printing input data -------------------------------------------------
    fprintf(fid,'Sw        Krw       Krow      pc  \n',mfilename);

    fprintf(fid,'%5d%10.4f%10.4f\n',relK.numpoint(1),.1,.0);
    for i=1:relK.numpoint(1)
        fprintf(fid,'%10.6f',relK.sw(i));
        for j=1:relK.numphase
            fprintf(fid,'%10.6f%10.6f',relK.kr(j,i));
        end
        fprintf(fid,'%10.6f\n',relK.pc(i));
    end

    fprintf(fid,'po        rhow      rhoo      muw     muo \n');
end
