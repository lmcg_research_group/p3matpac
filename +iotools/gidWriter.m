function gidWriter(basename,G,varargin)
%Write GiD mesh file format (*.msh)
%
% SYNOPSIS
%       iotools.gidWriter(filename,G)
%       iotools.gidWriter(filename, G, 'pn1', pv1,...)
%
% PARAMETERS
%   filename        - Name of GiD mesh and result file
%
%   G               - Grid structure mostly as detailed in grid_structure (see >> help
%                     gridprocessing.grid_structure).
%
% 'append'/value    - Property 'append' will be passed to write (Append data to the end
%                     of res file). OPTIONAL (value=false DEFAULT).
%
%   'pn'/pv         - List of property names/property values.  OPTIONAL.
%                     This list will be passed to block node data or cell data of
%                     res file (i.e. if 'pn' = 'X5' and pv = cheinit [from
%                     initcond.chemInitCond_] into res file will have the node
%                     variable called X5 with values cheinit, where
%                     numel(cheinit) = G.nodes.num )
%
%
% RETURNS
%   success   - True success write or False error
%
% EXAMPLE
%   1)>> iotools.gidWriter('fivespot',G);
%   2)>> ok = iotools.gidWriter('falha',G,'X5', cheinit);
%
% SEE ALSO:
%   fopen, fclose, fprintf, gridprocessing.grid_structure
%
% NOTES
%   to see mesh you need install GiD program (Download GiD on
%   http://www.gidhome.com/download/official-versions).
%

%% check inputs
assert(ischar(basename), ...
    'First argument must be a filename.')
assert(isstruct(G),...
    'First argument must be a struct variable see help gridprocessing.grid_structure.');
assert(mod(numel(varargin), 2) == 0,...
    'Last arguments must be a list of property names/property values')

opt = struct('timestep',0,'cycle',0, 'verbose', false);
[opt, varin] = merge_options(opt, varargin{:});


vararg = zeros(numel(varin)/2,2);
for i=1:2:numel(varin)
    assert(ischar(varin{i}),...
        'Last arguments must be a list of property names/property values');
    assert(isnumeric(varin{i+1}),...
        'Last arguments must be a list of property names/property values');
    [vararg(i,1), vararg(i,2)] = size(varin{i+1});
end
msh = strcat(basename,'.msh');
res = strcat(basename,'.res');

elist = [];
append = true;
if exist(res,'file')==0
    append = false;
end

if (~append)
    % Creating *msh file
    fprintf('** creating %s file\n', msh);
    fidmsh = fopen(msh,'w');
    elist=gidMesh(fidmsh,G);
    fclose(fidmsh);
    fprintf('** creating %s file\n', res);
    fidres = fopen(res,'w');
else
    fprintf('** appending new data in %s file...\n', res);
    fidres = fopen(res,'a');
end
% Creating *res file
gidResOld(fidres, opt, G, elist, varin, vararg)
fclose(fidres);

end



%-------------------------------------------------------------------------
% private functions
%
% -------------------------------------------------------------------------
% >>>>>>>>>>>>>>> MESH <<<<<<<<<<<<<<<
% Write gid mesh format
function elist=gidMesh(fidmsh,G)
nnelm = unique(G.cells.nnel);
elist=[]; e=0;

tot = G.nodes.num+G.cells.num;
progress = textprogressbar(tot,'startmsg','writing mesh file:');
ii=0;
for i=1:numel(nnelm)
    if (G.nodes.dim==2)
        switch nnelm(i)
            case (3)
                fprintf(fidmsh,'mesh "GroupID" dimension 2 elemtype triangle nnode 3\n');
                e=e+1;
                elist{e}='2DLT'; %#ok<*AGROW>
            case (6)
                fprintf(fidmsh,'mesh "GroupID" dimension 2 elemtype triangle nnode 6\n');
                e=e+1;
                elist{e}='2DQT';
            case (4)
                fprintf(fidmsh,'mesh "GroupID" dimension 2 elemtype quadrilateral nnode 4\n');
                e=e+1;
                elist{e}='2DLQ';
            case (2)
                fprintf(fidmsh,'mesh "GroupID" dimension 2 elemtype linear nnode 2\n');
                e=e+1;
                elist{e}='2DLS';
        end
    elseif (G.nodes.dim==3)
        switch nnelm(i)
            case (8)
                fprintf(fidmsh,'mesh "GroupID" dimension 3 elemtype hexahedra nnode 8\n');
                e=e+1;
                elist{e}='3DLH';
            case (4)
                fprintf(fidmsh,'mesh "GroupID" dimension 3 elemtype tetrahedra nnode 4\n');
                e=e+1;
                elist{e}='3DLT';
            case (2)
                fprintf(fidmsh,'mesh "GroupID" dimension 2 elemtype linear nnode 2\n');
                e=e+1;
                elist{e}='3DLS';
        end
    end

    %% Coordinates
    fprintf(fidmsh,'\ncoordinates\n');
    if (i==1)
        for j=1:G.nodes.num
            fprintf(fidmsh,'%10d ',j);
            for k=1:G.nodes.dim
                fprintf(fidmsh,' %22.10f',G.nodes.coords(j,k));
            end
            fprintf(fidmsh,'\n');
            ii=ii+1;
            progress(ii);
        end
    end
    fprintf(fidmsh,'end coordinates\n');

    %% Elements
    fprintf(fidmsh,'\nelements\n');

    idx = find(G.cells.nnel==nnelm(i));
    for j=1:numel(idx)
        fprintf(fidmsh,'%10d ',idx(j));
        for k=1:G.cells.nnel(idx(j))
            fprintf(fidmsh,'%10d ',G.cells.lnods(idx(j),k));
        end
        fprintf(fidmsh,'%10d \n',G.cells.mat(idx(j)));
        ii=ii+1;
        progress(ii);
    end
    fprintf(fidmsh,'end elements\n');
end
end



%---------------------------------------------------------------------------
% >>>>>>>>>>>>>>> RESULT <<<<<<<<<<<<<<<
% Write variable on Results file
function gidResOld(fidres,opt,G,elist,varin,vararg)
if isempty(elist)
    nnelm = unique(G.cells.nnel);
    e=0;
    for i=1:numel(nnelm)
        if (G.nodes.dim==2)
            switch nnelm(i)
                case (3)
                    e=e+1;
                    elist{e}='2DLT'; %#ok<*AGROW>
                case (6)
                    e=e+1;
                    elist{e}='2DQT';
                case (4)
                    e=e+1;
                    elist{e}='2DLQ';
                case (2)
                    e=e+1;
                    elist{e}='2DLS';
            end
        elseif (G.nodes.dim==3)
            switch nnelm(i)
                case (8)
                    e=e+1;
                    elist{e}='3DLH';
                case (4)
                    e=e+1;
                    elist{e}='3DLT';
                case (2)
                    e=e+1;
                    elist{e}='3DLS';
            end
        end
    end
else
    nonel = unique(G.cells.nnel);
    if (G.nodes.dim==2)
        for i=1:numel(nonel)
            if (nonel(i)==3),                       % Linear Triangle 2D
                writeGPTri(fidres);
            elseif (nonel(i)==4),                   % Linear Quadrilateral 2D
                writeGPQua(fidres)
            elseif (nonel(i)==6),                   % Quadratic Triangle 2D
                writeGPTri6(fidres)
            elseif (nonel(i)==2),                   % Linear segment 2D
                writeGPLin2D(fidres)
            else
                error(msbtrace(strcat(':UnsuportedElement',...
                    sprintf('***ERROR: Unsupported element type'))))
            end
        end
    elseif (G.nodes.dim==3)
        for i=1:numel(nonel)
            if (nonel(i)==4)                       % Linear Tetrahedron 3D
                writeGPTet(fidres);
            elseif (nonel(i)==8)                   % Linear Hexahedron 3D
                writeGPHex(fidres)
            elseif (nonel(i)==2)                   % Linear segment 3D
                writeGPLin3D(fidres)
            else
                error(msbtrace(strcat(':UnsuportedElement',...
                    sprintf('***ERROR: Unsupported element type'))));
            end
        end
    else
        error(msbtrace(strcat(':UnsuportedElement',...
                sprintf('***ERROR: Unsupported dimension 1'))))
    end
end
fprintf(fidres,'\n\n');
nn = G.nodes.num;
tot = 0;
for i=1:2:numel(varin)
    if (vararg(i,1) ~= nn), continue; end;
    tot = tot + nn;
end
tot = tot+2*nn;
progress = textprogressbar(tot,'startmsg','writing node data:');

% -----< NODAL DATA >-----
analysis_tp=1;                 % kind of analysis (GiD) ->1= time-step
time_step=opt.timestep;        % timestep
dat_dim=1;                     % data dimension
dat_loc=1;                    % position of data (GiD), 1 -> on the nodes
ndescr=0;                      % description of each component (GiD), 0 -> no descr
fprintf(fidres,'%15s%5d%22.13e%5d%5d%5d\n','BCOND_Flux',analysis_tp,...
    time_step,dat_dim,dat_loc,ndescr);
for i=1:nn
    fprintf(fidres,'%10d %10d\n',i,G.nodes.bcflux(i));
    progress(i);
end
fprintf(fidres,'%15s%5d%22.13e%5d%5d%5d\n','BCOND_Mech',analysis_tp,...
    time_step,dat_dim,dat_loc,ndescr);
for i=1:nn
    fprintf(fidres,'%10d %10d\n',i,G.nodes.bcmech(i));
    progress(nn+i);
end

%% ---------------------------------------------------- Point Data varargin
count = 2;
for i=1:2:numel(varin)
    if (vararg(i,1) ~= nn), continue; end;
    if (vararg(i,2)>1)
        fprintf(fidres,'%15s%5d%22.13e%5d%5d%5d\n',varin{i},analysis_tp,...
            time_step,vararg(i,2),dat_loc,ndescr);
        for j=1:nn
            fprintf(fidres,'%10d ',j);
            for k=1:vararg(i,2)
                fprintf(fidres,'%22.14e \n',varin{i+1}(j,k));
            end
            fprintf(fidres,'\n');
            progress(count*nn+j);
        end
    else
        fprintf(fidres,'%15s%5d%22.13e%5d%5d%5d\n',varin{i},analysis_tp,...
            time_step,dat_dim,dat_loc,ndescr);
        for j=1:nn
            fprintf(fidres,'%10d %22.14e\n',j,varin{i+1}(j));
            progress(count*nn+j);
        end
    end
    count = count + 1;
end

% -----< ELEMENTAL DATA >-----
analysis_tp=1;                 % kind of analysis (GiD) ->1= time-step
time_step=opt.timestep;        % timestep
dat_dim=1;                     % data dimension
dat_loc=2;                     % position of data (GiD), 2 -> onGP
ndescr=0;                      % description of each component (GiD), 0 -> no descr
nel = G.cells.num;

nonel = unique(G.cells.nnel);

%% ---------------------------------------------------- Cell Data varargin
tot = 0;
for i=1:2:numel(varin)
    for e=1:numel(elist)
        if (vararg(i,1) ~= nel), continue; end;     % not isa cell variable
        tot = tot + 1;
    end
end
if tot>0
    progress = textprogressbar(tot,'startmsg','writing cell data:');

    ii = 0;
    for i=1:2:numel(varin)
        for e=1:numel(elist)
            idx=find(G.cells.nnel==nonel(e));    % index of cell with same type
            if (vararg(i,1) ~= nel), continue; end;     % not isa cell variable
            if (vararg(i,2)>1)                          % vector or tensor var.
                fprintf(fidres,'%15s%5d%22.13e%5d%5d%5d "GP_%s"\n',varin{i},...
                    analysis_tp,time_step,vararg(i,2),dat_loc,ndescr,elist{e});
                for j=1:numel(idx)
                    fprintf(fidres,'%10d ',idx(j));
                    for k=1:vararg(i,2)
                        fprintf(fidres,'%22.14e \n',varin{i+1}(idx(j),k));
                    end
                    fprintf(fidres,'\n');
                end
            else                                        % scalar variable
                fprintf(fidres,'%15s%5d%22.13e%5d%5d%5d "GP_%s"\n',varin{i},...
                    analysis_tp,time_step,dat_dim,dat_loc,ndescr,elist{e});
                for j=1:numel(idx)
                    fprintf(fidres,'%10d %22.14e\n',idx(j),varin{i+1}(idx(j)));
                end
            end
            ii = ii + 1;
            progress(ii)
        end
    end
end

end



% ---------------------------------------------------------------------------
function writeGPTri(fid)
% Write corresponding set of Gauss points in Results file
    fprintf(fid,'GaussPoints "%s" ElemType %s "%s"\n','GP_2DLT','Triangle','GroupID');
    fprintf(fid,'Number Of Gauss Points: %d\n',1);
    fprintf(fid,'Natural Coordinates: %s\n','Internal');  % GiD Standard
    fprintf(fid,'End GaussPoints\n\n');
end



% ---------------------------------------------------------------------------
function writeGPTri6(res)
% Write corresponding set of Gauss points in Results file (Center element)
    % Gauss Points location
    GP = [.333333, .333333];
    fprintf(res,'GaussPoints "%s" ElemType %s "%s"\n','GP_2DQT','Triangle','GroupID');
    fprintf(res,'Number Of Gauss Points: %d\n',1);
    fprintf(res,'Natural Coordinates: %s\n','Given');  % GiD customized
    for i=1:length(GP)
        fprintf(res,' %E',GP(i));
    end
    fprintf(res,'\n');
    fprintf(res,'End GaussPoints\n\n');
    % Write corresponding set of Gauss points in Results file (Center element)
    % Gauss Points location
    GP = [.666666666, .166666666; .166666666, .666666666; .166666666, .166666666];
    fprintf(res,'GaussPoints "%s" ElemType %s "%s"\n','GP_S2DQT','Triangle','GroupID');
    fprintf(res,'Number Of Gauss Points: %d\n',3);
    fprintf(res,'Natural Coordinates: %s\n','Given');  % GiD customized
    for i=1:length(GP)
        fprintf(res,' %E %E\n',GP(i,1),GP(i,2));
    end
    fprintf(res,'End GaussPoints\n\n');
end



% ---------------------------------------------------------------------------
%
function writeGPQua(res)
% Write corresponding set of Gauss points in Results file (Center element)
    % Gauss Points location
    GP = [.0, .0];
    fprintf(res,'GaussPoints "%s" ElemType %s "%s"\n','GP_2DLQ','Quadrilateral','GroupID');
    fprintf(res,'Number Of Gauss Points: %d\n',1);
    fprintf(res,'Natural Coordinates: %s\n','Given');  % GiD Given (customized)
    for i=1:length(GP)
        fprintf(res,' %E',GP(i));
    end
    fprintf(res,'\n');
    fprintf(res,'End GaussPoints\n\n');
    % Write corresponding set of Gauss points in Results file (Center element)
    % Gauss Points location
    fprintf(res,'GaussPoints "%s" ElemType %s "%s"\n','GP_S2DLQ','Triangle','GroupID');
    fprintf(res,'Number Of Gauss Points: %d\n',4);
    fprintf(res,'Natural Coordinates: %s\n','Internal');  % GiD Standard
    fprintf(res,'End GaussPoints\n\n');
end



% ---------------------------------------------------------------------------
%
function writeGPLin2D(res)
% Write corresponding set of Gauss points in Results file (Center element)
    % Gauss Points location
    fprintf(res,'GaussPoints "%s" ElemType %s "%s"\n','GP_2DLS','Linear','GroupID');
    fprintf(res,'Number Of Gauss Points: %d\n Nodes not included\n',1);
    fprintf(res,'Natural Coordinates: %s\n','Internal');  % GiD Standard
    fprintf(res,'End GaussPoints\n\n');
end



% ---------------------------------------------------------------------------
%
function writeGPTet(res)
% Write corresponding set of Gauss points in Results file (Center element)
    % Gauss Points location
    GP = [.166666666, .166666666, .166666666];
    fprintf(res,'GaussPoints "%s" ElemType %s "%s"\n','GP_3DLT','Tetrahedra','GroupID');
    fprintf(res,'Number Of Gauss Points: %d\n',1);
    fprintf(res,'Natural Coordinates: %s\n','Given');  % GiD customized
    for i=1:length(GP)
        fprintf(res,' %E',GP(i));
    end
    fprintf(res,'\n');
    fprintf(res,'End GaussPoints\n\n');
end



% ---------------------------------------------------------------------------
%
function writeGPHex(res)
    % Write corresponding set of Gauss points in Results file (Gauss Points)
    % Gauss Points location
    GP = [0.0, 0.0, 0.0];
    fprintf(res,'GaussPoints "%s" ElemType %s "%s"\n','GP_3DLH','Hexahedra','GroupID');
    fprintf(res,'Number Of Gauss Points: %d\n',1);
    fprintf(res,'Natural Coordinates: %s\n','Given');  % GiD customized
    for i=1:length(GP)
        fprintf(res,' %E',GP(i));
    end
    fprintf(res,'\n');
    fprintf(res,'End GaussPoints\n\n');
end



% ---------------------------------------------------------------------------
%
function writeGPLin3D(res)
    % Write corresponding set of Gauss points in Results file (Center element)
    % Gauss Points location
    fprintf(res,'GaussPoints "%s" ElemType %s "%s"\n','GP_3DLS','Linear','GroupID');
    fprintf(res,'Number Of Gauss Points: %d\n Nodes not included\n',1);
    fprintf(res,'Natural Coordinates: %s\n','Internal');  % GiD Standard
    fprintf(res,'End GaussPoints\n\n');
end




%-------------------------------------------------------------------------
function gidMeshNew(fidmsh,G)
% Write gid new format
    x = 1; y = 2; z = 3;
    %% -----< Write header >-----
    fprintf(fidmsh,'# GRID FILE\n');
%     fprintf(fidres,'GiD Post Results File 1.0\n');

    % ... write nodes Coordinates
    %
    fprintf(fidmsh,'MESH "%s" dimension %d ElemType %s Nnode %d\n Coordinates\n',...
            'GroupID', G.nodes.dim,'Linear', 2);
    if (G.nodes.dim==2),
        for i=1:G.nodes.num
            fprintf(fidmsh,'%d %E %E 0.00000E+00\n',i,G.nodes.coords(i,x),...
                    G.nodes.coords(i,y));
        end
    else
        for i=1:G.nodes.num
            fprintf(fidmsh,'%d %E %E %E\n',i,G.nodes.coords(i,x),...
                    G.nodes.coords(i,y), G.nodes.coords(i,z));
        end
    end
    fprintf(fidmsh,'End Coordinates\n');

    %... write element connectivites over partial meshes associated
    %    to distinct element types
    etypes = unique(G.cells.type); % distinct elements types on mesh

    for i=1:numel(etypes)
        idx = find(G.cells.type==etypes(i));  % collect the i-th element type
        if (G.nodes.dim==2),
            if (etypes(i)==1),                      % Linear Triangle 2D
                writeMeshTri(fidmsh,G,idx);
            elseif or(etypes(i)==2,or(etypes(i)==3,...
                   or(etypes(i)==5,etypes(i)==6))), % Linear Quadrilateral 2D
                writeMeshQua(fidmsh,G,idx)
            elseif (etypes(i)==12),                 % Quadratic Triangle 2D
                writeMeshTri6(fidmsh,G,idx)
            elseif (etypes(i)==8),                  % Linear segment 2D
                writeMeshLin2D(fidmsh,G,idx)
            elseif (etypes(i)==101),                % Linear Triangle 2D (CFE)
                writeMeshTri6(fidmsh,G,idx)
            else
                error(msbtrace(strcat(':UnsuportedElement',...
                    sprintf('***ERROR: Unsupported element type %d',etypes(i)))))
            end
        elseif (G.nodes.dim==3),
            if (etypes(i)==1),                      % Linear Tetrahedron 3D
                writeMeshTet(fidmsh,G,idx);
            elseif (etypes(i)==3),                  % Linear Hexahedron 3D
                writeMeshHex(fidmsh,G,idx)
            elseif (etypes(i)==8),                  % Linear segment 3D
                writeMeshLin3D(fidmsh,G,idx)
            elseif (etypes(i)==101),                % Linear Tetrahedron 3D (CFE)
                writeMeshTet(fidmsh,G,idx)
            else
                error(msbtrace(strcat(':UnsuportedElement',...
                    sprintf('***ERROR: Unsupported element type %d',etypes(i)))))
            end
        else
            error(msbtrace(strcat(':UnsuportedElement',...
                    sprintf('***ERROR: Unsupported dimension 1'))))
        end
    end
end



% ---------------------------------------------------------------------------
function writeMeshTri(msh,G,idx)
% function writeTri(msh, res, G, idx)
% Write Linear Triangle 2D element connectivites and define the Gauss points.
    fprintf(msh,'MESH "%s" dimension %d ElemType %s Nnode %d\n Coordinates\n',...
            'GroupID', G.nodes.dim,'Triangle', G.cells.nnel(idx(1)));
    fprintf(msh,'End Coordinates\n');
    fprintf(msh,'Elements\n');
    for i=1:numel(idx)
        e = idx(i);
        fprintf(msh,'%d ',e);
        for j=1:G.cells.nnel(e)
            fprintf(msh,'%d ',G.cells.lnods(e,j));
        end
        fprintf(msh,'%d\n',G.cells.mat(e));
    end
    fprintf(msh,'End Elements\n');      % end partial mesh in grid file
end



% ---------------------------------------------------------------------------
function writeMeshTri6(msh,G,idx)
% Write Quadratic Triangle 2D element connectivites and define the Gauss points.
    fprintf(msh,'MESH "%s" dimension %d ElemType %s Nnode %d\n Coordinates\n',...
            'GroupID', G.nodes.dim,'Triangle', G.cells.nnel(idx(1)));
    fprintf(msh,'End Coordinates\n');
    fprintf(msh,'Elements\n');
    for i=1:numel(idx)
        e = idx(i);
        fprintf(msh,'%d ',e);
        for j=1:G.cells.nnel(e)
            fprintf(msh,'%d ',G.cells.lnods(e,j));
        end
        fprintf(msh,'%d\n',G.cells.mat(e));
    end
    fprintf(msh,'End Elements\n');      % end partial mesh in grid file
end



% ---------------------------------------------------------------------------
%
function writeMeshQua(msh,G,idx)
% Write Linear Quadrilateral 2D Element connectivites and define the Gauss points.
    fprintf(msh,'MESH "%s" dimension %d ElemType %s Nnode %d\n Coordinates\n',...
            'GroupID', G.nodes.dim,'Quadrilateral', G.cells.nnel(idx(1)));
    fprintf(msh,'End Coordinates\n');
    fprintf(msh,'Elements\n');
    for i=1:numel(idx)
        e = idx(i);
        fprintf(msh,'%d ',e);
        for j=1:G.cells.nnel(e)
            fprintf(msh,'%d ',G.cells.lnods(e,j));
        end
        fprintf(msh,'%d\n',G.cells.mat(e));
    end
    fprintf(msh,'End Elements\n');      % end partial mesh in grid file
end



% ---------------------------------------------------------------------------
%
function writeMeshLin2D(msh,G,idx)
% Write Linear segment 2D Element connectivites and define the Gauss points.
    fprintf(msh,'MESH "%s" dimension %d ElemType %s Nnode %d\n Coordinates\n',...
            'GroupID', G.nodes.dim,'Linear', G.cells.nnel(idx(1)));
    fprintf(msh,'End Coordinates\n');
    fprintf(msh,'Elements\n');
    for i=1:numel(idx)
        e = idx(i);
        fprintf(msh,'%d ',e);
        for j=1:G.cells.nnel(e)
            fprintf(msh,'%d ',G.cells.lnods(e,j));
        end
        fprintf(msh,'%d\n',G.cells.mat(e));
    end
    fprintf(msh,'End Elements\n');      % end partial mesh in grid file
end



% ---------------------------------------------------------------------------
%
function writeMeshTet(msh,G,idx)
% Write Linear Tetrahedron 3D Element connectivites and define the Gauss points.
    fprintf(msh,'MESH "%s" dimension %d ElemType %s Nnode %d\n Coordinates\n',...
            'GroupID', G.nodes.dim,'Tetrahedra', G.cells.nnel(idx(1)));
    fprintf(msh,'End Coordinates\n');
    fprintf(msh,'Elements\n');
    for i=1:numel(idx)
        e = idx(i);
        fprintf(msh,'%d ',e);
        for j=1:G.cells.nnel(e)
            fprintf(msh,'%d ',G.cells.lnods(e,j));
        end
        fprintf(msh,'%d\n',G.cells.mat(e));
    end
    fprintf(msh,'End Elements\n');      % end partial mesh in grid file
end



% ---------------------------------------------------------------------------
%
function writeMeshHex(msh,G,idx)
% Write Linear Hexahedron 3D Element connectivites and define the Gauss points.
    fprintf(msh,'MESH "%s" dimension %d ElemType %s Nnode %d\n Coordinates\n',...
            'GroupID', G.nodes.dim,'Hexahedra', G.cells.nnel(idx(1)));
    fprintf(msh,'End Coordinates\n');
    fprintf(msh,'Elements\n');
    for i=1:numel(idx)
        e = idx(i);
        fprintf(msh,'%d ',e);
        for j=1:G.cells.nnel(e)
            fprintf(msh,'%d ',G.cells.lnods(e,j));
        end
        fprintf(msh,'%d\n',G.cells.mat(e));
    end
    fprintf(msh,'End Elements\n');      % end partial mesh in grid file
end



% ---------------------------------------------------------------------------
%
function writeMeshLin3D(msh,G,idx)
% Write Linear segment 3D Element connectivites and define the Gauss points.
    fprintf(msh,'MESH "%s" dimension %d ElemType %s Nnode %d\n Coordinates\n',...
            'GroupID', G.nodes.dim,'Linear', G.cells.nnel(idx(1)));
    fprintf(msh,'End Coordinates\n\n');
    fprintf(msh,'Elements\n');
    for i=1:numel(idx)
        e = idx(i);
        fprintf(msh,'%d ',e);
        for j=1:G.cells.nnel(e)
            fprintf(msh,'%d ',G.cells.lnods(e,j));
        end
        fprintf(msh,'%d\n',G.cells.mat(e));
    end
    fprintf(msh,'End Elements\n');      % end partial mesh in grid file
end
