function gmshMeshWriter(gmsh_filename, g, varargin)
%Write gmsh mesh format (old format)
%
% SYNOPSIS
%       iotools.gmshMeshWriter(filename,G)
%       iotools.gmshMeshWriter(filename,G, 'pn1', pv1, ...)
%
% PARAMETERS
%   G        - Grid structure mostly as detailed in grid_structure (see >> help
%              gridprocessing.grid_structure).
%
%   filename - Name of vtk file
%
%   'pn'/pv  - List of property names/property values.  OPTIONAL.
%               The supported options are
%               - verbose   -- Whether or not to display informational
%                            messages throughout the computational process.
%                            Logical.  Default value: Verbose = false
%                            (don't display any informational messages).
%
%               - bfacestag  -- boundary faces tag, tag in order to id some
%                            problem/boundary conditions.number{int}. 
%                            default value: empty.
%                            format: node-id, nodetag
%
%               - bnodestag  -- boundary nodes tag, tag in order to id some
%                            problem/boundary conditions.number{int}. 
%                            default value: empty.
%                            format: g.faces.num(2)-by-1 array
%
% RETURNS
%   gmsh file in mesh format
%
% EXAMPLE
%   1)>> iotools.gmshMeshWriter('fivespot',G);
%
% SEE ALSO:
%   fopen, fclose, fprintf, gridprocessing.grid_structure
%

    %  check input
    assert(ischar(gmsh_filename), 'First argument must be a filename.');
    assert(isstruct(g),...
    'First argument must be a struct variable see help gridprocessing.grid_structure.');
    assert(mod(numel(varargin), 2) == 0,...
    'Last arguments must be a list of property names/property values')

    opt = struct('verbose' , false(1), 'bfacestag',  [], 'bnodestag', []);
    opt = merge_options(opt, varargin{:});
    
        
    
    gmsh = fopen ( gmsh_filename, 'wt' );
    if ( gmsh < 0 ) 
        fprintf ( 1, '\n' );
        fprintf ( 1, 'GMSH_MESH2D_WRITE - Error!\n' );
        fprintf ( 1, '  Could not open the output file.\n' );
        error ( 'GMSH_MESH2D_WRITE - Error!' );
    end
    
    dim = g.nodes.dim;
    
    %%%
    %  Write the data.
    %%%
    if opt.verbose, disp('* writting header'); end
    fprintf ( gmsh, '$MeshFormat\n' );
    fprintf ( gmsh, '2.2 0 8\n' );
    fprintf ( gmsh, '$EndMeshFormat\n' );

    if opt.verbose, disp('* writting nodes block'); end
    fprintf ( gmsh, '$Nodes\n' );
    fprintf ( gmsh, '%d\n', g.nodes.num );
    if dim<3
        for n = 1 : g.nodes.num
            fprintf ( gmsh, '%d %.17g %.17g 0\n', n, g.nodes.coords(n,:));
        end
    else
        for n = 1 : g.nodes.num
            fprintf ( gmsh, '%d %.17g %.17g %.17g\n', n, g.nodes.coords(n,:));
        end
    end
    fprintf ( gmsh, '$EndNodes\n' );
    
    nnodes  = 0; 
    if ~isempty(opt.bnodestag)
        assert(g.nodes.num <= size(opt.bnodestag,1),'"bnodestag" size incompatible');
        nnodes = size(opt.bnodestag,1);
    end
    nbfaces = 0;
    if ~isempty(opt.bfacestag)
        assert(g.faces.num(2) <= numel(opt.bfacestag),'"bfacestag" size incompatible');
        nbfaces = numel(opt.bfacestag);
    end
    
    if opt.verbose, disp('* writting elements block'); end
    fprintf ( gmsh, '$Elements\n' );
    fprintf ( gmsh, '  %d\n', g.cells.num+nbfaces+nnodes);
    if nnodes>0
        for vertex = 1 : nnodes
            fprintf ( gmsh, '%d 15 2 %d %d %d\n', vertex, ...
                opt.bnodestag(vertex,end),opt.bnodestag(vertex,1), opt.bnodestag(vertex,1));
        end
    end
    count = nnodes;

    if nbfaces>0
        bfaces = zeros(g.faces.num(2),1);
        [xmin, xmax, ymin, ymax] = deal(min(g.nodes.coords(:,1)),...
            max(g.nodes.coords(:,1)),min(g.nodes.coords(:,2)),...
            max(g.nodes.coords(:,2)));
        bfacesonside = find(g.faces.centroids(:,2) <= ymin+1e-6); 
        buf = numel(bfacesonside);
        A = [g.faces.centroids(bfacesonside,1), bfacesonside];
        B = sortrows(A);
        bfaces(1:buf) = B(:,end);
        bfacesonside = find(g.faces.centroids(:,1) >= xmax-1e-6); 
        buf2 = numel(bfacesonside);
        A = [g.faces.centroids(bfacesonside,2), bfacesonside];
        B = sortrows(A);
        bfaces(1+buf:buf2+buf) = B(:,end); buf = buf + buf2;
        bfacesonside = find(g.faces.centroids(:,2) >= ymax-1e-6);
        buf2 = numel(bfacesonside);
        A = [g.faces.centroids(bfacesonside,1), bfacesonside];
        B = sortrows(A,'descend');
        bfaces(1+buf:buf2+buf) = B(:,end); buf = buf + buf2;
        bfacesonside = find(g.faces.centroids(:,1) <= xmin+1e-6);
        buf2 = numel(bfacesonside);
        A = [g.faces.centroids(bfacesonside,2), bfacesonside];
        B = sortrows(A,'descend');
        bfaces(1+buf:buf2+buf) = B(:,end); buf = buf + buf2;
        if dim >2
            [zmin, zmax] = deal(min(g.nodes.coords(:,3)),...
                                max(g.nodes.coords(:,3)));
            bfacesonside = find(g.faces.centroids(:,3) <= zmin+1e-6);
            buf2 = numel(bfacesonside);
            A = [g.faces.centroids(bfacesonside,2), bfacesonside];
            B = sortrows(A,'descend');
            bfaces(1+buf:buf2+buf) = B(:,end); buf = buf + buf2;
            bfacesonside = find(g.faces.centroids(:,3) >= zmax-1e-6);
            buf2 = numel(bfacesonside);
            A = [g.faces.centroids(bfacesonside,2), bfacesonside];
            B = sortrows(A);
            bfaces(1+buf:buf2+buf) = B(:,end); buf = buf + buf2;
        end
        ecoder = face_decoder(g.faces.nodes(bfaces,:), dim);
        for f=1:g.faces.num(2)
            buf = bfaces(f)-g.faces.num(1);
            fprintf ( gmsh, '%d %d 2 %d 1', f+count, ecoder(f),...
                opt.bfacestag(buf));
            fprintf ( gmsh, ' %d', g.faces.nodes(bfaces(f),:));
            fprintf ( gmsh, '\n' );
        end
    end
    count = count + nbfaces;
    
    ecoder = elem_decoder(g.cells.type, dim);
    for e = 1 : g.cells.num
        fprintf ( gmsh, '%d %d 2 %d %d', count+e, ecoder(e), ...
            g.cells.mat(e), g.cells.mat(e));
        for vertex = 1 : g.cells.nnel(e)
            fprintf ( gmsh, ' %d', g.cells.lnods(e,vertex) );
        end
        fprintf ( gmsh, '\n' );
    end
    fprintf ( gmsh, '$EndElements\n' );
    fclose ( gmsh );
    return
end



% helper functions
function ecoder = face_decoder(face_nodes, dim)
    [faces,nnodes] = size(face_nodes);
    if dim==2
        ecoder = ones(faces,1);   
    else
        ecoder = 2*ones(faces,1);
        if nnodes == 4
            quads = face_nodes(:,end)>0;
            ecoder(quads) = 3;
        end
    end
    assert(~any(isnan(ecoder)),'Not supported element in grid');
end


function ecoder = elem_decoder(elem_t, dim)
    
    if dim==2
        ecoder = [2,nan(1,3),3,nan(1,2),1,nan(1,3),9];
        ecoder = ecoder(elem_t);        
    else
        ecoder = [4,nan,5,nan(1,4),1];
        ecoder = ecoder(elem_t);   
    end
    assert(~any(isnan(ecoder)),'Not supported element in grid');
end
