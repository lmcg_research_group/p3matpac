function printBLKO( fid, G, blko)
%write initial values block of petroleum problem
%
% SYNOPSIS:
%   iotools.printBLKO(fout, G, blko);
%
% PARAMETERS:
%   fout        -   Integer file identifier;
%   G           -   Grid structure mostly as detailed in grid_structure.
%   blko        -   A n-by-p or m-by-p array of initial values of 
%                   unknowns of petroleum problem;
%
% RETURNS:
%
% EXAMPLE:
%   iotools.printBLKO(fout,  G, blko);
%
% SEE ALSO:
%   fprintf, fopen, fclose, gridprocessing.grid_structure
%

% check input arguments
if sscanf(version('-release'),'%u%*s')<=2013
    if and(nargin<2,nargin>3)
        error('please usage: iotools.printBLKO(<file_name>, <grid_structure>, <blko>)');
    end
else
    narginchk(2, 3);
end

assert(fid > 0,...
    'Cannot open or no found file for writing.');

assert(isstruct(G),...
    'Second argument must be a struct variable see help gridprocessing.grid_structure.');

nn = G.nodes.num;
if nargin < 3 
    blko = [];
else
    assert( nn == length(blko), ...
        'Third argument error, length(blko) ~= %d',nn);
    [nn, nblko] = size(blko);
end

progress = textprogressbar(nn*nblko,'showremtime', true, ...
						 'startmsg','writting blko block   ');
fmt = '%5i'; 
if or(G.nodes.num>99999,G.cells.num>99999)
    fmt = '%10i';
end
if isempty(blko)
    fprintf(fid,[fmt,'  !Insert initial values of unknowns (others physics) using format F10.6 \n'],1);
    fprintf(fid,[fmt,'  !Insert initial values of unknowns (others physics) using format F10.6 \n'],G.nodes.num);
	progress(nblko);
else
    for j=1:nblko
        k=1;
        pt(k) = 1; %#ok<*NASGU>
        fprintf(fid,[fmt,'%15.8e\n'],1, blko(1,j));
        for i=2:nn-1
            if all(blko(i,j) == blko(i-1,j))
                continue;
            end
            k = k + 1;
            pt(k) = i;
            if (pt(k)-pt(k-1))==1   % node on boundary between two materials!!!!!
                fprintf(fid,[fmt,'%15.8e\n'],i, blko(i,j));
            else
                fprintf(fid,[fmt,'%15.8e\n'],i-1, blko(i-1,j));
                fprintf(fid,[fmt,'%15.8e\n'],i, blko(i,j));
            end
            progress(i)
        end
        fprintf(fid,[fmt,'%15.8e\n'],nn, blko(nn,j));
        progress(nn);
    end
end


end


