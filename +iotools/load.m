function [g,i] = load(fout)
%Load mesh data, *_grid and *_prop files.
%
% SYNOPSIS
%  g= iotools.load(fout);
%  [g, i]= iotools.load(fout);
%
% PARAMETERS
%	fout - Name of programa, i.e the name of prefix of *_grid.mat *_prop.mat
%
% RETURNS
%	G    - Grid structure mostly as detailed in gridprocessing.grid_structure.
%	I    - initval structure mostly as detailed in initcond.initval_structure.
%
% USAGE
%	1)>> g = iotools.load( 'falha');
%	1)>> [g,i] = iotools.load( 'falha');
%
% SEE ALSO
%   gridprocessing.grid_structure
%   initcond.initval_structure
%

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%}

fprintf('-> Into %s\n',mfilename)

% check input arguments
if (nargin>1)
    error('please usage: iotools.load(<file_name>)');
end

fname = strcat(fout,'_grid');
g = load(fname);

i = [];
fname = strcat(fout,'_prop');
if and(exist(fname)==2, nargout==2)
    i = load(fname);
end
