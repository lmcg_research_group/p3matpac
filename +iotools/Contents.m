% +IOTOOLS
%
% Files
%   GetFile       - This function get information about file name and path locations of file selected
%   printBLKO     - write initial values block of petroleum problem
%   printCHE      - write block of initial condition of reative transport problem
%   printPoroK    - write  Porosity block of problem
%   printTHM      - write block of initial condition of thermo-hydro-mechanical problem
%   vtkWriter     - Write vtkfile format of geometry
%   writeBlko     - Write *blko.dat file
%   writeGrid     - Write *gri.dat file
%   gidWriter    - Write GiD mesh file format (*.msh)
%   plotCellData - Plot exterior grid cells, coloured by given data, to current axes.
%   plotData     - Plot selection of coloured grid faces to current axes (reversed Z axis).
%   plotGrid     - This function plot grid.
%   writeGen     - Write *gen.dat file
