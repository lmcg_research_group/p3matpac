function save(fout, G, I)
%Write mesh data: grid and properties.
%
% SYNOPSIS
%  iotools.save(fout,G,I);
%  iotools.save(fout,G);
%
% PARAMETERS
%	fout - Name of programa, i.e the name of prefix of *_gri.dat *_gen.dat, the
%          string will be printted into root.dat.
%	G    - Grid structure mostly as detailed in gridprocessing.grid_structure.
%	I    - initval structure mostly as detailed in initcond.initval_structure.
%
% RETURNS
%	ascii files format with geometry data.
%
% USAGE
%	1)>> iotools.save( 'falha', G);
%
% SEE ALSO
%   gridprocessing.grid_structure
%   initcond.initval_structure
%

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%}

fprintf('-> Into %s\n',mfilename)

% check input arguments
if sscanf(version('-release'),'%u%*s')<=2013
    if and(nargin<2,nargin>3)
        error('please usage: iotools.save(<fid>,<G>,<I>)');
    end
else
    narginchk(2, 3);
end

assert(ischar(fout),...
    'First argument must be string type.');

assert(isstruct(G),...
    'Second argument must be a struct variable see help gridprocessing.grid_structure.');
    
if nargin == 3
    assert(isstruct(I),...
    'Second argument must be a struct variable see help initcond.initval_structure.');
end

fname = strcat(fout,'_grid');
save(fname,'-struct','G','-v7.3');

if nargin == 3
    fname = strcat(fout,'_prop');
    save(fname,'-struct','I','-v7.3');
end
end
