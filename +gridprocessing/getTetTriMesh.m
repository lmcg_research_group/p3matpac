function G = getTetTriMesh(str)
%Extract Grid structure from Tetgen/Triangle files (xxx.node and xxx.ele).
%
% SYNOPSIS
%  G = gridprocessing.getTetTriGrid(str);
%
% PARAMETERS
%  str - prefix of Tetgen/Triangle file generated (i.e., cub.1.node,
%       cub.1.ele str = 'cub.1').
%
% RETURNS
%  G - Grid structure mostly as detailed in grid_structure.
%
% EXAMPLE
%   >> G = gridprocessing.getTetTriMesh('apparatus.1');
%
% SEE ALSO
%   tetrahedralGrid, cartGrid
%
% reference:
% http://wias-berlin.de/software/tetgen/1.5/doc/manual/index.html
% https://www.cs.cmu.edu/~quake/triangle.html
%



disp(['-> ',mfilename])
%% check input
assert(ischar(str),'**ERROR: input args must be a string variable type')

%% process

% check str.node exist
nodes = strcat(str,'.node');
[stat,~]=fileattrib(nodes);
if stat == 0
    msg = sprintf('No such file Found [%s].',nodes);
    error('getTetTriMesh:noFileFound %s',msg);
end

% check str.ele exist
elem = strcat(str,'.ele');
[stat,~]=fileattrib(elem);
if stat == 0
    msg = sprintf('No such file Found [%s].',elem);
    error('getTetTriMesh:noFileFound %s',msg);
end


% open .node file to read
fid = fopen(nodes);
disp('* reading .node file...')

% check parameter line (1st line)
% <# of vertices> <dimension (2 or 3)> <# of attributes> <# of boundary markers (0 or 1)>
line = fgetl(fid);
param = sscanf(line,'%d');
if (length(param)~=4)
    error(['**ERROR: parameters in .node file incomplete numel(param)='....
        num2str(length(param))])
end

% split parameters read
nn = param(1); dim = param(2); natt = param(3); wbc = param(4);
fmt = repmat('%f ',1,1+dim+natt+wbc);

% reading .node file
% <vertex #> <xyz[d]> [attributes] [boundary marker]
% d is dimension (2d or 3d)
coords = zeros(nn,3+dim);
xyz = textscan(fid,fmt,nn);
% assign the  <vertex #> <xyz[d]>
for i=1:1+dim
    coords(:,i) = xyz{i};
end

% [attributes] [boundary marker] assign bcflux(positive) or
% bcmech(negative) numbers
if (natt+wbc>0)
    for i=2+dim:1+dim+natt+wbc
        vec = xyz{i};
        coords(vec<0,end-1)=abs(vec(vec<0));
        coords(vec>0,end)=abs(vec(vec>0));
    end
end
fclose(fid);


% reading .ele file
disp('* reading .ele file ...')
fid = fopen(elem);

% check parameter line (1st line)
%  <# of elements> <nodes per element> <# of attributes>
line = fgetl(fid);
param = sscanf(line,'%d');
if (length(param)~=3)
    error(['**ERROR: parameters in .node file incomplete numel(param)='....
        num2str(length(param))])
end

% split parameters read
ne = param(1); order = param(2); wbc = param(3);
fmt = repmat('%f ',1,1+order+wbc);

xyz = textscan(fid,fmt,ne);
lnods = ones(ne,3+order);

% assign the  <element id>
lnods(:,1) = xyz{1};
% connected element nodes
for i=1:order
    lnods(:,3+i) = xyz{i+1};
end
% group ID
if (wbc)
    lnods(:,2) = xyz{end};
end

G = gridprocessing.getGridInfo(coords,lnods);
G.type = [G.type, { mfilename }];
disp('Done!')
end
