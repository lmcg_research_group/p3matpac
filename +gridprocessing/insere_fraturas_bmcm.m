%% Subrotina para geracao de fraturas aleatorias
%
% criado por MACIEL, B. M. C. M., UFPE, Brasil. 

for ii = 1:sizeimat % Permite mais de um material
    
    GL.matfrag = mat==imat_joint_fragm(1,ii);
    GL.num = max(size(mat));
    GL.cellsnum = 1:GL.num;
    GL.elefrag = GL.cellsnum(GL.matfrag);
    
    %% montando GL.lnods:
    i=1;
    for e =1:GL.num
        if GL.matfrag(e) == 1
            GL.lnods(i,:) = lnods(e,:);
            i=i+1;
        end
    end
    
    %% Dimensoes do problema:
    [GL.nn GL.ndim]=size(coords);
    [GL.nel GL.npe]=size(GL.lnods);
    
    %% criar rand aqui
    if GLiopt==1
    A = randperm((GL.nel),GLQfrat); 
    B = randperm((GL.nel),GLQfrat); 
    C = [A;B];
    C=C';
    elseif GLiopt==2
    OrArand=GL.nel/2;
    A = randperm((OrArand),GLQfrat); 
    B = randperm((OrArand),GLQfrat);
    B = B+OrArand;
    C = [A;B];
    C=C';        
    end
    
    %% Coordenadas e condicoes de contorno:
    GL.xg=coords(:,1);
    GL.yg=coords(:,2);
    
    %% Alterar material em linha (malhas 2D)
    
    for ae = 1:GLQfrat      % ajustando iel1 e ile2
        
        iel1=C(ae,1);        % elemeneto inicial
        iel2=C(ae,2);        % elemento final
        
        if GL.ndim==2
            GL.x1=mean(GL.xg(GL.lnods(iel1,1:GL.npe)));
            GL.y1=mean(GL.yg(GL.lnods(iel1,1:GL.npe)));
            GL.x2=mean(GL.xg(GL.lnods(iel2,1:GL.npe)));
            GL.y2=mean(GL.yg(GL.lnods(iel2,1:GL.npe)));
            GL.normal=[GL.y2-GL.y1;-(GL.x2-GL.x1)];
            GL.normal=GL.normal/norm(GL.normal);
            GL.f0=-GL.x1*GL.normal(1)-GL.y1*GL.normal(2);
            for i=1:GL.nel
                GL.npositive=0;
                for j=1:GL.npe
                    GL.f=GL.f0+GL.normal(1)*GL.xg(GL.lnods(i,j))+GL.normal(2)*GL.yg(GL.lnods(i,j));
                    if (GL.f>0.0) GL.npositive=GL.npositive+1; end
                end
                GL.xc=mean(GL.xg(GL.lnods(i,1:GL.npe)));
                GL.yc=mean(GL.yg(GL.lnods(i,1:GL.npe)));
                GL.scalar_prod=1;
                if (GL.npositive>0 & GL.npositive<GL.npe)
                    GL.v1=[GL.xc-GL.x1;GL.yc-GL.y1];
                    GL.v2=[GL.xc-GL.x2;GL.yc-GL.y2];
                    GL.scalar_prod=GL.v1'*GL.v2;
                end
                if GL.scalar_prod<=0
                    GL.elemat = GL.elefrag(i);
                    mat(GL.elemat)=GLnewmat;
                end
            end
        end
    end % end for
    clearvars GL A B C iel1 iel2; % Permite mais de um material (importante)
end 

%%
disp([' '])
disp(['Fraturas inseridas.'])
disp([' '])
