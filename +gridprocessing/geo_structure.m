%Geometry structure used in package
%
% SYNOPSIS:
%   1) % Get geo structure from dxf structure extracted from dxf file format.
%      geo = gridprocessing.acad2geo(gridprocessing.dxf2coord('acad.dxf'));
%
% RETURNS:
%   geo - Geometry structure.  A master structure having the following fields:
%         - nodes -- A numberOfNodes-by-d array of physical nodal coordinates in R^d.
%                    Geometry node `i' is at physical coordinate [geo.nodes(i,:)]
%                    node (vertex) in the geometry.
%
%         - edges --  A numberOfedges-by-2 array of edges in R^d. Geometry edge 'e' has
%                     node1 [geo.edges(1,e)] at physical coordinate geo.nodes(node1,:),
%                     and node2 [geo.edges(2,e)] at physical coordinate geo.nodes(node2,:)
%
%         - surfc --  A numberOfsurfaces-by-numberOfedgesOnSurface cell array listing the
%                     edges.
