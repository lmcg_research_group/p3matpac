%Get coords and lnods block from *gri.dat files (CODE_BRIGHT grid format).
%
% SYNOPSIS
%   1) gridprocessing.GCLv3;
%   2) GCLv3; % make sure that gridprocessing is imported!!!
%
% PARAMETERS
%   none
%
% RETURNS
%   coords file  - Node coordinates. Must be an m-by-(3+problem dimension) matrix, one
%                  row for each node/point.
%   lnods file   - Connectivity list: an n-by-9 matrix for 2D problem or an n-by-11 matrix
%                  for 3D problem or where each row holds node numbers for a elements.
%
% EXAMPLE
%   gridprocessing.GCLv3;
%
%

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%
%  Programmer
%  ==========
%  Jonathan C. Teixeira
%
%  Creation
%  ========
%  Oct. 17, 2010
%
%  Modification
%  ============
%  ....
%  please when some programmer modify this script put you name, date of addiction
%  or modification statement and describe your modification/addiction like this:
%   * Tommy Hilfiger, Fri Oct 26 2012
%      Add --cflag, --cxxflag.  These *append* a specific flag.
%      Getting '--cflags' to quote reliably is a nightmare.
%
%}
import iotools.*

disp('** GCL v3')

%   Buscando o arquivo *gri.dat
[NomeArq,pathname] = GetFile('root.dat');
disp('open root.dat file ...')
file = strcat(pathname,filesep,NomeArq);
root = textread(file,'%s');

file = strcat(pathname,filesep,root{1},'_gen.dat');
fgri = fopen(file);
linha=fgets(fgri);
linha=fgets(fgri);

infor = 5;
formnp = infor+1;
formel = formnp+infor;
fordim = formel+infor;

stnp  = linha(1:formnp);        % numnp
stnel = linha(formnp:formel);   % numel
stdim = linha(formel:fordim);   % ndim

np    = str2double(stnp(1:infor));
nel   = str2double(stnel(1:infor));
ndim  = str2double(stdim(1:infor));

if ndim<1 && ndim>2
    errordlg('Dimensao do problema errado 1<=ndim<=3','Dimension Error');
    error('gridprocessing:GCLv3:dimMismatch','incompatible dimension see *gen.dat');
end

%   Lendo o arquivo *gri.dat e criando e armazenando o coords
disp('reading coords data block ...')

% Janela de dialogo (solicitaçao dos formatos dos cards do arquivo
% *gri.dat
prompt={'Formato do card "numero do no" (xxxxx => 5):',...
        'Formato do card "coordenada do no" (xxxx.xxxxx => 10)',...
        'Formato do card "Condicao de contorno" (xxxxx => 5)'};
name='Formatacao do arquivo *gri.dat';
numlines=1;
defaultanswer={'5','10','5'};
answer=inputdlg(prompt,name,numlines,defaultanswer);
%Formato do card "numero do no"
fornoh=str2num(answer{1});
%Formato do card "coordenada do no"
forcoord=str2num(answer{2}); %#ok<*ST2NM>
%Formato do card "Condicao de contorno"
forcond=str2num(answer{3});

for1=fornoh+1;

for21=for1+forcoord;
for22=for1+2*forcoord;
for23=for1+3*forcoord;

for31=for22+forcond;
for32=for23+forcond;

for41=for31+forcond;
for42=for32+forcond;

file = strcat(pathname,filesep,root{1},'_gri.dat');
arqgri=fopen(file);
if arqgri == -1
    errordlg('Arquivo nao encontrado','File Error');
    error('gridprocessing:GCLv3:griFileError','No such File Found [*gri.dat]');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%% COORDS  %%%%%%%%%%%%%%%%%%%%%%%%%%%%

coords = zeros(np, ndim + 3);
progress = textprogressbar(np,'showremtime', true, ...
						 'startmsg','reading coords block  ');
if ndim == 2
    fgets(arqgri);     % cabecalho do *gri.dat
    for i=1:np
        linha=fgets(arqgri);
        nodes=(linha(1:for1));                        % id noh
        coordx=(linha(for1:for21));                   % coords x
        coordy=(linha(for21:for22));                  % coords y
        mechbcond=(linha(for22:for31));               % mechbond
        fluxbcond=(linha(for31:for41));               % fluxbond

        coords(i,1)=str2double(nodes(1:fornoh));        % id noh
        coords(i,2)=str2double(coordx(1:forcoord));     % coords x
        coords(i,3)=str2double(coordy(1:forcoord));     % coords y
        coords(i,4)=str2double(mechbcond(1:forcond));   % mechbond
        coords(i,5)=str2double(fluxbcond(1:forcond));   % fluxbond

        progress(i);
    end
elseif ndim == 3
    fgets(arqgri);     % cabecalho do *gri.dat
    for i=1:np
        linha=fgets(arqgri);
        nodes=(linha(1:for1));               % id noh
        coordx=(linha(for1:for21));          % coords x
        coordy=(linha(for21:for22));         % coords y
        coordz=(linha(for22:for23));         % coords z
        mechbcond=(linha(for23:for32));      % mechbond
        fluxbcond=(linha(for32:for42));      % fluxbond

        coords(i,1)=str2double(nodes(1:fornoh));        % id noh
        coords(i,2)=str2double(coordx(1:forcoord));     % coords x
        coords(i,3)=str2double(coordy(1:forcoord));     % coords y
        coords(i,4)=str2double(coordz(1:forcoord));     % coords z
        coords(i,5)=str2double(mechbcond(1:forcond));   % mechbond
        coords(i,6)=str2double(fluxbcond(1:forcond));   % fluxbond

        progress(i);
    end
end
fecha=fclose(arqgri);

fornod=strcat('%',num2str(fornoh+2),'i');
forcor=strcat(' %',num2str(forcoord+2),'.6f');

dirout=strcat(pathname,filesep,strcat(root,'.coords'));
fidnod = fopen(dirout{1},'w');
progress = textprogressbar(np,'showremtime', true, ...
						 'startmsg','saving coords block  ');
for i=1:np,                         % coordenadas dos nos
    fprintf(fidnod,fornod,coords(i,1));
    for j=2:(ndim+1)
        fprintf(fidnod,forcor,coords(i,j));
    end
    fprintf(fidnod,' %4i %4i\n',coords(i,ndim+2),coords(i,ndim+3));
	progress(i);
end
fclose(fidnod);



%%%%%%%%%%%%%%%%%%%%%%%%%%%% LNODS  %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Janela de dialogo (solicita��o dos formatos dos cards do arquivo
% *gri.dat
prompt={'Formato do card "numero do elemento" (xxxxx => 5)',...
        'Formato do card "numero do material" (xxxxx => 5)',...
        'Formato do card "numero tipo de elemento" (xxxxx => 5)',...
        'Formato do card "conectividades" (xxxxx => 5)'};
name='Formatacao do arquivo *gri.dat';
numlines=1;
defaultanswer={'5','5','5','5'};
answer=inputdlg(prompt,name,numlines,defaultanswer);
%Formato do card "numero do elemento"
forel=str2num(answer{1});
%Formato do card "numero do material"
formate=str2num(answer{2});
%Formato do card "numero tipo de elemento"
fortypel=str2num(answer{3});
%Formato do card "conectividades"
forconec=str2num(answer{4});

for6=forel+1;
for7=for6+formate;
for8=for7+fortypel;

for91=for8+forconec;
for92=for8+2*forconec;
for93=for8+3*forconec;
for94=for8+4*forconec;
for95=for8+5*forconec;
for96=for8+6*forconec;
for97=for8+7*forconec;
for98=for8+8*forconec;
for99=for8+9*forconec;

file = strcat(pathname,filesep,root{1},'_gri.dat');
arqgri=fopen(file);
if arqgri == -1
    errordlg('Arquivo nao encontrado','File Error');
    error('gridprocessing:GCLv3:griFileError','No such File Found [*gri.dat]');
end

for i=1:np+2
    linha=fgets(arqgri);
end


progress = textprogressbar(nel,'showremtime', true, ...
						 'startmsg','reading lnods block  ');


i=1;
if ndim==2                             % problema bidimensional
    lnods = zeros(nel,9);
    for i=1:nel
        if str2num(linha(for7:for8-1))==1    % tipo elem. triangulo linear
            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2
            conec3=(linha(for92:for93));                % lnods 3

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));              % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            lnods(i,6)=str2double(conec3(1:forconec));            % lnods 3

        elseif str2num(linha(for7:for8-1))==5 || str2num(linha(for7:for8-1))==2 ...
                || str2num(linha(for7:for8-1))==9 % tipo elem. quadrado linear

            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2
            conec3=(linha(for92:for93));                % lnods 3
            conec4=(linha(for93:for94));                % lnods 4

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));             % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            lnods(i,6)=str2double(conec3(1:forconec));            % lnods 3
            lnods(i,7)=str2double(conec4(1:forconec));            % lnods 4

        elseif str2num(linha(for7:for8))== 8    % tipo elem. linear

            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));             % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2

        elseif str2num(linha(for7:for8))==12    % tipo elem. triangulo quadratico

            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2
            conec3=(linha(for92:for93));                % lnods 3
            conec4=(linha(for93:for94));                % lnods 4
            conec5=(linha(for94:for95));                % lnods 5
            conec6=(linha(for95:for96));                % lnods 6

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));              % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            lnods(i,6)=str2double(conec3(1:forconec));            % lnods 3
            lnods(i,7)=str2double(conec4(1:forconec));            % lnods 4
            lnods(i,8)=str2double(conec5(1:forconec));            % lnods 5
            lnods(i,9)=str2double(conec6(1:forconec));            % lnods 6


        elseif str2num(linha(for7:for8))==101    % tipo elem. triangulo - compatibilidade

            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2
            conec3=(linha(for92:for93));                % lnods 3
            conec4=(linha(for93:for94));                % lnods 4

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));             % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            lnods(i,6)=str2double(conec3(1:forconec));            % lnods 3
            lnods(i,7)=str2double(conec4(1:forconec));            % lnods 4
        end
        progress(i);
        linha=fgets(arqgri);
    end

elseif ndim==3                    % problema 3D
    lnods = zeros(nel,11);
    for i=1:nel
        if str2num(linha(for7:for8-1))==1    % tipo elem. tretraedro linear
            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2
            conec3=(linha(for92:for93));                % lnods 3
            conec4=(linha(for93:for94));                % lnods 4

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));              % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            lnods(i,6)=str2double(conec3(1:forconec));            % lnods 3
            lnods(i,7)=str2double(conec4(1:forconec));            % lnods 4

        elseif str2num(linha(for7:for8-1))==26    % tipo elem. primahtico triangular

            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2
            conec3=(linha(for92:for93));                % lnods 3
            conec4=(linha(for93:for94));                % lnods 4
            conec5=(linha(for94:for95));                % lnods 5
            conec6=(linha(for95:for96));                % lnods 6

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));              % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            lnods(i,6)=str2double(conec3(1:forconec));            % lnods 3
            lnods(i,7)=str2double(conec4(1:forconec));            % lnods 4
            lnods(i,8)=str2double(conec5(1:forconec));            % lnods 5
            lnods(i,9)=str2double(conec6(1:forconec));            % lnods 6

        elseif str2num(linha(for7:for8-1))==3    % tipo elem. primahtico quadrilateral (cubo)

            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2
            conec3=(linha(for92:for93));                % lnods 3
            conec4=(linha(for93:for94));                % lnods 4
            conec5=(linha(for94:for95));                % lnods 5
            conec6=(linha(for95:for96));                % lnods 6
            conec7=(linha(for96:for97));                % lnods 7
            conec8=(linha(for97:for98));                % lnods 8

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));              % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            lnods(i,6)=str2double(conec3(1:forconec));            % lnods 3
            lnods(i,7)=str2double(conec4(1:forconec));            % lnods 4
            lnods(i,8)=str2double(conec5(1:forconec));            % lnods 5
            lnods(i,9)=str2double(conec6(1:forconec));            % lnods 6
            lnods(i,10)=str2double(conec7(1:forconec));            % lnods 7
            lnods(i,11)=str2double(conec8(1:forconec));            % lnods 8

        elseif str2num(linha(for7:for8))==8    % tipo elem. linear
            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));              % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            
        elseif str2num(linha(for7:for8-1))==101    % tipo elem. tretraedro linear - compatibilidade
            elem=(linha(1:for6));                       % id nel
            material=(linha(for6:for7));                % material
            eltyp=(linha(for7:for8));                   % tipo elem.
            conec1=(linha(for8:for91));                 % lnods 1
            conec2=(linha(for91:for92));                % lnods 2
            conec3=(linha(for92:for93));                % lnods 3
            conec4=(linha(for93:for94));                % lnods 4
            conec5=(linha(for94:for95));                % lnods 5

            lnods(i,1)=str2double(elem(1:forel));                 % id nel
            lnods(i,2)=str2double(material(1:formate));           % material
            lnods(i,3)=str2double(eltyp(1:fortypel));              % tipo elem.
            lnods(i,4)=str2double(conec1(1:forconec));            % lnods 1
            lnods(i,5)=str2double(conec2(1:forconec));            % lnods 2
            lnods(i,6)=str2double(conec3(1:forconec));            % lnods 3
            lnods(i,7)=str2double(conec4(1:forconec));            % lnods 4
            lnods(i,8)=str2double(conec5(1:forconec));            % lnods 5
        end
        linha=fgets(arqgri);        
        progress(i);
    end
end
fecha=fclose(arqgri);

[nel, junk2]  = size(lnods);
forele=strcat('%',num2str(forel+2),'i');
forma=strcat(' %',num2str(formate+2),'i');
fortipo=strcat('%',num2str(fortypel+2),'i');
forlink=strcat('%',num2str(forconec+2),'i');

dirout2=strcat(pathname,filesep,strcat(root,'.lnods'));
progress = textprogressbar(nel,'showremtime', true, ...
						 'startmsg','saving lnods block   ');
fidlod = fopen(dirout2{1},'w');
for i=1:nel,                        % conectividades dos nos e materiais
    fprintf(fidlod,forele,lnods(i,1));
    fprintf(fidlod,forma,lnods(i,2));
    fprintf(fidlod,fortipo,lnods(i,3));
    for j=4:junk2
        fprintf(fidlod,forlink,lnods(i,j));
    end
    fprintf(fidlod,'\n');
	progress(i);
end
fclose(fidlod);

clearvars -except pathname root

% fim -> CCL

