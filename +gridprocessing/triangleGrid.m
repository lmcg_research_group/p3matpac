function G = triangleGrid (celldim, varargin)
%Construct 2d structured grid based in trangles in physical space.
%
% SYNOPSIS
%  G = gridprocessing.triangleGrid(celldim);
%  G = gridprocessing.triangleGrid(celldim, meshtype);
%  G = gridprocessing.triangleGrid(celldim, physdim);
%  G = gridprocessing.triangleGrid(celldim, physdim, meshtype);
%  G = triangleGrid(celldim);
%  G = triangleGrid(celldim, meshtype);
%  G = triangleGrid(celldim, physdim);
%  G = triangleGrid(celldim, physdim, meshtype);
%
% PARAMETERS
%	celldim   - Vector, length 2 , specifying number of cells (elements) in each
%               coordinate direction.
%	physdim   - Vector, length 2, of physical size in units of meters of the computational
%               domain.
%               OPTIONAL.  Default value == celldim
%               (i.e., each cell has physical dimension 1-by-1-by-1 m).
%	meshtype  - Orientation of mesh grid, string type variable ('right' or 'r' or 'R',
%               'left' or 'l' or 'L','alternated' or 'a' or 'A', 'crossed' or 'c' or 'C')
%               OPTIONAL.  Defaul value == 'right'
%
% RETURNS
%	G - Grid structure mostly as detailed in grid_structure.
%
% EXAMPLE
%	1)>> import gridprocessing.*
%     >> % Make a 23-by-12 grid on the domain of 100x60 m
%     nx = 23; ny = 12;
%     G = triangleGrid([nx, ny], [100, 60]);
%   2)>> % Make a 16-by-16 grid on the unit square with left oriented mesh
%	  >> nx = 16; ny = nx;
%	  >> G = gridprocessing.triangleGrid([nx, ny],'left');
%
% SEE ALSO
%   cartGrid, tetrahedralGrid
%

%% Statemants

switch (nargin)
    case(1)
        if (numel(celldim)<3)
            physdim = celldim; meshtype = 'r';
        else
            physdim = 0; meshtype = 'd';
        end
    case (2)
        if isnumeric(varargin{1})
            physdim  = varargin{1};         % physical space
            if (numel(physdim)<3)
                meshtype = 'r';
            else
                meshtype = 'a';
            end
        else
            meshtype = lower(varargin{1});  % meshtype
            physdim = celldim;
        end
    case(3)
        physdim  = varargin{1};             % physical space
        meshtype = lower(varargin{2});      % meshtype
    otherwise
        error('gridprocessing:triangleGrid input execeed 4 variable see help gridprocessing.triangleGrid');
end


switch ( meshtype )
    case {'right','r'}
        [coords, lnods] = meshRight(celldim,physdim);
    case {'left','l'}
        [coords, lnods] = meshLeft(celldim,physdim);
    case {'alternated','a'}
        [coords, lnods] = meshDelaunay(celldim,physdim);
    case {'delaunay','d'}
        [coords, lnods] = meshPointDelaunay(celldim);
    case {'crossed','c'}
        [coords, lnods] = meshCrossed(celldim,physdim);
    otherwise
        error('gridprocessing:triangleGrid meshtype only can be "right", "left" or "alternated"');
end
G = gridprocessing.getGridInfo(coords,lnods);

% Record grid constructur in grid.
G.type = [G.type, { mfilename }];
end

%-----------------------------------------------------------------------------------------
% Internal function

%% Right diagonal
function [coords, lnods] = meshRight(celldim, physdim)

    nn = (celldim(1)+1)*(celldim(2)+1);             % number of nodes
    nel= celldim(1)*celldim(2)*2;                   % number of elements

    coords = zeros(nn,5);
    lnods  = zeros(nel,3+6);
	
    progress = textprogressbar(nel+1,'showremtime', true, ...
                             'startmsg','building mesh         ');

    %-----------------------------------------------------------------------
    %% Nodes/Coordinates ----------------------------------------------------
    x = linspace(0, physdim(1), celldim(1)+1);
    y = linspace(0, physdim(2), celldim(2)+1);
    [xCoord, yCoord] = ndgrid(x, y);
    coords(:,1) = (1:nn)'; coords(:,2) = xCoord(:); coords(:,3) = yCoord(:);
	progress(i);

    %%-----------------------------------------------------------------------

    %-----------------------------------------------------------------------
    %% Cells/Connectivities -------------------------------------------------
    iel=0;
    for y = 1: celldim(2)
        for x = 1:celldim(1)
            iel = iel+1;
            lnods(iel,1) = iel;
            lnods(iel,2) = 1;
            lnods(iel,3) = 1;
            lnods(iel,4) = x + ( celldim(1) + 1 ) * ( y - 1 );
            lnods(iel,5) = x + ( celldim(1) + 1 ) * y + 1;
            lnods(iel,6) = x + ( celldim(1) + 1 ) * y;

            iel = iel+1;
            lnods(iel,1) = iel;
            lnods(iel,2) = 1;
            lnods(iel,3) = 1;
            lnods(iel,4) = x + ( celldim(1) + 1 ) * ( y - 1 );
            lnods(iel,5) = x + ( celldim(1) + 1 ) * ( y - 1 ) + 1;
            lnods(iel,6) = x + ( celldim(1) + 1 ) * y + 1;
			
			progress(1+iel);
        end
    end
    %%-----------------------------------------------------------------------

    % just for debug
%      triplot(lnods(:,4:6),coords(:,2),coords(:,3));
end

%% Left diagonal
function [coords, lnods] = meshLeft(celldim, physdim)
    nn = (celldim(1)+1)*(celldim(2)+1);             % number of nodes
    nel= celldim(1)*celldim(2)*2;                   % nomber of elements

    coords = zeros(nn,5);
    lnods  = zeros(nel,3+6);
    progress = textprogressbar(nel+1,'showremtime', true, ...
                             'startmsg','building mesh         ');

    %-----------------------------------------------------------------------
    %% Nodes/Coordinates ----------------------------------------------------
    x = linspace(0, physdim(1), celldim(1)+1);
    y = linspace(0, physdim(2), celldim(2)+1);
    [xCoord, yCoord] = ndgrid(x, y);
    coords(:,1) = (1:nn)'; coords(:,2) = xCoord(:); coords(:,3) = yCoord(:);
	progress(1);

    %-----------------------------------------------------------------------
    %% Cells/Connectivities -------------------------------------------------

    iel=0;
    for y = 1: celldim(2)
        for x = 1:celldim(1)
            iel = iel+1;
            lnods(iel,1) = iel;
            lnods(iel,2) = 1;
            lnods(iel,3) = 1;
            lnods(iel,4) = x + ( celldim(1) + 1 ) * ( y - 1 );
            lnods(iel,5) = x + ( celldim(1) + 1 ) * ( y - 1 ) + 1;
            lnods(iel,6) = x + ( celldim(1) + 1 ) * y;

            iel = iel+1;
            lnods(iel,1) = iel;
            lnods(iel,2) = 1;
            lnods(iel,3) = 1;
            lnods(iel,4) = x + ( celldim(1) + 1 ) * ( y - 1 ) + 1;
            lnods(iel,5) = x + ( celldim(1) + 1 ) * y + 1;
            lnods(iel,6) = x + ( celldim(1) + 1 ) * y;
			
			progress(1+iel);
        end
    end

    % just for debug
%     triplot(lnods(:,4:6),coords(:,2),coords(:,3));
end

%% alternated diagonal
function [coords, lnods] = meshDelaunay(celldim, physdim)

    progress = textprogressbar(2,'showremtime', true, ...
                             'startmsg','building mesh         ');
	
    if (numel(celldim)>3)
        [nn,ndim] = size(celldim);
        assert(ndim==2,'gridprocessing.triangleGrid node dimension must be 2')
        xCoord = celldim(:,1); yCoord = celldim(:,2);
    else
        nn = (celldim(1)+1)*(celldim(2)+1);             % number of nodes
        nel= celldim(1)*celldim(2)*2;                   % number of elements
        x = linspace(0, physdim(1), celldim(1)+1);
        y = linspace(0, physdim(2), celldim(2)+1);
        % Nodes/Coordinates ----------------------------------------------------
        [xCoord, yCoord] = meshgrid(x, y);
    end
    coords = [(1:nn)',xCoord(:), yCoord(:), zeros(nn,1), zeros(nn,1)];
	progress(1);

    %-----------------------------------------------------------------------
    %% Cells/Connectivities -------------------------------------------------

    if numel(physdim)>3
        [nel,col] = size(physdim);
        if (col == 3)
            lnods = [(1:nel)', ones(nel,1), ones(nel,1), physdim, zeros(nel,1), zeros(nel,1), zeros(nel,1)];
        else
            lnods = [(1:nel)', physdim(:,1), ones(nel,1), physdim(:,2:end), zeros(nel,1), zeros(nel,1), zeros(nel,1)];
        end
    else
        tri = delaunay(xCoord(:), yCoord(:));
        lnods = [(1:nel)', ones(nel,1), ones(nel,1), tri, zeros(nel,1), zeros(nel,1), zeros(nel,1)];
    end
	progress(2);
    % just for debug
%     triplot(tri,xCoord, yCoord);
end


%% Crossed diagonal
function [coords, lnods] = meshCrossed(celldim, physdim)

    %% Create vertices and cells:
    nn = (celldim(1) + 1)*(celldim(2) + 1) + celldim(1)*celldim(2);        % number of nodes
    nel= celldim(1)*celldim(2)*4;                                          % number of elements

    coords = zeros(nn,5);
    lnods  = zeros(nel,3+6);
	
    progress = textprogressbar(nn+nel,'showremtime', true, ...
                             'startmsg','building mesh         ');

    %% Create main vertices:
    vertex = 0; x =zeros(2,1);
    a = 0.0; b = physdim(1); c = 0.0; d = physdim(2);
    for iy = 1:celldim(2)+1
        x(2) = 0.0 + ((iy-1)*(physdim(2) - 0.0)/(celldim(2)));
        for ix = 1:celldim(1)+1
            x(1) = 0.0 + ((ix-1)*(physdim(1) - 0.0)/(celldim(1)));
            vertex = vertex+1;
            coords(vertex,1) = vertex; coords(vertex,2:3) = x;
			progress(vertex);
        end
    end

    %% Create midpoint vertices if the mesh type is crossed
    for iy = 1:celldim(2)
        x(2) = 0.0 +((iy-1) + 0.5)*(physdim(2) - 0.0)/(celldim(2));
        for ix = 1:celldim(1)
            x(1) = 0.0 + ((ix-1) + 0.5)*(physdim(1) - 0.0)/(celldim(1));
            vertex = vertex+1;
            coords(vertex,1) = vertex; coords(vertex,2:3) = x;
			progress(vertex);
        end
    end

    %% Create triangles
    cell = 0;
    for iy = 1:celldim(2)
        for ix = 1:celldim(1)
            v0 = (iy - 1)*(celldim(1) + 1) + (ix - 1)+1;
            v1 = v0 + 1;
            v2 = v0 + (celldim(1) + 1);
            v3 = v1 + (celldim(1) + 1);
            vmid = (celldim(1) + 1)*(celldim(2) + 1) + (iy - 1)*celldim(1) + (ix - 1) + 1;

            % Note that v0 < v1 < v2 < v3 < vmid.
            cell = cell + 1;
            lnods(cell,1) = cell; lnods(cell,2) = 1; lnods(cell,3) = 1;
            lnods(cell,4) = v0; lnods(cell,5) = v1; lnods(cell,6) = vmid;
            cell = cell + 1;
            lnods(cell,1) = cell; lnods(cell,2) = 1; lnods(cell,3) = 1;
            lnods(cell,4) = v0; lnods(cell,5) = vmid; lnods(cell,6) = v2;
            cell = cell + 1;
            lnods(cell,1) = cell; lnods(cell,2) = 1; lnods(cell,3) = 1;
            lnods(cell,4) = v1; lnods(cell,5) = v3; lnods(cell,6) = vmid;
            cell = cell + 1;
            lnods(cell,1) = cell; lnods(cell,2) = 1; lnods(cell,3) = 1;
            lnods(cell,4) = v2; lnods(cell,5) = vmid; lnods(cell,6) = v3;
			
			progress(nn+cell);
        end
    end
end

%% meshing from points
function [coords, lnods] = meshPointDelaunay(p)

    progress = textprogressbar(2,'showremtime', true, ...
                             'startmsg','building mesh         ');
	nn = size(p,1);
    xCoord = p(:,1); yCoord = p(:,2);
    coords = [(1:nn)',xCoord(:), yCoord(:), zeros(nn,1), zeros(nn,1)];
	progress(1);

    %-----------------------------------------------------------------------
    %% Cells/Connectivities -------------------------------------------------
    tri = delaunay(xCoord(:), yCoord(:));
    nel = size(tri,1);
    lnods = [(1:nel)', ones(nel,1), ones(nel,1), tri, zeros(nel,1), zeros(nel,1), zeros(nel,1)];
    
	progress(2);
    % just for debug
%     triplot(tri,xCoord, yCoord);
end