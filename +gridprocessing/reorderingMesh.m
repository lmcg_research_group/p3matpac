function G = reorderingMesh(meshFile)
%Reordering node indices from mesh type file generated into gmsh.
% SYNOPSIS
%  G = gridprocessing.reorderingMesh(meshFile);
%
% PARAMETERS
%     meshFile  - .msh or root.dat file
%
% RETURNS
%	G - Grid structure mostly as detailed in grid_structure.
%
% EXAMPLE
%    1) >> G = gridprocessing.reorderingMesh('fault.msh')
%
%    2) >> G = gridprocessing.reorderingMesh('root.dat')
%
% SEE ALSO
%   getGridInfo, load_gmsh, msh2G
%
%----------------------------------------------------------------------------------------
% NOTE:
%   Code (tag) to identity boundary condition types (hydrodynamical and mechanical) into
%   msh file:
%       - hydrodynamical conditon : ]100,199]
%       - mechanical conditon : ]200,299]
%       - material : ]300,399]
%
%

%% Statemants
%
% clc;
[~, name, ext] = fileparts(meshFile);
disp('-> In reorderingMesh');
if (strcmp(ext,'dat'))
    G = gridprocessing.getGridInfo();
else
    msh = gridprocessing.load_gmsh(meshFile);
    G   = gridprocessing.msh2G(msh);
end

if ((all(G.cells.type==1)) || (all(G.cells.type==12)))
    if (G.nodes.dim == 2)
        Gn = gridprocessing.triangulation_rcm(G);
    else
        Gn = gridprocessing.tetrahedral_rcm(G);
    end
    G = Gn;

else
    nn = G.nodes.num;
    ne = G.cells.num;
    progress = textprogressbar(nn+ne+ne,'showremtime', true, ...
                             'startmsg','sorting nodes         ');
    node = [G.nodes.coords, G.nodes.bcmech, G.nodes.bcflux];
    node = sortrows(sortrows([node, (1:G.nodes.num)']),2);

    idx = zeros(nn,1);
    for i=1:nn
       idx(node(i,end)) = i;
	   progress(i);
    end

    coords = [(1:nn)', node(:,1:end-1)];
    kxx = zeros(G.cells.num,3+size(G.cells.lnods,2));
    kxx(:,1) = (1:G.cells.num)';
    kxx(:,2) = G.cells.mat;
    kxx(:,3) = G.cells.type;

    cellcenter = zeros(G.cells.num, size(G.nodes.coords,2)+1);
    cellcenter(:,end) = 1:G.cells.num;


    for iel=1:G.cells.num
        nnel = G.cells.nnel(iel);
        for inel=1:nnel
            kxx(iel,3+inel) = idx(G.cells.lnods(iel,inel));
            cellcenter(iel,1)=cellcenter(iel,1)+G.nodes.coords(G.cells.lnods(iel,inel),1)/nnel;
            cellcenter(iel,2)=cellcenter(iel,2)+G.nodes.coords(G.cells.lnods(iel,inel),2)/nnel;
        end
		progress(nn+iel);
    end


    cell = sortrows(sortrows(cellcenter),2);
    lnods = zeros(G.cells.num,3+size(G.cells.lnods,2));
    for i=1:G.cells.num
        lnods(i,2:end) = kxx(cell(i,end),2:end);
        lnods(i,1) = i;
		progress(nn+ne+i);
    end



    G = gridprocessing.getGridInfo(coords,lnods);
end

G.type = [G.type, { mfilename }];
end
