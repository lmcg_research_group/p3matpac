function dxf2geo(acad,bname)
%DXF2GEO Write geometric file (*.geo) from geometric data converted by acad2geo.m
%gmsh geometry file format (*.geo).
%
% SYNOPSIS:
%   gridprocessing.dxf2geo(geo,filename)
%
% PARAMETERS:
%   acad     - geo structure (see geo_structure).
%   filename - file name (prefix) of *.geo file to be created.
%
% RETURNS:
%   gmsh geometry file format (*.geo).
%
% EXAMPLE:
%   DBdxf=gridprocessing.dxf2coord('toy.dxf')
%   geo  =gridprocessing.acad2geo(BDdxf);
%   gridprocessing.dxf2geo(geo,'toy');
%
% SEE ALSO:
%   geo_structure, dxf2coord, acad2geo.
%
%
%Write geometric file (*.geo) from geometric data

node=acad.nodes;
edge=acad.edges;
face=acad.surfc;

[np,ndim]=size(node);
[ne,~]=size(edge);

% Sort rows in ascending order
edge = [min(edge,[],2),max(edge,[],2)];

% writting output files
filegeo=strcat(bname,'.geo');
fid=fopen(filegeo,'w');
fprintf(fid,'// File Created by MMGT from .dxf file \n');
fprintf(fid,'// Geometry points \n');
for i=1:np
    fprintf(fid,'Point(%i) = {%f, %f, 0.0, 1.0};\n',i,node(i,1),node(i,2));
end
fprintf(fid,'// Geometry lines \n');
for i=1:ne
    fprintf(fid,'Line(%i) = {%i, %i};\n',i,edge(i,1), edge(i,2));
end

% fprintf(fid,'// Geometry surfaces: MMGT align each surface as differentes materials \n');
% % build face contour to counterclockwise edge ordering
% setOrder='stable';
% for i=1:length(face)
% %
%     fprintf(fid,'Line Loop(%i) = {',i);
%     loopLinepl = face{i}       % loop lines from acad polylines
%     xyLoop = unique(node(edge(loopLinepl,:)',:),'rows',setOrder);   % unique coordinates
%     xyLoop = [xyLoop; xyLoop(1,:)]; %#ok<AGROW>
%     x = xyLoop(:,1); y = xyLoop(:,2);
%
%     if (~ispolycw(xyLoop(:,1),xyLoop(:,2)))
%         [x,y]=poly2ccw(xyLoop(:,1),xyLoop(:,2));  % node in couterclockwise ordering
%     end
%     nn = length(x);
%
% % loop over each edge of the face
%     for inp = 1: nn-2
%         edfactor = 1;
%         n1ed = find( and( x(inp) == node(:,1), y(inp) == node(:,2) ) );
%         n2ed = find( and( x(inp+1) == node(:,1), y(inp+1) == node(:,2) ) );
%
%         [i1, j1] = find(edge == n1ed); [i2, j2] = find(edge == n2ed);
%
%         iface = intersect(i1,i2);
%         if (edge(iface,1) ~= n1ed)
%             edfactor = -1;
%         end
%         fprintf(fid,'%i, ',edfactor*iface);
%     end
%     n1ed = find( and( x(nn-1) == node(:,1), y(nn-1) == node(:,2) ) );
%     n2ed = find( and( x(1) == node(:,1), y(1) == node(:,2) ) );
%
%     [i1, j1] = find(edge == n1ed); [i2, j2] = find(edge == n2ed);
%
%     iface = intersect(i1,i2);
%     if (edge(iface,1) ~= n1ed)
%         edfactor = -1;
%     end
%
%     fprintf(fid,'%i}; \n',edfactor*iface);
%     fprintf(fid,'Plane Surface(%i) = {%i}; \n',i,i);
%     fprintf(fid,'Physical Surface(%i) = {%i};\n',i,i);
% end
fprintf(fid,'// $End \n');
fclose(fid);

end
