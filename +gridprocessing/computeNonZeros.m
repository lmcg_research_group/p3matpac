function nz=computeNonZeros(G)
disp(['-> In ',mfilename()]);
nn   = G.nodes.num;
ndim = G.nodes.dim; 
nel  = G.cells.num;
npe  = G.cells.nnel;

iaux = zeros(nn,1);

k=0;
progress = textprogressbar(nn,'startmsg','computing nonzeros');
for inp=1:nn
	kk=0;
	for iel=1:nel
        for inel=1:npe(iel)
            if (G.cells.lnods(iel,inel)==inp)
                for jnel=1:inel-1
                    kk=kk+1;
                    iaux(kk)=G.cells.lnods(iel,jnel);
                end
                for jnel=inel+1:npe(iel)
                    kk=kk+1;
                    iaux(kk)=G.cells.lnods(iel,jnel);
                end
            end
        end
    end
    kk=kk+1;
    iaux(kk)=inp;

    kkk=1;
    while kkk==1
        kkk=0;
        for i=1:kk-1
            i1=iaux(i);
            i2=iaux(i+1);
            if (i1>i2)
                iaux(i)=i2;
                iaux(i+1)=i1;
                kkk=1;
            end
        end
    end

    k=k+1;
    for i=2:kk
        if (iaux(i)>iaux(i-1))
            k=k+1;
        end
    end

    iaux(1:kk)=0;
    progress(inp)

end

nz=k;
end