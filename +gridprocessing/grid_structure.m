%Grid structure used in packages
%
% SYNOPSIS:
%   1) Construct Cartesian grid.
%        G = triangleGrid(...);
%
%   2) Read grid specification from file.
%        G = gridding.getGridInfo();
%
% RETURNS:
%   G - Grid structure.  A master structure having the following fields:
%         - nodes --  A structure specifying properties for each individual
%                     node (vertex) in the grid.  See NODES below for
%                     details.
%
%         - cells --  A structure specifying properties for each individual
%                     cell in the grid.  See CELLS below for details.
%
%         - type  --  A cell array of strings describing the history of
%                     grid constructor and modifier functions through which
%                     a particular grid structure has been defined
%
%
%  NODES - Node (vertex) structure G.nodes:
%    - num       --  Number of global nodes (vertices) in grid.
%
%    - dim       --  Grid dimension R^d.
% 
%    - coords    --  A G.nodes.num-by-d array of physical nodal
%                    coordinates in R^d. Global node `i' is at physical
%                    coordinate [coords(i,:)]
%
%    - bcmech    --  A G.bcmech.num-by-1 array of index of mechanical
%                    boundary
%
%    - bcflux    --  A G.bcflux.num-by-1 array of index of hydrodynamic
%                    boundary
% 
%    - cells     -- Number of cells shared per node
%
%  CELLS - Cell structure G.cells:
%    - num       --  Number of cells in global grid.
%
%    - nummat    --  Number of materials in global grid.
%
%    - mat       --  A G.cells.num-by-1 array of physical materials index.
%
%    - type      --  Type of cells/elements. Types of elements availables:
%                    
%                    type == 1      Linear triangle: mainly used in flow problems.
%                    
%                    type == 2      Linear quadrilateral with 4 integration points.
%                    
%                    type == 12     Quadratic triangle with 3 integration points. Corner
%                                    nodes: 1,2,3; side nodes: 4,5,6.
%                    
%                    type == 1      Linear tetrahedron.
%                    
%                    type == 3      Linear hexahedron with 8 integration points.
%
%    - lnods     --  A G.cells.lnods.num-by-mnnel array of cell nodes. for 2D problems
%                    mnnel = 6, for 3D problems mnnel = 8.
% 
%    - nnel      -- number of nodes on cells 
%
