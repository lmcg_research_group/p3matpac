function G = evalGrid(x, y, varargin)
%Construct Cartesian grid ( elements are quadrilaterals or hexahedron) with variable physical cell sizes.
%
% SYNOPSIS:
%   G = gridprocessing.evalGrid(x, y)
%   G = gridprocessing.evalGrid(x, y, z)
%
% PARAMETERS:
%   x,y,z    - Vectors giving cell vertices, in units of meters, of individual
%              coordinate directions.  Specifically, the grid cell at
%              logical location (I,J,K) will have a physical dimension of
%              [x(I+1)-x(I), y(J+1)-y(I), z(K+1)-z(I)] (meters).
%
% RETURNS:
%   G - Grid structure mostly as detailed in grid_structure.
%
% EXAMPLE:
%   % Make a 20-by-10-by-2 grid on the unit cube.
%   x = linspace(0, 1, 21);
%   y = linspace(0, 1, 11);
%   z = linspace(0, 1,  3);
%   G = gridprocessing.evalGrid(x, y, z);
%
% SEE ALSO:
%   grid_structure, cartGrid, triangleGrid, tetrahedralGrid
%

   if mod(nargin, 2), %3D
      G = tensorGrid3D(x, y, varargin{:});
   else
      G = tensorGrid2D(x, y, varargin{:});
   end

   % Record grid constructor in grid.
   G.type = [G.type, {mfilename }];
end

function G = tensorGrid3D(x, y, z, varargin)

   %-----------------------------------------------------------------------
   % Check input data -----------------------------------------------------
   x = x(:); dx=diff(x);
   if ~all(dx > 0),
      warning('evalGrid:xData', 'Nonmonotone x-data, truncating..');
      while ~all(dx>0)
         i = find(dx>0); x = x([1; i+1]); dx = diff(x);
      end
   end
   y=y(:); dy = diff(y);
   if ~all(dy > 0),
      warning('evalGrid:yData', 'Nonmonotone y-data, truncating..');
      while ~all(dy>0)
         i = find(dy>0); y = y([1; i+1]); dy = diff(y);
      end
   end
   z=z(:); dz = diff(z);
   if ~all(dz > 0),
      warning('evalGrid:yData', 'Nonmonotone z-data, truncating..');
      while ~all(dz>0)
         i = find(dz>0); z = z([1; i+1]); dz = diff(z);
      end
   end

   sx = numel(x)-1; sy = numel(y)-1; sz = numel(z)-1;

   numC = sx * sy * sz;             % Number of cells.
   numN = (sx+1) * (sy+1) * (sz+1); % Number of nodes.

   %--------------------------------------------------------------------------
   % Nodes/Coordinates -------------------------------------------------------

   [xCoord, yCoord, zCoord] = ndgrid(x, y, z);
   coords = [xCoord(:), yCoord(:), zCoord(:)];

   %-----------------------------------------------------------------------
   % Generate cell nodes -------------------------------------------------
   % Index to first node in each cell
   k  = index([sx+1, sy+1, sz+1], 1:sx, 1:sy, 1:sz);
   di = 1;
   dj = sx+1;
   dk = (sx+1)*(sy+1);
   cNodes = [k, k+di, k+di+dj, k+dj, k+dk, k+di+dk, k+di+dj+dk, k+dj+dk];

   %--------------------------------------------------------------------------
   % Assemble structure -----------------------------------------------------
   idx = 1:numN; nodes = [idx',coords,zeros(numN,2)];
   idx = 1:numC; lnods = [idx',ones(numC,1),3*ones(numC,1),cNodes];

   G = gridprocessing.getGridInfo(nodes,lnods);
end


function G =tensorGrid2D(x, y, varargin)

   %-----------------------------------------------------------------------
   % Check input data -----------------------------------------------------
   x = x(:); dx=diff(x);
   if ~all(dx > 0),
      warning('evalGrid:xData', 'Nonmonotone x-data, truncating..');
      while ~all(dx>0)
         i = find(dx>0); x = x([1; i+1]); dx = diff(x);
      end
   end
   y=y(:); dy = diff(y);
   if ~all(dy > 0),
      warning('evalGrid:yData', 'Nonmonotone y-data, truncating..');
      while ~all(dy>0)
         i = find(dy>0); y = y([1; i+1]); dy = diff(y);
      end
   end

   sx = numel(x)-1; sy = numel(y)-1;

   numC = sx * sy;             % Number of cells.
   numN = (sx+1) * (sy+1);     % Number of nodes.s

   %-----------------------------------------------------------------------
   % Nodes/Coordinates ----------------------------------------------------

   [xCoord, yCoord] = ndgrid(x, y);
   coords = [xCoord(:), yCoord(:)];

   %-----------------------------------------------------------------------
   %% Generate cell nodes -------------------------------------------------
   % Index to first node in each cell
   k  = index([sx+1, sy+1], 1:sx, 1:sy);
   di = 1;
   dj = sx+1;
   cNodes = [k, k+di, k+di+dj, k+dj];

   %--------------------------------------------------------------------------
   % Assemble structure -----------------------------------------------------
   idx = 1:numN; nodes = [idx',coords,zeros(numN,2)];
   idx = 1:numC; lnods = [idx',ones(numC,1),5*ones(numC,1),cNodes];

   G = gridprocessing.getGridInfo(nodes,lnods);

end



% ----------------------------------------------------------------------------------------
% private functions
%

function k = index(sz, varargin)
   assert(numel(sz) == numel(varargin));
   assert(all(cellfun(@min, varargin) > 0));
   assert(all(cellfun(@max, varargin)<= sz));

   r       = cell(size(varargin));
   [r{:}]  = ndgrid(varargin{:});
   k       = reshape(sub2ind(sz, r{:}), [], 1);
end
