function [geo] = acad2geo(DBdxf)
%ACAD2GEO Convert dxf structures (polylines and polylayer) data to geo structure (nodes, edges and surfaces).
% and surfaces see "help gridprocessing.geometry_structure"). This function MUST be
% runned after dxf2cooord.
%
% SYNOPSIS
%   geo   - gridprocessing.acad2geo(BDdxf);
%
% PARAMETERS
%   DBdxf - dxf structure mostly as detailed in dxf_structure.m .
%
% RETURNS
%   geo   - geometry structure mostly as detailed in geo_structure.m
%
% EXAMPLE
%   DBdxf=gridprocessing.dxf2coord('toy.dxf')
%   geo  =gridprocessing.acad2geo(BDdxf);
%
% SEE ALSO
%   geo_structure, dxf_structure, dxf2coord
%

acad    = DBdxf.polylines;
layers  = DBdxf.polylayer;


[np,col]=size(acad);
[nl]=length(layers);

polylines = acad(:,1:3);   % por enquanto somente 2d
nlayer = zeros(nl,1);

nod = acad(:,2:3);

ed =zeros(np-nl,2);
ned=0; nly=0;
idx=acad(1,1); % index of layers
for i=2:np
    if (idx==acad(i,1))
        nly=nly+1;
        ned=ned+1;
        ed(ned,1)=i-1;
        ed(ned,2)=i;
    else
        nlayer(idx)=nly;
        nly=0;
        idx=acad(i,1);
    end
end
nlayer(idx)=nly;
%  checking node with same coordinates
naux = zeros(np,1);
naux2=naux;
for i=1:np
    for j=1:np
        if (i==j)
            continue
        end
        if (abs(nod(i,1)-nod(j,1))<0.00005) && (abs(nod(i,2)-nod(j,2))< ...
                0.00005) && (j>i)
            naux(j)=i;
            if (naux(i)~=0)
                naux(j)=naux(i);
            end
        end
    end
end
%  checking edges with same points
[ned,foo]=size(ed);
eaux = zeros(ned,1);
eaux2 = eaux;
for i=1:ned
    for j=1:ned
        if (i==j)
            continue;
        end
        if ((((abs(nod(ed(i,1),1)-nod(ed(j,1),1)))<0.0005)  && ...
            ((abs(nod(ed(i,1),2)-nod(ed(j,1),2)))<0.0005)  && ...
            ((abs(nod(ed(i,2),1)-nod(ed(j,2),1)))<0.0005)  && ...
            ((abs(nod(ed(i,2),2)-nod(ed(j,2),2)))<0.0005)) || ...
            (((abs(nod(ed(i,1),1)-nod(ed(j,2),1)))<0.0005) && ...
            ((abs(nod(ed(i,1),2)-nod(ed(j,2),2)))<0.0005)  && ...
            ((abs(nod(ed(i,2),1)-nod(ed(j,1),1)))<0.0005)  && ...
            ((abs(nod(ed(i,2),2)-nod(ed(j,1),2)))<0.0005))) && ...
             (j>i)
           eaux(j)=i;
        end
    end
end
% removing repeated
nn=0;
for i=1:np
    if (naux(i)==0)
        nn=nn+1;
        node(nn,:)=nod(i,:);
        naux2(i)=nn;
    end
end

nedge=0;
for i=1:ned
    if (eaux(i)==0)
        nedge=nedge+1;
        eaux2(i)=nedge;
        for j=1:2
            if (naux(ed(i,j))==0)
                edge(nedge,j) = naux2(ed(i,j));
            else
                edge(nedge,j) = naux2(naux(ed(i,j)));
            end
        end
        if (edge(nedge,1)==edge(nedge,2))
            nedge=nedge-1;
            eaux2(i)=NaN;
        end
    end
end

%  build face
iec=0;
for i=1:nl
    for j=1:nlayer(i)
        iec=iec+1;
        if isnan(eaux2(iec))
            continue;
        end
        if (eaux(iec)==0)
            face{i}(j)=eaux2(iec);
        else
            face{i}(j)=eaux2(eaux(iec));
        end
    end
end

geo.nodes=node;
geo.edges=edge;
geo.surfc=face;


