function [coords,lnods,mat,etype,bcond,nodes_at_bound,CLmean,nel8]=fragmenta(      ...
          model_name,thick,imat,imat_joint_fragm,imat_joint_conti,MMP_A, ...
          imat_joint_bound,imat_hole_1d,imat_hole_bound,imat_hole_conti,ibcondmech,...
          GLiopt,GLnewmat,GLQfrat)
%% Programa para fragmentar malhas no formato do CODE_BRIGHT:
% Gera arquivo model_name_f_gri.dat com malha fragmentada, preservando condicoes de contorno.
% Gera tambem saidas para visualizacao da malha fragmentada no GID.
% Parametros de entrada:
%   model_name              % string com nome do modelo no arquivo GRI.dat
%   thick                   % espessura das interfaces
%   imat                    % material a ser fragmentado
%   imat_joint_fragm        % material das interfaces na regiao fragmentada
%   imat_joint_conti        % material das interfaces no contato com outros materiais
%   imat_joint_bound        % material das interfaces no contorno
%   imat_hole_1d            % material segmento linear para conectar nos dos buracos entre interfaces
%   imat_hole_conti         % material dos buracos entres interfaces no interior da regia fragmentada
%   imat_hole_bound         % material dos buracos entres interfaces na fronteira da regiao fragmentada
%   ibcondmech              % condicao de contorno mecanica de apoio totalmente fixo
%   mporo                   % porosity (de cada material)
%   MMP_A                   % Usar caso imat_joint_conti=-1 [mat em contato , material da interface criada]
% OBS: ao atribuir ZERO para uma das variaveis imat_*, seu respectivo material NAO sera gerado. 
%% Atualiza��o v2.8
% Fragmenta mais de um material por vez;
% Diferentes materiais de interface na regiao fragmentada;
% Diferentes materiais no contato com outros materiais;
% Diferentes materiais nas interfaces no contorno;
% Fragmenta malha com linhas como material;
% Insere fraturas totalmente aleatorias;
% Mesmos ajustes para 3D;
% Aceita e fragmenta malha com elementos quadrilateros.
%
%
% imat_joint_conti == -3 (colocar nas instru��es de uso)
%
%
% Atualizado por MACIEL, B. M. C. M., UFPE, Brasil.
%
%% instru��es de uso
% Ex1: fragmentando 1(um) material.
% Ex2: fragmentando 2(dois) materiais.
% Ex3: fragmentando v�rios materiais.
%
% Fragmenta mais de um material por vez:
% Ex1: imat=1;       Fragmenta o material 1
% Ex2: imat=[1 2];   Fragmenta o material 1 e 2
% Ex3: imat=[1 5 8]; Fragmenta o material 1, 5 e 8
%
% Diferentes materiais de interface na regiao fragmentada:
% Ex1: imat_joint_fragm=2;         Material das interfaces na regiao fragmentada = 2
% Ex2: imat_joint_fragm=[3 4];     Material das interfaces na regiao fragmentada = 3 e 4
% Ex3: imat_joint_fragm=[9 10 11]; Material das interfaces na regiao fragmentada = 9, 10 e 11
%
% Diferentes materiais na interface de contato com outros materiais:
% Ex1: imat_joint_conti=(n�o existe)
% Ex2: imat_joint_conti=(Quando dois materiais em contato s�o fragmentados, o material da interface de contato
% � automaticamente criado com material igual aos da interface fragmentada primeiro. Neste exemplo o material ser� = 3)
% Ex3: imat_joint_conti=[12 13 14];
%
% Diferentes materiais na interface de contato com outros materiais (definido pelo material em contato):
% Ex: imat_joint_conti=-1;
% MMP_A = [material_em_contato material_criado];
% Ex: existem 3 materiais(1, 2 e 3), vamos fragmentar o material 2 que est� em contato com o material 1 e 3 como fica:
% MMP_A = [1 4; 3 5];
% o material na interface com o material 1 ser� = 4 e
% o material na interface com o material 3 ser� = 5.
%
% Diferentes materiais na interface de contato com outros materiais (definido pelo material em contato):
% Ex: imat_joint_conti=-2;
% Atribui automaticamente o material em contato
% Ex: existem 3 materiais(1, 2 e 3), vamos fragmentar o material 2 que est� em contato com o material 1 e 3 como fica:
% MMP_A = [0 0];
% o material na interface com o material 1 ser� = 1 e
% o material na interface com o material 3 ser� = 3.
%
% Diferentes materiais nas interfaces no contorno:
% caso necess�rio adicionar na mesma ordem do imat
% Ex1:  imat_joint_bound=3;
% Ex2:  imat_joint_bound=[5 6];
% Ex3:  imat_joint_bound=[0 0 17];
% Obs: caso s� precise adicionar em uma regiao colocar o valor somente pra ela.
%
% Fragmenta malha com linhas como material:
% Atribui automaticamente o material da linha atribuido na gera��o da malha.
%
%

%% Inicio
disp('## Programa FRAGMENTA')

% Obtem malha original:
if ((exist([model_name,'.coords']) == 2) && (exist([model_name,'.lnods']) == 2))
    disp('obtendo malha original...')
    D       = load([model_name,'.coords']); 
    [nn,c]  = size(D); ndim = c - 3;
    coords0 = zeros(nn,ndim);
    for i=1:ndim
        coords0(:,i) = D(:,i+1);
    end
    bcond0 = D(:,ndim+2:end);
    
    D       = load([model_name,'.coords']); 
    [ne,c]  = size(D);
    
    mat0    = D(:,2);
    etype0  = D(:,3);
    lnods0  = zeros(ne,3);
    lnods0(:,1:3)= D(:,4:6);
    if (ndim==3 | any(etype0==5))
        lnods0(i,4)= D(:,7);
    end
else
    % Abre arquivo GRI.dat e obtem malha original:
    disp('Abre arquivo GRI.dat e obtem malha original...')
    fidin = fopen([model_name '_gri.dat']);
    tline=fgetl(fidin);
    k=1; i=1;

    % Obtem ndim:
    tline=fgetl(fidin);
    pontos = findstr(tline,'.');

    ii=5; 
    if (pontos(1) > 11)
        ii = 10;
    end
    ff=10; 
    if (pontos(2) > 26)
        ff = 20;
    end    
    
    ndim=length(pontos);

    %% ler condi��es de contorno e os nos
    % Obtem lnods0 e coords0 (malha original):
    if (ndim==2)
        icoords1=(ii+1):(ii+ff);
        icoords2=icoords1(end)+1:icoords1(end)+ff;
        ibcond=icoords2(end)+1:icoords2(end)+10;
        
        imaterial=(ii+1):(ii+5);
        ietype=(imaterial(end)+1):(imaterial(end)+5);
        ilnods1=(ietype(end)+1):(ietype(end)+ii);
        ilnods2=(ilnods1(end)+1):(ilnods1(end)+ii);
        ilnods3=(ilnods2(end)+1):(ilnods2(end)+ii);
        ilnods4=(ilnods3(end)+1):(ilnods3(end)+ii);

    elseif (ndim==3)
        icoords1=(ii+1):(ii+ff);
        icoords2=icoords1(end)+1:icoords1(end)+ff;
        icoords3=icoords2(end)+1:icoords2(end)+ff;
        ibcond=icoords3(end)+1:icoords3(end)+10;
        
        imaterial=(ii+1):(ii+5);
        ietype=(imaterial(end)+1):(imaterial(end)+5);
        ilnods1=(ietype(end)+1):(ietype(end)+ii);
        ilnods2=(ilnods1(end)+1):(ilnods1(end)+ii);
        ilnods3=(ilnods2(end)+1):(ilnods2(end)+ii);
        ilnods4=(ilnods3(end)+1):(ilnods3(end)+ii); 
    else
        error(' �\_(?)_/�  ndim tem que ser igual a 2 ou 3');
    end    
    
    while (k==i)
        coords0(i,1)=str2num(tline(icoords1));
        coords0(i,2)=str2num(tline(icoords2));
        if (ndim==3)
            coords0(i,3)=str2num(tline(icoords3));
        end
        bcond0(i,:)=str2num(tline(ibcond));
        tline=fgetl(fidin);
        i=str2num(tline(1:5));
        k=k+1;
    end

    %% ler numero de materiais(mat), tipo(etype) e elementos(lnods)
    k=1; i=1;
    while (k==i)
        mat0(i)=str2num(tline(imaterial));
        etype0(i)=str2num(tline(ietype));
        lnods0(i,1)=str2num(tline(ilnods1));
        lnods0(i,2)=str2num(tline(ilnods2));
        lnods0(i,3)=str2num(tline(ilnods3));
        if (ndim==3 | etype0(i)==5)
            lnods0(i,4)=str2num(tline(ilnods4));
        end
        tline=fgetl(fidin);
        if (sum(size(tline))>5)
            i=str2num(tline(1:5)); % evita erros de fim de arquivo
        end
        k=k+1;
    end
    fclose(fidin);
    clearvars icoords1 icoords2 icoords3 ibcond imaterial ietype ilnods1...
        ilnods2 ilnods3 ilnods4 k i stline;
end
%% Inicia variaveis da malha fragmentada:
% (a variavel 'bcond' sera criada no final desta subrrotina)
coords=coords0;
lnods=lnods0;
mat=mat0;
etype=etype0;

% Dimensoes do problema:
[nn ndim]=size(coords);
[nel npe]=size(lnods);
nn0=nn;
nel0=nel;
nel8=nel;
sizeimat=max(size(imat));

% para inicializacao
lok=0;
CLmean=[ ];

%% separa linha (criado)
if any(etype==8) % verificar existencia de linhas com materiais atribuidos
    lok=1;
    R3=0; R6=0;
    for i = 1:nel0
        if (etype(i)~=8)
            R2=1+R3;
            lnods2(R2,:)=lnods0(i,:);
            etype2(:,R2)=etype0(:,i);
            mat2(:,R2)=mat0(:,i);
            R3=R3+1;
        end
        if (etype(i)==8)
            R4=1+R6;
            lnods8(R4,:)=lnods0(i,:);
            etype8(:,R4)=etype0(:,i);
            mat8(:,R4)=mat0(:,i);
            R6=R6+1;
        end
    end
    lnods0=lnods2;
    etype0=etype2;
    mat0=mat2;
    lnods=lnods0;
    mat=mat0;
    etype=etype0;
    [nn ndim]=size(coords);
    [nel npe]=size(lnods);
    nn0=nn;
    nel0=nel;
    
%% centroides da linha (criado)
    [nel8 npe8]=size(lnods8);
    for i=1:nel8
        xmean8 = mean(coords(lnods8(i,1:2),1));
        ymean8 = mean(coords(lnods8(i,1:2),2));
        CLmean(i,:) = [xmean8, ymean8];
    end
end % end if any

%% matriz para verifica materiais a serem fragmentados (criado)
matverfra=zeros(nel0,1);
for e=1:nel0
    for count=1:sizeimat
        if (mat(e)==imat(1,count))
            matverfra(e)=1;
        end
    end
end

%% Cria elementos internos (do continuo) na regiao fragmentada:
disp('Cria elementos internos (do continuo) na regiao fragmentada...')

% (alterado)

k=nn0;
for i=1:nel0
    if (matverfra(i)==1)
        if (etype(i)==1 & ndim==2) %%%
            npe=3;                   %
        end                          % Verifica Elem. Triangular ou Quadrilatero
        if (etype(i)==5 & ndim==2)   %
            npe=4;                   %
        end                        %%%
        for j=1:npe
            k=k+1;
            lnods(i,j)=k;
        end
    end
end
clearvars count i;

%% Cria base de dados de faces:
disp('Cria base de dados de faces...')

% (alterado)

facematrix=sparse(nn0*nn0*nn0,1);
% facematrix(:)  =   0 : face interna do dominio continuo (nao fragmentado)
% facematrix(:)  =   1 : face do contorno do dominio continuo
% facematrix(:)  =   2 : face entre os dominios continuo e fragmentado
% facematrix(:)  =   3 : face do contorno do dominio fragmentado
% facematrix(:)  =   4 : face interna do dominio fragmentado

if (ndim==2) % 2d
    ii=[1 2 3];
    jj=[2 3 1];
end
if (ndim==3) % 3d
    ii=[1 4 1 1];
    jj=[2 3 4 3];
    kk=[3 2 2 4];
end

for e=1:nel0
    if (etype(e)==1 & ndim==2)%%%
        npe=3;                  %
        ii=[1 2 3];             %
        jj=[2 3 1];             %
    end                         % Verifica Elem. Triangular ou Quadrilatero
    if (etype(e)==5 & ndim==2)  %
        npe=4;                  %
        ii=[1 2 3 4];           %
        jj=[2 3 4 1];           %
    end                       %%%
    for ie=1:npe
        if (ndim==2) % 2d
            nodes=sort([lnods0(e,ii(ie)) lnods0(e,jj(ie))]);
            index=(nodes(1)-1)*nn0+nodes(2);
        end
        if (ndim==3) % 3d
            nodes=sort([lnods0(e,ii(ie)) lnods0(e,jj(ie)) lnods0(e,kk(ie))]);
            index=(nodes(1)-1)*nn0*nn0+(nodes(2)-1)*nn0+nodes(3);
        end
        facematrix(index)=2; % = 2 (face entre os dominios continuo e fragmentado)
    end
end

%% informa pontos nodais da malha fragmentada contidos em cada face
facenodes=sparse(nn0*nn0*nn0,6);  % facenodes(:,:) = Matriz que informa pontos nodais da malha fragmentada contidos em cada face

% (alterado)

for e=1:nel0
    if (matverfra(e)==1)
        if (etype(e)==1 & ndim==2)%%%
            npe=3;                  %
            ii=[1 2 3];             %
            jj=[2 3 1];             %
        end                         % Verifica Elem. Triangular ou Quadrilatero
        if (etype(e)==5 & ndim==2)  %
            npe=4;                  %
            ii=[1 2 3 4];           %
            jj=[2 3 4 1];           %
        end                       %%%
        for ie=1:npe
            if (ndim==2) % 2d
                nodes=sort([lnods0(e,ii(ie)) lnods0(e,jj(ie))]);
                index=(nodes(1)-1)*nn0+nodes(2);
                if (facenodes(index,1)==0)
                    facenodes(index,1)=lnods(e,ii(ie));
                    facenodes(index,2)=lnods(e,jj(ie));
                else
                    facenodes(index,3)=lnods(e,ii(ie));
                    facenodes(index,4)=lnods(e,jj(ie));
                end
            end
            if (ndim==3) % 3d
                nodes=sort([lnods0(e,ii(ie)) lnods0(e,jj(ie)) lnods0(e,kk(ie))]);
                index=(nodes(1)-1)*nn0*nn0+(nodes(2)-1)*nn0+nodes(3);
                if (facenodes(index,1)==0)
                    facenodes(index,1)=lnods(e,ii(ie));
                    facenodes(index,2)=lnods(e,jj(ie));
                    facenodes(index,3)=lnods(e,kk(ie));
                else
                    facenodes(index,4)=lnods(e,ii(ie));
                    facenodes(index,5)=lnods(e,jj(ie));
                    facenodes(index,6)=lnods(e,kk(ie));
                end
            end
            facematrix(index)=facematrix(index)+1;
        end   
    end % end matverfra
end

%% completa construcao de facematrix (n�o fragmentado)

% (alterado)

for e=1:nel0
    if (matverfra(e)~=1) % completa construcao de facematrix
        if (etype(e)==1 & ndim==2)%%%
            npe=3;                  %
            ii=[1 2 3];             %
            jj=[2 3 1];             %
        end                         % Verifica Elem. Triangular ou Quadrilatero
        if (etype(e)==5 & ndim==2)  %
            npe=4;                  %
            ii=[1 2 3 4];           %
            jj=[2 3 4 1];           %
        end                       %%%
        for ie=1:npe
            if (ndim==2) % 2d
                nodes=sort([lnods0(e,ii(ie)) lnods0(e,jj(ie))]);
                index=(nodes(1)-1)*nn0+nodes(2);
            end
            if (ndim==3) % 3d
                nodes=sort([lnods0(e,ii(ie)) lnods0(e,jj(ie)) lnods0(e,kk(ie))]);
                index=(nodes(1)-1)*nn0*nn0+(nodes(2)-1)*nn0+nodes(3);
            end
            facematrix(index)=facematrix(index)-1;
        end
    end % end matverfra
end

%% Cria novos nos na malha fragmentada:
disp('Cria novos nos na malha fragmentada...')

% (alterado)

k=0;
for e=1:nel0
    if (matverfra(e)==1)
        
        if (ndim==2 & etype(e)==1) % 2d & elem. triangular
            npe=3;         %
            ii=[1 2 3];    %
            jj=[2 3 1];    %
            [x_interno y_interno]=internal_points_2D(thick,e,etype,coords,coords0,lnods0,facematrix);
            for j=1:npe
                k=k+1;
                coords(nn+k,:)=[x_interno(j) y_interno(j)];
            end
        end
        
        if (ndim==2 & etype(e)==5) % 2d & elem. quadrilatero
            npe=4;         %
            ii=[1 2 3 4];  %
            jj=[2 3 4 1];  %
            [x_interno y_interno]=internal_points_2D_quad(thick,e,etype,coords,coords0,lnods0,facematrix);
            for j=1:npe
                k=k+1;
                coords(nn+k,:)=[x_interno(j) y_interno(j)];
            end
        end
        
        if (ndim==3) % 3d
            [x_interno y_interno z_interno]=internal_points_3D(thick,e,coords,coords0,lnods0,facematrix);
            for j=1:npe
                k=k+1;
                coords(nn+k,:)=[x_interno(j) y_interno(j) z_interno(j)];
            end
        end
        
    end % end matverfra
end

%% Novas dimensoes de coords:
[nn ndim]=size(coords);

%% matriz de flags que indica se elementos de cada face ja foram criados
checkface=sparse(nn0*nn0*nn0,1);

%% Elementos de interface:
disp('Cria elementos de interface...')

% (alterado)
% aqui cria o elemento de interface

nodes_at_bound=zeros(nn,1);
% nodes_at_bound(:)  =   0 : noh do continuo nao fragmentado ou interno do dominio fragmentado conectando elementos do continuo e interfaces
% nodes_at_bound(:)  =   1 : noh da malha original dentro do dominio fragmentado (deve ter deslocamentos fixados)
% nodes_at_bound(:)  =   2 : noh na face entre os dominios continuo e fragmentado: facematrix(:)=2 
% nodes_at_bound(:)  =   3 : noh na face do contorno do dominio fragmentado: facematrix(:)=3
% nodes_at_bound(:)  =   4 : noh na interseccao facematrix(:)=2 e facematrix(:)=3

k=nel0;
for e=1:nel0
    if (matverfra(e)==1) % materiais fragmentados
        
        if (etype(e)==1 | etype(e)==5) % 1=triangulo 5=quadrilatero
            if (etype(e)==1 & ndim==2)%%%
                npe=3;                  %
                ii=[1 2 3];             %
                jj=[2 3 1];             %
            end                         % Verifica Elem. Triangular ou Quadrilatero
            if (etype(e)==5 & ndim==2)  %
                npe=4;                  %
                ii=[1 2 3 4];           %
                jj=[2 3 4 1];           %
            end                       %%%
            for i=1:npe
                if (ndim==2) % 2d
                    node1=lnods0(e,ii(i));
                    node2=lnods0(e,jj(i));
                    nodes=sort([node1 node2]);
                    index=(nodes(1)-1)*nn0+nodes(2);
                end
                if (ndim==3) % 3d
                    node1=lnods0(e,ii(i));
                    node2=lnods0(e,jj(i));
                    node3=lnods0(e,kk(i));
                    nodes=sort([node1 node2 node3]);
                    index=(nodes(1)-1)*nn0*nn0+(nodes(2)-1)*nn0+nodes(3);
                end
                face=facematrix(index);
                
                if (checkface(index)==0)
                    
                    for j=1:ndim
                        if (face==3)
                            if (nodes_at_bound(nodes(j))==2 | nodes_at_bound(nodes(j))==4)
                                nodes_at_bound(nodes(j))=4; % 4 : noh na interseccao facematrix(:)=2 e facematrix(:)=3
                            else
                                nodes_at_bound(nodes(j))=3; % 3 : noh na face do contorno do dominio fragmentado: facematrix(:)=3
                            end
                        end
                        if (face==2)
                            if (nodes_at_bound(nodes(j))==3 | nodes_at_bound(nodes(j))==4)
                                nodes_at_bound(nodes(j))=4; % 4 : noh na interseccao facematrix(:)=2 e facematrix(:)=3
                            else
                                nodes_at_bound(nodes(j))=2;
                            end
                        end
                        if (nodes_at_bound(nodes(j))==0) nodes_at_bound(nodes(j))=1; end
                    end
                    
                    sizeimatJConti=size(imat_joint_conti,2);
                    
                    if (face==2 & imat_joint_conti~=0)   % aresta da interface entre materiais
                        %% 2d
                        if (ndim==2) % 2d
                            k=k+1;
                            lnods(k,1)=facenodes(index,1);       % noh face oposta (sempre o primeiro)
                            lnods(k,2)=node1;                    % noh face adjacente
                            lnods(k,3)=node2;                    % noh face adjacente
                            
                            if sizeimatJConti ==1
                                
                                if imat_joint_conti == -3
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_A(:,1)==MMP_mat(1,1) &...
                                        MMP_A(:,2)==MMP_mat(1,2) |...
                                        MMP_A(:,2)==MMP_mat(1,1) &...
                                        MMP_A(:,1)==MMP_mat(1,2));
                                    
                                    MMP_L = MMP_A((MMP_Lmat),3);
                                    
                                    mat(k) = MMP_L; % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -2
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    mat(k) = MMP_mat(MMP_Lmat);  % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -1
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    MMP_B = MMP_mat(MMP_Lmat);
                                    
                                    MMP_L = MMP_A((MMP_A(:,1)==MMP_B),2); %% criar matriz MMP_A na entrada de dados   **************************
                                    
                                    mat(k) = MMP_L;              % material das interfaces no contato com outros materiais
                                    
                                else
                                    mat(k)=imat_joint_conti;     % material das interfaces no contato com outros materiais
                                end
                                
                            else
                                for count=1:sizeimatJConti       % material das interfaces no contato com outros materiais
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_conti(1,count);
                                    end
                                end
                            end
                            
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1)=node2;                     % noh face oposta (sempre o primeiro)
                            lnods(k,2)=facenodes(index,2);        % noh face adjacente
                            lnods(k,3)=facenodes(index,1);        % noh face adjacente
                            
                            if sizeimatJConti ==1
                                
                                if imat_joint_conti == -3
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_A(:,1)==MMP_mat(1,1) &...
                                        MMP_A(:,2)==MMP_mat(1,2) |...
                                        MMP_A(:,2)==MMP_mat(1,1) &...
                                        MMP_A(:,1)==MMP_mat(1,2));
                                    
                                    MMP_L = MMP_A((MMP_Lmat),3);
                                    
                                    mat(k) = MMP_L; % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -2
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    mat(k) = MMP_mat(MMP_Lmat);  % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -1
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    MMP_B = MMP_mat(MMP_Lmat);
                                    
                                    MMP_L = MMP_A((MMP_A(:,1)==MMP_B),2); %% criar matriz MMP_A na entrada de dados   **************************
                                    
                                    mat(k) = MMP_L;              % material das interfaces no contato com outros materiais
                                    
                                else
                                    mat(k)=imat_joint_conti;     % material das interfaces no contato com outros materiais
                                end
                                
                            else
                                for count=1:sizeimatJConti       % material das interfaces no contato com outros materiais
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_conti(1,count);
                                    end
                                end
                            end
                            
                            etype(k)=1;
                        end
                        %% 3d
                        if (ndim==3) % 3d
                            [newfacenodes inodeout1 inodelist]=check_nodes_3D(...
                                facenodes(index,1),...
                                facenodes(index,2),...
                                facenodes(index,3),...
                                node1,node2,node3,coords);
                            k=k+1;
                            lnods(k,1)=newfacenodes(4);
                            lnods(k,2)=newfacenodes(5);
                            lnods(k,3)=newfacenodes(6);
                            lnods(k,4)=newfacenodes(1);
                            
                            if sizeimatJConti ==1
                                
                                if imat_joint_conti == -3
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_A(:,1)==MMP_mat(1,1) &...
                                        MMP_A(:,2)==MMP_mat(1,2) |...
                                        MMP_A(:,2)==MMP_mat(1,1) &...
                                        MMP_A(:,1)==MMP_mat(1,2));
                                    
                                    MMP_L = MMP_A((MMP_Lmat),3);
                                    
                                    mat(k) = MMP_L; % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -2
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    mat(k) = MMP_mat(MMP_Lmat);  % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -1
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    MMP_B = MMP_mat(MMP_Lmat);
                                    
                                    MMP_L = MMP_A((MMP_A(:,1)==MMP_B),2); %% criar matriz MMP_A na entrada de dados   **************************
                                    
                                    mat(k) = MMP_L;              % material das interfaces no contato com outros materiais
                                    
                                else
                                    mat(k)=imat_joint_conti;     % material das interfaces no contato com outros materiais
                                end
                                
                            else
                                for count=1:sizeimatJConti       % material das interfaces no contato com outros materiais
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_conti(1,count);
                                    end
                                end
                            end
                            
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1)=newfacenodes(1);
                            lnods(k,2)=newfacenodes(2);
                            lnods(k,3)=newfacenodes(3);
                            lnods(k,4)=newfacenodes(inodeout1);
                            
                            if sizeimatJConti ==1
                                if imat_joint_conti == -3
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_A(:,1)==MMP_mat(1,1) &...
                                        MMP_A(:,2)==MMP_mat(1,2) |...
                                        MMP_A(:,2)==MMP_mat(1,1) &...
                                        MMP_A(:,1)==MMP_mat(1,2));
                                    
                                    MMP_L = MMP_A((MMP_Lmat),3);
                                    
                                    mat(k) = MMP_L; % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -2
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    mat(k) = MMP_mat(MMP_Lmat);  % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -1
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    MMP_B = MMP_mat(MMP_Lmat);
                                    
                                    MMP_L = MMP_A((MMP_A(:,1)==MMP_B),2); %% criar matriz MMP_A na entrada de dados   **************************
                                    
                                    mat(k) = MMP_L;              % material das interfaces no contato com outros materiais
                                    
                                else
                                    mat(k)=imat_joint_conti;     % material das interfaces no contato com outros materiais
                                end
                                
                            else
                                for count=1:sizeimatJConti       % material das interfaces no contato com outros materiais
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_conti(1,count);
                                    end
                                end
                            end
                            
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1:4)=newfacenodes(inodelist);
                            
                            if sizeimatJConti ==1
                                if imat_joint_conti == -3
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_A(:,1)==MMP_mat(1,1) &...
                                        MMP_A(:,2)==MMP_mat(1,2) |...
                                        MMP_A(:,2)==MMP_mat(1,1) &...
                                        MMP_A(:,1)==MMP_mat(1,2));
                                    
                                    MMP_L = MMP_A((MMP_Lmat),3);
                                    
                                    mat(k) = MMP_L; % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -2
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    mat(k) = MMP_mat(MMP_Lmat);  % material das interfaces no contato com outros materiais
                                    
                                elseif imat_joint_conti == -1
                                    
                                    MMP_mf = mat(e);  % material fragmentado
                                    
                                    MMP_Ln3 = any(lnods0==node3,2);
                                    MMP_Ln2 = any(lnods0==node2,2);
                                    MMP_Ln1 = any(lnods0==node1,2);
                                    MMP_Lo = (MMP_Ln1 & MMP_Ln2 & MMP_Ln3);
                                    
                                    MMP_Point = 1:size(lnods0,1);
                                    MMP_id = MMP_Point(MMP_Lo);
                                    MMP_mat = mat0(MMP_id);
                                    
                                    MMP_Lmat = (MMP_mat(1,:)~=MMP_mf);
                                    
                                    MMP_B = MMP_mat(MMP_Lmat);
                                    
                                    MMP_L = MMP_A((MMP_A(:,1)==MMP_B),2); %% criar matriz MMP_A na entrada de dados   **************************
                                    
                                    mat(k) = MMP_L;              % material das interfaces no contato com outros materiais
                                    
                                else
                                    mat(k)=imat_joint_conti;     % material das interfaces no contato com outros materiais
                                end
                                
                            else
                                for count=1:sizeimatJConti       % material das interfaces no contato com outros materiais
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_conti(1,count);
                                    end
                                end
                            end
                            
                            etype(k)=1;
                        end
                    end
                    
                    sizeimatJBound=size(imat_joint_bound,2);
                    
                    if (face==3 & imat_joint_bound>0)             % aresta do contorno externo
                        if (ndim==2)
                            k=k+1;
                            lnods(k,1)=facenodes(index,1);              % noh face oposta (sempre o primeiro)
                            lnods(k,2)=node1;                           % noh face adjacente
                            lnods(k,3)=node2;                           % noh face adjacente
                            if sizeimatJBound ==1
                                mat(k)=imat_joint_bound;
                            else
                                for count=1:sizeimatJBound
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_bound(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1)=node2;                           % noh face oposta (sempre o primeiro)
                            lnods(k,2)=facenodes(index,2);              % noh face adjacente
                            lnods(k,3)=facenodes(index,1);              % noh face adjacente
                            if sizeimatJBound ==1
                                mat(k)=imat_joint_bound;
                            else
                                for count=1:sizeimatJBound
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_bound(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                        end
                        if (ndim==3)
                            [newfacenodes inodeout1 inodelist]=check_nodes_3D(...
                                facenodes(index,1),...
                                facenodes(index,2),...
                                facenodes(index,3),...
                                node1,node2,node3,coords);
                            k=k+1;
                            lnods(k,1)=newfacenodes(4);
                            lnods(k,2)=newfacenodes(5);
                            lnods(k,3)=newfacenodes(6);
                            lnods(k,4)=newfacenodes(1);
                            if sizeimatJBound ==1
                                mat(k)=imat_joint_bound;
                            else
                                for count=1:sizeimatJBound
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_bound(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1)=newfacenodes(1);
                            lnods(k,2)=newfacenodes(2);
                            lnods(k,3)=newfacenodes(3);
                            lnods(k,4)=newfacenodes(inodeout1);
                            if sizeimatJBound ==1
                                mat(k)=imat_joint_bound;
                            else
                                for count=1:sizeimatJBound
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_bound(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1:4)=newfacenodes(inodelist);
                            if sizeimatJBound ==1
                                mat(k)=imat_joint_bound;
                            else
                                for count=1:sizeimatJBound
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_bound(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                        end
                    end
                    
                    sizeimatJFragm=size(imat_joint_fragm,2);
                    
                    if (face==4 & imat_joint_fragm>0)       % aresta interna do dominio fragmentado
                        if (ndim==2)
                            k=k+1;
                            lnods(k,1)=facenodes(index,1);  % noh face oposta (sempre o primeiro)
                            lnods(k,2)=facenodes(index,4);  % noh face adjacente
                            lnods(k,3)=facenodes(index,3);  % noh face adjacente
                            if sizeimatJFragm == 1
                                mat(k)=imat_joint_fragm;
                            else
                                for count=1:sizeimatJFragm             % material das interfaces na regiao fragmentada
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_fragm(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1)=facenodes(index,3);  % noh face oposta (sempre o primeiro)
                            lnods(k,2)=facenodes(index,2);  % noh face adjacente
                            lnods(k,3)=facenodes(index,1);  % noh face adjacente
                            if sizeimatJFragm == 1
                                mat(k)=imat_joint_fragm;
                            else
                                for count=1:sizeimatJFragm             % material das interfaces na regiao fragmentada
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_fragm(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                        end
                        if (ndim==3)
                            [newfacenodes inodeout1 inodelist]=check_nodes_3D(...
                                facenodes(index,1),...
                                facenodes(index,2),...
                                facenodes(index,3),...
                                facenodes(index,4),...
                                facenodes(index,5),...
                                facenodes(index,6),coords);
                            k=k+1;
                            lnods(k,1)=newfacenodes(4);
                            lnods(k,2)=newfacenodes(5);
                            lnods(k,3)=newfacenodes(6);
                            lnods(k,4)=newfacenodes(1);
                            if sizeimatJFragm == 1
                                mat(k)=imat_joint_fragm;
                            else
                                for count=1:sizeimatJFragm             % material das interfaces na regiao fragmentada
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_fragm(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1)=newfacenodes(1);
                            lnods(k,2)=newfacenodes(2);
                            lnods(k,3)=newfacenodes(3);
                            lnods(k,4)=newfacenodes(inodeout1);
                            if sizeimatJFragm == 1
                                mat(k)=imat_joint_fragm;
                            else
                                for count=1:sizeimatJFragm             % material das interfaces na regiao fragmentada
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_fragm(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                            k=k+1;
                            lnods(k,1:4)=newfacenodes(inodelist);
                            if sizeimatJFragm == 1
                                mat(k)=imat_joint_fragm;
                            else
                                for count=1:sizeimatJFragm             % material das interfaces na regiao fragmentada
                                    if (mat(e)==imat(1,count))
                                        mat(k)=imat_joint_fragm(1,count);
                                    end
                                end
                            end
                            etype(k)=1;
                        end
                    end
                    
                    checkface(index)=1;     % flag que indica que a face ja foi visitada
                    
                end
                
            end
        end % end etype triangulo
        
    end % end matverfra
end

clearvars sizeimatJBound sizeimatJConti sizeimatJFragm;

%% Novas dimensoes de lnods:
[nel npe]=size(lnods); 

%% Elementos triangulares do buraco (apenas 2D):
disp('Cria elementos triangulares (ndim=2D) dos buracos entre interfaces...')
k=nel;
for e=nel0+1:nel
  if (mat(e)==imat_joint_fragm)   

    if (ndim==2)
      xnode=zeros(nn0,ndim);
      xnode(:,1)=xnode(:,1)+coords(lnods(e,1),1); % coordenadas do noh da face oposta do elemento de interface
      xnode(:,2)=xnode(:,2)+coords(lnods(e,1),2); % coordenadas do noh da face oposta do elemento de interface
      if (ndim==3)
      xnode(:,3)=xnode(:,3)+coords(lnods(e,1),3); % coordenadas do noh da face oposta do elemento de interface
      end
    
      dist=(coords(1:nn0,:)-xnode);      
      norma_dist=sqrt(dist(:,1).^2+dist(:,2).^2);
      [dist_min node]=min(norma_dist);
      
      if (nodes_at_bound(node)==3)
        if (imat_hole_bound>0)
          k=k+1;
          lnods(k,1)=node;
          lnods(k,2)=lnods(e,2);
          lnods(k,3)=lnods(e,1);
          mat(k)=imat_hole_bound;
          etype(k)=1;
        end
      else
        if (imat_hole_conti>0)
          k=k+1;
          lnods(k,1)=node;
          lnods(k,2)=lnods(e,2);
          lnods(k,3)=lnods(e,1);
          mat(k)=imat_hole_conti;
          etype(k)=1;
        end
      end
      
    end 
    
  end   
end


%% Novas dimensoes de lnods:
[nel npe]=size(lnods); 

%% Elementos segmentos 1D dos buracos entre interfaces (2D e 3D):
disp('Cria elementos segmentos 1D dos buracos entre interfaces e reconstroi condicoes de contorno...')
bcond=[bcond0' zeros(2,nn-nn0)]';
k=nel;

% (alterado)

for e=1:nel0
    if(matverfra(e)==1)
        if (etype(e)==1 & ndim==2) %%%
            npe=3;                   %
        end                          % Verifica Elem. Triangular ou Quadrilatero
        if (etype(e)==5 & ndim==2)   %
            npe=4;                   %
        end                        %%%
        
        for i=1:npe
            %% Cria os novos elementos:
            if (imat_hole_1d>0)
                k=k+1;
                lnods(k,1)=lnods0(e,i);
                lnods(k,2:npe)=lnods(e,i);
                mat(k)=imat_hole_1d;
                etype(k)=8;           % segmento 1D do CB (2D e 3D)
            end
            
            %% Reconstroi condicoes de contorno mecanicas:
            if (nodes_at_bound(lnods0(e,i))==1)     % Interior da malha fragmentada
                bcond(lnods0(e,i),1)=ibcondmech;    % Ponto nodal da malha original: fixa deslocamento
            end
            
            if (imat_joint_bound==0)  % Caso nao haja elemento de interface no contorno fragmentado
                
                if (nodes_at_bound(lnods0(e,i))==4       )      % Interseccao entre limites do dominio fragmentado: fronteira e contato com meio nao fragmentado
                    bcond(lnods(e,i),1)=bcond0(lnods0(e,i),1);  % Transfere condicao de contorno para noh interno
                end
                
                if (nodes_at_bound(lnods0(e,i))==3)             % Fronteira da malha fragmentada
                    bcond(lnods0(e,i),1)=ibcondmech;            % Ponto nodal da malha original: fixa deslocamento
                    bcond(lnods(e,i),1)=bcond0(lnods0(e,i),1);  % Transfere condicao de contorno para noh interno
                end
            end
            
        end
    end % end matverfra
end

%% Continua a reconstruir condicoes de contorno mecanicas:

% (alterado)

clear checkface
checkface=sparse(nn0*nn0*nn0,1); % matriz de flags que indica se elementos de cada face ja foram criados

if (imat_joint_bound==0)         % Caso nao haja elemento de interface no contorno fragmentado
    for e=1:nel0                 % Termina de transferir condicao de contorno para noh interno
        if(matverfra(e)==1)
            if (etype(e)==1 & ndim==2) %%%
                npe=3;                   %
            end                          % Verifica Elem. Triangular ou Quadrilatero
            if (etype(e)==5 & ndim==2)   %
                npe=4;                   %
            end                        %%%
            
            for i=1:npe
                if (ndim==2)
                    node1=lnods0(e,ii(i));
                    node2=lnods0(e,jj(i));
                    nodes=sort([node1 node2]);
                    index=(nodes(1)-1)*nn0+nodes(2);
                end
                if (ndim==3)
                    node1=lnods0(e,ii(i));
                    node2=lnods0(e,jj(i));
                    node3=lnods0(e,kk(i));
                    nodes=sort([node1 node2 node3]);
                    index=(nodes(1)-1)*nn0*nn0+(nodes(2)-1)*nn0+nodes(3);
                end
                face=facematrix(index);
                if (checkface(index)==0)
                    if (face==3)
                        if (bcond(lnods(e,ii(i)),1)==0 &                       ...
                                bcond(lnods0(e,ii(i)),1)~=ibcondmech)              ...
                                bcond(lnods(e,ii(i)),1)=bcond(lnods0(e,ii(i)),1);
                        end
                        if (bcond(lnods(e,jj(i)),1)==0 &                       ...
                                bcond(lnods0(e,jj(i)),1)~=ibcondmech)              ...
                                bcond(lnods(e,jj(i)),1)=bcond(lnods0(e,jj(i)),1);
                        end
                        if (ndim==3)
                            if (bcond(lnods(e,kk(i)),1)==0 &                       ...
                                    bcond(lnods0(e,kk(i)),1)~=ibcondmech)              ...
                                    bcond(lnods(e,kk(i)),1)=bcond(lnods0(e,kk(i)),1);
                            end
                        end
                    end
                end
            end
            checkface(index)=1;  % flag que indica que a face ja foi visitada
            
        end % end matverfra
    end
end

%% Inserir fraturas aleatorias na malha fragmentada

if GLiopt==1 | GLiopt==2
    insere_fraturas_bmcm;
end

%% Corrige colocacao do ibcondmech quando nos estao no contorno com outro material:
% disp('Ultima correcao nas condicoes de contorno mecanicas...')

for e=1:nel
    if (mat(e)~=imat_hole_1d)
        if (etype(e)==1 & ndim==2) %%%
            npe=3;                   %
        end                          % Verifica Elem. Triangular ou Quadrilatero
        if (etype(e)==5 & ndim==2)   %
            npe=4;                   %
        end                        %%%
        for i=1:npe
            if (bcond(lnods(e,i),1)==ibcondmech)
                bcond(lnods(e,i),1)=0;
            end
        end
    end
end

%% centroides triangulos (gera CTnormal)(criado)

if lok == 1
    [nel npe]=size(lnods);
    for i=1:nel
        xmean = mean(coords(lnods(i,1:3),1));
        ymean = mean(coords(lnods(i,1:3),2));
        CTnormal(i,:) = [xmean, ymean];
    end
    
    %% cria centroide entre dois elementos para comparar com o centro da linha
    for i=1:nel
        if mod(i,2) == 1
            xmean = mean(CTnormal(i:i+1,1));
            ymean = mean(CTnormal(i:i+1,2));
            CTmean(i,:) = [xmean, ymean];
        else
            xmean = mean(CTnormal(i-1:i,1));
            ymean = mean(CTnormal(i-1:i,2));
            CTmean(i,:) = [xmean, ymean];
        end
    end
    
    %% Compara centroides com os pontos medios das linhas
    k=1;
    for e = 1:nel                                                                        % contador numero de elementos total
        for i = 1:nel8                                                                   % contador numero de elementos linha
            if mat(e) >= min(imat_joint_fragm) & etype(e) == 1 & ...                     % separa por materiais fragmentados e por tipo de elemento
                    CTmean(e,:) < CLmean(i,:)+thick & CTmean(e,:) > CLmean(i,:)-thick    % separa por proximidade dos centroides
                Emodificar(k,1)=e;                                                       % salva o elemento que deve ser modificado
                Emodificar(k,2)=mat8(i);                                                 % salva qual o material que deve modificar no elemento
                k=k+1;
            end
        end
    end
    %% Substitui os materiais na linha
    count=size(Emodificar,1);
    e = 1:count;
    mat(Emodificar(e,1))=Emodificar(e,2);

end % if Lok

%%
disp('## Fragmentacao finalizada.')

end % end fragmenta





%% %%%%%%%%%%%%%%%%%%%%%%%%     FUNCOES     %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 2d triangular

function [x_interno y_interno]=internal_points_2D(thick,e,etype,coords,coords0,lnods0,facematrix)

[nn0 ndim]=size(coords0);
[nel0 npe]=size(lnods0);
npe=3;
% figure
% axis equal
% hold on

for j=1:npe
    conect(j)=lnods0(e,j);
    x(j)=coords(lnods0(e,j),1);
    y(j)=coords(lnods0(e,j),2);
end

x(4)=x(1); y(4)=y(1);
conect(4)=conect(1);
% plot(x,y);

for j=1:npe
    
    nodes=sort([conect(j) conect(j+1)]);
    index=(nodes(1)-1)*nn0+nodes(2);
    
    espessura=thick/2.0;                 % afastamento nas interfaces internas
    
    if (facematrix(index)==2 | ...       % afastamento nas interfaces da fronteira
            facematrix(index)==3)        % da regiao fragmentada
        espessura=thick;
    end
    
    xm=(x(j+1)+x(j))/2.0;
    ym=(y(j+1)+y(j))/2.0;
    
    v=[x(j+1)-x(j) y(j+1)-y(j)];
    na(j,:)=[-v(2) v(1)]/norm(v);
    
    xa(j)=xm+na(j,1)*espessura;
    ya(j)=ym+na(j,2)*espessura;
    
    %  quiver(xm,ym,na(j,1)*espessura,na(j,2)*espessura);
    %  plot(xm,ym,'o');
    %  plot(xa(j),ya(j),'x');
    %  pause   
end

na(4,:)=na(1,:);
xa(4)=xa(1);
ya(4)=ya(1);
index=[2 3 1];

for j=1:npe
    
    A=[na(j,1)   na(j,2)
        na(j+1,1) na(j+1,2)];
    B=[na(j,1)*xa(j)+na(j,2)*ya(j)  na(j+1,1)*xa(j+1)+na(j+1,2)*ya(j+1)]';
    X=inv(A)*B;
    x_interno(index(j))=X(1);
    y_interno(index(j))=X(2);
    
end
% plot(x_interno,y_interno,'r');
% pause
end


%% 2d quadrilatero

function [x_interno y_interno]=internal_points_2D_quad(thick,e,etype,coords,coords0,lnods0,facematrix)

[nn0 ndim]=size(coords0);
[nel0 npe]=size(lnods0);

% figure
% axis equal
% hold on

for j=1:npe
    conect(j)=lnods0(e,j);
    x(j)=coords(lnods0(e,j),1);
    y(j)=coords(lnods0(e,j),2);
end

x(5)=x(1); y(5)=y(1);
conect(5)=conect(1);
% plot(x,y);

for j=1:npe
    
    nodes=sort([conect(j) conect(j+1)]);
    index=(nodes(1)-1)*nn0+nodes(2);
    
    espessura=thick/2.0;                 % afastamento nas interfaces internas
    
    if (facematrix(index)==2 | ...       % afastamento nas interfaces da fronteira
            facematrix(index)==3       )     % da regiao fragmentada
        espessura=thick;
    end
    
    xm=(x(j+1)+x(j))/2.0;
    ym=(y(j+1)+y(j))/2.0;
    
    v=[x(j+1)-x(j) y(j+1)-y(j)];
    na(j,:)=[-v(2) v(1)]/norm(v);
    
    xa(j)=xm+na(j,1)*espessura;
    ya(j)=ym+na(j,2)*espessura;
    
    %  quiver(xm,ym,na(j,1)*espessura,na(j,2)*espessura);
    %  plot(xm,ym,'o');
    %  plot(xa(j),ya(j),'x');
    %  pause   
end

na(5,:)=na(1,:);
xa(5)=xa(1);
ya(5)=ya(1);
index=[2 3 4 1];

for j=1:npe
    
    A=[na(j,1)   na(j,2)
        na(j+1,1) na(j+1,2)];
    B=[na(j,1)*xa(j)+na(j,2)*ya(j)  na(j+1,1)*xa(j+1)+na(j+1,2)*ya(j+1)]';
    X=inv(A)*B;
    x_interno(index(j))=X(1);
    y_interno(index(j))=X(2);
    
end
% plot(x_interno,y_interno,'r');
% pause
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 3d

function [x_interno y_interno z_interno]=internal_points_3D(thick,e,coords,coords0,lnods0,facematrix)

[nn0 ndim]=size(coords0);
[nel0 npe]=size(lnods0); 

% figure
% axis equal
% hold on

%    face1 face2 face3 face4 
ii=[   1     4     1     1   ];
jj=[   2     3     4     3   ];
kk=[   3     2     2     4   ];

% node1: interseccao faces 1,3,4
% node2: interseccao faces 1,2,3
% node3: interseccao faces 1,2,4
% node4: interseccao faces 2,3,4

nodeface=[1 3 4
          1 2 3
          1 2 4
          2 3 4];

for node=1:npe
    
  for face=1:ndim

    nodes=[lnods0(e,ii(nodeface(node,face))) lnods0(e,jj(nodeface(node,face))) lnods0(e,kk(nodeface(node,face)))];

    nodes_sort=sort(nodes);
    
    index=(nodes_sort(1)-1)*nn0*nn0+(nodes_sort(2)-1)*nn0+nodes_sort(3);
    
    espessura=thick/2.0;                 % afastamento nas interfaces internas
  
    if (facematrix(index)==2 | ...       % afastamento nas interfaces da fronteira
        facematrix(index)==3       )     % da regiao fragmentada
      espessura=thick;
    end
    
    for i=1:ndim
        x(i)=coords(nodes(i),1);
        y(i)=coords(nodes(i),2);
        z(i)=coords(nodes(i),3);
    end
    
    xm=mean(x); ym=mean(y); zm=mean(z);
    
    v1=[x(2)-x(1) y(2)-y(1) z(2)-z(1)];
    v2=[x(3)-x(1) y(3)-y(1) z(3)-z(1)];
    
    normal_face=cross(v1,v2);
    NF(face,:)=normal_face./norm(normal_face);

    xf=xm+NF(face,1)*espessura;
    yf=ym+NF(face,2)*espessura;
    zf=zm+NF(face,3)*espessura;
    
%    plot3(x,y,z);
%    quiver3(xm,ym,zm,NF(face,1)*espessura,NF(face,2)*espessura,NF(face,3)*espessura);
%    plot3(xf,yf,zf,'o');
%    pause
    
    B(face)=NF(face,:)*[xf yf zf]';
    
  end
  
  X=inv(NF)*B';
  
  x_interno(node)=X(1);
  y_interno(node)=X(2);
  z_interno(node)=X(3);  
  
%  plot3(x_interno,y_interno,z_interno,'x')
%  pause
  
end
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [newfacenodes inodeout1 inodelist]=check_nodes_3D(node1,node2,node3,node4,node5,node6,coords);

face1=sort([node1 node2 node3]);
face2=sort([node4 node5 node6]);

% Verificando numeraçao face 1:

det1=det([ 1 coords(face1(1),1) coords(face1(1),2) coords(face1(1),3)
           1 coords(face1(2),1) coords(face1(2),2) coords(face1(2),3)
           1 coords(face1(3),1) coords(face1(3),2) coords(face1(3),3)
           1 coords(face2(1),1) coords(face2(1),2) coords(face2(1),3)]);

if (det1<0) face1(2:3)=[face1(3) face1(2)]; end

% Verificando numeraçao face 2:

det2=det([ 1 coords(face2(1),1) coords(face2(1),2) coords(face2(1),3)
           1 coords(face2(2),1) coords(face2(2),2) coords(face2(2),3)
           1 coords(face2(3),1) coords(face2(3),2) coords(face2(3),3)
           1 coords(face1(1),1) coords(face1(1),2) coords(face1(1),3)]);

if (det2<0) face2(2:3)=[face2(3) face2(2)]; end

newfacenodes(1:6)=[face1 face2];   
    
% Distancias do node1 aos pontos da face 2:

xnode=zeros(3,3);
xnode(:,1)=xnode(:,1)+coords(face1(1),1); % coordenadas do noh da face oposta do elemento de interface
xnode(:,2)=xnode(:,2)+coords(face1(1),2); % coordenadas do noh da face oposta do elemento de interface
xnode(:,3)=xnode(:,3)+coords(face1(1),3); % coordenadas do noh da face oposta do elemento de interface

dist=(coords(face2,:)-xnode);

norma_dist=sqrt(dist(:,1).^2+dist(:,2).^2+dist(:,3).^2);

% Ponto da face 2 mais distante do node1:

[dist inodeout]=sort(norma_dist);

inodeout1=inodeout(3)+3;
    
% PRIMEIRA TETANTIVA: distancias do node2 aos pontos da face 2:

nodetent=2;  % tentative node

xnode=zeros(3,3);
xnode(:,1)=xnode(:,1)+coords(face1(nodetent),1); % coordenadas do noh da face oposta do elemento de interface
xnode(:,2)=xnode(:,2)+coords(face1(nodetent),2); % coordenadas do noh da face oposta do elemento de interface
xnode(:,3)=xnode(:,3)+coords(face1(nodetent),3); % coordenadas do noh da face oposta do elemento de interface

dist=(coords(face2,:)-xnode);

norma_dist=sqrt(dist(:,1).^2+dist(:,2).^2+dist(:,3).^2);

% Ponto da face 2 mais proximo do node2:

[dist inodeout]=sort(norma_dist);

inodeout2=inodeout(1)+3;

inodelist=[1 nodetent inodeout1 inodeout2];

if (inodeout1==inodeout2)
       
% SEGUNDA TETANTIVA: distancias do node3 aos pontos da face 2:

  nodetent=3;  % tentative node
  
  xnode=zeros(3,3);
  xnode(:,1)=xnode(:,1)+coords(face1(nodetent),1); % coordenadas do noh da face oposta do elemento de interface
  xnode(:,2)=xnode(:,2)+coords(face1(nodetent),2); % coordenadas do noh da face oposta do elemento de interface
  xnode(:,3)=xnode(:,3)+coords(face1(nodetent),3); % coordenadas do noh da face oposta do elemento de interface

  dist=(coords(face2,:)-xnode);

  norma_dist=sqrt(dist(:,1).^2+dist(:,2).^2+dist(:,3).^2);

  % Ponto da face 2 mais proximo do node3:

  [dist inodeout]=sort(norma_dist);

  inodeout2=inodeout(1)+3;    

  inodelist=[1 inodeout1 nodetent inodeout2];
    
end

end
       
