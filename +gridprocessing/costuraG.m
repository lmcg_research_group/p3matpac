function G =costuraG(etype,G1,X1,Y1,Z1,G2,X2,Y2,Z2,matlist1costura,newmatcompat,...
                     extra_node_bcond_mech,extra_node_bcond_flow)
%Construct 2d or 3d sewn grid (only implemented for tri,tetra,qua and hexa).
%
% SYNOPSIS:
%   G = gridprocessing.costura(etype,coords1,lnods1,mat1,bcond1,X1,Y1,Z1,...
%                              coords2,lnods2,mat2,bcond2,X2,Y2,Z2, ...
%                              matlist1costura,newmatcompat,
%                              extra_node_bcond_mech,extra_node_bcond_flow)
%
% PARAMETERS:
%   etype                 -  coupled element type (101 -> triangle or tetrahedron
%                                                  102 -> quadrilateral)
%   GN                    - Grid structure (N =1: coarse mesh; =2: fine mesh)
%   XN,YN,ZN              - Are the coordinates to shift N-th mesh
%   matlist1costura       - material list along coarse mesh "to seaw" fine mesh
%   newmatcompat          - material/Group ID for coupled elements that will be created
%   extra_node_bcond_mech - mechanical BC flag to assign on coupled nodes (extra node that belongs to coupled elements)
%   extra_node_bcond_flow - flux BC flag to assign on coupled nodes (extra node that belongs to coupled elements)
%
% RETURNS:
%   G - "sewn" Grid structure mostly as detailed in grid_structure
%
% EXAMPLE:
%   % Make a 10-by-5 grid on the unit square.
%   nx = 10; ny = 5;
%   G1 = gridprocessing.triangleGrid([nx, ny], [1, 1]);
%   X1 = 0.; Y1 = 0.; Z1 = 0.;  % not shift G1 mesh
%   % Make a 10-by-5 grid on the unit square.
%   nx = 10; ny = 10*ny;
%   G2 = gridprocessing.triangleGrid([nx, ny], [1, 1]);
%   X2 = 1; Y2 = 0.; Z2 = 0.;  % shift G2 mesh
%   % seaw meshes
%   etype = 101; % because meshes are triangular!!
%   matlist1costura = 1; newmatcompat=2;
%   G = gridprocessing.costuraG(etype,G1,X1,Y1,Z1,G2,X2,Y2,Z2,matlist1costura,...
%       newmatcompat,4,4);
%   iotools.plotGrid(G)
%
% SEE ALSO:
%   grid_structure, getGridInfo, plotGrid, triangleGrid, tetrahedralGrid, cartGrid
%

assert(isstruct(G1),...
    'G1 argument must be a struct variable see help gridprocessing.grid_structure.');
assert(isstruct(G2),...
    'G2 argument must be a struct variable see help gridprocessing.grid_structure.');

coords1 = G1.nodes.coords;
bcond1  = [G1.nodes.bcmech, G1.nodes.bcflux];
if any(G1.cells.type>100)
    lnods1  = G1.cells.lnods(:,1:max(G1.cells.nnel)+1);
else
    lnods1  = G1.cells.lnods(:,1:max(G1.cells.nnel));
end
mat1    = G1.cells.mat;

coords2 = G2.nodes.coords;
bcond2  = [G2.nodes.bcmech, G2.nodes.bcflux];
if any(G2.cells.type>100)
    lnods2  = G2.cells.lnods(:,1:max(G2.cells.nnel)+1);
else
    lnods2  = G2.cells.lnods(:,1:max(G2.cells.nnel));
end
mat2    = G2.cells.mat;

[nn1 ,ndim]=size(coords1);
[nn2 ,ndim]=size(coords2);
[nel1,npe1]=size(lnods1);
[nel2,npe2]=size(lnods2);

disp(['-> ',mfilename])

disp('merge meshes...')

if ndim==2
  if etype==101, npe=3; ltype = 1; end            % triangulo
  if etype==102, npe=4; ltype = 5; end            % quadrilatero
elseif ndim==3
  if etype==101, npe=4; ltype = 1; end            % tetraedro
  if etype==102, npe=8; ltype = 3; end            % hexaedro
end

for el=1:nel1
  if npe==npe1
    lnods1(el,npe+1)=0;
  end
end

for el=1:nel2
  lnods2(el,1:npe)=lnods2(el,1:npe)+nn1*ones(1,npe);
  if npe==npe2
    lnods2(el,npe+1)=0;
  elseif lnods2(el,npe+1)>0
    lnods2(el,npe+1)=lnods2(el,npe+1)+nn1;
  end
end

if ndim==2
  coords1=coords1+[X1*ones(nn1,1) Y1*ones(nn1,1)];
  coords2=coords2+[X2*ones(nn2,1) Y2*ones(nn2,1)];
elseif ndim==3
  coords1=coords1+[X1*ones(nn1,1) Y1*ones(nn1,1) Z1*ones(nn1,1)];
  coords2=coords2+[X2*ones(nn2,1) Y2*ones(nn2,1) Z2*ones(nn2,1)];
end
coords=[coords1;
        coords2];

lnods=[lnods1;
       lnods2];

mat=[mat1;
     mat2];

bcond=[bcond1;
       bcond2];

X=coords2(:,1);
Y=coords2(:,2);
if ndim==3, Z=coords2(:,3); end

node_index=1:nn2;

disp('creating coupled elements...')

nodes=[];
elements=[];
for el=1:nel1

  lia_mat=ismember(mat1(el),matlist1costura);

  if (lia_mat>0)

    xv=coords1(lnods1(el,1:npe),1);
    yv=coords1(lnods1(el,1:npe),2);

    if ndim==2
      in=inpolygon(X,Y,xv,yv);
      if sum(in)>0
       new_nodes=node_index(in);
       lia=ismember(new_nodes,nodes);
       for i=1:size(new_nodes,2)
         if lia(i)==0
           nodes=[nodes new_nodes(i)];
           elements=[elements el];
         end
       end
      end
    elseif ndim==3
      zv=coords1(lnods1(el,1:npe),3);
      vert=[xv yv zv];
      tess = convhulln(vert);
      [points in] = inConvHull(vert,tess,[X Y Z]);
      if sum(in)>0
       new_nodes=node_index(in);
       lia=ismember(new_nodes,nodes);
       for i=1:size(new_nodes,2)
         if lia(i)==0
           nodes=[nodes new_nodes(i)];
           elements=[elements el];
         end
       end
      end
    end

  end

end

for i=1:size(nodes,2)

    k=1;
    for j=1:npe
      if lnods(elements(i),j)>0
        lnods(nel1+nel2+i,j)=lnods(elements(i),j);
        k=k+1;
      end
    end
    lnods(nel1+nel2+i,k)=nn1+nodes(i);

    mat(nel1+nel2+i)=newmatcompat;

    bcond(nn1+nodes(i),1)=extra_node_bcond_mech;
    bcond(nn1+nodes(i),2)=extra_node_bcond_flow;

end

disp('building output...')
nn = 1:length(coords); nel = 1:length(mat); elm = ltype*ones(length(mat),1); elm(lnods(:,end)~=0)=etype;
G = gridprocessing.getGridInfo([nn',coords,bcond],[nel',mat,elm,lnods]);

disp('Done!')



