function G = cartGrid(celldim, varargin)
%Construct 2d or 3d Cartesian grid (based in quadrilateral or hexahedron).
%
% SYNOPSIS:
%   G = gridprocessing.cartGrid(celldim);
%   G = gridprocessing.cartGrid(celldim, physdim);
%
% PARAMETERS:
%   celldim  - Vector, length 2 or 3, specifying number of
%              cells in each coordinate direction.
%   physdim  - Vector, length numel(celldim), of physical size in units of
%              meters of the computational domain.
%              OPTIONAL.  Default value == celldim
%              (i.e., each cell has physical dimension 1-by-1-by-1 m).
%
% RETURNS:
%   G - Grid structure mostly as detailed in help gridprocessing.grid_structure
%
% EXAMPLE:
%   % Make a 10-by-5-by-2 grid on the unit cube.
%      nx = 10; ny = 5; nz = 2;
%      G = gridprocessing.cartGrid([nx, ny, nz], [1, 1, 1]);
%
%   % Plot the grid in 3D-view.
%      iotools.plotGrid(G); view(3);
%
% SEE ALSO:
%   grid_structure, evalGrid
%

if ~all(celldim > 0),
   error('CELLDIM must be positive');
end

physdim = celldim;
if nargin > 1 && isnumeric(varargin{1}),
   physdim  = varargin{1};
   varargin = varargin(2 : end);
end

x = linspace(0, physdim(1), celldim(1)+1);
y = linspace(0, physdim(2), celldim(2)+1);
if numel(celldim) == 3,
   z = linspace(0, physdim(3), celldim(3)+1);
   G = gridprocessing.evalGrid(x, y, z, varargin{:});
else
   G = gridprocessing.evalGrid(x, y, varargin{:});
end


% Record grid constructur in grid.
G.type = [G.type, { mfilename }];
end
