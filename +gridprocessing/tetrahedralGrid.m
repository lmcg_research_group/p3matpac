function G = tetrahedralGrid(p, varargin)
%Construct valid grid definition from points and tetrahedron list
%
% SYNOPSIS:
%   G = gridprocessing.tetrahedralGrid(P)
%   G = gridprocessing.tetrahedralGrid(P, T)
%
% PARAMETERS:
%   P - Node coordinates.  Must be an m-by-3 matrix, one row for each
%       node/point.
%
%   T - Tetrahedron list (point tesselation): an n-by-4 matrix where each
%       row holds node numbers for a tetrahedron.
%
%       OPTIONAL.  Default value: T = DELAUNAY3(P(:,1), P(:,2), P(:,3))
%
% RETURNS:
%   G  - Valid grid definition.
%
% EXAMPLE:
%   % Make a 20-by-10-by-2 grid on the unit cube.
%   x = linspace(0, 1, 20+1);
%   y = linspace(0, 1, 10+1);
%   z = linspace(0, 1, 2+1);
%   [xCoord, yCoord, zCoord] = ndgrid(x, y, z);
%   p = [xCoord(:), yCoord(:), zCoord(:)];
%   G = gridprocessing.tetrahedralGrid(p);
%
% SEE ALSO:
%   triangleGrid, grid_structure, cartGrid
%
%

   assert (size(p, 2) == 3, ...
          ['Function ''%s'' is only supported in three ', ...
           'space dimensions.'], mfilename);

    progress = textprogressbar(3,'showremtime', true, ...
                             'startmsg','building mesh         ');
   t = tesselate(p, varargin{:});

   % This must be implemented to make code robust wrt t.
%    t = permuteNodes(t, p);

   numC   = size(t, 1);
   cNodes = [t, zeros(numC,4)];

   numN   = size(p,1);
   progress(1);

   %--------------------------------------------------------------------------
   % Assemble structure -----------------------------------------------------

   idx = 1:numN;
   nodes = [idx',p,zeros(numN,2)];
   progress(2);

   idx = 1:numC;
   lnods = [idx',ones(numC,2),cNodes];
   progress(3);

   G = gridprocessing.getGridInfo(nodes, lnods);

    % Record grid constructur in grid.
    G.type = [G.type, { mfilename }];

end

%--------------------------------------------------------------------------

function t = tesselate(p, varargin)
   if nargin < 2,
      t = delaunay(p(:,1), p(:,2), p(:,3));

   elseif isnumeric(varargin{1}) && (size(varargin{1}, 2) ==4),
      t = varargin{1};

      assert (all(max(t) <= size(p, 1)), ...
              'Tetrahedron list ''T'' references invalid points.');

      assert (all(min(t) > 0), ...
              'Tetrahedron list ''T'' references invalid points.');

   else
      error(['Input parameter ''T'' does not appear to be a valid ', ...
             'tesselation of the points ''P''.']);
   end
end

%--------------------------------------------------------------------------

function t = permuteNodes(t, p)
%T = permuteNodes(T,P)
%
%  permute nodes in tetrahedra such that the triple product
%  p(t(:,2:4),:)-p(t(:,1),:) (i.e., the volume) is positive.

   v = tripleProduct(t, p);
   t(v>0,1:2) = t(v>0, [2,1]);
end

%--------------------------------------------------------------------------

function v = tripleProduct(t, p)
%
% Compute triple product (v2 x v3)·v4 where vi = p(t(:,i),:)-p(t(:,1),:).

   x = p(:,1); y = p(:,2); z = p(:,3);
   X = x(t);   Y = y(t);   Z = z(t);

   dx = bsxfun(@minus, X(:, 2:4), X(:,1));
   dy = bsxfun(@minus, Y(:, 2:4), Y(:,1));
   dz = bsxfun(@minus, Z(:, 2:4), Z(:,1));

   v= dot(cross([dx(:,1), dy(:,1), dz(:,1)], ...
             [dx(:,2), dy(:,2), dz(:,2)]), ...
             [dx(:,3), dy(:,3), dz(:,3)], 2);
end
