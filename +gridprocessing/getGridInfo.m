function G = getGridInfo (coord, lnods)
%This function get grid information.
%
% SYNOPSIS
%   G = gridprocessing.getGridInfo ()
%   G = gridprocessing.getGridInfo (coord, lnods)
%
% PARAMETERS
%   coords - Node coordinates.  Must be an m-by-(3+problem dimension) matrix, one row for
%            each node/point.
%   lnods  - Connectivity list: an n-by-9 matrix for 2D problem or an n-by-11 matrix for 3D
%            problem or where each row holds node numbers for a elements.
%
% RETURNS
%   G - Grid structure mostly as detailed in grid_structure
%
% EXAMPLE
%   1)>> G = gridprocessing.getGridInfo ()      % if you don't have coords and lnods files
%
%   2)>> G = gridprocessing.getGridInfo (coord, lnods)
%
%   3)>> import gridprocessing.*
%     >> G = getGridInfo ();        % Again... if you don't have coords and lnods files
%
%   4)>> import gridprocessing.*
%     >> G = getGridInfo (coord, lnods);
%
% SEE ALSO
%   grid_structure, GCLv3

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%
%  Programmer
%  ==========
%  Jonathan C. Teixeira
%
%  Creation
%  ========
%  Aug. 13, 2012
%
%  Modification
%  ============
%  ....
%  please when some programmer modify this script put you name, date of addiction
%  or modification statement and describe your modification/addiction like this:
%   * Tommy Hilfiger, Fri Oct 26 2012
%      Add --cflag, --cxxflag.  These *append* a specific flag.
%      Getting '--cflags' to quote reliably is a nightmare.
%
%}
    
    disp(['-> ',mfilename]);
    switch ( nargin )
        case 0
            gridprocessing.GCLv3;

            curdir = pwd;
            file = strcat(pathname,filesep,root{1},'.coords');
            nodes = load(file);
            if (pwd ~= curdir)
                movefile(file,curdir);
            end

            file = strcat(pathname,filesep,root{1},'.lnods');
            cells = load(file);
            if (pwd ~= curdir)
                movefile(file,curdir);
            end

        case 1
            G = load(coord);
            return
            
        case 2
            nodes = coord;
            cells = lnods;
            
        otherwise
            error('gridprocessing:getGridInfo:argchk',...
                '***ERROR: input incompatibility');
            G.info = -1;
            return;
    end


    % ----- coordinates
    [nn, nodecol] = size(nodes);
    numN    = nn;                      % number of global nodes (vertices) in grid.
    griddim = nodecol-3;               % dimension of grid
    coords  = nodes(:,2:(nodecol-2));  % node list.
    bcmech  = nodes(:,nodecol-1);      % list of mechanical boundary cond.
    bcflux  = nodes(:,nodecol);        % list of hydrodynamic boundary cond.

    % ----- Connectivity
    [nel, cellcol] = size(cells);
    numC   = nel;                       % number of cells in global grid.
    nummat = max(cells(:,2));           % Number of materials grid.
    mat    = cells(:,2);                % list of materials in global grid.
    ctype  = cells(:,3);                % list of element type indexes;
    nnel   = zeros(numC,1);             % number of nodes on cells


    %............................................ list of elements nodes.
    if (griddim==2)
        if (cellcol==9)
            lnods  = cells(:,4:end);
        else
            add = zeros(numC,9-cellcol);
            lnods  = [cells(:,4:end),add(:,1:end)];
        end
    elseif (griddim==3)
        if (cellcol==11)
            lnods  = cells(:,4:end);
        else
            add = zeros(numC,11-cellcol);
            lnods  = [cells(:,4:end),add(:,1:end)];
        end
    end
     
    if nel<5000000
        progress = textprogressbar(nel,'showremtime', true, ...
                                 'startmsg','validate mesh elements');
    else
        progress = @(i) (fprintf('\b\b\b\b\b\b%5.2f%%',i/nel*100));
        fprintf('validate mesh elements...  00.0%%');
    end

    % ----- Compute volumes and centroids
    cellVolumes   = zeros(nel,1);
    cellCentroids = zeros(nel,griddim);
    if griddim == 2,
        for i=1:nel
            ltype = ctype(i);
            switch ltype
                case 1                  % linear triangle
                    nnel(i)         = 3;
                    if ispolycw(coords(lnods(i,1:3),1),coords(lnods(i,1:3),2))
                        lnods(i,1:3) = lnods(i,[2,1,3]);
                    end
                    cellVolumes(i) = tri_volume(coords(lnods(i,1:3),:));
                case {5}                % linear quadrilateral
                    nnel(i) = 4;
                    if ispolycw(coords(lnods(i,1:4),1),coords(lnods(i,1:4),2))
                        lnods(i,1:4) = lnods(i,[3,2,1,4]);
                    end
                    cellVolumes(i) = quad_volume(coords(lnods(i,1:4),:));
                case 8                  % linear segment(line 2-nodes)
                    nnel(i) = 2;
                    cellVolumes(i)  = line_volume(coords(lnods(i,1:2),:));
                case 12                 % quadratic triangle
                    nnel(i) = 6;
                    if ispolycw(coords(lnods(i,1:3),1),coords(lnods(i,1:3),2))
                        lnods(i,1:6) = lnods(i,[2,1,3,4,6,5]);
                    end
                    cellVolumes(i)  = tri_volume(coords(lnods(i,1:3),:));
                case 101    % compatibility element
                    nnel(i) = 3;        % linear triangle [compatibility elm]
                    if ispolycw(coords(lnods(i,1:3),1),coords(lnods(i,1:3),2))
                        lnods(i,1:3) = lnods(i,[2,1,3]);
                    end
                    cellVolumes(i)  = tri_volume(coords(lnods(i,1:3),:));
                case 102    % compatibility element
                    if ispolycw(coords(lnods(i,1:4),1),coords(lnods(i,1:4),2))
                        lnods(i,1:4) = lnods(i,[3,2,1,4]);
                    end
                    nnel(i) = 4;        % linear quadrilateral [compatibility elm]
                    cellVolumes(i)  = quad_volume(coords(lnods(i,1:4),:));
                otherwise
                    progress(nel);
                    error(msbtrace(strcat(':UnsuportedElement',...
                        sprintf('\n***ERROR: ltype on lnods block not supported(iel=%d)',i))))
            end
            if (cellVolumes(i)<0)
                progress(nel);
                error(msbtrace(strcat(':NegativeVolume',sprintf(...
                    '\n***ERROR:Negative volume computed(iel=%d)=%f',i,cellVolumes(i)))));
            end
            xmean = mean(coords(lnods(i,1:nnel(i)),1));
            ymean = mean(coords(lnods(i,1:nnel(i)),2));
            cellCentroids(i,:) = [xmean, ymean];
            progress(i);
        end
    else
        for i=1:nel
            ltype = ctype(i);
            switch ltype
                case 1                  % linear tetrahedron
                    nnel(i) = 4;
                    cellVolumes(i) = tet_volume(coords(lnods(i,1:4),:));
                case 3                  % linear hexahedron
                    nnel(i) = 8;
                    cellVolumes(i) = hex_volume(coords(lnods(i,1:8),:));
                    if cellVolumes(i)<0
                        lnods(i,[3,2,1,4,7,6,5,8]) = lnods(i,:);
                        cellVolumes(i) = hex_volume(coords(lnods(i,1:8),:));
                    end
                case 8                  % linear segment(line 2-nodes)
                    nnel(i) = 2;
                    cellVolumes(i)  = line_volume(coords(lnods(i,1:2),:));
                case 101                % compatibility element
                    nnel(i) = 4;
                    cellVolumes(i)  = tet_volume(coords(lnods(i,1:4),:));
                case 102                  % compatibility element
                    nnel(i) = 8;
                    cellVolumes(i) = hex_volume(coords(lnods(i,1:8),:));
                otherwise
                    progress(nel);
                    error(msbtrace(strcat(':UnsuportedElement',...
                        sprintf('\n***ERROR: ltype on lnods block not supported(iel=%d)',i))))
            end
            if (cellVolumes(i)<0),
                progress(nel);
                error(msbtrace(strcat(':NegativeVolume',sprintf(...
                    '\n***ERROR:Negative volume computed(iel=%d)= %f',i,cellVolumes(i)))));
            end
            xmean = mean(coords(lnods(i,1:nnel(i)),1));
            ymean = mean(coords(lnods(i,1:nnel(i)),2));
            zmean = mean(coords(lnods(i,1:nnel(i)),3));
            cellCentroids(i,:) = [xmean, ymean, zmean];
            progress(i);
        end
    end

    G.nodes = struct('num',         numN, ...
                     'dim',      griddim, ...
                     'coords',    coords, ...
                     'bcflux',    bcflux, ...
                     'bcmech',    bcmech );

    G.cells = struct('num',               numC, ...
                    'nummat',           nummat, ...
                    'mat',                 mat, ...
                    'type',              ctype, ...
                    'lnods',             lnods, ...
                    'nnel',               nnel, ...
                    'centroids', cellCentroids, ...
                    'volumes',     cellVolumes);

    G.type = [{ mfilename }];
end



% ------------------------------------------------------------------------
% Local functions
%

%-------------------------------------------------------------------------
% compute volume of line element;
function vol = line_volume(xy)
    [~,d]=size(xy);
    if d==2
        vol = norm([diff(xy(:,1)), diff(xy(:,2))]);
    else
        vol = norm([diff(xy(:,1)), diff(xy(:,2)), diff(xy(:,3))]);
    end
end

%--------------------------------------------------------------------------

function volelm = tri_volume(xy)

x1 = xy(1,1); y1 = xy(1,2);
x2 = xy(2,1); y2 = xy(2,2);
x3 = xy(3,1); y3 = xy(3,2);

a1=+x2*y3-x3*y2;
a2=-x1*y3+x3*y1;
a3=+x1*y2-x2*y1;

x=(a1+a2+a3);
volelm=x/2;

end

%--------------------------------------------------------------------------

function volelm = quad_volume(xy)

volelm = 0;
x1 = xy(1,1); y1 = xy(1,2);
x2 = xy(2,1); y2 = xy(2,2);
x3 = xy(3,1); y3 = xy(3,2);
x4 = xy(4,1); y4 = xy(4,2);

chi1=-1; eta1=-1; chi2=+1; eta2=-1;
chi3=+1; eta3=+1; chi4=-1; eta4=+1;

chi01=-.577350269189626;             % 4 integration points
eta01=-.577350269189626;
chi02=+.577350269189626;
eta02=-.577350269189626;
chi03=+.577350269189626;
eta03=+.577350269189626;
chi04=-.577350269189626;
eta04=+.577350269189626;

nval=4;
weight=1.0;

for ival=1:nval
    if (ival==1)
       chi=chi01;
       eta=eta01;
    elseif (ival==2)
       chi=chi02;
       eta=eta02;
    elseif (ival==3)
       chi=chi03;
       eta=eta03;
    elseif (ival==4)
       chi=chi04;
       eta=eta04;
    end
    % local variable derivatives at gaus point
    dn1dchi=chi1*(1.d0+eta*eta1)/4;
    dn1deta=eta1*(1.d0+chi*chi1)/4;
    dn2dchi=chi2*(1.d0+eta*eta2)/4;
    dn2deta=eta2*(1.d0+chi*chi2)/4;
    dn3dchi=chi3*(1.d0+eta*eta3)/4;
    dn3deta=eta3*(1.d0+chi*chi3)/4;
    dn4dchi=chi4*(1.d0+eta*eta4)/4;
    dn4deta=eta4*(1.d0+chi*chi4)/4;

    a11= x1*dn1dchi+x2*dn2dchi+x3*dn3dchi+x4*dn4dchi;     % dx/dchi
    a12= y1*dn1dchi+y2*dn2dchi+y3*dn3dchi+y4*dn4dchi;     % dy/dchi
    a21= x1*dn1deta+x2*dn2deta+x3*dn3deta+x4*dn4deta;     % dx/deta
    a22= y1*dn1deta+y2*dn2deta+y3*dn3deta+y4*dn4deta;     % dy/deta

    det=a11*a22-a12*a21;
    volelm = volelm + det*weight;
end
end

%--------------------------------------------------------------------------

function volelm = tet_volume(xy)

volelm = 0;
x1 = xy(1,1); y1 = xy(1,2); z1 = xy(1,3);
x2 = xy(2,1); y2 = xy(2,2); z2 = xy(2,3);
x3 = xy(3,1); y3 = xy(3,2); z3 = xy(3,3);
x4 = xy(4,1); y4 = xy(4,2); z4 = xy(4,3);

b1=-(y3*z4-y4*z3)+(y2*z4-y4*z2)-(y2*z3-y3*z2); %#ok<*NASGU>
b2=+(y3*z4-y4*z3)-(y1*z4-y4*z1)+(y1*z3-y3*z1);
b3=-(y2*z4-y4*z2)+(y1*z4-y4*z1)-(y1*z2-y2*z1);
b4=+(y2*z3-y3*z2)-(y1*z3-y3*z1)+(y1*z2-y2*z1);

c1=+(x3*z4-x4*z3)-(x2*z4-x4*z2)+(x2*z3-x3*z2);
c2=-(x3*z4-x4*z3)+(x1*z4-x4*z1)-(x1*z3-x3*z1);
c3=+(x2*z4-x4*z2)-(x1*z4-x4*z1)+(x1*z2-x2*z1);
c4=-(x2*z3-x3*z2)+(x1*z3-x3*z1)-(x1*z2-x2*z1);

d1=-(x3*y4-x4*y3)+(x2*y4-x4*y2)-(x2*y3-x3*y2);
d2=+(x3*y4-x4*y3)-(x1*y4-x4*y1)+(x1*y3-x3*y1);
d3=-(x2*y4-x4*y2)+(x1*y4-x4*y1)-(x1*y2-x2*y1);
d4=+(x2*y3-x3*y2)-(x1*y3-x3*y1)+(x1*y2-x2*y1);

a1= x2*y3*z4+x3*y4*z2+x4*y2*z3-x4*y3*z2-x3*y2*z4-x2*y4*z3;
a2=-x1*y3*z4-x3*y4*z1-x4*y1*z3+x4*y3*z1+x3*y1*z4+x1*y4*z3;
a3=+x1*y2*z4+x2*y4*z1+x4*y1*z2-x4*y2*z1-x2*y1*z4-x1*y4*z2;
a4=-x1*y2*z3-x2*y3*z1-x3*y1*z2 +x3*y2*z1+x2*y1*z3+x1*y3*z2;

x=(a1+a2+a3+a4); volelm=x/6.0;

end

%--------------------------------------------------------------------------

function volelm = hex_volume(xy)

volelm = 0;
x1 = xy(1,1); y1 = xy(1,2); z1 = xy(1,3);
x2 = xy(2,1); y2 = xy(2,2); z2 = xy(2,3);
x3 = xy(3,1); y3 = xy(3,2); z3 = xy(3,3);
x4 = xy(4,1); y4 = xy(4,2); z4 = xy(4,3);
x5 = xy(5,1); y5 = xy(5,2); z5 = xy(5,3);
x6 = xy(6,1); y6 = xy(6,2); z6 = xy(6,3);
x7 = xy(7,1); y7 = xy(7,2); z7 = xy(7,3);
x8 = xy(8,1); y8 = xy(8,2); z8 = xy(8,3);

chi1=-1.d0;
eta1=-1.d0;
tet1=-1.d0;

chi2=+1.d0;
eta2=-1.d0;
tet2=-1.d0;

chi3=+1.d0;
eta3=+1.d0;
tet3=-1.d0;

chi4=-1.d0;
eta4=+1.d0;
tet4=-1.d0;

chi5=-1.d0;
eta5=-1.d0;
tet5=+1.d0;

chi6=+1.d0;
eta6=-1.d0;
tet6=+1.d0;

chi7=+1.d0;
eta7=+1.d0;
tet7=+1.d0;

chi8=-1.d0;
eta8=+1.d0;
tet8=+1.d0;

chi01=-.577350269189626;             % 8 integration points
eta01=-.577350269189626;
tet01=-.577350269189626;
chi02=+.577350269189626;
eta02=-.577350269189626;
tet02=-.577350269189626;
chi03=+.577350269189626;
eta03=+.577350269189626;
tet03=-.577350269189626;
chi04=-.577350269189626;
eta04=+.577350269189626;
tet04=-.577350269189626;

chi05=-.577350269189626;
eta05=-.577350269189626;
tet05=+.577350269189626;
chi06=+.577350269189626;
eta06=-.577350269189626;
tet06=+.577350269189626;
chi07=+.577350269189626;
eta07=+.577350269189626;
tet07=+.577350269189626;
chi08=-.577350269189626;
eta08=+.577350269189626;
tet08=+.577350269189626;

nval=8;
weight=1.0;

for ival=1:nval
    switch (ival)
        case (1)
            chi=chi01;
            eta=eta01;
            tet=tet01;
        case (2)
            chi=chi02;
            eta=eta02;
            tet=tet02;
        case (3)
            chi=chi03;
            eta=eta03;
            tet=tet03;
        case (4)
            chi=chi04;
            eta=eta04;
            tet=tet04;
        case (5)
            chi=chi05;
            eta=eta05;
            tet=tet05;
        case (6)
            chi=chi06;
            eta=eta06;
            tet=tet06;
        case (7)
            chi=chi07;
            eta=eta07;
            tet=tet07;
        case (8)
            chi=chi08;
            eta=eta08;
            tet=tet08;
    end
    dn1dchi=chi1*(1.d0+eta*eta1)*(1.d0+tet*tet1)/8.;
    dn1deta=eta1*(1.d0+chi*chi1)*(1.d0+tet*tet1)/8.;
    dn1dtet=tet1*(1.d0+chi*chi1)*(1.d0+eta*eta1)/8.;
    dn2dchi=chi2*(1.d0+eta*eta2)*(1.d0+tet*tet2)/8.;
    dn2deta=eta2*(1.d0+chi*chi2)*(1.d0+tet*tet2)/8.;
    dn2dtet=tet2*(1.d0+chi*chi2)*(1.d0+eta*eta2)/8.;
    dn3dchi=chi3*(1.d0+eta*eta3)*(1.d0+tet*tet3)/8.;
    dn3deta=eta3*(1.d0+chi*chi3)*(1.d0+tet*tet3)/8.;
    dn3dtet=tet3*(1.d0+chi*chi3)*(1.d0+eta*eta3)/8.;
    dn4dchi=chi4*(1.d0+eta*eta4)*(1.d0+tet*tet4)/8.;
    dn4deta=eta4*(1.d0+chi*chi4)*(1.d0+tet*tet4)/8.;
    dn4dtet=tet4*(1.d0+chi*chi4)*(1.d0+eta*eta4)/8.;
    dn5dchi=chi5*(1.d0+eta*eta5)*(1.d0+tet*tet5)/8.;
    dn5deta=eta5*(1.d0+chi*chi5)*(1.d0+tet*tet5)/8.;
    dn5dtet=tet5*(1.d0+chi*chi5)*(1.d0+eta*eta5)/8.;
    dn6dchi=chi6*(1.d0+eta*eta6)*(1.d0+tet*tet6)/8.;
    dn6deta=eta6*(1.d0+chi*chi6)*(1.d0+tet*tet6)/8.;
    dn6dtet=tet6*(1.d0+chi*chi6)*(1.d0+eta*eta6)/8.;
    dn7dchi=chi7*(1.d0+eta*eta7)*(1.d0+tet*tet7)/8.;
    dn7deta=eta7*(1.d0+chi*chi7)*(1.d0+tet*tet7)/8.;
    dn7dtet=tet7*(1.d0+chi*chi7)*(1.d0+eta*eta7)/8.;
    dn8dchi=chi8*(1.d0+eta*eta8)*(1.d0+tet*tet8)/8.;
    dn8deta=eta8*(1.d0+chi*chi8)*(1.d0+tet*tet8)/8.;
    dn8dtet=tet8*(1.d0+chi*chi8)*(1.d0+eta*eta8)/8.;

    a11= x1*dn1dchi+x2*dn2dchi+x3*dn3dchi+x4*dn4dchi+x5*dn5dchi+x6*dn6dchi...
        +x7*dn7dchi+x8*dn8dchi;
    a12= y1*dn1dchi+y2*dn2dchi+y3*dn3dchi+y4*dn4dchi+y5*dn5dchi+y6*dn6dchi...
        +y7*dn7dchi+y8*dn8dchi;
    a13= z1*dn1dchi+z2*dn2dchi+z3*dn3dchi+z4*dn4dchi+z5*dn5dchi+z6*dn6dchi...
        +z7*dn7dchi+z8*dn8dchi;
    a21= x1*dn1deta+x2*dn2deta+x3*dn3deta+x4*dn4deta+x5*dn5deta+x6*dn6deta...
        +x7*dn7deta+x8*dn8deta;
    a22= y1*dn1deta+y2*dn2deta+y3*dn3deta+y4*dn4deta+y5*dn5deta+y6*dn6deta...
        +y7*dn7deta+y8*dn8deta;
    a23= z1*dn1deta+z2*dn2deta+z3*dn3deta+z4*dn4deta+z5*dn5deta+z6*dn6deta...
        +z7*dn7deta+z8*dn8deta;
    a31= x1*dn1dtet+x2*dn2dtet+x3*dn3dtet+x4*dn4dtet+x5*dn5dtet+x6*dn6dtet...
        +x7*dn7dtet+x8*dn8dtet;
    a32= y1*dn1dtet+y2*dn2dtet+y3*dn3dtet+y4*dn4dtet+y5*dn5dtet+y6*dn6dtet...
        +y7*dn7dtet+y8*dn8dtet;
    a33= z1*dn1dtet+z2*dn2dtet+z3*dn3dtet+z4*dn4dtet+z5*dn5dtet+z6*dn6dtet...
        +z7*dn7dtet+z8*dn8dtet;


    det=a11*a22*a33+a12*a23*a31+a21*a32*a13-a13*a22*a31-a12*a21*a33-a23*a32*a11;
    b11= (a22*a33-a23*a32)/det;
    b12=-(a12*a33-a13*a32)/det;
    b13= (a12*a23-a13*a22)/det;
    b21=-(a21*a33-a23*a31)/det;
    b22= (a11*a33-a13*a31)/det;
    b23=-(a11*a23-a13*a21)/det;
    b31= (a21*a32-a22*a31)/det;
    b32=-(a11*a32-a12*a31)/det;
    b33= (a11*a22-a12*a21)/det;

    volelm=volelm+det*weight;
end
end


