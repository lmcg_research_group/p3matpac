function [coords lnods mat bcond]=costura(etype,                                ...   % tipo do elemento de compatibilidade no code_bright                     
                                          coords1,lnods1,mat1,bcond1,X1,Y1,Z1,  ...   % primeira malha (mais grossa)
                                          coords2,lnods2,mat2,bcond2,X2,Y2,Z2,  ...   % segunda malha  (mais fina)
                                          matlist1costura,                      ...   % lista de materiais da primeira malha onde a segunda malha será costurada
                                          newmatcompat,                         ...   % novo material a ser criado com elementos de compatibilidade
                                          extra_node_bcond_mech,                ...   % condicao de contorno de fluxo do noh extra do elemento de compatibilidade
                                          extra_node_bcond_flow                   )   % condicao de contorno de fluxo do noh extra do elemento de compatibilidade
%Construct 2d or 3d sewn grid.
%
% SYNOPSIS:
% [coords,lnods,mat,bcond]= gridprocessing.costura(etype,coords1,lnods1,mat1,bcond1,X1,Y1,Z1,...
%                              coords2,lnods2,mat2,bcond2,X2,Y2,Z2, ...
%                              matlist1costura,newmatcompat,
%                              extra_node_bcond_mech,extra_node_bcond_flow)
%
% PARAMETERS:
%   etype                 -  coupled element type (101 -> triangle or tetrahedron
%                                                  102 -> quadrilateral)
%   coordsN               - Node coordinates matrix.  Must be an m-by-problem dimension matrix,
%                           one row for each node/point (N =1: coarse mesh; =2: fine mesh)
%   lnodsN                - Connectivity list: an n-by-nnel matrix, where each row holds node
%                           numbers for a elements (nnel=3 for triangle; =4 for quadrilateral or
%                           tetrahedron)
%   matN                  - material lisr: an n-by-1 array, one row for each elements
%   bcondN                - boundary condition matrix, an n-by-2 matrix, first column for mechBC
%                           and second column holds fluxBC
%   XN,YN,ZN              - Are the coordinates to shift N-th mesh
%   matlist1costura       - material list along coarse mesh "to seaw" fine mesh
%   newmatcompat          - material/Group ID for coupled elements that will be created
%   extra_node_bcond_mech - mechanical BC flag to assign on coupled nodes (extra node that belongs to coupled elements)
%   extra_node_bcond_flow - flux BC flag to assign on coupled nodes (extra node that belongs to coupled elements)
%
% RETURNS:
%   coords - node coordinates matrix of sewn mesh. an m-by-problem dimension matrix.
%   lnods  - Connectivity list of sewn mesh: an n-by-nnel matrix, where each row holds node
%            numbers for a elements.
%   mat    - material list of sewn mesh: an n-by-1 array, one row for each elements
%   bcond  - boundary condition matrix of sewn mesh, an n-by-2 matrix, first column for mechBC
%            and second column holds fluxBC
%
% EXAMPLE:
%   % Make a 10-by-5 grid on the unit square.
%   nx = 10; ny = 5;
%   G1 = gridprocessing.triangleGrid([nx, ny], [1, 1]);
%   X1 = 0.; Y1 = 0.; Z1 = 0.;  % not shift G1 mesh
%   % Make a 10-by-5 grid on the unit square.
%   nx = 10; ny = 10*ny;
%   G2 = gridprocessing.triangleGrid([nx, ny], [1, 1]);
%   X2 = 1; Y2 = 0.; Z2 = 0.;  % shift G2 mesh
%   % seaw meshes
%   etype = 101; % because meshes are triangular!!
%   matlist1costura = 1; newmatcompat=2;
%   [coords, lnods, mat, bcond] = gridprocessing.costura(etype,G1.nodes.coords,G1.cells.lnods(:,1:3),...
%       G1.cells.mat,[G1.nodes.bcmech,G1.nodes.bcflux],X1,Y1,Z1,G2.nodes.coords,G2.cells.lnods(:,1:3),...
%       G2.cells.mat,[G2.nodes.bcmech,G2.nodes.bcflux],X2,Y2,Z2,matlist1costura,...
%       newmatcompat,4,4);
%   nn = 1:length(coords); nel = 1:length(mat); elm = ones(length(mat),1); elm(lnods(:,end)~=0)=etype;
%   G = gridprocessing.getGridInfo([nn',coords,bcond],[nel',mat,elm,lnods]);
%   iotools.plotGrid(G)
%
% SEE ALSO:
%   grid_structure, getGridInfo, plotGrid, triangleGrid, tetrahedralGrid, cartGrid
%
[nn1  ndim]=size(coords1);
[nn2  ndim]=size(coords2);
[nel1 npe1]=size(lnods1);
[nel2 npe2]=size(lnods2);

disp('COSTURA: ')

disp('concatena malhas...')

if ndim==2
  if etype==101, npe=3; end            % triangulo
  if etype==102, npe=4; end            % quadrilatero
elseif ndim==3
  if etype==101, npe=4; end            % tetraedro
  if etype==102, npe=8; end            % hexaedro
end

if npe == npe1
    lnods1 = [lnods1, zeros(nel1,1)];
end
for el=1:nel2
  lnods2(el,1:npe)=lnods2(el,1:npe)+nn1*ones(1,npe);
  if npe==npe2 
    lnods2(el,npe+1)=0; 
  elseif lnods2(el,npe+1)>0
    lnods2(el,npe+1)=lnods2(el,npe+1)+nn1; 
  end 
end

if ndim==2
  coords1=coords1+[X1*ones(nn1,1) Y1*ones(nn1,1)];
  coords2=coords2+[X2*ones(nn2,1) Y2*ones(nn2,1)];
elseif ndim==3
  coords1=coords1+[X1*ones(nn1,1) Y1*ones(nn1,1) Z1*ones(nn1,1)];
  coords2=coords2+[X2*ones(nn2,1) Y2*ones(nn2,1) Z2*ones(nn2,1)];
end
coords=[coords1;
        coords2];

lnods=[lnods1;
       lnods2];

mat=[mat1;
     mat2];

bcond=[bcond1;
       bcond2];

X=coords2(:,1);
Y=coords2(:,2);
if ndim==3, Z=coords2(:,3); end

node_index=1:nn2;

disp('cria elementos de compatibilidade...')

nodes=[];
elements=[];
for el=1:nel1

  lia_mat=ismember(mat1(el),matlist1costura);  

  if (lia_mat>0)

    xv=coords1(lnods1(el,1:npe),1);
    yv=coords1(lnods1(el,1:npe),2);

    if ndim==2
      in=inpolygon(X,Y,xv,yv);
      if sum(in)>0 
       new_nodes=node_index(in);
       lia=ismember(new_nodes,nodes);
       for i=1:size(new_nodes,2)
         if lia(i)==0 
           nodes=[nodes new_nodes(i)]; 
           elements=[elements el];
         end
       end
      end 
    elseif ndim==3
      zv=coords1(lnods1(el,1:npe),3);
      vert=[xv yv zv];
      tess = convhulln(vert);
      [points in] = inConvHull(vert,tess,[X Y Z]);
      if sum(in)>0 
       new_nodes=node_index(in);
       lia=ismember(new_nodes,nodes);
       for i=1:size(new_nodes,2)
         if lia(i)==0 
           nodes=[nodes new_nodes(i)]; 
           elements=[elements el];
         end
       end
      end 
    end

  end

end

for i=1:size(nodes,2)

    k=1;
    for j=1:npe
      if lnods(elements(i),j)>0
        lnods(nel1+nel2+i,j)=lnods(elements(i),j);
        k=k+1;
      end
    end 
    lnods(nel1+nel2+i,k)=nn1+nodes(i);

    mat(nel1+nel2+i)=newmatcompat;

    bcond(nn1+nodes(i),1)=extra_node_bcond_mech;
    bcond(nn1+nodes(i),2)=extra_node_bcond_flow;

end

disp('FIM.')

