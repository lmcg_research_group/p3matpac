function G = removeCells(G, cells)
%remove selected cells of grid structure
%
% SYNOPSIS
%  G = gridprocessing.removeCell(G, cells );
%
% PARAMETERS
%	G           - Grid structure mostly as detailed in grid_structure.
%   cells       - array or boolean array with cells to remove from grid 
%                 struture.
%
% RETURNS
%	G           - new grid structure without cells
%
% USAGE
%	 g = gridprocessing.removeCell(G, cells );;
%
% SEE ALSO
%   gridprocessing.grid_structure
%

%{
%  Important note
%  ==============
%  This software is under development and may present several bugs.
%
%  Permission to use
%  =================
%  Provided a suitable reference is made to this work, this software may
%  be freely used for personal noncommercial purposes.
%  All other forms of use are subject to prior arrangement with the author.
%
%  Disclaimer
%  ==========
%  The user of the software should note that, from the legal point of view,
%  there is no expressed or implied warranty that the software is free of
%  error or will prove suitable for a particular application.
%  By using the software the user accepts full responsibility for the results
%  produced, and the author disclaims all liability from any consequences
%  arising from use of the software.
%
%
%  Programmer
%  ==========
%  Jonathan C. Teixeira
%
%  Creation
%  ========
%  Jan. 31, 2019
%
%  Modification
%  ============
%  ....
%  please when some programmer modify this script put you name, date of addiction
%  or modification statement and describe your modification/addiction like this:
%   * Tommy Hilfiger, Fri Oct 26 2012
%      Add --cflag, --cxxflag.  These *append* a specific flag.
%      Getting '--cflags' to quote reliably is a nightmare.
%
%}

progress = textprogressbar( 4, ...
                'startmsg','removing invalid cells       ');
G.cells.mat(cells) = 0;
active = G.cells.mat > 0;

vnodes = G.cells.lnods(active,:);
progress(1);
% making uniques numering
vnodes = unique(reshape(vnodes,[numel(vnodes),1]));
idx = zeros(max(vnodes),1);
idx(vnodes,1) = 1:numel(vnodes);
progress(2);

n = 1:numel(vnodes);
nodes = [n', G.nodes.coords(vnodes,:),G.nodes.bcmech(vnodes), ...
        G.nodes.bcflux(vnodes)];
progress(3);

active = G.cells.mat > 0;
c = 1:sum(G.cells.mat);
cNodes = [c', G.cells.mat(active), G.cells.type(active), ...
          reshape(idx(G.cells.lnods(active,:)),[max(c),8])];
progress(4);
G = gridprocessing.getGridInfo(nodes, cNodes);
