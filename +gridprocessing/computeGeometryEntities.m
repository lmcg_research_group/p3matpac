function G = computeGeometryEntities(G, varargin)
% This function compute geometry structure of a mesh used for FV and MFEM
% discretization
%
% SYNOPSIS
%   Gn = gridprocessing.computeGeometryEntities(G)
%   Gn = gridprocessing.computeGeometryEntities(G,'pn',pv, ...)
%
% PARAMETERS
%   G       - Grid structure mostly as detailed in grid_structure
%
%   'pn'/pv - List of 'key'/value pairs for supplying optional parameters.
%             The supported options are
%               - verbose   -- Whether or not to display informational
%                            messages throughout the computational process.
%                            Logical.  Default value: Verbose = false
%                            (don't display any informational messages).
%
%               - addfaces  -- whether or not compute faces topology.
%                              logical. default value: addfaces = true
%                              (compute faces topology).
%
%               - addenlst  -- whether or not compute elements surround nodes
%                              topology. logical. default value:
%                              addenlst = false (don't compute elements
%                              surround node topology).
%
%               - addeelst  -- whether or not compute elements surround
%                              elements topology. logical. default value:
%                              addeelst = false (don't compute elements
%                              surround element topology).
%
%               - addfnlst  -- whether or not compute faces surround nodes 
%                              topology. logical. default value: 
%                              addfnlst = false (don't compute faces surround
%                              nodes topology).
%
%               - addfelst  -- whether or not compute faces surround elements
%                              logical. default value: addfelst = false
%                              (don't compute faces surround element topology).
%
%
% RETURNS:
%   Gn - Grid structure with added fields:
%%         - cells
%             - cellPos      -- Indirection map of size [G.cells.num+1,1] 
%                            into the 'cells.neigh' array. Specifically, the
%                            cells surrounding information of cell 'i' is
%                            found in the sub-matrix:
%
%                               G.cells.neigh(cellPos(i):cellPos(i+1))
%
%                            The number of faces of each cell may be computed
%                            using the statement DIFF(cellPos)
%
%             - neigh        -- A (G.cells.cellsMap(end)-1)-by-1 array of
%                            global cells connected to the given cell.
%
%             - facesPos     -- Indirection map of size [num+1,1] into the
%                            'cells.faces' array. Specifically, the face 
%                            information of cell 'i' is found in the sub-matrix
%
%                               G.cells.faces(facePos(i) : facePos(i+1)-1, :)
%
%                            The number of faces of each cell may be computed
%                            using the statement DIFF(facePos).
%
%             - faces        -- A (G.cells.facePos(end)-1)-by-1 array of global
%                            faces connected to a given cell.
%
%
%%         - faces
%             - num          -- A 2-by-1 array of number of internal and
%                            external faces in mesh. [ifaces, bfaces]
%
%             - nodes        -- A G.faces.num-by-order array of vertices in
%                            the grid. Specifically, if 'faces.nodes(i,1)==j',
%                            then local vertex `i' is part of global face
%                            number `j' and corresponds to global vertex
%                            'faces.nodes(i,2)'.  To conserve memory, only the
%                            last column is stored.  The first column can be
%                            constructed using the statement
%
%             - neighbors    -- A G.faces.num-by-2 array of neighboring info.
%                            Global face number `i' is shared by global cells
%                            'neighbors(i,1)' and 'neighbors(i,2)'.
%
%             - normals      -- A G.faces.num-by-G.nodes.dim array of face
%                            normals (versor).
%
%             - centroids -- A G.faces.num-by-SIZE(G.nodes.coords, 2) array
%                            of (approximate) face centroids.
%
%
%%         - nodes
%             - cellPos      -- Indirection map of size [nodes.num+1,1] into
%                            the nodes.cells array. Specifically, the cell 
%                            information of the i-th cell around 'j' node is
%                            found in the sub-matrix:
%
%                               G.nodes.cells(nodes.cellPos(j)+i-1)
%
%                            The number of faces of each cell may be computed
%                            using the statement DIFF(nodes.cellPos)
%
%             - cells        -- A (G.nodes.cellPos(end)-1)-by-1 array of global
%                            cells connected to a given nodes.
%
%             - facePos      -- Indirection map of size [nodes.num+1,1] into
%                            the nodes.faces array. Specifically, the face
%                            information of the i-th faces around 'j' node is
%                            found in the sub-matrix:
%
%                               G.nodes.faces(nodes.facePos(j)+i-1)
%
%                            The number of faces of each faces may be computed
%                            using the statement DIFF(nodes.facePos)
%
%             - faces        -- A (G.nodes.facePos(end)-1)-by-1 array of global
%                            faces connected to a given nodes.
%
%
% EXAMPLE
%   1)>> G2 = gridprocessing.computeGeometryEntities(G)
%
% SEE ALSO
%   grid_structure, ...
%
% DEVELOPED BY JCT
%  

%% Setup
build = struct('verbose' , false(1), ...
               'addFaces',  true(1), ...
               'addENlst', false(1), ...
               'addEElst', false(1), ...
               'addFNlst', false(1), ...
               'addFElst', false(1));
build = merge_options( build, varargin{:});

numC = G.cells.num;
numN = G.nodes.num;

ele    = zeros(numC+1,1);
ndglno = zeros(sum(G.cells.nnel),1);
iel = 0;
for i=1:numC
    ele(i) = iel+1; loc = G.cells.nnel(i);
    ndglno(iel+1:iel+loc) = G.cells.lnods(i,1:loc);
    iel = iel + loc;
end
ele(end) = iel+1;


%% compute basic geometry features
disp('-> computing Geometry');
% Note that the NEList and EEList are needed to calculate all lists.
loc   = sum(G.cells.nnel);


%% ----- Element-Node List
if (build.verbose), disp('* computing Element-Node list'); end
[no2el, n2e] = NODELE(numC, loc, ndglno, ele, numN);


%% ----- Element-Element list
if (build.verbose), disp('* computing Element-Element list'); end
% max. number of node on faces
mxface = 0;
if (G.nodes.dim==2)
    mnnel=max(G.cells.nnel);
    if (mnnel==4)
        mxface = 4;
    else
        mxface = 3;
    end
elseif (G.nodes.dim==3)
    mnnel=max(G.cells.nnel);
    if (mnnel==8)
        mxface = 6;
    else
        mxface = 4;
    end
end

el2el = elmelm( numC, loc, ndglno, ele, numN, no2el, n2e, mxface, G.nodes.dim);

% pointer for element-element list (ee)
ee  = ones(G.cells.num+1,1);
if (G.nodes.dim==2)
    ee(1:numC) = cumsum(G.cells.nnel); nel1 = ee(1);
    ee = ee - nel1+1; ee(end) = loc+1; ee(1) = 1;
elseif (G.nodes.dim ==3)
    ntet = length(find(G.cells.type==1));
    nhex = length(find(G.cells.type==3));
    loc = 4*ntet + 6*nhex;
    if (ntet==0)
        ee(:) = cumsum(6*ones(G.cells.num+1,1));
        nel1 = ee(1); ee = ee - nel1+1; ee(end) = loc+1; ee(1) = 1;
    else
        ee(:) = cumsum(4*ones(G.cells.num+1,1));
        nel1 = ee(1); ee = ee - nel1+1; ee(end) = loc+1; ee(1) = 1;
    end
end


% count number of boundary and internal faces and
% set e2e (el2el representation for saving memory purpose)
nbfaces = 0;
e2e = ones(loc,1); iel=0;
if (G.nodes.dim == 2)
    nofa = 2;
    for i=1:numC
        nface = ele(i+1)-ele(i);
        for iface=1:nface
            iel=iel+1; e2e(iel)=el2el(iface,i);
            if (el2el(iface,i)==0), nbfaces = nbfaces+1; end
        end
    end
elseif (G.nodes.dim == 3)
    for i=1:numC
        nface = ele(i+1)-ele(i); nofa = 3;
        % hexahedron has nface~=nnel
        if (nface==8)
            nface=6; nofa = 4;
        end
        for iface=1:nface
            iel=iel+1; e2e(iel)=el2el(iface,i);
            if (el2el(iface,i)==0), nbfaces = nbfaces+1; end
        end
    end
end
nifaces = (loc-nbfaces)/2; % internal face number


%% ----- Face List
if or(build.addFaces,or(build.addFNlst,build.addFElst))

    if (build.verbose), disp('* computing Face list'); end
    [faces, normal] = mkface( numC, ndglno, ele, mxface, ...
        el2el, nifaces, nbfaces, G.nodes.coords);

    % compute face centroids.
    fCenter = zeros(nifaces+nbfaces,G.nodes.dim);
    if (G.nodes.dim==2)
        for i=1:(nifaces+nbfaces)
            xmean = mean(G.nodes.coords(faces(i,1:nofa),1));
            ymean = mean(G.nodes.coords(faces(i,1:nofa),2));
            fCenter(i,:) = [xmean, ymean];
        end
    else
        for i=1:(nifaces+nbfaces)
            xmean = mean(G.nodes.coords(faces(i,1:nofa),1));
            ymean = mean(G.nodes.coords(faces(i,1:nofa),2));
            zmean = mean(G.nodes.coords(faces(i,1:nofa),3));
            fCenter(i,:) = [xmean, ymean, zmean];
        end
    end
end


%% ----- Face-Element list
if (build.addFElst)
    if (build.verbose), disp('* computing Face-Element list'); end
    [el2fa] = elmedg(numN,numC,ndglno,ele,nifaces,nbfaces,faces, ...
        mxface, G.nodes.dim);


    % set f2e and fe arrays (el2el representation for saving memory purpose)
    fe  = ee;
    f2e = ones(loc,1); iel=0;
    if (G.nodes.dim == 2)
        nofa = 2;
        for i=1:numC
            nface = ele(i+1)-ele(i);
            for iface=1:nface
                iel=iel+1; f2e(iel)=el2fa(i,iface);
            end
        end
    elseif (G.nodes.dim == 3)
        for i=1:numC
            nface = ele(i+1)-ele(i); nofa = 3;
            % hexahedron has nface~=nnel
            if (nface==8)
                nface=6; nofa = 4;
            end
            for iface=1:nface
                iel=iel+1; f2e(iel)=el2fa(i,iface);
            end
        end
    end
end

%% ----- Face-Nodes List
if or(build.addFNlst,build.addENlst)
    if (build.verbose), disp('* computing Face-Nodes list'); end
    [inedno,nednod] = nodedg(numN,nifaces,nbfaces,faces,G.nodes.dim);
    if (G.nodes.dim==2)
        if (build.verbose), disp('* sorting Element-Node list'); end
        [conele] = sortENlist(numN, inedno,nednod,faces,loc, n2e);
        n2e = conele;
    end
end

%% ----- update Grid
if (build.addENlst)
    G.nodes.cellPos   = no2el;
    G.nodes.cells     = n2e;
end

if (build.addEElst)
    G.cells.neigh     = e2e;
    G.cells.cellPos  = ee;
end

if (build.addFaces)
    G.faces.num       = [nifaces; nbfaces];
    G.faces.nodes     = faces(:,1:nofa);
    G.faces.neighbors = faces(:,nofa+1:end);
    G.faces.normals   = normal;
    G.faces.centroids = fCenter;
end

if (build.addFElst)
    G.cells.facePos   = fe;
    G.cells.faces     = f2e;
end

if (build.addFNlst)
    G.nodes.facePos   =  inedno;
    G.nodes.faces     =  nednod;
end


if ~isfield(G, 'type')
   warning(msbtrace('GridType:Unknown'),                            ...
          ['Input grid has no known type. ',                     ...
           'I''ll assume it arose from the primordial soup...']);
   G.type = { 'Primordial Soup' };
end

G.type = [G.type, { mfilename }];
end







% -------------------------------------------------------------------------
% This fun calculates the node to element list
function [nodptr,conele] = NODELE(totele, nloc, ndglno, iele, nonods)
    % allocate outs
    conele = zeros(nloc,1);
    nodptr = zeros(nonods+1,1);

    nlist  = zeros(nonods,1);
    inlist = zeros(nonods,1);

    for ele=1:totele
        nel=iele(ele+1)-iele(ele);
        for iloc=1:nel
            inod=ndglno(iele(ele)-1+iloc);
            nlist(inod)=nlist(inod)+1;   % number of elements inod is part of
        end
    end

    count=0;
    for n=1:nonods
        nodptr(n)=count+1;     % nodptr => esup2
        count=count+nlist(n);
    end
    nodptr(nonods+1)=count+1;

    for ele=1:totele
        nnel=iele(ele+1)-iele(ele);
        for iloc=1:nnel
            inod=ndglno(iele(ele)-1+iloc);
            inlist(inod)=inlist(inod)+1;
            conele(nodptr(inod)-1+inlist(inod))=ele;  % conele => esup1
        end
    end

    clear nlist inlist

end


% -------------------------------------------------------------------------
% this function returns ordered list of elements connected to 'ele' zero 
% entry indicates a free boundary, ie. on the surface. for more details of 
% how these data are computed read textbook:
% lohner, r. applied computational fluid dynamics techniques: an
% introduction based on finite element methods. second edition.
% john wiley & sons, ltd, 2008. isbn 978-0-470-51907-3
function [elcoel] = elmelm(totele,nloc,ndglno,iele,nonods,inod,conele,nfael, griddim) %#ok<INUSL>
    % preallocations
    nodmark = zeros(nonods,1);
    nofamrk = zeros(nfael,1);
    if (griddim==2)
        nofa  = zeros(2, nfael);
        nofa2 = zeros(2, nfael);
        ncoel = zeros(2,1);
        elcoel  = zeros(4, totele);
    elseif (griddim==3)
        nofa  = zeros(4, nfael);
        nofa2 = zeros(4, nfael);
        ncoel = zeros(4,1);
        elcoel  = zeros(6, totele);
    end


    if (griddim==2)
        for ele=1:totele       % loop over elements
            nface=iele(ele+1)-iele(ele);   % set element topology
            switch (nface)
                case (3) % triangle elm
                    nonofa=2; nofa(1:nonofa,1:nface) = reshape([1,2,2,3,3,1],[2,nface]);
                case (4) % quadrangle elm
                    nonofa=2; nofa(1:nonofa,1:nface) = reshape([1,2,2,3,3,4,4,1],[2,nface]);
                otherwise
                    error('topology in griddim=2 only impl for triangles and quadrangles')
            end

            for iface=1:nface    % loop over element faces
                if (elcoel(iface,ele)~=0), continue; end    % face were set
                nofamrk(1:nonofa) = ndglno(iele(ele)-1+nofa(1:nonofa,iface)); % obtain nodes of this face
                nodmark(nofamrk(1:nonofa))=1;                                 % mark nodes of this face
                ncoel(:)=1000000;

                ncoel(1:nonofa)=inod(nofamrk(1:nonofa))-inod(nofamrk(1:nonofa)+1);  % connected elem index
                [~,idx] = min(ncoel, [], 1);
                inp=nofamrk(idx);            % catch node with smallest number of connected elem

                for iptr=inod(inp):inod(inp+1)-1            % loop over the connected elements
                    ele2=conele(iptr);                       % element id
                    if (ele2~=ele)
                        nface2=iele(ele2+1)-iele(ele2);                    % set element topology
                        switch (nface2)
                            case (3) % triangle elm
                                nonofa2=2;
                                nofa2(1:nonofa2,1:nface2)=reshape([1,2,2,3,3,1],[2,nface2]);
                            case (4) % quadrangle elm
                                nonofa2=2;
                                nofa2(1:nonofa2,1:nface2) = reshape([1,2,2,3,3,4,4,1], [2,nface2]);
                            otherwise
                                error('Geometry in griddim=2 only impl.for triangles and quadrangles')
                        end

                        for jface=1:nface2                    % loop over the element faces and
                                                              % checks if nodes match
                            count=0;
                            for jnofa=1:nonofa2                  % count the times that nodes match
                                jnp=ndglno(iele(ele2)-1+nofa2(jnofa,jface)); % obtain node of this face
                                count=count+nodmark(jnp);
                            end

                            if(count==nonofa)
                                elcoel(iface,ele)=ele2; elcoel(jface,ele2)=ele;
                                break;
                            end
                        end
                    end
                end
                nodmark(:)=0;         % reset marked nodes
            end
        end
    elseif (griddim==3)
        for ele=1:totele       % loop over elements
            nface=iele(ele+1)-iele(ele);   % set element topology
            switch (nface)
                case (4) % tetrahedron
                    nonofa=3;
                    nofa(1:nonofa,1:nface) = reshape([1,3,2,1,2,4,3,1,4,2,3,4],[3,nface]);
                case (8) % hexahedron
                    nonofa=4; nface = 6;
                    facelist = [1,4,3,2,5,6,7,8,1,2,6,5,3,4,8,7,1,5,8,4,2,3,7,6];
                    nofa(1:nonofa,1:nface) = reshape(facelist,[4,nface]);
                otherwise
                    error('Geometry in griddim=3 only impl for tetrahedron and hexahedron')
            end

            for iface=1:nface    % loop over element faces
                if (elcoel(iface,ele)~=0), continue; end    % face were set
                nofamrk(1:nonofa) = ndglno(iele(ele)-1+nofa(1:nonofa,iface)); % obtain nodes of this face
                nodmark(nofamrk(1:nonofa))=1;                                 % mark nodes of this face
                ncoel(:)=1000000;
                ncoel(1:nonofa)=abs(inod(nofamrk(1:nonofa))-inod(nofamrk(1:nonofa)+1));  % number of connected elem
                [~,idx] = min(ncoel, [], 1);
                inp=nofamrk(idx);            % catch node with smallest number of connected elem

                for iptr=inod(inp):inod(inp+1)-1            % loop over the connected elements
                    ele2=conele(iptr);                       % element id
                    if (ele2~=ele)
                        nface2=iele(ele2+1)-iele(ele2);                    % set element topology
                        switch (nface2)
                            case (4) % tetrahedron
                                nonofa2 = nonofa; nofa2  = nofa;
                            case (8) % quadrangle elm
                                nonofa2 = nonofa; nofa2   = nofa;
                                nface2 = nface;
                            otherwise
                                error('Geometry in griddim=3 only impl.for tetrahedron and hexahedron')
                        end

                        for jface=1:nface2                    % loop over the element faces and
                                                              % checks if nodes match
                            count=0;
                            for jnofa=1:nonofa2                  % count the times that nodes match
                                jnp=ndglno(iele(ele2)-1+nofa2(jnofa,jface)); % obtain node of this face
                                count=count+nodmark(jnp);
                            end

                            if(count==nonofa)
                                elcoel(iface,ele)=ele2; elcoel(jface,ele2)=ele;
                                break;
                            end
                        end
                    end
                end
                nodmark(:)=0;         % reset marked nodes
            end
        end
    end
end


%--------------------------------------------------------------------------
% This function returns list of faces (internal and external), i.e 
% ifaces(4,ninternal) and bfaces(3,nexternal) where the ninternal are faces
% on interior domain and the nexternal remain are on boundary. field 1-2 
% storage node list (ifaces(1,:)<ifaces(2,:)) fields 3-4 storage the elements
% connected to faces (for ninternal). for nexternal field 1-2 storage node 
% list (not necessarily faces(1,:)<faces(2,:) and field 4 always zero. where
% zero entry indicates a free boundary, ie. on the surface. for more details
% of how these data are computed read textbook:
% lohner, r. applied computational fluid dynamics techniques: an
% introduction based on finite element methods. second edition.
% john wiley & sons, ltd, 2008. isbn 978-0-470-51907-3.
function [faces, normal] = mkface(totele,ndglno,iele,nufael,elcoel, ...
    intern,extern, xyz)
    %... preallocations
    [~,griddim] = size(xyz);

    normal  = zeros(intern+extern, griddim);
    if (griddim==2)
        faces   = zeros(intern+extern,4)';
        nofa    = zeros(2,nufael);
    elseif (griddim==3)
        if ((iele(2)-iele(1))==4)
            faces = zeros(intern+extern,5)';
        elseif ((iele(2)-iele(1))==8)
            faces = zeros(intern+extern,6)';
        end
        nofa = zeros(4,nufael);
    end

    ied=0; ibe=0;
    if (griddim==2)
        R90 = [0 -1; 1 0];
        for ele=1:totele       % loop over elements
            nface=iele(ele+1)-iele(ele);   % set element topology
            switch (nface)
                case (3) % triangle elm
                    nonofa=2;
                    nofa(1:nonofa,1:nface)=reshape([1,2,2,3,3,1],[2,nface]);
                case (4) % quadrangle elm
                    nonofa=2;
                    nofa(1:nonofa,1:nface) = reshape([1,2,2,3,3,4,4,1],[2,nface]);
                otherwise
                    error('Geometry for griddim=2 only impl. for triangles and quadrangles')
            end

            for i=1:nface    % loop over element faces
                if(elcoel(i,ele)==0)             % border
                    ibe=ibe+1;
                    inod=ndglno(iele(ele)-1+nofa(1,i));
                    jnod=ndglno(iele(ele)-1+nofa(2,i));
                    faces(1,intern+ibe) = inod;
                    faces(2,intern+ibe) = jnod;
                    faces(3,intern+ibe) = ele;
                    normal(intern+ibe,:) = R90*(xyz(inod,:)-xyz(jnod,:))';
                elseif(elcoel(i,ele)>ele)          % inner
                    ied=ied+1;
                    inod=ndglno(iele(ele)-1+nofa(1,i));
                    jnod=ndglno(iele(ele)-1+nofa(2,i));
                    if (inod<jnod)
                        faces(1,ied) = inod;
                        faces(2,ied) = jnod;
                        faces(3,ied) = ele;
                        faces(4,ied) = elcoel(i,ele);
                    else
                        faces(2,ied) = inod;
                        faces(1,ied) = jnod;
                        faces(4,ied) = ele;
                        faces(3,ied) = elcoel(i,ele);
                    end
                    normal(ied,:)=R90*(xyz(faces(1,ied),:) - ...
                        xyz(faces(2,ied),:))';
                end
            end
        end
    elseif (griddim==3)
        for ele=1:totele       % loop over elements
            nface=iele(ele+1)-iele(ele);   % set element topology
            switch (nface)
                case (4) % tetrahedron
                    nonofa=3;
                    nofa(1:nonofa,1:nface) = reshape([1,3,2,1,2,4,3,1,4,2,3,4],[3,nface]);
                case (8) % hexahedron
                    nonofa=4; nface = 6;
                    facelist = [1,4,3,2,5,6,7,8,1,2,6,5,3,4,8,7,1,5,8,4,2,3,7,6];
                    nofa(1:nonofa,1:nface) = reshape(facelist,[4,nface]);
                otherwise
                    error('Geometry for griddim=3 only impl.for tetrahedron&hexahedron')
            end

            for i=1:nface    % loop over element faces
                if(elcoel(i,ele)==0)             % border
                    ibe=ibe+1;
                    inod=ndglno(iele(ele)-1+nofa(1:nonofa,i));
                    faces(1:nonofa,intern+ibe) = inod;
                    faces(nonofa+1,intern+ibe) = ele;

                    va = xyz(inod(2),:)-xyz(inod(1),:);
                    vb = xyz(inod(end),:)-xyz(inod(1),:);
                    normal(intern+ibe,:) = cross(va,vb);
                elseif(elcoel(i,ele)>ele)          % inner
                    ied=ied+1;
                    inod=ndglno(iele(ele)-1+nofa(1:nonofa,i));
                    faces(1:nonofa,ied) = inod;
                    faces(nonofa+1,ied) = ele;
                    faces(nonofa+2,ied) = elcoel(i,ele);

                    va = xyz(inod(2),:)-xyz(inod(1),:);
                    vb = xyz(inod(end),:)-xyz(inod(1),:);
                    normal(ied,:) = cross(va,vb);
                end
            end
        end
    end

    assert(intern==ied,'oops, number of internal faces not match with calculated')
    assert(extern==ibe,'oops, number of external faces not match with calculated')

    faces = faces';
end


% ------------------------------------------------------------------------
% this function returns list of faces connected to element. for more details
% of how these data are computed read textbook:
% lohner, r. applied computational fluid dynamics techniques: an
% introduction based on finite element methods. second edition.
% john wiley & sons, ltd, 2008. isbn 978-0-470-51907-3.
function [edcoel] = elmedg(nonods,totele,ndglno,iele,nifac,iface,faces, ...
    nufael, griddim)
    %... preallocations
    nfaces = nifac+iface;
    fcol = size(faces,2);
    if (griddim==2)
        nofa = zeros(2,nufael);
    elseif (griddim==3)
        nofa = zeros(4,nufael);
    end
    nodmark = zeros(nonods,1);
    edcoel  = zeros(totele, nufael);

    if (griddim==2)
        for ed=1:nfaces
            for side=fcol:-1:fcol-1
                ele=faces(ed,side); if (~ele), continue; end
                nface=iele(ele+1)-iele(ele);   % set element topology
                switch (nface)
                    case (3) % triangle elm
                        nonofa=2;
                        nofa(1:nonofa,1:nface)=reshape([1,2,2,3,3,1],[2,nface]);
                    case (4) % quadrangle elm
                        nonofa=2;
                        nofa(1:nonofa,1:nface) = reshape([1,2,2,3,3,4,4,1],[2,nface]);
                    otherwise
                        error('Geometry for griddim=2 only impl. for triangles and quadrangles')
                end

                nodmark(faces(ed,1:nonofa))=1;           % mark nodes of this face
                for iface=1:nface                        % loop over the element faces and
                                                         % checks if nodes match
                    count=0;
                    for inofa=1:nonofa                   % count the times that nodes match
                        inod=ndglno(iele(ele)-1+nofa(inofa,iface));  % obtain node of this face
                        count=count+nodmark(inod);
                    end
                    if(count==nonofa)
                        edcoel(ele,iface)=ed;
                        break;
                    end
                end
                nodmark(:)=0;                               % reset marks
            end
        end
    elseif (griddim==3)
        for ed=1:nfaces
            for side=fcol:-1:fcol-1
                ele=faces(ed,side); if (~ele), continue; end
                nface=iele(ele+1)-iele(ele);   % set element topology
                switch (nface)
                    case (4) % tetrahedron
                        nonofa=3;
                        nofa(1:nonofa,1:nface) = reshape([1,3,2,1,2,4,3,1,4,2,3,4],[3,nface]);
                    case (8) % hexahedron
                        nonofa=4; nface = 6;
                        facelist = [1,4,3,2,5,6,7,8,1,2,6,5,3,4,8,7,1,5,8,4,2,3,7,6];
                        nofa(1:nonofa,1:nface) = reshape(facelist,[4,nface]);
                    otherwise
                        error('Geometry for griddim=2 only impl. for triangles and quadrangles')
                end

                nodmark(faces(ed,1:nonofa))=1;           % mark nodes of this face
                for iface=1:nface                        % loop over the element faces and
                                                         % checks if nodes match
                    count=0;
                    for inofa=1:nonofa                   % count the times that nodes match
                        inod=ndglno(iele(ele)-1+nofa(inofa,iface));  % obtain node of this face
                        count=count+nodmark(inod);
                    end
                    if(count==nonofa)
                        edcoel(ele,iface)=ed;
                        break;
                    end
                end
                nodmark(:)=0;                            % reset marks
            end
        end
    end
end


% ------------------------------------------------------------------------
% this function returns list of faces connected to nodes. for more details 
% of how these data are computed read textbook:
% lohner, r. applied computational fluid dynamics techniques: an
% introduction based on finite element methods. second edition.
% john wiley & sons, ltd, 2008. isbn 978-0-470-51907-3.
function  [inedno,nednod] = nodedg(nonods,intern,extern,faces,griddim)
    % preallocations ...
    isborder = false(nonods,1);
    nlist    = zeros(nonods,1);
    inlist   = zeros(nonods,1);
    inedno   = zeros(nonods+1,1);

    nonofa = size(faces,2)-2;

    for ed=1:intern
      for inp=1:nonofa
        inod=faces(ed,inp);
        nlist(inod)=nlist(inod)+1;  % number of faces that emanates from inod
      end
    end
    for ed=intern+1:(intern+extern)
      for inp=1:nonofa
        inod=faces(ed,inp);
        isborder(inod)=true(1);
        nlist(inod)=nlist(inod)+1;  % number of faces that emanates from inod
      end
    end

    count=0;
    for nod=1:nonods
      inedno(nod)=count+1; count=count+nlist(nod);
    end
    inedno(nonods+1)=count+1;
    nednod = zeros(count,1);

    for ed=1:intern+extern
      for inp=1:nonofa
        inod=faces(ed,inp);
        inlist(inod)=inlist(inod)+1;
        nednod(inedno(inod)-1+inlist(inod))=ed;
      end
    end

    if (griddim==2)
    emrk      = zeros(max(nlist),1);
    inlist(:) = 0;
%     nednod2 = nednod;

    for nod=1:nonods
        elist = nednod(inedno(nod):inedno(nod+1)-1); % catch list of faces emanating from nod
        emrk(:)=0;
        if (isborder(nod))
            for ied=1:length(elist)      % loop over faces emanating from nod
                if and(nod==faces(elist(ied),1),faces(elist(ied),end)==0)
                    inlist(nod)=inlist(nod)+1;
                    nednod(inedno(nod)-1+inlist(nod))=elist(ied);
                    emrk(ied)=1;  % mark face on list
                    break;
                end
            end
            lele = faces(elist(ied),nonofa+1);
            finished = false(1);
            while (~finished)
                resetloop = false(1);
                for ied=1:size(elist)   % loop to find the the next face that have the lele on right-side
                    if (emrk(ied)), continue; end         % face already marked
                    if (elist(ied)>intern), continue; end % not is an border
                    % on right-side and no is a border face
                    if and(lele==faces(elist(ied),nonofa+2), ...
                            nod==faces(elist(ied),1))
                        inlist(nod)=inlist(nod)+1;
                        nednod(inedno(nod)-1+inlist(nod))=elist(ied);
                        emrk(ied)=1; lele=faces(elist(ied),nonofa+1);
                        resetloop=1; break;
                    elseif and(lele==faces(elist(ied),nonofa+1), ...
                            nod==faces(elist(ied),2))
                        inlist(nod)=inlist(nod)+1;
                        nednod(inedno(nod)-1+inlist(nod))=elist(ied);
                        emrk(ied)=1; lele=faces(elist(ied),nonofa+2);
                        resetloop=1; break;
                    end
                end % jed=1:size(elist)
                if (resetloop), continue; end
                finished=1;
            end
            % close loop over vertex nod
            for ied=1:size(elist)
                if(emrk(ied)), continue; end          % face already marked
                if(elist(ied)<=intern), continue; end % not is inner
                % face that have the lele on right-side
                if(lele==faces(elist(ied),nonofa+1))
                    inlist(nod)=inlist(nod)+1;
                    nednod(inedno(nod)-1+inlist(nod))=elist(ied);
                    emrk(ied)=1; break
                end
            end % jed=1,size(elist)
        else     %................................................. inp is inner
            ned=length(elist);
            ed=elist(1);
            inlist(nod)=inlist(nod)+1;
            nednod(inedno(nod)-1+inlist(nod))=elist(1);
            emrk(1)=1;     % mark face on list

            if (nod==faces(ed,1))
                lele=faces(ed,nonofa+1);      % left element
            else
                lele=faces(ed,nonofa+2);      % left element
            end

            % loop to find the the next face that have the iel on right-side
            finished = false(1);
            while(~finished)
                resetloop = false(1);
                for ied=2:ned
                    if(emrk(ied)), continue; end % face already marked
                    if and(lele==faces(elist(ied),nonofa+2), ...
                            nod==faces(elist(ied),1))
                        inlist(nod)=inlist(nod)+1;
                        nednod(inedno(nod)-1+inlist(nod))=elist(ied);
                        emrk(ied)=1; lele=faces(elist(ied),nonofa+1);
                        resetloop=1; break;
                    elseif and(lele==faces(elist(ied),nonofa+1), ...
                            nod==faces(elist(ied),2))
                        inlist(nod)=inlist(nod)+1;
                        nednod(inedno(nod)-1+inlist(nod))=elist(ied);
                        emrk(ied)=1; lele=faces(elist(ied),nonofa+2);
                        resetloop=1; break;
                    end
                end %
                if (resetloop), continue; end
                finished=1;
            end
        end % (isborder(nod))
    end % nod=1,nonods
    end
end


% ------------------------------------------------------------------------
% this function returns list of elements connected to nodes orderly. using
% list of faces connected to nodes previously build
function [conele] = sortENlist(nonods,inedno,nedno,faces,nloc)
    nonofa = size(faces,2)-2;

    conele = zeros(nloc,1);
    count=0;
    for nod=1:nonods
        noed=inedno(nod+1)-inedno(nod);
        for ed=1:noed
            ied=nedno(inedno(nod)-1+ed);
            if(nod==faces(ied,1))
                lele=faces(ied,nonofa+1);                 % left element
            else
                lele=faces(ied,nonofa+2);                 % left element
            end
            if(lele~=0)
                count=count+1;
                conele(count)=lele;
            end
        end
    end
end
