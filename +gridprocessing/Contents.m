% +GRIDPROCESSING
%
% Files
%   acad2geo        - Convert dxf structures (polylines and polylayer) data to geo structure (nodes, edges and surfaces).
%   cartGrid        - Construct 2d or 3d Cartesian grid (based in quadrilateral or hexahedron).
%   dxf2coord       - Get polylines and polylayers from *.dxf files.
%   dxf2geo         - Write geometric file (*.geo) from geometric data converted by acad2geo.m
%   evalGrid        - Construct Cartesian grid ( elements are quadrilaterals or hexahedron) with variable physical cell sizes.
%   GCLv3           - Get coords and lnods block from *gri.dat files (CODE_BRIGHT grid format).
%   getGridInfo     - This function get grid information.
%   grid_structure  - Grid structure used in packages
%   load_gmsh       - Reads a mesh in msh format (from gmsh), version 1 or 2
%   reorderingMesh  - Reordering node indices from mesh type file generated into gmsh.
%   tetrahedralGrid - Construct valid grid definition from points and tetrahedron list
%   triangleGrid    - Construct 2d structured grid based in trangles in physical space.
%   dxf_structure   - dxf structure used in package:
%   geo_structure   - Geometry structure used in package
%   msh2G           - Convert msh to G struture
%   computeGeometryFVM - This function compute geometry structure of a mesh used for FV discretization
%   costura            - Construct 2d or 3d sewn grid.
%   costuraG           - Construct 2d or 3d sewn grid (only implemented for tri,tetra,qua and hexa).
%   getTetTriMesh      - Extract Grid structure from Tetgen/Triangle files (xxx.node and xxx.ele).
