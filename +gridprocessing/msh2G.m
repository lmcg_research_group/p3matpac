function G = msh2G(msh)
%Convert msh to G struture
%
% SYNOPSIS:
%   G = msh2G(msh)
%
% PARAMETERS:
%   msh    - gridprocessing struture mostly as detailed in load_gmsh.m.
%
% RETURNS:
%   G - Grid structure mostly as detailed in grid_structure.
%
% EXAMPLE:
%   msh = gridprocessing.load_gmsh('fault.msh');
%   G   = gridprocessing.msh2G(msh);
%   % Plot the grid in 3D-view.
%   iotools.plotGrid_(G); view(3);
%
% SEE ALSO:
%   grid_structure, load_gmsh
%
%
%----------------------------------------------------------------------------------------
%
%   Code (tag) to identity boundary condition types (hydrodynamical and mechanical) into
%   msh file:
%       - hydrodynamical conditon : ]100,199]
%       - mechanical conditon : ]200,299]
%       - material : ]300,399]
%
%



wdim   = any(diff(msh.POS(:,3))~=0);
idtype = max(msh.ELE_INFOS(:,2));

%% check dimension
if (wdim)
    ndim=3;
    coords=msh.POS;
% total 'elements' - LINES(1), TRIANGLES(2), QUADS(3), LINES3(8), TRIANGLES6(9), ...
% QUADS9(10), POINTS(15), QUADS8(16)
    nel = sum(msh.nbElm)-msh.nbType(1)-msh.nbType(2)-msh.nbType(3)-msh.nbType(8)-...
        msh.nbType(9)-msh.nbType(10)-msh.nbType(16)-msh.nbType(15);
    lnods=zeros(nel,8);
else
    ndim=2;
    coords=msh.POS(:,1:2);
% total 'elements' - LINES(1), LINES3(8), POINTS(15)
    nel=sum(msh.nbElm)-msh.nbType(1)-msh.nbType(8)-msh.nbType(15);
    lnods=zeros(nel,6);
end

%% allocation of variables
mech=zeros(msh.nbNod,1);
flux=zeros(msh.nbNod,1);
mat=zeros(nel,1);
ElmType=mat;

el=0;
progress = textprogressbar(msh.nbElm+msh.nbElm,'showremtime', true, ...
						 'startmsg','deconding gmsh cells  ');

%% decoding elements and boundary condition
bm = true(size(flux));
bf = true(size(flux));
for iel=1:msh.nbElm
    %--------------------------------------------------- 4-nodes tetrahedron
    if and(msh.ELE_INFOS(iel,2)==4,ndim==3)
        el=el+1;
        if (msh.ELE_TAGS(iel,1)>300) && (msh.ELE_TAGS(iel,1)<399) % materials
            mat(el)=msh.ELE_TAGS(iel,1)-300;
        end
        lnods(el,1:4)=msh.ELE_NODES(iel,1:4);
        ElmType(el)=1;
    %---------------------------------------------------- 8-nodes hexahedron
    elseif and(msh.ELE_INFOS(iel,2)==5,ndim==3)
        el=el+1;
        if (msh.ELE_TAGS(iel,1)>300) && (msh.ELE_TAGS(iel,1)<399) % materials
            mat(el)=msh.ELE_TAGS(iel,1)-300;
        end
        lnods(el,1:8)=msh.ELE_NODES(iel,1:8);
        ElmType(el)=3;
    %-------------------------------------------------------- 3-nodes triangle
    elseif (msh.ELE_INFOS(iel,2)==2)
        if (ndim==2)
            el=el+1;
            if (msh.ELE_TAGS(iel,1)>300) && (msh.ELE_TAGS(iel,1)<399) % materials
                mat(el)=msh.ELE_TAGS(iel,1)-300;
            end
            lnods(el,1:3)=msh.ELE_NODES(iel,1:3);
            ElmType(el)=1;
        else
            if (msh.ELE_TAGS(iel,1)>100) && (msh.ELE_TAGS(iel,1)<199) % fluxtype b.c
                idx = msh.ELE_NODES(iel,1:3); idx = idx(bf(idx)); bf(idx) = false;
                flux(idx)=msh.ELE_TAGS(iel,1)-100; 
            end
            if (msh.ELE_TAGS(iel,1)>200) && (msh.ELE_TAGS(iel,1)<299) % mechtype b.c
                idx = msh.ELE_NODES(iel,1:3); idx = idx(bm(idx)); bm(idx) = false;
                mech(idx)=msh.ELE_TAGS(iel,1)-200;
            end
        end
    %-------------------------------------------------- 4-nodes quadrilateral
    elseif (msh.ELE_INFOS(iel,2)==3)
        if (ndim==2)
            el=el+1;
            if (msh.ELE_TAGS(iel,1)>300) && (msh.ELE_TAGS(iel,1)<399) % materials
                mat(el)=msh.ELE_TAGS(iel,1)-300;
            end
            lnods(el,1:4)=msh.ELE_NODES(iel,1:4);
            ElmType(el)=5;
        else
            if (msh.ELE_TAGS(iel,1)>100) && (msh.ELE_TAGS(iel,1)<199) % fluxtype b.c
                idx = msh.ELE_NODES(iel,1:4); idx = idx(bf(idx)); bf(idx) = false;
                flux(idx)=msh.ELE_TAGS(iel,1)-100; 
            end
            if (msh.ELE_TAGS(iel,1)>200) && (msh.ELE_TAGS(iel,1)<299) % mechtype b.c
                idx = msh.ELE_NODES(iel,1:4); idx = idx(bm(idx)); bm(idx) = false;
                mech(idx)=msh.ELE_TAGS(iel,1)-200;
            end
        end
    %----------------------------------------------------- 6-nodes triangle.
    elseif and(msh.ELE_INFOS(iel,2)==9,ndim==2)
        el=el+1;
        if (msh.ELE_TAGS(iel,1)>300) && (msh.ELE_TAGS(iel,1)<399) % materials
            mat(el)=msh.ELE_TAGS(iel,1)-300;
        end
        lnods(el,1:6)=msh.ELE_NODES(iel,1:6);
        ElmType(el)=12;
    %-------------------------------------------------------- 2-node line.
    elseif (msh.ELE_INFOS(iel,2)==1)
        if (msh.ELE_TAGS(iel,1)>100) && (msh.ELE_TAGS(iel,1)<199) % fluxtype b.c
            idx = msh.ELE_NODES(iel,1:2); bf(idx) = false;
            flux(idx)=msh.ELE_TAGS(iel,1)-100;
        end
        if (msh.ELE_TAGS(iel,1)>200) && (msh.ELE_TAGS(iel,1)<299) % mechtype b.c
            idx = msh.ELE_NODES(iel,1:2); bm(idx) = false;
            mech(idx)=msh.ELE_TAGS(iel,1)-200;
        end
        if (msh.ELE_TAGS(iel,1)>300) % materials... so 2-node line is a material
            el=el+1;
            if (msh.ELE_TAGS(iel,1)>300)% materials
                mat(el)=msh.ELE_TAGS(iel,1)-300;
            end
            lnods(el,1:2)=msh.ELE_NODES(iel,1:2);
            ElmType(el)=8;
        end
    end
	progress(iel);
end

for iel=1:msh.nbElm
    %----------------------------------------------------- 1-node point.
    if (msh.ELE_INFOS(iel,2)==15)
        if (msh.ELE_TAGS(iel,1)>100) && (msh.ELE_TAGS(iel,1)<199) % fluxtype b.c
            flux(msh.ELE_NODES(iel,1))=msh.ELE_TAGS(iel,1)-100;
        end
        if (msh.ELE_TAGS(iel,1)>200) && (msh.ELE_TAGS(iel,1)<299) % mechtype b.c
            mech(msh.ELE_NODES(iel,1))=msh.ELE_TAGS(iel,1)-200;
        end
    end
	progress(msh.nbElm+iel);
end

idx =1:msh.nbNod; % number of global nodes (vertices) in grid.
nodes = [idx',coords,mech,flux];

idx =1: el;       % number of cells in global grid.
cells = [idx',mat,ElmType,lnods];

G = gridprocessing.getGridInfo(nodes,cells);
G.type = [G.type, { mfilename }];
end
