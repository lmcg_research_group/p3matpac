function startup
%Amend MATLAB PATH to handle P3MATPAC implementation.

% $Date: 2015-04-02$
% $Revision: 1 $

   addpath(fileparts(mfilename('fullpath')));

   % add util folder too
   addpath(fullfile(p3ROOTDIR,'util'));

   % add third_party subfolders(genpath)
   addpath(genpath(fullfile(p3ROOTDIR,'third_party')));
   v=fileread(fullfile(p3ROOTDIR,'VERSION'));
   disp(['* p3matpac ',v(1:end-1),' were set into MATLAB PATH'])



end
