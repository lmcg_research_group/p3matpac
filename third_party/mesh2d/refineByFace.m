function [p,t,m] = refineByFace(v,e,f,fsize,hdata,options)
%Generate a 2D unstructured mesh for polygonal geometry. refining by face
%
% SYNOPSIS:
% [p,t,m]=refineByFace(v,e,f,fsize,hdata)
%
% PARAMETERS:
% v     - xy geometry vertices. Must be a nv-by-2 matrix, one row for
%         each vertices. v=[x1,y1;x2,y2; etc];
% e     - defines the connectivity between the vertices in 'v' as a list
%         of edges. e=[n1, n2; n2, n3; etc]
% f     - defines the edges included in each geometry face. Each face is a
%         vector of edges ID, stored ina cell array. f{1}=[e1,e2,e3, etc];
%         f{2}=[ei,ej,etc];
% fsize - defines the element size on specified face.
%         fsize=[fi,hi; fj,hj, etc];
% hdata - is a structure containing user defined mesh constraint used by
%         mesh2d program. OPTIONAL. It can include the following fields:
%         + hmax  = h0                      - max allowable element size
%         + edgeh = [ei, hi; ej, hj; etc];  - element size on specified edge
% options-is a structure array that allows some of the "tuning" parameters
% used in the solver to be modified(OPTIONAL):
%   options.mlim   : The convergence tolerance. The maximum percentage
%                    change in edge length per iteration must be less than
%                    MLIM { 0.02, 2.0% }.
%   options.maxit  : The maximum allowable number of iterations { 20 }.
%   options.dhmax  : The maximum allowable (relative) gradient in the size
%                    function { 0.3, 30.0% }.
%   options.output : Displays the mesh and the mesh statistics upon
%                    completion { TRUE }.
%   options.opt    : Set the options to generate the triangulation method { TRUE }.
%                    opt = 0 :: original version
%                    opt = 1 :: used for older MATLAB version < R2010b
%                    opt = 3 :: MATHWORKS suggestions
%                    opt = 4 :: for newer MATLAB versions > R2011a
%
% RETURNS:
% p     - node coordinates of mesh generated. a np-by-2 matrix
% t     - connectivity of each triangles generate. nt-by-3 matrix
% m     - group ID array. An array numerate each triangle in t that belongs
%         a face.
% USAGE:
%   node = [ ...
%       0.0, 0.0;
%       4.0, 0.0;
%       4.0, 4.0;
%       3.0, 4.0;
%       2.0, 4.0;
%       0.0, 4.0;
%       2.0, 3.0;
%       3.0, 3.0 ];
%
%   edge = [ ...
%       1, 2; ...
%       2, 3; ...
%       3, 4; ...
%       4, 5; ...
%       5, 6; ...
%       6, 1; ...
%       5, 7; ...
%       7, 8; ...
%       8, 4; ...
%       7, 1 ];
%
%   face{1} = [ 1, 2, 3, 9, 8, 10 ];
%   face{2} = [10, 7, 5, 6 ];
%   face{3} = [ 8, 9, 4, 7 ];
%
%   % setting element size for face{1}= 0.02 and face{3} = 0.1. face{2}
%   % will be coarse!
%   fsize = [1,0.02;3,0.1];
%
%   [p,t,m] = refineByFace(node,edge,face,fsize)
%
%
% TIPS: sometimes the triangulation tends to be slightly over-refined on
% complex boundaries. Adding the field 'complex_geometry' as true in hdata,
% helps prevent over refinement and a nice mesh
%

%% checking inputs
[nv,d] = size(v);
assert(d==2,'**ERROR: the vertices must has SIZE(v,2)=2');
[ne,de]=size(e);
assert(de==2,'**ERROR: the edges must has SIZE(e,2)=2');
assert(iscell(f),'**ERROR: face input must be a cell array');
[nh,df] = size(fsize);
assert(df==2,'**ERROR: the fsize must has SIZE(fsize,2)=2');
assert(nh<=numel(f),'**ERROR: the fsize must has SIZE(fsize,1)<=numel(f)');
if (nargin<=5)
    assert(isstruct(hdata),'**ERROR: hdata must be a structure');
    options = [];
end
if (nargin==6)
    assert(isstruct(options),'**ERROR: options must be a structure');
end


%% process
% meshing coarse mesh (base_mesh)
[p,t,m] = coarseMesh(v,e,f,hdata,options);
close;

% setting base_mesh element data:
[elemdata] = centroid_elem(p,t);

% compute max distance between vertices
dist = maxVertexDist(v);

% setting element size by face
hsize = dist*ones(numel(f),1);
hsize(fsize(:,1))=fsize(:,2);
tsize = hsize(m);

% setting user define constraints
hdata.fun = @(x,y)hfun(x,y,elemdata,tsize,dist);

[p,t,m] = meshfaces(v,e,f,hdata,options);


end


% -------------------------------------------------------------------------
% compute centroid of elements
function [elemdata]=centroid_elem(p,t)
[nt,n] = size(t);

x = zeros(nt,1);
y = x;
for i=1:nt
    x(i) = mean(p(t(i,:),1));
    y(i) = mean(p(t(i,:),2));
end
elemdata = struct('x',x,'y',y);
end


% -------------------------------------------------------------------------
function elem=find_elem(x,y,elemdata)
% Encontra elemento onde o ponto x,y est� contido
dist2=sqrt((x-elemdata.x).^2+(y-elemdata.y).^2);
[~,elem]=min(dist2);
end


% -------------------------------------------------------------------------
function h=hfun(x,y,elemdata,elemsize,hmax)
% Determina o tamanho do elemento em cada ponto x,y com base na malha grossa

h=hmax*ones(size(x)); % needed because sometimes x=y=[]

[ni,nj]=size(x);

for i=1:ni
    for j=1:nj
        elem=find_elem(x(i,j),y(i,j),elemdata);
        if elem>0
           h(i,j)=elemsize(elem);
        else
           h(i,j)=hmax;
        end
    end
end
end


% -------------------------------------------------------------------------
function h=hfun2(x,y,elemdata,elemsize,hmax)
% Determina o tamanho do elemento em cada ponto x,y com base na malha grossa

h=hmax*ones(size(x)); % needed because sometimes x=y=[]

[ni,nj]=size(x);

for i=1:ni
    for j=1:nj
        elem=find_elem(x(i,j),y(i,j),elemdata);
        if elem>0
           h(i,j)=elemsize(elem);
        else
           h(i,j)=hmax;
        end
    end
end
end
