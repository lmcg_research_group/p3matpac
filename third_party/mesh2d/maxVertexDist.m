function dist = maxVertexDist(v)
%compute max distance between vertices
%
% SYNOPSIS:
% dist = maxVertexDist(v)
%
% PARAMETERS:
% v     - xy geometry vertices. Must be an nv-by-2 matrix, one row for
%         each vertices. v=[x1,y1;x2,y2; etc];
%
% RETURNS:
% dist  - max distance between vertices
% 
% USAGE:
%   node = [ ...
%       0.0, 0.0;
%       4.0, 0.0;
%       4.0, 4.0;
%       3.0, 4.0;
%       2.0, 4.0;
%       0.0, 4.0;
%       2.0, 3.0;
%       3.0, 3.0 ];
%
%   dist = maxVertexDist(node)
%

[nv,d]=size(v);
x = v(:,1); y = v(:,2);
if d>2, z = v(:,3); end

dist=-1e10;
idx = 1:nv;
if d<3
    for i=1:nv
        dist2= max(sqrt((x(i)-x(idx~=i)).^2 +(y(i)-y(idx~=i)).^2));
        dist = max([dist,dist2]);
    end
else
    for i=1:nv
        dist2= max(sqrt((x(i)-x(idx~=i)).^2 +(y(i)-y(idx~=i)).^2 +(z(i)-z(idx~=i)).^2));
        dist = max([dist,dist2]);
    end
end

end

