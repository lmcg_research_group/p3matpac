%Test to construct 2d "costura grid".
clc; clear; close all;

%% setting parameters
ltype=1;               % tipo de elemento da malha 3D: tri or tet (1), qua (5), hex (3)
ltype_compat=101;      % tipo de elemento de compatibilidade 3D:  tri or tet (101) / qua or hex (102)
newmatcompat=4;        % novo material a ser criado com elementos de compatibilidade

%% Malha 1 (mais fina):
% set number of layers on x-, y- and z-direction.
Nx=15; Ny=15; Nz=20;

% set points on x-, y- and z-direction.
Lx=10; Ly=10; Lz=10;
x = linspace(0, Lx, Nx+1);
y = linspace(0, Ly, Ny+1);
z = linspace(0, Lz, Nz+1);
[xCoord, yCoord, zCoord] = ndgrid(x, y, z);
p = [xCoord(:), yCoord(:), zCoord(:)];

% meshing
G = gridprocessing.tetrahedralGrid(p);

% get parameters
[nn1,ndim]=size(G.nodes.coords);
[nel1,npe]=size(G.cells.lnods);
coords1 = G.nodes.coords;
bcond1=[G.nodes.bcmech,G.nodes.bcflux];
mat1=G.cells.mat;
lnods1 = G.cells.lnods;

%% Malha 2 (intermediaria):
% set number of layers on x-, y- and z-direction.
Nx=20; Ny=15; Nz=20;

% set points on x-, y- and z-direction [0,1].
Lx=20; Ly=10; Lz=10;
x = linspace(0, Lx, Nx+1);
y = linspace(0, Ly, Ny+1);
z = linspace(0, Lz, Nz+1);
[xCoord, yCoord, zCoord] = ndgrid(x, y, z);
p = [xCoord(:), yCoord(:), zCoord(:)];

% meshing
G = gridprocessing.tetrahedralGrid(p);

% get parameters
[nn2,ndim]=size(G.nodes.coords);
[nel2,npe]=size(G.cells.lnods);
coords2 = G.nodes.coords;
bcond2=[G.nodes.bcmech,G.nodes.bcflux];
mat2=2*G.cells.mat;
lnods2 = G.cells.lnods;

%% Malha 3 (mais grossa):
% set number of layers on x-, y- and z-direction.
Nx=5; Ny=5; Nz=5;

% set points on x-, y- and z-direction [0,1].
Lx=10; Ly=10; Lz=10;
x = linspace(0, Lx, Nx+1);
y = linspace(0, Ly, Ny+1);
z = linspace(0, Lz, Nz+1);
[xCoord, yCoord, zCoord] = ndgrid(x, y, z);
p = [xCoord(:), yCoord(:), zCoord(:)];

% meshing
G = gridprocessing.tetrahedralGrid(p);
% get parameters
[nn3,ndim]=size(G.nodes.coords);
[nel3,npe]=size(G.cells.lnods);
coords3 = G.nodes.coords;
bcond3=[G.nodes.bcmech,G.nodes.bcflux];
mat3=3*G.cells.mat;
lnods3 = G.cells.lnods;

%% Costura malhas 1 e 2:
matlist1costura=[2];  
extra_node_bcond_mech=5;
extra_node_bcond_flow=4;
[coords,lnods,mat,bcond]=gridprocessing.costura(ltype_compat,             ...  % tipo de elemento de compatibilidade no code_bright
                                 coords2,lnods2,mat2,bcond2,  0,  0,  0,  ...  % primeira malha (mais grossa)
                                 coords1,lnods1,mat1,bcond1,  0, 10,  0,  ...  % segunda malha  (mais fina)
                                 matlist1costura,               ...            % lista de materiais da primeira malha onde a segunda malha será costurada
                                 newmatcompat,                  ...            % novo material a ser criado com elementos de compatibilidade
                                 extra_node_bcond_mech,         ...            % condicao de contorno de fluxo do noh extra do elemento de compatibilidade
                                 extra_node_bcond_flow           );            % condicao de contorno de fluxo do noh extra do elemento de compatibilidade

%% Costura malhas 3:
matlist1costura=[3];  
extra_node_bcond_mech=6;
extra_node_bcond_flow=5;
[coords,lnods,mat,bcond]=gridprocessing.costura(ltype_compat,                            ...  % tipo de elemento de compatibilidade no code_bright
                                 coords3,lnods3,mat3,bcond3, 10, 10,  0,  ...  % primeira malha (mais grossa)
                                 coords ,lnods ,mat , bcond,  0,  0,  0,  ...  % segunda malha  (mais fina)
                                 matlist1costura,               ...            % lista de materiais da primeira malha onde a segunda malha será costurada
                                 newmatcompat,                  ...            % novo material a ser criado com elementos de compatibilidade
                                 extra_node_bcond_mech,         ...            % condicao de contorno de fluxo do noh extra do elemento de compatibilidade
                                 extra_node_bcond_flow           );            % condicao de contorno de fluxo do noh extra do elemento de compatibilidade


[nn,ndim]=size(coords);
[nel,npe]=size(lnods);

%% building G structure
blcoord = [reshape(1:nn,[nn,1]),coords,bcond];
etype = ltype*ones(nel,1);
etype(mat==newmatcompat) = ltype_compat;
bllnods = [reshape(1:nel,[nel,1]),mat,etype,lnods];
G = gridprocessing.getGridInfo(blcoord, bllnods);

%% Ploting...
% plot grid
iotools.plotGrid(G);
view(3);
iotools.gidWriter(mfilename,G)
anykey = input('Press <ENTER> to finish ...');
close
