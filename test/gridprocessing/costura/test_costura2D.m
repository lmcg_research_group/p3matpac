%Test to construct 2d "costura grid".
clc; clear; close all;

%% setting parameters
ltype       = 3;       % element type: tri or tet (1), qua (5), hex (3)
ltype_compat=102;      % coupled element type:  tri or tet (101) ou qua or hex (102)
newmatcompat=3;        % novo material a ser criado com elementos de compatibilidade

%% Malha 1:
Lx=10;
Ly=5;
Nx=8;
Ny=6;

G = gridprocessing.triangleGrid([Nx,Ny],[Lx,Ly],'c');  % crossed
[nn1,ndim]=size(G.nodes.coords);
[nel1,npe]=size(G.cells.lnods);
coords1 = G.nodes.coords;
bcond1=[G.nodes.bcmech,G.nodes.bcflux];
mat1=G.cells.mat;
lnods1 = G.cells.lnods;


%% Malha 2:
Lx=10;
Ly=10;
Nx=15;
Ny=15;
G = gridprocessing.triangleGrid([Nx,Ny],[Lx,Ly],'r');  % right
[nn2,ndim]=size(G.nodes.coords);
[nel2,npe]=size(G.cells.lnods);
coords2 = G.nodes.coords;
bcond2=[G.nodes.bcmech,G.nodes.bcflux];
mat2=2*G.cells.mat;
lnods2 = G.cells.lnods;


%% Malha final:
X1=0;
Y1=0;
Z1=0;
X2=10;
Y2=0;
Z2=0;
matlist1costura=[1];
extra_node_bcond_mech=5;
extra_node_bcond_flow=4;

%% costurando malhas
[coords,lnods,mat,bcond]= gridprocessing.costura(ltype_compat,...               % tipo de elemento de compatibilidade no code_bright
                                 coords1,lnods1,mat1,bcond1,X1,Y1,Z1,    ...    % primeira malha (mais grossa)
                                 coords2,lnods2,mat2,bcond2,X2,Y2,Z2,    ...    % segunda malha  (mais fina)
                                 matlist1costura,               ...             % lista de materiais da primeira malha onde a segunda malha será costurada
                                 newmatcompat,                  ...             % novo material a ser criado com elementos de compatibilidade
                                 extra_node_bcond_mech,         ...             % condicao de contorno de fluxo do noh extra do elemento de compatibilidade
                                 extra_node_bcond_flow           );             % condicao de contorno de fluxo do noh extra do elemento de compatibilidade

[nn,ndim]=size(coords);
[nel,npe]=size(lnods);

%% building G structure
blcoord = [reshape(1:nn,[nn,1]),coords,bcond];
etype = ltype*ones(nel,1);
etype(mat==newmatcompat) = ltype_compat;
bllnods = [reshape(1:nel,[nel,1]),mat,etype,lnods];
G = gridprocessing.getGridInfo(blcoord, bllnods);


%% Ploting...
% plot grid
iotools.plotGrid(G);
pause(0.7)

% plot bcflux
figure();
iotools.plotData(G, G.nodes.bcflux); colormap(jet); c = colorbar;
c.Label.String = 'Hidrodynmic BD index';

anykey = input('Press <ENTER> to finish ...');
close
