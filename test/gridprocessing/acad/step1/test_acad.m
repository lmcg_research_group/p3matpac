%Test to extract geometry entities from dxf CAD file and create a gmsh geometry file
%format, step1.

%% Extract polylayers and polylines from dxf file.
disp('* Extract polylines & polylayers from dxf file.')
dxf = gridprocessing.dxf2coord('CARMOD_REAL.DXF');

%% Convert polylayers and polylayers to geo structure.
disp('* Convert dxf structure to geo structure')
geo = gridprocessing.acad2geo(dxf);

%% create a gmsh geometry file format with geometry entities (polylines&polylayers)
% extracted from dxf file.
disp('* Create *.geo file (gmsh geometry format) ...');
iotools.dxf2geo(geo,'CARMOD_REAL');

%% display next step to build a model..
disp('NOTE: Edit the *.geo file assigning:')
disp('       + Boundary condition (hydrodynamic & mechanic types')
disp('       + Surfaces (Geometry>>Add>Surface);')
disp('       + Materials ID - Physical entities')
disp('       + Boundary condition (hydrodynamic & mechanic types) - Physical entities')
disp(''); disp('');
disp(' REMARKS: coding (Physical entities ID)for materials and boundary conditions are:');
disp('         + hydrodynamical BCs ]100,199]');
disp('         + mechanical BCs ]200,299]');
disp('         + materials ]300,399]');

anykey = input('Press <ENTER> to finish ...');

close all
