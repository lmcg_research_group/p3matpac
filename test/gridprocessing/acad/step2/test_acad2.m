
%Test acad, step 2...




%% build grid information from gmsh mesh file format
% reading *.msh file format
msh = gridprocessing.load_gmsh('carmod2-test.msh');

% converting msh format to G format structure
G   = gridprocessing.msh2G(msh);

disp('** write out *_gri.dat file')
iotools.writeGrid('carmod2',G);

anykey = input('Press <ENTER> to finish ...');

close all
