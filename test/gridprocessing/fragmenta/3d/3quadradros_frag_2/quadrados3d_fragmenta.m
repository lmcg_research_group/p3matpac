%% Entradas do programa FRAGMENTA

%% Limpa dados anteriores
clear
clc

%% Dados de fragmentacao:

model_name='3dfrag2';                       % nome do modelo no arquivo GRI.dat
thick=0.00001;                              % espessura das interfaces
imat=[2];                                   % material a ser fragmentado
imat_joint_fragm=[4];                       % material das interfaces na regiao fragmentada
imat_joint_conti=[-1];                      % material das interfaces no contato com outros materiais
imat_joint_bound=0;                         % material das interfaces no contorno
imat_hole_1d=0;                             % material segmento linear para conectar nos dos buracos entre interfaces
imat_hole_conti=0; % 2D!                    % material dos buracos entres interfaces no interior da regia fragmentada
imat_hole_bound=0; % 2D!                    % material dos buracos entres interfaces na fronteira da regiao fragmentada
ibcondmech=9;                               % condicao de contorno mecanica de apoio totalmente fixo
MMP_A = [1 5; 3 6];                         % Usar caso imat_joint_conti=-1 [mat em contato , mat da interface criada]
% OBS: ao atribuir ZERO para uma das variaveis imat_*, seu respectivo material NAO sera gerado. 

%% Inserir fraturas aleatorias na malha fragmentada
GLiopt=0;     % 0=nao insere; 1=insere.
GLnewmat=0;   % material da fratura criada
GLQfrat=00;   % quantidade de fraturas criadas

%% Chama subrotina de fragmentacao:
[coords, lnods, mat, etype, bc, nodes_at_bound, linhasCentroids, nel8] = ...
    gridprocessing.fragmenta(model_name,thick,imat,imat_joint_fragm, ...
    imat_joint_conti,MMP_A,imat_joint_bound,imat_hole_1d,imat_hole_bound, ...
    imat_hole_conti,ibcondmech,GLiopt,GLnewmat,GLQfrat);

% OBS: variavel 'NODES_AT_BOUND' tem informacoes nodais uteis para
% colocacao das condicoes de contorno (pode ser visualizada no GID)

%% Subrrotina para gerar estrutura I 
I = struct('poro', 1:max(mat));

%% Subrrotina para gerar estrutura G
coord = [(1:size(coords,1))', coords, bc];
elem  = [(1:numel(mat))'    , mat', etype', lnods];
G = gridprocessing.getGridInfo(coord, elem);


%% Subrrotina para gerar model_name_f_GRI.dat
iotools.writeGrid([model_name '_f'],G,I);

%% Subrrotina para gerar model_name_f_GEN.dat
% Abrir witreGen para maiores infomações.
iotools.writeGen([model_name '_f'], G, [], 'H2M'); % hydro-mechanical (2phase)

%% Chama subrrotina para gerar arquivos para o GID .msh:
iotools.gidWriter([model_name '_f'],G)

