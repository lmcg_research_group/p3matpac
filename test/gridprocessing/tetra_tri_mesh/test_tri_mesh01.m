%Test to construct 2d or 3d grid (based in triangles and
% tetrahedron).

%% Example:: build structured triangular mesh 1
% Make a 5-by-3 grid.
disp('* Test to construct 2d triangle grid.')
% set number of layers on x- and y-direction.
nx = 5; ny = 3;
% build grid structure domain size 1x1 m.
G2D = gridprocessing.triangleGrid([nx, ny]);

% Plot the grid in 2D-view.
figure('color','w','Name','Triangular MeshGrid'); % set backgournd color white and figure title
iotools.plotGrid(G2D);