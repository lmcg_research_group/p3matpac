%Test to construct 2d or 3d grid (based in triangles and
% tetrahedron).

%% Example:: build structured triangular mesh 2
% Make a 5-by-3 grid.
disp('* Test to construct 2d triangle grid - unary square with crossed diagonal.')
% set number of layers on x- and y-direction.
nx = 10; ny = 6;
% build grid structure domain size 1x1 m.
G2D = gridprocessing.triangleGrid([nx, ny],[1, 1],'crossed');

% Plot the grid in 2D-view.
figure('color','w','Name','Triangular MeshGrid'); % set backgournd color white and figure title
iotools.plotGrid(G2D);
