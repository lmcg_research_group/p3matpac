%Test to construct 2d or 3d grid (based in triangles and
% tetrahedron).

%% Example:: build structured tetrahedral mesh 1
% Make a 10-by-5-by-2 grid on the unit cube.
disp('* Test to construct 3d grid - unitary cube.')
% set number of layers on x-, y- and z-direction.
nx = 10; ny = 5; nz = 3;
% set points on x-, y- and z-direction [0,1].
x = linspace(0, 1, nx+1);
y = linspace(0, 1, ny+1);
z = linspace(0, 1, nz+1);
[xCoord, yCoord, zCoord] = ndgrid(x, y, z);
p = [xCoord(:), yCoord(:), zCoord(:)];
% build grid structure domain size 1x1x1 m.
G3D = gridprocessing.tetrahedralGrid(p);

% Plot the grid in 3D-view.
figure('color','w','Name','Tetrahedral MeshGrid'); % set backgournd color white and figure title
iotools.plotGrid(G3D,'facea',1); view(3); % to make face transparence remove ,'facea',1
anykey = input('Press <ENTER> to finish ...');

close all
