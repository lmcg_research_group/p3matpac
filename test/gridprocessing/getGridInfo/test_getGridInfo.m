%Test of how to use grid information from *gri.dat
clc; clear; close all;

%% Example::build grid information (grid structure) from *gri.dat
% get grid information from *gri.dat
disp('* Test of how to get grid information from *gri.dat');
Gfgrid = gridprocessing.getGridInfo ();

% Plot the grid.
figure('color','w','Name','MeshGrid from *gri.dat'); % set backgournd color white and figure title
iotools.plotGrid(Gfgrid);

% Plot the grid, coloring material ID.
figure('color','w','Name','MeshGrid & GroupID from *gri.dat');
iotools.plotCellData(Gfgrid,Gfgrid.cells.mat); colorbar;

% Plot the grid coloring flux boundary condition application.
figure('color','w','Name','MeshGrid & hydrodynamic BC from *gri.dat'); % set backgournd color white and figure title
iotools.plotData(Gfgrid,Gfgrid.nodes.bcflux); colorbar;

anykey = input('press any <ENTER> to continue ...');

%% Example::grid information from coords and lnods files
% get grid information from coords and lnods data files
disp('* Test of how to get grid information from coords and lnods files');
Gfcl = gridprocessing.getGridInfo(load('coords.tst'),load('lnods.tst'));

% Plot the grid.
figure('color','w','Name','MeshGrid from coords.tst and lnods.tst data files'); % set backgournd color white and figure title
iotools.plotGrid(Gfcl);
anykey = input('press any <ENTER> to finish ...');

close all
