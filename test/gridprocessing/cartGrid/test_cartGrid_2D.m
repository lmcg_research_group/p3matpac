%Test to construct 2d or 3d Cartesian grid (based in quadrilateral or
% hexahedron) in physical space.
clc; clear; close all;

%% 2D Cartesian grid example
% Make a 5-by-3 grid on the unit quadrilateral.
disp('* Test to construct 2d Cartesian grid.')
% set number of layers on x- and y-direction.
nx = 5; ny = 3;
% build mesh 1x1 m.
G2D = gridprocessing.cartGrid([nx, ny], [1, 1]);

% Plot the grid in 2D-view.
figure('color','w','Name','Cartesian 2d MeshGrid'); % set backgournd color white and figure title
iotools.plotGrid(G2D);
anykey = input('Press <ENTER> to continue ...');