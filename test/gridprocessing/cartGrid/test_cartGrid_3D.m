%Test to construct 2d or 3d Cartesian grid (based in quadrilateral or
% hexahedron) in physical space.
clc; clear; close all;


%% 3D Cartesian grid example
% Make a 10-by-5-by-2 grid on the unit cube.
disp('* Test to construct 3d Cartesian grid.')
% set number of layers on x-, y- and z-direction.
nx = 10; ny = 5; nz = 3;
% build mesh 1x1x1 m.
G3D = gridprocessing.cartGrid([nx, ny, nz], [1, 1, 1]);

% changing material of last 4 elements
G3D.cells.mat(G3D.cells.num-4:end) = 2;

% Plot the grid in 3D-view.
figure('color','w','Name','Cartesian 3d MeshGrid'); % set backgournd color white and figure title
iotools.plotGrid(G3D); view(3);
anykey = input('Press <ENTER> to continue ...');