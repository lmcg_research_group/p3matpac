%Test to extract Grid structure from Tetgen files (xxx.node and xxx.ele)
% xxx = cube.2 and pmdc.1
clc; clear; close all;

%% Example::Build grid information (grid structure) from Tetgen files - cube
% get grid information from tetgen files
disp('* Test of how to extract grid information from Tetgen mesh');

G = gridprocessing.getTetTriMesh('cube.2');

% changing material of last 4 elements
G.cells.mat(G.cells.num-4:end) = 2;

% Plot the grid coloring material ID.
figure('color','w','Name',' MeshGrid from Tetgen mesh file'); % set backgournd color white and figure title
iotools.plotCellData(G,G.cells.mat); 
colormap(jet);
c = colorbar;
c.Label.String = 'Group ID';

% plot mesh
figure();
iotools.plotGrid(G,'facea',1); % to make face transparence remove ,'facea',1


anykey = input('press any <ENTER> to continue ...');

%% Example::Build grid information (grid structure) from Tetgen files - pmdc
G2 = gridprocessing.getTetTriMesh('pmdc.1');
% plot mesh in vtk format 
iotools.vtkWriter('pmdc',G2); % use this print out when number of element for large
anykey = input('press any <ENTER> to finish ...');

close all