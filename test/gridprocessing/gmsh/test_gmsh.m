%Test howto convert msh format (from gmsh generator) to G struture
%
% NOTE: coding (Physical entities ID)for materials and boundary conditions are:');
%          + hydrodynamical BCs ]100,199]');
%          + mechanical BCs ]200,299]');
%          + materials ]300,399]');
clc; clear; close all;

%% Example:: build grid information (grid structure) from gmsh mesh file format
% *** README ***
% before run this script you have to have generated the mesh into gmsh
% (using reservatorio.geo file)

% reading *.msh file format
M = gridprocessing.load_gmsh('reservatorio.msh');

% converting msh format to G format structure
G   = gridprocessing.msh2G(M);

% Plot the grid in 2D-view.
figure('color','w','Name',' MeshGrid'); % set backgournd color white and figure title
iotools.plotGrid(G);
view(3);
anykey = input('press any <ENTER> to finish ...');

close all
