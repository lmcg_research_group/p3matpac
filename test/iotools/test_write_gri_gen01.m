% Test howto use iotools package to write CODE_BRIGHT (UFPE version) files.
% *** without initial condition  ***
%
disp('** Test to write *gri.dat and gen.dat files (CODE_BRIGHT files)')
close all; clc; clear;

% -----------------------
% building a simple mesh
disp('-> building grid structure...')
% setting number of layers on x- and y-direction.
nx=10; ny=20;
G = gridprocessing.cartGrid([nx, ny]);


% -----------------------
% changing materialID of 5 columns of elements to 2
disp('-> setting material...')
for i=1:5
    G.cells.mat(i:nx:nx*ny) = 2; 
end
G.cells.nummat = 2;

% ---------------------------------
% plotting elemental data variables
figure('color','w','Name',' Elemental data variable plot'); % set backgournd color white and figure title
iotools.plotCellData(G,G.cells.mat); colorbar;
iotools.plotGrid(G);


%--------------------------------------------------------------------------
% Adding boundary comdition -----------------------------------------------
disp('-> setting BCs...')
% set on left - hydrodynamic
G.nodes.bcflux(G.nodes.coords(:,1)==min(G.nodes.coords(:,1)))=1;
% set on right - hydrodynamic
G.nodes.bcflux(G.nodes.coords(:,1)==max(G.nodes.coords(:,1)))=2;
% set on right - mechanical
G.nodes.bcmech(G.nodes.coords(:,1)==min(G.nodes.coords(:,1)))=1;

% ------------------------------
% plotting nodal data variables
figure('color','w','Name',' Nodal data variable plot'); % set backgournd color white and figure title
iotools.plotData(G,G.nodes.bcflux); colormap(jet); c = colorbar;
c.Label.String = 'hidrodynamic BC index';


%--------------------------------------------------------------------------
% write *_gri,dat file ----------------------------------------------------
iotools.writeGrid('square',G);

%--------------------------------------------------------------------------
% write *_gen,dat file ----------------------------------------------------
iotools.writeGen('square','thm',G); % thermo-hydro-mechanical problem ('thm')

%--------------------------------------------------------------------------
% write *.msh file --------------------------------------------------------
iotools.gidWriter('square',G);

