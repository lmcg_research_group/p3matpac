% Test howto use iotools package plotting variable distribution (Nodal)
% on mesh into MATLAB env.
%
disp('** plotting nodal and elemental data variable into Matlab')

% building a simple mesh
disp('-> building grid structure...')
% setting number of layers on x- and y-direction.
nx=10; ny=20;
% setting domain size (x- and y-direction).
Lx=25; Ly=100;
G = gridprocessing.cartGrid([nx, ny], [Lx, Ly]);


disp('-> setting material...')
% changing materialID of 5 columns of elements to 2
for i=1:5
    G.cells.mat(i:nx:nx*ny) = 2; 
end
G.cells.nummat = 2;

% plotting elemental data variables
figure('color','w','Name',' Elemental data variable plot'); % set backgournd color white and figure title
iotools.plotCellData(G,G.cells.mat); colorbar;
iotools.plotGrid(G);

disp('-> setting BCs...')
% set on left
G.nodes.bcflux(G.nodes.coords(:,1)==min(G.nodes.coords(:,1)))=1;
% set on right
G.nodes.bcflux(G.nodes.coords(:,1)==max(G.nodes.coords(:,1)))=2;

% plotting nodal data variables
figure('color','w','Name',' Nodal data variable plot'); % set backgournd color white and figure title
iotools.plotData(G,G.nodes.bcflux); 
colormap(jet);
c = colorbar;
c.Label.String = 'hidrodynamic BC index';


anykey = input('Press <ENTER> to finish ...');

close all