% Test howto use iotools package to write on VTK legacy format Hybrid mesh
% fragmented mesh
disp('** write on VTK legacy format Hybrid mesh - fragmented mesh')

% load coords and lnods blocks
coords=load('coords.tst');
lnods = load('lnods.tst');

G = gridprocessing.getGridInfo(coords,lnods);
iotools.gidWriter('Fragmented',G);