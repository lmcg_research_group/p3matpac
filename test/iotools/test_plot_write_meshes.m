%Test howto use iotools package to plot mesh and write mesh files (GiD formats)
%
disp('* Test howto use iotools package to plot mesh and write GiD and vtk files...')

%% Example::
disp('** plotting mesh into matlab')
load CutSphere

% building tetrahedra mesh (grid structure)
Gs = gridprocessing.tetrahedralGrid(CutSphere(:,2:4));

% plot mesh into Matlab
figure('color','w','Name',' MeshGrid'); % set backgournd color white and figure title
iotools.plotGrid(Gs,'facea',1);

% write *.msh file (GiD mesh format)
disp('** write out GiD mesh file format (*.msh)')
iotools.gidWriter('shpereflavia',Gs);


anykey = input('Press <ENTER> to finish ...');

close all
