% Test howto use iotools package to write CODE_BRIGHT (UFPE version) files.
% *** with initial conditions ***
%
disp('** Test to write *gri.dat and gen.dat files (CODE_BRIGHT files)')
close all; clc; clear;

% -----------------------
% building a simple mesh
disp('-> building grid structure...')
% setting number of layers on x- and y-direction.
nx=10; ny=20;
G = gridprocessing.cartGrid([nx, ny]);


% -----------------------
% changing materialID of 5 columns of elements to 2
disp('-> setting material...')
for i=1:5
    G.cells.mat(i:nx:nx*ny) = 2; 
end
G.cells.nummat = 2;

% ---------------------------------
% plotting elemental data variables
figure('color','w','Name',' Elemental data variable plot'); % set backgournd color white and figure title
iotools.plotCellData(G,G.cells.mat); colorbar;
iotools.plotGrid(G);


%--------------------------------------------------------------------------
% Adding boundary comdition -----------------------------------------------
disp('-> setting BCs...')
% set on left - hydrodynamic
G.nodes.bcflux(G.nodes.coords(:,1)==min(G.nodes.coords(:,1)))=1;
% set on right - hydrodynamic
G.nodes.bcflux(G.nodes.coords(:,1)==max(G.nodes.coords(:,1)))=2;
% set on right - mechanical
G.nodes.bcmech(G.nodes.coords(:,1)==min(G.nodes.coords(:,1)))=1;

% ------------------------------
% plotting nodal data variables
figure('color','w','Name',' Nodal data variable plot'); % set backgournd color white and figure title
iotools.plotData(G,G.nodes.bcflux); colormap(jet); c = colorbar;
c.Label.String = 'hidrodynamic BC index';

%--------------------------------------------------------------------------
% Assemble Initval structure ----------------------------------------------
% more information type: help initcond.initval_struture
disp0 = zeros(G.nodes.num,G.nodes.dim);   % initial displacement values (cte)
temp0 = 5.0*ones(G.nodes.num,1);          % initial temperature values (cte)

% initial pressure distribution values: apply gradient of pressure (hydrostatic)
% -------< look!!!!! >-------
% mode#1: expression...gradient applied gradP=0.00981 MPa/m
pres0 = abs(G.nodes.coords(:,end)-max(G.nodes.coords(:,end)))*.00981;
% mode#2: using gradFunction-> Pl(z) = Pl0 + gradP*(z-z0)
% pres0 = gradFunction([],[],abs(G.nodes.coords(:,end)),.00981);

% ------------------------------
% plotting nodal data variables
figure('color','w','Name',' Initial Condition'); % set backgournd color white and figure title
iotools.plotData(G,pres0); colormap(jet); c = colorbar;
c.Label.String = 'Initial Liquid pressure, [MPa]';

% initial stress distribution values: apply gradient of stress (geostatic)
% -------< look!!!!! >-------
% * mode#1: expression...gradient applied gradS=0.00981 MPa/m
sigm0 = -0.1-abs(G.cells.centroids(:,end)-max(G.nodes.coords(:,end)))*.00981;
% * mode#2: using gradFunction-> S(z) = S0 + gradS*(z-z0)
% sigm0 = gradFunction(-.1,[],-abs(G.nodes.coords(:,end)),.00981);

% ------------------------------
% plotting nodal data variables
figure('color','w','Name',' Initial Condition stress'); % set backgournd color white and figure title
iotools.plotCellData(G,sigm0); colormap(jet); c = colorbar;
c.Label.String = 'Initial Stress YY, MPa';


% build intial isotropic stress tensor [sxx, syy, sxy]
sigma = [repmat(sigm0,1,G.nodes.dim),zeros(G.cells.num,1)];

% append intial unknowns [ux,uy,pl,temp] for thm problem
thm = [disp0, pres0, temp0];

% apply porosity:
mporo = [0.3,0.15]; % by-material [mat#1=0.3, mat#2=0.15]
% mporo = 0.3*ones(G.cells.nummat,1); % homogeneous [mat#1=mat#2=0.3]

% apply permeability aniso.:
mperm =  zeros(G.cells.nummat,G.nodes.dim); % without aniso.

% build/initialize I structure
I = struct('che',             [],            ... % not required (only for HG problem)
           'blko',            [],            ... % not required (only for specific impl.)
           'poro',          mporo,           ... % porosity
           'perm',          mperm,           ... % optional (only for aniso. permeability)
           'thm',             thm,           ... % required (init. values of unknowns)
           'stresses',      sigma);              % required only for mechanical)


%--------------------------------------------------------------------------
% write *_gri,dat file ----------------------------------------------------
iotools.writeGrid('square02',G,I);

%--------------------------------------------------------------------------
% write *_gen,dat file ----------------------------------------------------
iotools.writeGen('square02','thm',G); % thermo-hydro-mechanical problem ('thm')

%--------------------------------------------------------------------------
% write *.msh file --------------------------------------------------------
iotools.gidWriter('square02',G,...
    'porosity',0.3.*ones(G.cells.num,1),...
    'pl',pres0);

anykey = input('Press <ENTER> to finish ...');