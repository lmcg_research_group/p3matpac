%Test howto create block of initial condition of reative transport problem
%

disp('* Test to create block of initial condition of reative transport problem.')
% load mesh data sample
load novafalha-test

% into Concentracao file there are five columns where:
%     col#1  col#2  col#3  col#4  col#5
% hierarchy conc#1 conc#2 conc#3 conc#4
load Concentracao

h = Concentracao(:,1);      % materials hierarchy
C = Concentracao(:,2:end);  % concentration of minerals belongs of chemical system
                            % REMARK: the contains of file should be a
                            % pressure or temperature also

% build chemical block variable [cheinit]
cheinit = initcond.chemInitCond(G, h, C);

% plot chemical concentration [X1]
figure('color','w','Name',' MeshGrid & Chemical IC'); % set backgournd color white and figure title
iotools.plotGrid(G); hold on
iotools.plotData(G,cheinit(:,1));
colormap(jet);
c = colorbar;
c.Label.String = 'Concentration X5';
hold off
anykey = input('Press <ENTER> to finish ...');

close all
