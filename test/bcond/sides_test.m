%Test of how to use nodeOnSide function
clear; clc; close all;
%% Example:: get nodes at left and top of grid
disp('Example1:: get nodes at left and top of 2D grid')
% build mesh
G = gridprocessing.cartGrid([10,10]);

% get left nodes {'West' , 'XMin', 'Left'}
left1 = nodesOnSide(G,'XMin');

% set some bc
G.nodes.bcmech(left1) = 1;

% get top nodes  {'Upper', 'ZMax', 'Top'}
% for 2D grids ZMax->YMax
top = nodesOnSide(G,'Upper');

% set some bc
G.nodes.bcmech(top) = 2;

% get left nodes {'West' , 'XMin', 'Left'} between y=[2.5,5.5] 
left2 = nodesOnSide(G,'XMin',and(G.nodes.coords(:,2)<=5.5, ...
    G.nodes.coords(:,2)>=2.5));

% set some bc
G.nodes.bcmech(left2) = 3;


% check where was set bcs
iotools.plotData(G,G.nodes.bcmech)
colormap(jet);
c = colorbar;
c.Label.String = 'mechanical BC index';


%% Example:: get nodes at back and bottom of grid
disp('Example2:: get nodes at back and bottom of 3D grid')
% build mesh
x = linspace(0, 1, 11); y = linspace(0, 1, 11); z = linspace(0, 1, 11);
[x, y, z] = ndgrid(x, y, z);
G = gridprocessing.tetrahedralGrid([x(:),y(:),z(:)]);

% get back nodes {'Back'  , 'South', 'YMin'}
back = nodesOnSide(G,'South');

% set some bc
G.nodes.bcmech(back) = 1;

% get bottom nodes {'Lower', 'ZMin', 'Bottom'}
bottom = nodesOnSide(G,'Lower');

% set some bc
G.nodes.bcmech(bottom) = 3;


% check where was set bcs
iotools.gidWriter('side_test',G)