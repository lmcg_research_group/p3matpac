%Test to add boundary condition (mechanical and hydrodynamical types) in 2D
%

load mesh3D-test

%% fixbcond
% add on node, line or plane
Gnew = bcond.fixbcond(G);
save test_bcond_3D

%% fixonboundary
clear Gnew
% add on domain boundary
GnewB = bcond.fixonboundary(G);
save test_onboundary_3D
