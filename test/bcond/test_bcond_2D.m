%Test to add boundary condition (mechanical and hydrodynamical types) in 2D

% load mesh data
load mesh2D-test

%% fixbcond
% assign boundary condition on: point, line, groupID (material), ...
Gnew = bcond.fixbcond(G);
save test_bcond_2D

if any(diff(Gnew.nodes.bcflux)~=0)
    iotools.plotData(Gnew, Gnew.nodes.bcflux);
    colormap(jet); c = colorbar;
    c.Label.String = 'Hidrodynamic BD index';
elseif any(diff(Gnew.nodes.bcmech)~=0)
    iotools.plotData(Gnew, Gnew.nodes.bcmech);
    colormap(jet); c = colorbar;
    c.Label.String = 'Mechanical BD index';
end

clear Gnew

%% fixonboundary
% assign boundary condition on domain borders
GnewB = bcond.fixonboundary(G);
save test_onboundary_2D

if any(diff(GnewB.nodes.bcflux)~=0)
    iotools.plotData(GnewB, GnewB.nodes.bcflux);
    colormap(jet); c = colorbar;
    c.Label.String = 'Hidrodynamic BD index';
else
    iotools.plotData(GnewB, GnewB.nodes.bcmech);
    colormap(jet); c = colorbar;
    c.Label.String = 'Mechanical BD index';
end