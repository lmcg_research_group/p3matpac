function [ p, t ] = facedemo ( n )

%*****************************************************************************80
%
% FACEDEMO: Example polygonal geometries for MESHFACES.
%
%  Author:
%
%    Darren Engwirda (Original)
%    Johnnathan Teixeira (modified to support p3matpac modules)
%
  if ( nargin < 1 )
    disp('');
    disp('The usual call is to FACEDEMO ( N ), with N = 1 or 2.\n' );
    disp('  Since N was not specified, the value 1 will be chosen.\n' );
    n = 1;
  end
  
  


% The triangulation tends to be slightly over-refined on complex boundaries.
% For some applications this is a bonus, but for hydrodymanic modelling it
% is not really necessary
% This helps prevent over refinement and a nice mesh.
% just create the field 'complex_geometry' in hdata variable to enable this
% option, otherwise DON'T!!! create this field.
% 
% hdata.complex_geometry = true;


% the mesh2d software don't works in newer versions of MATLAB <R2010 tho
% fix this bug, use the options structure to ran mesh2d on your MATLAB
% version, follows below:
%  options.opt :: option created to fix the current issues. Defaults 4.
%       opt = 0 -- uses the original version of mesh2d (just works for old
%                   MATLAB version <2010
%       opt = 1 -- if you are running in the old MATLAB version <2010
%       opt = 2 -- for MATLAB version >2010, uses WARNING MATLAB suggestion
%       opt = 4 -- for MATLAB version >2010, recommended option
  options.opt = 4;



  switch n

    case 1

      node = [0.0, 0.0; 1.0,0.0; 1.0,1.0; 0.0,1.0; 1.01,0.0; 1.01,1.0; 3.0,0.0; 3.0,1.0];
      edge = [1,2; 2,3; 3,4; 4,1; 2,5; 5,6; 6,3; 5,7; 7,8; 8,6];
      face{1} = [1,2,3,4];
      face{2} = [5,6,7,2];
      face{3} = [8,9,10,6];

      [p,t] = meshfaces ( node, edge, face, [], options );

    case 2

      % Geometry
      dtheta = pi/36;
      theta = (-pi:dtheta:(pi-dtheta))';
      node1 = [cos(theta), sin(theta)];
      node2 = [-2.0,-2.0; 2.0,-2.0; 2.0,2.0; -2.0, 2.0];
      edge1 = [(1:size(node1,1))',[(2:size(node1,1))'; 1]];
      edge2 = [1,2; 2,3; 3,4; 4,1];

      edge = [edge1; edge2+size(node1,1)];
      node = [node1; node2];

      face{1} = 1:size(edge1,1);
      face{2} = 1:size(edge,1);

      [p,t] = meshfaces(node,edge,face, [], options);

    otherwise
      error('Invalid demo. N must be between 1-2');

  end

  return
end
