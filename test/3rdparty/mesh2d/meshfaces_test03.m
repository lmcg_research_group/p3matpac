function [p,t]=meshfaces_test03 ( )

%*****************************************************************************80
%
%% MESHFACES_TEST03: Circle in a square.
%
%  Modified:
%
%    23 August 2014
%
%  Author:
%
%    Darren Engwirda
%
clc
clear
close all;

if sscanf(version('-release'),'%u%*s')<=2010
    options.opt = 1;        % older version
else
    options.opt = 4;
end
  fprintf ( 1, '\n' );
  fprintf ( 1, 'MESHFACES_TEST03\n' );
  fprintf ( 1, '  Draw a circle inside a square.\n' );
  fprintf ( 1, '  Mesh both regions.\n' );

  dtheta = pi/36;
  theta = (-pi:dtheta:(pi-dtheta))';
  node1 = [cos(theta), sin(theta)];
  node2 = [-2.0,-2.0; 2.0,-2.0; 2.0,2.0; -2.0, 2.0];
  edge1 = [(1:size(node1,1))',[(2:size(node1,1))'; 1]];
  edge2 = [1,2; 2,3; 3,4; 4,1];

  edge = [edge1; edge2+size(node1,1)];
  node = [node1; node2];

  face{1} = 1:size(edge1,1);
  face{2} = 1:size(edge,1);
%
%  Calling with no output arguments means we will simply see an image
%  of the mesh.
%
  [p,t]=meshfaces ( node, edge, face, [], options );

  filename = 'test03.png';
  print ( '-dpng', filename );

  fprintf ( 1, '\n' );
  fprintf ( 1, '  An image of the mesh was saved as "%s"\n', filename );

  return
end
