function [p1,t1,p2,t2,p3,t3,p4,t4,p5,t5] = meshfaces_test ( )
% MAIN tests the MESHFACES library.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    23 August 2014
%
%  Author:
%
%    John Burkardt
%
disp('');
disp(['->',mfilename]);
disp('* Test the MESHFACES library.' );

[p1,t1] = meshfaces_test01 ( );
[p2,t2] = meshfaces_test02 ( );
[p3,t3] = meshfaces_test03 ( );
[p4,t4] = meshfaces_test04 ( );
[p5,t5] = meshfaces_test05 ( );
%
%  Terminate.
%

disp('  Normal termination.' );

 return
end
