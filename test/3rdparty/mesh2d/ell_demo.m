function ell_demo
% ELL_DEMO demonstrates MESH2D on the L-shaped region.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    11 April 2014
%    09 August 2016
%
%  Author:
%
%    John Burkardt (Original)
%    Johnnathan Teixeira (modified to support p3matpac modules)
%
disp('');
disp(['->',mfilename]);
disp('* Demonstrate MESH2D on the L-shaped region.');
close all
%
%  #1) Simple input, 6 vertices.
%
disp('');
disp('  EXAMPLE 1:' );
disp('  Minimal Input' );
disp('  Use 6 vertices on the boundary.' );

v = [ 0.0, 0.0; 2.0, 0.0; 2.0, 1.0; 1.0, 1.0; 1.0, 2.0; 0.0, 2.0 ];


% The triangulation tends to be slightly over-refined on complex boundaries.
% For some applications this is a bonus, but for hydrodymanic modelling it
% is not really necessary
% This helps prevent over refinement and a nice mesh.
% just create the field 'complex_geometry' in hdata variable to enable this
% option, otherwise DON'T!!! create this field.
% 
% hdata.complex_geometry = true;


% the mesh2d software don't works in newer versions of MATLAB <R2010 tho
% fix this bug, use the options structure to ran mesh2d on your MATLAB
% version, follows below:
%  options.opt :: option created to fix the current issues. Defaults 4.
%       opt = 0 -- uses the original version of mesh2d (just works for old
%                   MATLAB version <2010
%       opt = 1 -- if you are running in the old MATLAB version <2010
%       opt = 2 -- for MATLAB version >2010, uses WARNING MATLAB suggestion
%       opt = 4 -- for MATLAB version >2010, recommended option
options.opt = 4;

[ p, t ] = mesh2d ( v, [], [], options);

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

%
%  #2) = Example #1, with some small segments.
%
disp('');
disp('  EXAMPLE 2:\n' );
disp('  Set a few small boundary segments.' );
disp('  Use 8 vertices on the boundary.' );

v = [ 0.0, 0.0; 2.0, 0.0; 2.0, 0.25; 2.0, 0.5; 2.0, 1.0; 1.0, 1.0; 1.0, 2.0; 0.0, 2.0 ];

[ p, t ] = mesh2d ( v, [], [], options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

%
%  #3) = Example #1, but now I specify a maximum element size of 0.2.
%
disp('');
disp('  EXAMPLE 3:' );
disp('  Set maximum element size HDATA.HMAX = 0.1' );

v = [ 0.0, 0.0; 2.0, 0.0; 2.0, 1.0; 1.0, 1.0; 1.0, 2.0; 0.0, 2.0 ];

hdata = [];
hdata.hmax = 0.1;

[ p, t ] = mesh2d ( v, [], hdata, options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

%
%  #4) = Example #1, but now specify a density function.
%
disp('');
disp('  EXAMPLE 4:' );
disp('  Specify small elements near reentrant corner using a size function.' );

v = [ 0.0, 0.0; 2.0, 0.0; 2.0, 1.0; 1.0, 1.0; 1.0, 2.0; 0.0, 2.0 ];

hdata = [];
hdata.fun = @hfun1;

[ p, t ] = mesh2d ( v, [], hdata, options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );
disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

%
%  #5) Same as #1, but refine.
%
disp('' );
disp('  EXAMPLE 5:' );
disp('  Repeat example #1, then call refine ( ).' );

v = [ 0.0, 0.0; 2.0, 0.0; 2.0, 1.0; 1.0, 1.0; 1.0, 2.0; 0.0, 2.0 ];

[ p, t ] = mesh2d ( v, [], [], options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );
disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

[ p, t ] = refine ( p, t );
%
%  Since REFINE doesn't redraw the mesh, I had to guess how to do it here.
%
clf
hold on
plot ( v(:,1 ), v(:,2), 'r.', 'MarkerSize', 32 );
axis equal
pause
plot(p(:,1),p(:,2),'b.', 'Markersize', 16 )
plot ( v(:,1), v(:,2), 'r.', 'MarkerSize', 32 );
pause ( )
patch('faces',t(:,:),'vertices',p,'facecolor','w','edgecolor','b');
%  patch('faces',edge,'vertices',v,'facecolor','none','edgecolor','k')
plot ( p(:,1), p(:,2), 'b.', 'Markersize', 16 )
plot ( v(:,1), v(:,2), 'r.', 'MarkerSize', 32 );
axis equal off;
hold off

print ( '-dpng', 'ell_mesh5.png' );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

%
%  #6) Same as #2, but smooth.
%
disp('' );
disp('  EXAMPLE 6:' );
disp('  Repeat example #2, then call smoothmesh ( ).' );

v = [ 0.0, 0.0; 2.0, 0.0; 2.0, 0.25; 2.0, 0.5; 2.0, 1.0; 1.0, 1.0; 1.0, 2.0; 0.0, 2.0 ];

[ p, t ] = mesh2d ( v, [], [], options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

[ p, t ] = smoothmesh ( p, t );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');


%
%  Close the figure.
%
close all
%
%  Terminate.
%
disp('' );
disp('Normal termiation.' );

return
end


% =========================================================================
% internal functions

function h = hfun1 ( x, y )

% -------------------------------------------------------------------------
% HFUN1 is a size-function for the L-shaped region.
%
%  Discussion:
%
%    The smallest size is at (1.0,1.0), and sizes increase as their distance
%    from that point increases.
%
    h = 0.01 + 0.1 * sqrt ( ( x - 1.0 ).^2  + ( y - 1.0 ).^2 );
    return
end
