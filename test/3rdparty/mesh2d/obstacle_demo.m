% OBSTACLE_DEMO demonstrates MESH2D on the 8x2 channel with square obstacle.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    13 November 2011
%
%  Author:
%
%    John Burkardt
%
clc
clear
close all;

if sscanf(version('-release'),'%u%*s')<=2010
    options.opt = 1;        % older version
else
    options.opt = 4;
end
disp('');
disp(['->',mfilename]);
disp('* Demonstrate MESH2D on the channel with a square obstacle.' );
close all


%
%  #1) Simple input, channel
%
disp('' );
disp('  EXAMPLE 1:\n' );
disp('  Channel, no obstacle.\n' );

v = [ 0.0, -1.0; 8.0, -1.0; 8.0, +1.0; 0.0, +1.0 ];

[ p, t ] = mesh2d ( v, [], [], options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

%
%  #2) Repeat, but request elements of size 1/4.
%
disp('' );
disp('  EXAMPLE 2:' );
disp('  Channel, max element size is 0.25.' );

v = [ 0.0, -1.0; 8.0, -1.0; 8.0, +1.0; 0.0, +1.0 ];

hdata = [];
hdata.hmax = 0.25;

[ p, t ] = mesh2d ( v, [], hdata, options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to continue ...');

%
%  #3) Repeat, but include hole.
%
disp('' );
disp('  EXAMPLE 3:' );
disp('  Channel, max element size is 0.25, obstacle inserted.' );

v = [ 0.0, -1.0; 8.0, -1.0; 8.0, +1.0; 0.0, +1.0;
    1.5, -0.5; 1.5, +0.5; 2.5, +0.5; 2.5, -0.5 ];
e = [ 1, 2; 2, 3; 3, 4; 4, 1;
    5, 6; 6, 7; 7, 8; 8, 5 ];
hdata = [];
hdata.hmax = 0.25;

[ p, t ] = mesh2d ( v, e, hdata, options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);
ans = input('Press <ENTER> to finish ...');

