% CIRCLES_DEMO demonstrates MESH2D on a circle with an off-center hole.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    11 April 2013
%    09 August 2016
%
%  Author:
%
%    John Burkardt (Original)
%    Johnnathan Teixeira (modified to support p3matpac modules)
%
disp('');
disp(['->',mfilename]);
disp('* Demonstrate MESH2D on a circle with a hole, max element size is 0.50.' );
close all
%
%  Display the nodes.
%
n = 20;

v = zeros ( 2 * n, 2 );
e = zeros ( 2 * n, 2 );
%
%  We trace the outer boundary in anti-clockwise order.
%
r = 3.0;
i = 0;
for j=0:n-1
    theta = j * 2.0 * pi / n;
    x = r * cos ( theta );
    y = r * sin ( theta );
    i = i + 1;
    v(i,1) = x;
    v(i,2) = y;
    e(i,1) = i;
    e(i,2) = mod ( i, n ) + 1;
end
%
%  We have to trace the hole in clockwise order.
%
r = 1.0;
for j=n-1:-1:0
    theta = j * 2.0 * pi / n;
    x = r * cos ( theta ) + 1.0;
    y = r * sin ( theta ) + 1.0;
    i = i + 1;
    v(i,1) = x;
    v(i,2) = y;
    e(i,1) = i;
    e(i,2) = mod ( i, n ) + n + 1;
end

hdata = [];
hdata.hmax = 0.25;

% The triangulation tends to be slightly over-refined on complex boundaries.
% For some applications this is a bonus, but for hydrodymanic modelling it
% is not really necessary
% This helps prevent over refinement and a nice mesh.
% just create the field 'complex_geometry' in hdata variable to enable this
% option, otherwise DON'T!!! create this field.
% 
% hdata.complex_geometry = true;


% the mesh2d software don't works in newer versions of MATLAB <R2010 tho
% fix this bug, use the options structure to ran mesh2d on your MATLAB
% version, follows below:
%  options.opt :: option created to fix the current issues. Defaults 4.
%       opt = 0 -- uses the original version of mesh2d (just works for old
%                   MATLAB version <2010
%       opt = 1 -- if you are running in the old MATLAB version <2010
%       opt = 2 -- for MATLAB version >2010, uses WARNING MATLAB suggestion
%       opt = 4 -- for MATLAB version >2010, recommended option
options.opt = 4;

[ p, t ] = mesh2d ( v, e, hdata, options );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);

coord = [(1:np)',p, zeros(np,2)];
conne = [(1:nt)',ones(nt,2),t];
G = gridprocessing.getGridInfo(coord, conne);

