% REFINE_DEMO demonstrates the MESH2D refinement option on the L-shaped region.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    17 October 2013
%
%  Author:
%
%    John Burkardt
%
clc
clear
close all;

if sscanf(version('-release'),'%u%*s')<=2010
    options.opt = 1;        % older version
else
    options.opt = 4;
end
disp('');
disp(['->',mfilename]);
disp('* Demonstrate MESH2D refinement option on the L-shaped region.' );

disp('  1) Create a default mesh.' );
disp('  2) Use the default refinement option.' );
disp('  3) Use the refinement option with a mask vector.' );
close all

%
%  #1) Simple input, 6 vertices.
%
disp('' );
disp('  EXAMPLE 1:' );
disp('  Minimal Input' );
disp('  Use 6 vertices on the boundary.' );

v = [ 0.0, 0.0; 2.0, 0.0; 2.0, 1.0; 1.0, 1.0; 1.0, 2.0; 0.0, 2.0 ];


[ p, t ] = mesh2d ( v, [], [], options );
title ( 'Initial mesh', 'Fontsize', 24 )
print ( '-dpng', 'refine_demo1.png' );

[ nv, ~ ] = size ( v );
[ np, ~ ] = size ( p );
[ nt, ~ ] = size ( t );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np)]);
disp(['mesh elements : ',num2str(nt)]);

%
%  #2) Same as #1, but the refine (only those triangles with centroid above y=1/2).
%
disp('' );
disp('  EXAMPLE 2:' );
disp('  Refine all triangles with centroid above y=0.5' )

c = centroid_mesh ( p, t );
ti = zeros ( nt, 1 );

ti = logical ( ti );
ti(1:nt,1) = ( 0.5 <= c(1:nt,2) );

[ p2, t2 ] = refine ( p, t, ti );


figure ( 2 )
clf
hold on
axis equal
patch ( 'faces', t2(:,:), 'vertices', p2, 'facecolor', 'w', 'edgecolor', 'b' );
plot ( c(:,1), c(:,2), 'k.', 'markersize', 8 );
plot ( p2(:,1), p2(:,2), 'b.', 'markersize', 8 );
plot ( v(:,1), v(:,2), 'r.', 'MarkerSize', 8 );
axis equal off;
hold off
title ( 'Refine triangles with Y centroid above 1/2', 'Fontsize', 24 )

print ( '-dpng', 'refine_demo2.png' );

[ nv, ~ ] = size ( v );
[ np2, ~ ] = size ( p2 );
[ nt2, ~ ] = size ( t2 );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np2)]);
disp(['mesh elements : ',num2str(nt2)]);
%
%  #3) Now refine those triangles with centroid x < 1/2.
%
disp('');
disp('  EXAMPLE 3:' );
disp('  Refine all triangles with centroid x < 1.' )

c = centroid_mesh ( p2, t2 );

ti = zeros ( nt2, 1 );
ti = logical ( ti );
ti(1:nt2,1) = ( c(1:nt2,1) < 0.5 );

[ p3, t3 ] = refine ( p2, t2, ti );


figure ( 3 )
clf
hold on
axis equal
patch ( 'faces', t3(:,:), 'vertices', p3, 'facecolor', 'w', 'edgecolor', 'b' );
plot ( c(:,1), c(:,2), 'k.', 'markersize', 8 );
plot ( p3(:,1), p3(:,2), 'b.', 'markersize', 8 );
plot ( v(:,1), v(:,2), 'r.', 'MarkerSize', 8 );
axis equal off;
hold off
title ( 'Refine triangles with X centroid less than 0.5.', 'Fontsize', 24 )

print ( '-dpng', 'refine_demo3.png' );

[ nv, ~ ] = size ( v );
[ np3, ~ ] = size ( p3 );
[ nt3, ~ ] = size ( t3 );

disp('Mesh info:');
disp(['input vertices: ', num2str(nv)]);
disp(['mesh nodes    : ', num2str(np3)]);
disp(['mesh elements : ',num2str(nt3)]);
%
%  Terminate.
%
disp('  Normal termination.' );

