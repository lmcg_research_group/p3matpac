
close all;
clear all;
clc;

% Get mesh information
%
try
    G = gridprocessing.getGridInfo(load('coords'),load('lnods'));
catch
    G = gridprocessing.getGridInfo();
end

% -------------------------------------------------------------------------
% Assemble meshdata struct ------------------------------------------------

% prescbcond = [1];       % condicoes de contorno inj
prescbcond = [2];       % condicoes de contorno prd
% meshdata.maxnodo=3915;     % m�ximo no com fluxo prescrito na malha
axinodo    = 0;                  % n� no eixo de simetria
factime    = 1.0;                % fator que divide o tempo (mesmo do root_gen.dat)
factflux   = [ ...
               1.0; ...        % fator que divide fluxo 1 (kg/s)
%                1.0; ...        % fator que divide fluxo 2 (kg/s)
%                1.0; ...        % fator que divide fluxo 2 (kg/s)
             ];
colflux    = [ ...
%                2;   ...        % coluna do flux1 no fort.70
               3;   ...        % coluna do flux2 no fort.70
%                4;   ...        % coluna do flux3 no fort.70
             ];

postdata = struct(...
                  'bcIdx'   ,   prescbcond,	... % condicoes de contorno
                  'axinode' ,   axinodo,    ... % node no eixo de simetria
                  'facttime',   factime,    ... % fator que divide o tempo (mesmo do root_gen.dat)
                  'factflux',   factflux,   ... % fator que divide fluxo (kg/s)
                  'iflux'   ,   colflux,    ... % coluna do flux no fort.70
                  'verbose' ,      true    ... % verbose process
                    );

[flux] = post.intflux(G,postdata);

anykey = input('Press <ENTER> to finish ...');

close all