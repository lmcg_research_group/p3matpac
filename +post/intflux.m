function [fluxes] = intflux(G, postdata)
%post-proocess flux and flux integration from fort.70 output file.
%
% SYNOPSIS:
%   fluxes = post.intflux(G,postdata);
%
% PARAMETERS:
%   G        - Grid structure mostly as detailed type
%               help gridprocessing.grid_structure
%
%   postdata - struct containing data need for intflux to compute flux and
%              flux integration.
%              postdata  - post-processing structure.  A master structure
%                          having the following fields:
%                 -> bcIdx    -- A b-by-1 array of boundary condition index
%                                to be extract information.
%
%                 -> axinode  -- A scalar contained node index that stay on
%                                axisymmetry axis (if 0 or empty theres'nt
%                                axisymmetry).
%
%                 -> facttime -- A scalar, factor for converting seconds into
%                                another output time units.
%                                ( Example: facttime = 86400, for outs in
%                                days).
%
%                 -> factflux -- A n-by-1 array or scalar (if fort.70 has only
%                                a unique flux value - column), factor for
%                                converting flux values into another output
%                                flux units.
%
%                 -> iflux    -- A n-by-1 array or scalar (if fort.70 has only
%                                a unique flux value - column), iflux must
%                                to contain number of flux column into
%                                for.70.
%
%                 -> verbose  -- enable (true or 1) or disable (false or 0)
%                                verbose mode
%
% RETURNS:
%   fluxes   - flux [flux] and flux integration [sumflux] over time step.
%               fluxes.flux     - flux;
%               fluxes.sumflux  - sumflux;
%               fluxes.time     - timestep
%
% EXAMPLE:
%   % Get mesh information
%	G = gridprocessing.getGridInfo();
%
%	%----------------------------------------------------------------------
%	% Assemble postdata struct --------------------------------------------
%   prescbcond = [1; 3; 5];   % boundary condition index
%	axinodo    = [];          % node on axisymmetry axis [theres'nt axism.]
%	factime    = 60;          % factor for converting seconds into min
%	factflux   = [ 1.0; ...   % factor for converting flux#1 values into another units.
%                  1.0; ...   % factor for converting flux#2 values into another units.
%                  1.0; ...   % factor for converting flux#3 values into another units.
%                ];           % fluxes in kg/s
%   colflux    = [ 3;   ...   % column of flux#1 into fort.70
%                  4;   ...   % column of flux#2 into fort.70
%                  2;   ...   % column of flux#3 into fort.70
%                ];
%
%   postdata = struct(...
%                   'bcIdx'   ,   prescbcond, ...
%                   'axinode' ,   axinodo,    ...
%                   'facttime',   factime,    ...
%                   'factflux',   factflux,   ...
%                   'iflux'   ,   colflux,    ...
%                   'verbose' ,         1     ... % enable verbose mode
%                     );
%
%	[sumflux.flux] = post.intflux(G,postdata);
%
% SEE ALSO:
%   grid_structure, getGridInfo
%


%  check input
% check input arguments
if sscanf(version('-release'),'%u%*s')<=2013
    if and(nargin~=2)
        error('please usage: fluxes = post.intflux(<G>, <postdata>);');
    end
else
    narginchk(2, 2);
end

assert(isstruct(G),...
    'First argument must be a struct variable see help gridprocessing.grid_structure.');

assert(isstruct(postdata),...
    'Second argument must be a struct variable see help post.intflux');

assert(length(postdata.factflux) == length(postdata.iflux),...
    'length(postdata.factflux) ~= length(postdata.iflux) they must have same lenght');

nbc = length(postdata.bcIdx);
for i=1:nbc
    idx = find(G.nodes.bcflux == postdata.bcIdx(i), 1);
    if isempty(idx)
        msg = sprintf('%d-th boundary condition not found into problem',...
            postdata.bcIdx(i));
        error(msg);
    end
end


% aux. variables
verbose = postdata.verbose;
nflux   = length(postdata.factflux);
vflux   = [1:nflux]';
form    = repmat(['%f'],1,max(postdata.iflux));

ndim = G.nodes.dim;     % dimension of world
nn   = G.nodes.num;     % number of nodes


% finding node with boundary condition index
prescnode = [];
for i=1:nbc
    idx = find(G.nodes.bcflux == postdata.bcIdx(i));
    prescnode = [prescnode; idx];
end

prescnode = unique(prescnode);


% reading title
if verbose
    disp('* reading fort.70 file');
end
fid=fopen('fort.70');
for j=1:7, fgets(fid); end

% time var.
t(1,1) = 0.0; timei = 0.0;
it  = 2;

% fluxes
f(1,1:nflux) = 0;
sumflux(1,1:nflux) = 0;

% getting flux until eof
while ~feof(fid)
    tline=fgets(fid);                     % get line
    time=str2num(tline(51:end));           % get time step value
    if verbose, fprintf('** reading %f timestep\n',time); end;
    dtime=(time-timei)*postdata.facttime; % converting to seconds
    timei=time;
    tline=fgets(fid); %#ok<*NASGU>
    j=1;
    nodo=0;
    f(it,1:nflux) = 0;

    while ~feof(fid)
        tline=fgets(fid);
        x=sscanf(tline,form);
        if isempty(x), break; end;
        nodo=x(1);
        if (j > length(prescnode)), continue, end;
        if nodo==prescnode(j)
            if nodo==postdata.axinode
                f(it,vflux) = f(it,vflux) + x(postdata.iflux)'/2.0;
            else
                f(it,vflux) = f(it,vflux) + x(postdata.iflux)';
            end
            j=j+1;
        end
    end
    sumflux(it,vflux)=sumflux(it-1,vflux)+f(it,vflux).*dtime;  % kg/s * s -> kg

    t(it,1)=time;
    it=it+1;
end
fclose(fid);


for i=1:it-1
    for j=1:nflux
        sumflux(i,j) = sumflux(i,j)./postdata.factflux(j);
        f(i,vflux)       = f(i,j)/postdata.factflux(j);
    end
end


%--------------------------------------------------------------------------
% Assemble fluxes
if verbose, fprintf('\n** Assembling fluxes struct\n'); end;
fluxes = struct( 'flux'   ,     f      ,    ...
                 'sumflux',     sumflux,    ...
                 'time'   ,     t           ...
                 );

%%%%%%% Pos-processamento:
if verbose
    timelabel = sprintf('time/ %f [s/s]', postdata.facttime);
    for i=1:nflux
        figure
        sflabel = sprintf('sumflux(%d)', i);
        plot(t,sumflux(:,i));
        xlabel(timelabel)
        ylabel(sflabel)
        title(strcat('evolution of ',sflabel))

        figure
        plot(t,f(:,i));
        sflabel = sprintf('flux(%d)', i);
        xlabel(timelabel)
        ylabel(sflabel)
        title(strcat('evolution of ',sflabel))
    end
end
