Pre- and Post-Processing MATLAB package - p3MatPac
=======================================================

A collection of routines, functions and extensions to provide high-level operations for pre and post-processing tasks within LMCG research group and/or contributors (persons, other research groups or corporations).


Getting started
---------------
For the best introduction to get started with **p3MatPac**, is read the help documentation
into functions/routines or type 'help <packagename>.<functionname>' and run the tests
examples on test folder (./test)


FAQ
---
In implementation


Please help out
---------------
This project is still under development. Feedback and suggestions are very
welcome and I encourage you to use the developer e-mails located in [AUTHORS](https://bitbucket.org/jontateixeira/p3matpac/src/master/contributors.txt)
to provide any feedback.

Feel free to fork this repo and to commit your additions. For a list of all
contributors, please see the [AUTHORS](https://bitbucket.org/jontateixeira/p3matpac/src/master/contributors.txt) file.


License terms
-------------
**p3MatPac** is published under the liberal terms of the BSD License, see the
[LICENSE](https://bitbucket.org/jontateixeira/p3matpac/src/master/LICENSE) file. Although the BSD License does not require you to share
any modifications you make to the source code, you are very much encouraged and
invited to contribute back your modifications to the community, preferably by email.