% +BCOND
%
% Files
%   fixbcond      - Add boundary condition mechanical and hydrodynamical types.
%   fixonboundary - Add boundary condition mechanical and/or hydrodynamical types on domain borders
%   bc_structure  - Boundary Condition structure
%   buildBC       - build boundary condition structure
%   initBCFlux    - Initialize a hydrodynamic boundary condition type structure
%   initBCMech    - Initialize a mechanical boundary condition type structure
