function BC = initBCFlux(varargin)
%Initialize a hydrodynamic boundary condition type structure
%
% SYNOPSIS
%  BC = initBCFlux()
%  BC = initBCFlux(arrayOfParam)
%  BC = initBCFlux('pn1', pv1,...)
%
% PARAMETERS
%   arrayOfParam - An array 20-by-1 of parameters. OPTIONAL
%
%   'pn'/pv  - List of property names/property values.  OPTIONAL.
%              This list will be passed to hydrodynamic BC parameters.
%              Supported property names:
%               - 'wwg', 'w_wg'       is omega_wg (Kg/Kg) - arrayOfParam(1)
%               - 'jg', 'j_g'         is j_g(Kg/s)        - arrayOfParam(2)
%               - 'pg', 'p_g'         is P_g(MPa)         - arrayOfParam(3)
%               - 'gammag', 'gamma_g' is gamma_g          - arrayOfParam(4)
%               - 'betag', 'beta_g'   is beta_g           - arrayOfParam(5)
%               - 'rhog', 'rho_g'     is rhog(Kg/m3)      - arrayOfParam(6)
%               - 'whl', 'w_hl'       is omega_hl(Kg/Kg)  - arrayOfParam(7)
%               - 'wal', 'w_al'       is omega_al(Kg/Kg)  - arrayOfParam(8)
%               - 'jl', 'j_l'         is j_l(Kg/s)        - arrayOfParam(9)
%               - 'pl', 'p_l'         is P_l(MPa)         - arrayOfParam(10)
%               - 'gammal', 'gamma_l' is gamma_l          - arrayOfParam(11)
%               - 'betal', 'beta_l'   is beta_l           - arrayOfParam(12)
%               - 'rhol', 'rho_l'     is rho_l(Kg/m3)     - arrayOfParam(13)
%               - 'je', 'j_e'         is j_E(J/s)         - arrayOfParam(14)
%               - 't', 'temp'         is T(C)             - arrayOfParam(15)
%               - 'gammae', 'gamma_e' is gamma_E(J/s/C)   - arrayOfParam(16)
%               - 'lambdal','lambda_l'is lambda_E(1/s)    - arrayOfParam(17)
%               - 'delta'             is delta            - arrayOfParam(19)
%               - 'index', 'ind'      is index            - arrayOfParam(20)
%
% RETURNS
%   BC - hydrodynamic boundary condition type structure mostly as detailed 
%        in bcond.bc_structure 
%
% EXAMPLE
%   1)>> BC = bcond.initBCFlux()
%
%   2)>> pbc=[0,0,0,0,0,0,0,0,0,10,1e5,0,0,0,0,0,0,0,0,1]';
%        BC = bcond.initBCFlux(pbc)
%
%   3)>> BC = bcond.initBCFlux('pl',10,'gamma_l',1e5)
%
% SEE ALSO
%   BC_STRUCTURE, INITBCMECH, BUILDBC
%

% check input
nparam = 20;
listOfParam = [];
if nargin>0
    if (mod(numel(varargin), 2) == 0) % input like a list of property names/property values
        listOfParam = zeros(nparam ,1);
        for i=1:2:numel(varargin)
            switch upper(varargin{i})
                case {'WWG','W_WG'}
                    listOfParam(1) = varargin{i+1};
                case {'JG','J_G'}
                    listOfParam(2) = varargin{i+1};
                case {'PG','P_G'}
                    listOfParam(3) = varargin{i+1};
                case {'GAMMAG','GAMMA_G'}
                    listOfParam(4) = varargin{i+1};
                case {'BETAG','BETA_G'}
                    listOfParam(5) = varargin{i+1};
                case {'RHOG','RHO_G'}
                    listOfParam(6) = varargin{i+1};
                case {'WHL','W_HL'}
                    listOfParam(7) = varargin{i+1};
                case {'WAL','W_AL'}
                    listOfParam(8) = varargin{i+1};
                case {'JL','J_L'}
                    listOfParam(9) = varargin{i+1};
                case {'PL','P_L'}
                    listOfParam(10) = varargin{i+1};
                case {'GAMMAL','GAMMA_L'}
                    listOfParam(11) = varargin{i+1};
                case {'BETAL','BETA_L'}
                    listOfParam(12) = varargin{i+1};
                case {'RHOL','RHO_L'}
                    listOfParam(13) = varargin{i+1};
                case {'JE','J_E'}
                    listOfParam(14) = varargin{i+1};
                case {'T','TEMP'}
                    listOfParam(15) = varargin{i+1};
                case {'GAMMAE','GAMMA_E'}
                    listOfParam(16) = varargin{i+1};
                case {'LAMBDAE','LAMBDA_E'}
                    listOfParam(17) = varargin{i+1};
                case {'DELTA'}
                    listOfParam(19) = varargin{i+1};
                case {'INDEX','IND'}
                    listOfParam(20) = varargin{i+1};
            end
        end
    elseif (numel(varargin{1})==20)   % input like a 20-by-1 array
        listOfParam = varargin{1};
    end
end

% build BC variable - default parameters (all zeros)
BC = setDefault();
if ~isempty(listOfParam),
    BC = setBCParam(BC,listOfParam);
end
end



% -------------------------------------------------------------------------
function bc=setDefault()
% set default hydrodynamic parameters
fid = fopen(fullfile(p3ROOTDIR,'templates','flux_param.dat'),'r');
param = textscan(fid,'%s');
fclose(fid);
% set number of parameters
npar = numel(param{1});
pars = zeros(npar,1);

% build BC variable
bc = struct('paramName',param,'value',pars);
end



% -------------------------------------------------------------------------
function bc = setBCParam(ibc,value)
% set hydrodynamic parameters to specific value
bc = ibc;
bc.value = value;
end