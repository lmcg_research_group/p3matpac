%Boundary Condition structure
%
%   BC - Boundary Condition structure.  A master structure having the following fields:
%       - flux --  A structure specifying properties for hydrodynamic boundary 
%                  condition type.
%
%       - mech --  A structure specifying properties for mechanical boundary 
%                  condition type.
%
%
%   FLUX -  hydrodynamic boundary condition structure (created from bcond.initBCFlux):
%       - paramName -- hydrodynamic boundary condition parameter name.
%                      A npar-by-1 cell string array.
%
%       - value     -- hydrodynamic boundary condition parameter value.
%                      A npar-by-1 array.
%
%
%   MECH -  mechanical boundary condition structure (created from bcond.initBCMech):
%       - paramName -- mechanical boundary condition parameter name.
%                      A npar-by-1 cell string array.
%
%       - value     -- mechanical boundary condition parameter value.
%                      A npar-by-1 array.
%
%
%
% SEE ALSO
%   INITBCMECH, BUILDBC, INITBCFLUX
%       
%
