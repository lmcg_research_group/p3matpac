function BC = buildBC(BCflux,BCmech)
%build boundary condition structure
%
% SYNOPSIS
%  BC = buildBC(BCflux,BCmech)
%
% PARAMETERS
%   BCflux  - An structure builded from bcond.initBCFlux.
%   BCmech  - An structure builded from bcond.initBCFlux.
%
% RETURNS
%   BC - mechanical boundary condition type structure mostly as detailed 
%        in bcond.bc_structure 
%
% EXAMPLE
%     BCmech = bcond.initBCMech(); BCflux = bcond.initBCFlux();
%     BC = bcond.buildBC(BCflux,BCmech)
%
% SEE ALSO
%   BC_STRUCTURE, INITBCMECH, INITBCFLUX
%

% check inputs

bcs_flux = [];
if (isstruct(BCflux))
    bcs_flux = BCflux;
end

bcs_mech = [];
if (isstruct(BCmech))
    bcs_mech = BCmech;
end
BC = struct('flux', bcs_flux, 'mech', bcs_mech);

end

