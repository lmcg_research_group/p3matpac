function Gnew = fixbcond(G)
%Add boundary condition mechanical and hydrodynamical types.
% Boundary condition can be add on :
% - nodes;  - line; - plane; - area (only for 2D); - nodes on material;
%
% SYNOPSIS
%   Gn = bcond.fixbcond(G);
%   % Make sure that gridding package was imported
%   Gn = fixbcond(G);
%
% PARAMETERS
%   G - Grid structure mostly as detailed in help gridprocessing.grid_structure.
%
% RETURNS
%   Gn - new Grid structure (update boundary conditon) mostly as detailed
%        in help gridprocessing.grid_structure.
%
% EXAMPLE
%   % Make a 10-by-5-by-2 grid on the unit cube.
%      nx = 10; ny = 5; nz = 2;
%      G = gridprocessing.cartGrid([nx, ny, nz], [1, 1, 1]);
%      Gn = bcond.fixbcond(G);
%
% SEE ALSO
%   GRIDPROCESSING.GRID_STRUCTURE, GRIDPROCESSING.CARTGRID, GRIDPROCESSING.TRIANGLEGRID, 
%   FIXONBOUNDARY
%

disp(' ')
disp('->FIXBCOND: Programa de pre-processo para CODE_BRIGHT');
disp(' ')

%Dimens?es do problema (ndim)
ndim=G.nodes.dim;
nn = G.nodes.num;
%M?ximo n?mero de n?s por elemento
npe = max(G.cells.nnel);

% Coordenadas e condicoes de contorno:
xg=G.nodes.coords(:,1);
yg=G.nodes.coords(:,2);
if (ndim==3)
   zg=G.nodes.coords(:,3);
else
   zg=zeros(nn,1);
end
mechbcond=G.nodes.bcmech;
fluxbcond=G.nodes.bcflux;

% Vizualizar ou alterar bcond:
iopt=0;
pto=iopt;
while(iopt<6)
    iopt=menu('FIXBCOND',...
        'Ver bcond (.vtk)',...      % iopt == 1
        'Alterar mechbcond',...     % iopt == 2
        'Alterar fluxbcon',...      % iopt == 3
        'Salvar',...                % iopt == 4
        'Salvar e Sair',...         % iopt == 5
        'Sair');                    % iopt == 6
    if (iopt==1)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %          GERACAO DO ARQUIVO *flavia.msh e *flavia.res:
        %          =============================================
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        iotools.vtkWriter('rootBC',G)

    elseif (iopt==2 || iopt==3)                % altera bcond
        if (iopt==2), bcond=mechbcond; end
        if (iopt==3), bcond=fluxbcond; end
        bcondopt=0;
        while(bcondopt~=7)
            bcondopt=menu('Alterar bcond em:',...
                'Toda malha',...
                'Ponto Nodal',...
                'Linha',...
                'Plano',...
                'Area(2D)',...
                'nos do material',...
                'Sair');
            if (bcondopt==1)                  % add bcond em toda malha
                prompt={'Numero da nova bcond: '};
                name='bcond';
                numlines=1;
                answer=inputdlg(prompt,name,numlines);
                ibcond=str2num(answer{1});
                nodos=1:nn;
                for i=1:nn
                    bcond(i)=ibcond;
                end
            elseif(bcondopt==2)               % add bcond em no
                prompt={'Numero da nova bcond: ',...
                    'Entre numero do no: '};
                name='bcond para ponto nodal';
                numlines=1;
                answer=inputdlg(prompt,name,numlines);
                ibcond=str2num(answer{1});
                node=str2num(answer{2});
                bcond(node)=ibcond;
                pto=pto+1;
                nodos(pto)=node;
            elseif(bcondopt==3)               % add bcond em uma linha (reta)
                prompt={'Numero da nova bcond: ',...
                    'Entre numero do no#1: ',...
                    'Entre numero do no#2: ',...
                    'toler (close enough): '};
                name='bcond para linha';
                numlines=1;
                def={' ',' ',' ','1e-3'};
                answer=inputdlg(prompt,name,numlines,def);
                ibcond=str2num(answer{1});
                node1=str2double(answer{2});
                node2=str2num(answer{3});
                toler=str2num(answer{4});
                if ndim==3               % para 3D
                    P1=[xg(node1),yg(node1),zg(node1)]';
                    P2=[xg(node2),yg(node2),zg(node2)]';
                    
                    v=(P2-P1); % vetor P1P2 (diretor)
                    xyz=v==0; % ver se vetor alinhado com eixos cartesianos
                    for i=1:nn,
                        if (i==node1 || i==node2)
                            bcond(i)=ibcond;
                            pto=pto+1;
                            nodos(pto)=i;
                        else
                            X=[xg(i),yg(i),zg(i)]';
                            if any(xyz),
                                h=(X(~xyz)-P1(~xyz))./v(~xyz); % X=P1+h*(P1P2)
                                if any(abs(diff(h))>toler)  % fora do segmento!!
                                    continue
                                end
                                if all(X(xyz)==P1(xyz)), % mesmo valor (eixo)
                                    bcond(i)=ibcond;
                                    pto=pto+1;
                                    nodos(pto)=i;
                                end                                
                            else
                                h=(X-P1)./v;    % X=P1+h*(P1P2)
                                if ~any(h>1)||~any(h<0)  % fora do segmento!!
                                    bcond(i)=ibcond;
                                    pto=pto+1;
                                    nodos(pto)=i;
                                end
                            end
                        end
                    end
                elseif ndim==2               % para 2D
                    dy=yg(node1)-yg(node2);
                    dx=xg(node1)-xg(node2);

                    xmax=max([xg(node1),xg(node2)]);
                    xmin=min([xg(node1),xg(node2)]);

                    ymax=max([yg(node1),yg(node2)]);
                    ymin=min([yg(node1),yg(node2)]);
                    if dx==0
                       for i=1:nn
                           if xg(i)==xg(node1) && yg(i)>=ymin &&...
                                   yg(i)<=ymax
                               bcond(i)=ibcond;
                               pto=pto+1;
                               nodos(pto)=i;
                           end
                       end
                    elseif dy==0
                        for i=1:nn
                           if yg(i)==yg(node1) && xg(i)>=xmin && ...
                                   xg(i)<=xmax
                               bcond(i)=ibcond;
                               pto=pto+1;
                               nodos(pto)=i;
                           end
                        end
                    else
                        mm=dy/dx;
                       for i=1:nn
                           reta=mm*(xg(i)-xg(node1))+yg(node1)-yg(i);
                           if reta==0 && xg(i)>=xmin &&  xg(i)<=xmax && ...
                                    yg(i)>=ymin &&  xg(i)<=ymax
                               bcond(i)=ibcond;
                               pto=pto+1;
                               nodos(pto)=i;
                           end
                       end
                    end
                end
            elseif(bcondopt==4)
                prompt={'Numero da nova bcond: ',...
                    'Entre numero do no#1: ',...
                    'Entre numero do no#2: ',...
                    'Entre numero do no#3: '};
                name='bcond para plano';
                numlines=1;
                answer=inputdlg(prompt,name,numlines);
                ibcond=str2num(answer{1});
                node1=str2num(answer{2});
                node2=str2num(answer{3});
                node3=str2num(answer{4});

                X1=[xg(node1),yg(node1),zg(node1)]';
                X2=[xg(node2),yg(node2),zg(node2)]';
                X3=[xg(node3),yg(node3),zg(node3)]';
                v1=X2-X1;               % vetor na se?ao.
                v2=X3-X1;               % outro vetor no plano da se?ao.
                normal=[v1(2)*v2(3)-v1(3)*v2(2), ...        % vetor normal ao plano da se?ao
                    v1(3)*v2(1)-v1(1)*v2(3), ...            % (calculado segundo regra da mao direita com os
                    v1(1)*v2(2)-v1(2)*v2(1)]';              % 3 pontos fornecidos previamente para o plano).
                norma=norm(normal);
                if abs(norma)<10^-5
                    disp('Os 3 nos fornecidos nao podem ser colineares!!!')
                else
                    normal=normal./norma;
                    for i=1:nn,
                        X=[xg(i),yg(i),zg(i)]';
                        if abs(normal'*(X-X1))<10^-5
                            bcond(i)=ibcond;
                            pto=pto+1;
                            nodos(pto)=i;
                        end
                    end
                end
            elseif (bcondopt==5)
                if (ndim==2),
                    prompt={'Numero da nova bcond: ',...
                        'Entre com o numero do no#1 - superior esquerdo da regiao',...
                        'Entre com o numero do no#2 - inferior direito da regiao'};
                    name='Limites da regiao';
                    numlines=1;
                    pontos=inputdlg(prompt,name,numlines);
                    ibcond=str2num(cell2mat(pontos(1)));         % novo bcond
                    ptosupc=str2num(cell2mat(pontos(2)));        % ponto superior esquerdo da malha
                    ptoinfc=str2num(cell2mat(pontos(3)));        % ponto inferior direito da malha

                    xmin = (G.nodes.coords(ptosupc,1)); xmax = (G.nodes.coords(ptoinfc,1));
                    ymin = (G.nodes.coords(ptoinfc,2)); ymax = (G.nodes.coords(ptosupc,2));

                    for i=1:nn
                        if xmin<=G.nodes.coords(i,1) && xmax>=G.nodes.coords(i,1) && ...
                                ymin<=G.nodes.coords(i,2) && ymax>=G.nodes.coords(i,2)
                            bcond(i)=ibcond;
                            pto=pto+1;
                            nodos(pto)=i;
                        end
                    end
                end
            elseif (bcondopt==6)
                prompt={'Numero da nova bcond: ',...
                    'Entre com a ID do material: '};
                name='Identificacao do material';
                numlines=1;
                pontos=inputdlg(prompt,name,numlines);
                ibcond=str2num(cell2mat(pontos(1)));        % novo bcond
                imater=str2num(cell2mat(pontos(2)));        % ID material

                for ii=1:G.cells.num
                    if G.cells.mat(ii)==imater
                        for jj=1:npe
                            if G.cells.lnods(ii,jj)~=0
                                bcond(G.cells.lnods(ii,jj))=ibcond;
                                pto=pto+1;
                                nodos(pto)=G.cells.lnods(ii,jj);
                            end
                        end
                    end
                end
            end
        end
        if (iopt==2)
            mechbcond=bcond;
        end
        if (iopt==3)
            fluxbcond=bcond;
        end
    elseif (iopt==4)
        Gnew.nodes.bcflux = fluxbcond;
        Gnew.nodes.bcmech = mechbcond;
    elseif (iopt==5)     % Escreve novas bcond para o arquivo ROOT_grid.dat do CODE_BRIGHT
        fid = fopen('coords.out','w');
        fidno = fopen('nodos.out','w');
        %disp(['Escrevendo saida para ROOT_gri.dat no arquivo: COORDS'])
        for i=1:nn,
            if (ndim==3)
                fprintf(fid,'%5i%10.4f%10.4f%10.4f%5i%5i\n', ...
                    i,xg(i),yg(i),zg(i),mechbcond(i),fluxbcond(i));
            elseif (ndim==2)
                fprintf(fid,'%5i%10.4f%10.4f%5i%5i\n',i,xg(i),yg(i)...
                    ,mechbcond(i),fluxbcond(i));
            end
        end

        for i=1:pto
            fprintf(fidno,' %5i %5i%5i\n',nodos(i),mechbcond(nodos(i)),...
                fluxbcond(nodos(i)));
        end
        fclose(fid);
        fclose(fidno);
        Gnew.nodes.bcflux = fluxbcond;
        Gnew.nodes.bcmech = mechbcond;
        if (nargout == 1)
            Gnew = G;
            Gnew.nodes.bcflux = fluxbcond;
            Gnew.nodes.bcmech = mechbcond;
        end
        return
    elseif iopt==6
        if (nargout == 1)
            Gnew = G;
            Gnew.nodes.bcflux = fluxbcond;
            Gnew.nodes.bcmech = mechbcond;
        end
        return
    end
end
end
