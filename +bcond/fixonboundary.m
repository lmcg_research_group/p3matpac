function Gnew = fixonboundary(G)
%Add boundary condition mechanical and/or hydrodynamical types on domain borders
%
% SYNOPSIS
%   Gn = bcond.fixonboundary(G);
%
% PARAMETERS
%   G - Grid structure mostly as detailed in help gridprocessing.grid_structure.
%
% RETURNS
%   Gn - new Grid structure (update boundary conditon) mostly as detailed
%        in help gridprocessing.grid_structure.
%
% EXAMPLE
%   % Make a 10-by-5-by-2 grid on the unit cube.
%      nx = 10; ny = 5; nz = 2;
%      G = gridprocessing.cartGrid([nx, ny, nz], [1, 1, 1]);
%	   Gn = bcond.fixonboundary(G);
%
% SEE ALSO
%   GRIDPROCESSING.GRID_STRUCTURE, GRIDPROCESSING.CARTGRID, GRIDPROCESSING.TRIANGLEGRID, 
%   FIXBCOND
%


%dimension and number of nodes of domain
ndim=G.nodes.dim;
nn = G.nodes.num;
%Maximum number of de nodes per element
npe = max(G.cells.nnel);

% Coordinates e boundary condition vectors:
xg=G.nodes.coords(:,1);
yg=G.nodes.coords(:,2);
if (ndim==3)
   zg=G.nodes.coords(:,3);
else
   zg=zeros(nn,1);
end
mechbcond=G.nodes.bcmech;
fluxbcond=G.nodes.bcflux;


if (ndim==2)
    % getting boundary of domain
    %------------------------------------------- sides
    %--------------     left side
    ibcL = 4;
    idxL = find(G.nodes.coords(:,1)==min(G.nodes.coords(:,1)));


    %--------------     right side
    ibcR = 2;
    idxR = find(G.nodes.coords(:,1)==max(G.nodes.coords(:,1)));


    %--------------     bottom side
    ibcB = 1;
    idxB = find(G.nodes.coords(:,2)==min(G.nodes.coords(:,2)));


    %--------------     top side
    ibcT = 3;
    idxT = find(G.nodes.coords(:,2)==max(G.nodes.coords(:,2)));


    %------------------------------------------- corners
    %--------------     left-bottom corner
    ibcLB = 5;
    idxLB = intersect(idxL,idxB);


    %--------------     left-top corner
    ibcLT = 8;
    idxLT = intersect(idxL,idxT);


    %--------------     right-bottom corner
    ibcRB = 6;
    idxRB = intersect(idxR,idxB);


    %--------------     right-top corner
    ibcRT = 7;
    idxRT = intersect(idxR,idxT);
elseif (ndim==3)
    % getting boundary of domain
    %------------------------------------------- sides
    %--------------     left side - x
    ibcL = 1;
    idxL = find(G.nodes.coords(:,1)==min(G.nodes.coords(:,1)));


    %--------------     right side - x
    ibcR = 2;
    idxR = find(G.nodes.coords(:,1)==max(G.nodes.coords(:,1)));


    %--------------     front side - y
    ibcF = 3;
    idxF = find(G.nodes.coords(:,2)==min(G.nodes.coords(:,2)));


    %--------------     back side - y
    ibcK = 4;
    idxK = find(G.nodes.coords(:,2)==max(G.nodes.coords(:,2)));


    %--------------     bottom side - z
    ibcB = 5;
    idxB = find(G.nodes.coords(:,3)==min(G.nodes.coords(:,3)));


    %--------------     top side - z
    ibcT = 6;
    idxT = find(G.nodes.coords(:,3)==max(G.nodes.coords(:,3)));


    %------------------------------------------- lines
    %--------------     left-bottom line
    ibcLB = 7;
    idxLB = intersect(idxL,idxB);
    %--------------     left-front line
    ibcLF = 8;
    idxLF = intersect(idxF,idxL);
    %--------------     bottom-front line
    ibcBF = 9;
    idxBF = intersect(idxF,idxB);

    %--------------------------------------------------
    %--------------     left-back line
    ibcLK = 10;
    idxLK = intersect(idxK,idxL);

    %--------------------------------------------------
    %--------------     left-top line
    ibcLT = 11;
    idxLT = intersect(idxL,idxT);
    %--------------     top-back line
    ibcTK = 12;
    idxTK = intersect(idxK,idxT);

    %--------------------------------------------------
    %--------------     right-front line
    ibcRF = 13;
    idxRF = intersect(idxF,idxR);
    %--------------     top-front line
    ibcTF = 14;
    idxTF = intersect(idxF,idxT);

    %--------------------------------------------------
    %--------------     right-bottom line
    ibcRB = 15;
    idxRB = intersect(idxR,idxB);
    %--------------     right-back line
    ibcRK = 16;
    idxRK = intersect(idxK,idxR);
    %--------------     bottom-back line
    ibcBK = 17;
    idxBK = intersect(idxK,idxB);

    %--------------------------------------------------
    %--------------     right-top line
    ibcRT = 18;
    idxRT = intersect(idxR,idxT);



    %------------------------------------------- corners
    %--------------     left-bottom-front corner
    ibcLBF = 19;
    idxLBF = intersect(idxF,intersect(idxL,idxB));


    %--------------     left-top-front corner
    ibcLTF = 20;
    idxLTF = intersect(idxF,intersect(idxL,idxT));


    %--------------     left-bottom-back corner
    ibcLBK = 21;
    idxLBK = intersect(idxK,intersect(idxL,idxB));


    %--------------     left-top-back corner
    ibcLTK = 22;
    idxLTK = intersect(idxK,intersect(idxL,idxT));


    %--------------     right-bottom-front corner
    ibcRBF = 23;
    idxRBF = intersect(idxF,intersect(idxR,idxB));


    %--------------     right-top-front corner
    ibcRTF = 24;
    idxRTF = intersect(idxF,intersect(idxR,idxT));



    %--------------     right-bottom-back corner
    ibcRBK = 25;
    idxRBK = intersect(idxK,intersect(idxR,idxB));


    %--------------     right-top-back corner
    ibcRTK = 26;
    idxRTK = intersect(idxK,intersect(idxR,idxT));
end

% View and change bcond:
iopt=0;
pto=iopt; bcondm = 0;
while(iopt<4)
    iopt=menu('FixOnBounday',...
        'Change mechbcond',...                  % iopt == 1
        'Change fluxbcon',...                   % iopt == 2
        'Change both (mechbcond&fluxbcond)',... % iopt == 3
        'Salvar e Sair');                       % iopt == 4
    if (iopt>=1) && (iopt<=3)
        if (iopt==1), bcond=mechbcond; end
        if (iopt==2), bcond=fluxbcond; end
        if (iopt==3), bcond=fluxbcond; bcondm=mechbcond; end
        if (ndim==2)
            %------------------------------------------ sides
            bcond(idxL) = ibcL;
            bcond(idxR) = ibcR;
            bcond(idxT) = ibcT;
            bcond(idxB) = ibcB;

            %------------------------------------------- corners
            bcond(idxLB) = ibcLB;
            bcond(idxLT) = ibcLT;
            bcond(idxRB) = ibcRB;
            bcond(idxRT) = ibcRT;

            if (length(bcondm)>1)
                %------------------------------------------ sides
                bcondm(idxL) = ibcL;
                bcondm(idxR) = ibcR;
                bcondm(idxT) = ibcT;
                bcondm(idxB) = ibcB;

                %------------------------------------------- corners
                bcondm(idxLB) = ibcLB;
                bcondm(idxLT) = ibcLT;
                bcondm(idxRB) = ibcRB;
                bcondm(idxRT) = ibcRT;
            end
        elseif (ndim==3)
            %------------------------------------------ sides
            bcond(idxL) = ibcL;
            bcond(idxR) = ibcR;
            bcond(idxT) = ibcT;
            bcond(idxB) = ibcB;
            bcond(idxF) = ibcF;
            bcond(idxK) = ibcK;

            %------------------------------------------- line
            bcond(idxLB) = ibcLB;
            bcond(idxLF) = ibcLF;
            bcond(idxBF) = ibcBF;
            bcond(idxLT) = ibcLT;
            bcond(idxLF) = ibcLF;
            bcond(idxTF) = ibcTF;
            bcond(idxLB) = ibcLB;
            bcond(idxLK) = ibcLK;
            bcond(idxBK) = ibcBK;
            bcond(idxLT) = ibcLT;
            bcond(idxLK) = ibcLK;
            bcond(idxTK) = ibcTK;
            bcond(idxRB) = ibcRB;
            bcond(idxRF) = ibcRF;
            bcond(idxBF) = ibcBF;
            bcond(idxRT) = ibcRT;
            bcond(idxRF) = ibcRF;
            bcond(idxTF) = ibcTF;
            bcond(idxRB) = ibcRB;
            bcond(idxRK) = ibcRK;
            bcond(idxBK) = ibcBK;
            bcond(idxRT) = ibcRT;
            bcond(idxRK) = ibcRK;
            bcond(idxTK) = ibcTK;

            %------------------------------------------- corners
            bcond(idxLBF) = ibcLBF;
            bcond(idxLTF) = ibcLTF;
            bcond(idxLBK) = ibcLBK;
            bcond(idxLTK) = ibcLTK;
            bcond(idxRBF) = ibcRBF;
            bcond(idxRTF) = ibcRTF;
            bcond(idxRBK) = ibcRBK;
            bcond(idxRTK) = ibcRTK;

            if (length(bcondm)>1)
                %------------------------------------------ sides
                bcondm(idxL) = ibcL;
                bcondm(idxR) = ibcR;
                bcondm(idxT) = ibcT;
                bcondm(idxB) = ibcB;
                bcondm(idxF) = ibcF;
                bcondm(idxK) = ibcK;

                %------------------------------------------- line
                bcondm(idxLB) = ibcLB;
                bcondm(idxLF) = ibcLF;
                bcondm(idxBF) = ibcBF;
                bcondm(idxLT) = ibcLT;
                bcondm(idxLF) = ibcLF;
                bcondm(idxTF) = ibcTF;
                bcondm(idxLB) = ibcLB;
                bcondm(idxLK) = ibcLK;
                bcondm(idxBK) = ibcBK;
                bcondm(idxLT) = ibcLT;
                bcondm(idxLK) = ibcLK;
                bcondm(idxTK) = ibcTK;
                bcondm(idxRB) = ibcRB;
                bcondm(idxRF) = ibcRF;
                bcondm(idxBF) = ibcBF;
                bcondm(idxRT) = ibcRT;
                bcondm(idxRF) = ibcRF;
                bcondm(idxTF) = ibcTF;
                bcondm(idxRB) = ibcRB;
                bcondm(idxRK) = ibcRK;
                bcondm(idxBK) = ibcBK;
                bcondm(idxRT) = ibcRT;
                bcondm(idxRK) = ibcRK;
                bcondm(idxTK) = ibcTK;

                %------------------------------------------- corners
                bcondm(idxLBF) = ibcLBF;
                bcondm(idxLTF) = ibcLTF;
                bcondm(idxLBK) = ibcLBK;
                bcondm(idxLTK) = ibcLTK;
                bcondm(idxRBF) = ibcRBF;
                bcondm(idxRTF) = ibcRTF;
                bcondm(idxRBK) = ibcRBK;
                bcondm(idxRTK) = ibcRTK;
            end
        end
        if (iopt==1), mechbcond=bcond; end
        if (iopt==2), fluxbcond=bcond; end
        if (iopt==3), fluxbcond=bcond; mechbcond=bcondm; end




    elseif (iopt==4)
        fid = fopen('coords.out','w');
		progress = textprogressbar(nn,'showremtime', true, ...
								'startmsg','saving coords block');
        if (ndim==3)
            for i=1:nn
                fprintf(fid,'%5i%10.4f%10.4f%10.4f%5i%5i\n', ...
                    i,xg(i),yg(i),zg(i),mechbcond(i),fluxbcond(i));
				progress(i);
            end
        elseif (ndim==2)
            for i=1:nn
                fprintf(fid,'%5i%10.4f%10.4f%5i%5i\n',i,xg(i),yg(i)...
                    ,mechbcond(i),fluxbcond(i));
				progress(i);
            end
        end


        fclose(fid);
        if (nargout == 1)
            Gnew = G;
            Gnew.nodes.bcflux = fluxbcond;
            Gnew.nodes.bcmech = mechbcond;
        end
        return
    end
end
end
