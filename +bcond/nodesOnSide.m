function out = nodesOnSide(G, side, varargin)
% Retuns index of nodes located at 'side' on grid.
%
% SYNOPSIS:
%   id = nodesOnSide(G,side,varargin);
%
% PARAMETERS:
%   G      - Grid structure
%   side   - Global side from which to extract indices.  String.  Must
%           (case insensitively) match one of six alias groups:
%
%              1) {'West' , 'XMin', 'Left'  }
%              2) {'East' , 'XMax', 'Right' }
%              3) {'South', 'YMin', 'Back'  }
%              4) {'North', 'YMax', 'Front' }
%              5) {'Upper', 'ZMax', 'Top'   }
%              6) {'Lower', 'ZMin', 'Bottom'}
%
%           These groups correspond to the cardinal directions mentioned as
%           the first alternative in each group.
% varargin - list of logical statements. index ranges for local (in-plane) 
%           axes.  An empty index range ([]) is interpreted as covering the
%           entire corresponding local axis of 'side' in the grid 'G'.
% 
% RETURNS:
%   id   - logical array of nodes located at global side in grid
%
% USAGE
%   >> left = nodesOnSide(G,'left');
%
% SEE ALSO
%   grid_structure, find
% 

assert(isstruct(G), ['Grids passed to ', mfilename, ...
'must be a structure variable'])

if isempty(varargin)
    bool = true(G.nodes.num,1);
else
    bool=varargin{:};
end

x=1; y=2; z=3; if (G.nodes.dim<3), z=2; end

% Determine which indices and face tags to look for
switch lower(side),
   case {'left'  , 'west' , 'xmin'},
      bool = and(G.nodes.coords(:,x)==min(G.nodes.coords(:,x)),bool);

   case {'right' , 'east' , 'xmax'},
      bool = and(G.nodes.coords(:,x)==max(G.nodes.coords(:,x)),bool);

   case {'back'  , 'south', 'ymin'},
      bool = and(G.nodes.coords(:,y)==min(G.nodes.coords(:,y)),bool);

   case {'front' , 'north', 'ymax'},
      bool = and(G.nodes.coords(:,y)==max(G.nodes.coords(:,y)),bool);

   case {'top'   , 'upper', 'zmax'},
      bool = and(G.nodes.coords(:,z)==max(G.nodes.coords(:,z)),bool);

   case {'bottom', 'lower', 'zmin'},
      bool = and(G.nodes.coords(:,z)==min(G.nodes.coords(:,z)),bool);

   otherwise
      error([caller, ':Side:Unknown'],                  ...
            'Boundary side ''%s'' not supported in %s', ...
            side, caller);
end

out = bool;
end