function BC = initBCMech(varargin)
%Initialize a mechanical boundary condition type structure (only impl. for 2D)
%
% SYNOPSIS
%  BC = initBCMech()
%
% PARAMETERS
%   BCParam  - An array 18-by-1 of parameters. OPTIONAL
%
%   'pn'/pv  - List of property names/property values.  OPTIONAL.
%              This list will be passed to hydrodynamic BC parameters.
%              Supported property names:
%               - 'fx','f_x','sx','s_x' is force/stress in X- BCParam(1)
%               - 'fy','f_y','sy','s_y' is force/stress in X- BCParam(2)
%               - 'ux', 'u_x'           is X displ. rate    - BCParam(3)
%               - 'uy', 'u_y'           is Y displ. rate    - BCParam(4)
%               - 'uxpres','ux_pres'    is X dir. precribed - BCParam(5)
%               - 'uypres','uy_pres'    is Y dir. precribed - BCParam(8)
%               - 'multiplier','gamma'  is multiplier(gamma)- BCParam(9)
%               - 'dfx', 'd_fx'         is ramp loading in X- BCParam(10)
%               - 'dfy', 'd_fy'         is ramp loading in X- BCParam(11)
%               - 'index', 'ind'        is index            - BCParam(18)
%
% RETURNS
%   BC - mechanical boundary condition type structure mostly as detailed 
%        in bcond.bc_structure 
%
% EXAMPLE
%   1)>> BC = bcond.initBCMech()
%
%   2)>> pbc=[0,-10,0,0,0,0,0,0,1e+15,0,0,0,0,0,0,0,0,1];
%        BC = bcond.initBCMech(pbc)
%
%   3)>> BC = bcond.initBCMech('fx',10,'multiplier',1e15,'index',1.0)
%
% SEE ALSO
%   BC_STRUCTURE, BUILDBC, INITBCFLUX
%

% check input
nparam = 18; % same number of lines into $(p3ROOTDIR)/templates/mech_param.dat 
listOfParam = [];
if nargin>0
    if (mod(numel(varargin), 2) == 0) % input like a list of property names/property values
        listOfParam = zeros(nparam ,1);
        for i=1:2:numel(varargin)
            switch upper(varargin{i})
                case {'FX','F_X','SX','S_X'}
                    listOfParam(1) = varargin{i+1};
                case {'FY','F_Y','SY','S_Y'}
                    listOfParam(2) = varargin{i+1};
                case {'UX','U_X'}
                    listOfParam(3) = varargin{i+1};
                case {'UY','U_Y'}
                    listOfParam(4) = varargin{i+1};
                case {'UXPRES','UX_PRES'}
                    listOfParam(5) = varargin{i+1};
                case {'UYPRES','UY_PRES'}
                    listOfParam(8) = varargin{i+1};
                case {'GAMMA','MULTIPLIER'}
                    listOfParam(9) = varargin{i+1};
                case {'DFX','D_FX'}
                    listOfParam(10) = varargin{i+1};
                case {'DFY','D_FY'}
                    listOfParam(11) = varargin{i+1};
                case {'INDEX','IND'}
                    listOfParam(18) = varargin{i+1};
            end
        end
    elseif (numel(varargin{1})==nparam)   % input like a 20-by-1 array
        listOfParam = varargin{1};
    end
end

% build BC variable - default parameters (all zeros)
BC = setDefault();
if ~isempty(listOfParam),
    BC = setBCParam(BC,listOfParam);
end
end



% -------------------------------------------------------------------------
function bc=setDefault()
% set default mechanical parameters
fid = fopen(fullfile(p3ROOTDIR,'templates','mech_param.dat'),'r');
param = textscan(fid,'%s');
fclose(fid);
% set number of parameters
npar = numel(param{1});
pars = zeros(npar,1);

% build BC variable
bc = struct('paramName',param,'value',pars);
end



% -------------------------------------------------------------------------
function bc = setBCParam(ibc,value)
% set mechanical parameters to specific value
bc = ibc;
bc.value = value;
end
