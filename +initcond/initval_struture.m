%initval structure used in packages
% 
% 	I - initial values structure.  A master structure having the 
%             following fields:
%               - che   -- A n-by-m array of initial chemical concentration
%                          of the m species;
%               - blko  -- A n-by-p array of initial values of  unknowns of
%                          petroleum problem;
%               - poro  -- A e-by-1 or m-by-1 array of initial porosity
%                          values;
%               - perm  -- A e-by-2*d or m-by-2*d array of initial permeability
%                          tensor multipler factor and angle of direction of 
%                          anisotropy for permeability
%               - stresses -- A e-by-2*d array of initial values of stresses
%               - thm   -- A n-by-q array of initial values of  unknowns of
%                          thm problem;
%  
%	where:
%       n ~ number of node of problem;
%       e ~ number of elements of problem;
%       m ~ number of materials of problem;
%       p ~ number of unknowns of reservoir problem;
%       q ~ number of unknowns of thm problem;
%       d ~ dimension of problem 
% 