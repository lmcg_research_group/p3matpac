function cheinit = chemInitCond( G, h, C )
%Create block of initial condition of reative transport problem
%
% SYNOPSIS:
%   cheinit = initcond.chemInitCond(G, h, C);
%
% PARAMETERS:
%   G - Grid structure mostly as detailed in help mesh.grid_structure
%   h - Vector, G.cells.nummat-by-1, of materials hierarchy.
%       if h = [3; 1; 2] the nodes that belongs to material #1 has the
%       minor hierarchy, while that the nodes that belongs to material #2
%       has the major hierarchy.
%       i.e. the nodes that stay on interface between material #1 and #2
%       must to have the concentration of material #2 [C(2,:)]. Similarly
%       idea to nodes that stay on interface between material #3 and #1, they
%       must to have the concentration od materia #3 [C(3,:)])
%   C - Matrix, G.cells.nummat-by-m, of concentration of minerals belongs
%       of chemical system (m vary of chemical system)
%       OPTIONAL.  Default value == 1:G.cells.nummat
%
% RETURNS:
%   cheinit - Matrix, G.nodes.num-by-m, of initial species chemical
%       concetration.
%       OPTIONAL. if do not exist the function will be create a file
%       cheinit.out containing data
%
% EXAMPLE:
%   Czero = initcond.chemInitCond(G, h, C);
%   initcond.chemInitCond(G, h, Czero);
%
% SEE ALSO:
%   gridprocessing.grid_structure, gridprocessing.evalGrid, iotools.printCHE
%

%  check input
if sscanf(version('-release'),'%u%*s')<=2013
    if and(nargin<2,nargin>3)
        error('please usage: che0 = initcond.chemInitCond(<grid_structure>, <mat_hierarchy_vector>, <concentration_matrix>)');
    end
else
    error(nargchk(2, 3, nargin)); %#ok<NCHKN>
end

assert(isstruct(G),...
    'First argument must be a struct variable see help mesh.grid_structure.');

assert( numel(h) == G.cells.nummat,...
    'Second argument [h] must to have numel(h) = %d',...
    G.cells.nummat);

nmat = numel(h);
if nargin < 3
    C = (1:nmat).*1111; species = 1; C = C';
else
    assert( size(C,1) == numel(h),...
        'Third argument [C] must have length(C) = %d', numel(h));
    species = size(C,2);
end

% creating of output - zero matrix
cheinit = zeros(G.nodes.num, species);

% auxiliar matrix
che = zeros(G.nodes.num, 1);

[hierar, ix] = sort(h,'descend'); %#ok<ASGLU>

progress = textprogressbar(nmat,'showremtime', true, ...
						 'startmsg','building chemical blk.');

for imat = 1:nmat
    % find all elements with material ix(imat)
    idx = G.cells.mat == ix(imat);
    % all nodes of elements of material ix(imat)
    nodes = G.cells.lnods(idx,1:G.cells.nnel(idx));
    % erase repeated nodes
    nodes = unique(nodes);
    % assign material id on nodes
    che(nodes) = ix(imat);
	progress(imat);
end

cheinit(:,1:species) = C(che,1:species);
fidout = fopen('cheinit.out','w');
iotools.printCHE(fidout,G, cheinit);
return
end

