#!/bin/bash

# works with a file called VERSION in the current directory,
# the contents of which should be a semantic version number
# such as "1.2.3"

# this script will display the current version, automatically
# suggest a "minor" version update, and ask for input to use
# the suggestion, or a newly entered value.

# once the new version number is determined, the script will
# pull a list of changes from git history, prepend this to
# a file called CHANGES (under the title of the new version
# number).

usage() {
    echo "usage: bump-version <version-id>"
}

if [ $# -ne 1 ]; then
    usage
    exit 1
fi

INPUT_STRING=$1
now=$(date +'%Y/%m/%d')

if [ -f VERSION ]; then
    BASE_STRING=`cat VERSION`
    BASE_LIST=(`echo $BASE_STRING | tr '.' ' '`)
    V_MAJOR=${BASE_LIST[0]}
    V_MINOR=${BASE_LIST[1]}
    V_PATCH=${BASE_LIST[2]}
    echo "Current version : $BASE_STRING"
    echo "Will set new version to be $INPUT_STRING"
    echo $INPUT_STRING > VERSION
    echo "$INPUT_STRING:" > tmpfile
    echo "------" >> tmpfile
    echo "Release date: **$now**" >> tmpfile
    git log --pretty=format:" * %s" "$BASE_STRING"...HEAD >> tmpfile
    echo "" >> tmpfile
    echo "" >> tmpfile
    cat CHANGELOG >> tmpfile
    mv tmpfile CHANGELOG
    git add CHANGELOG VERSION
    git commit -m "Bumped version number to $INPUT_STRING"
#    git tag -a "$INPUT_STRING" -m "Tagging version $INPUT_STRING"
#    git push origin --tags
else
    echo "Could not find a VERSION file"
    read -p "Do you want to create a version file and start from scratch? [y]" RESPONSE
    if [ "$RESPONSE" = "" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Y" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "YES" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "y" ]; then
        echo "0.1.0" > VERSION
        echo "0.1.0" > CHANGELOG
        echo "------" >> CHANGELOG
        echo "Release date: **$now**" >> CHANGELOG
        git log --pretty=format:" * %s" >> CHANGELOG
        echo "" >> CHANGELOG
        echo "" >> CHANGELOG
        git add VERSION CHANGELOG
        git commit -m "Added VERSION and CHANGELOG files, Version bump to 0.1.0"
#        git tag -a -m "Tagging version 0.1.0" "0.1.0"
#        git push origin --tags
    fi

fi

echo "Files modified successfully, version bumped to $INPUT_STRING"
echo ""
echo "After add new tag using: git tag -a <tagname> -m <tagmessage>"
echo "Don't forget push tags to remote using: git push origin --tags"
